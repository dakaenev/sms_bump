# SMS_Bump
===============================================================================================
The task:
Task name: OTP verification using an SMS message

Task definition:

You have to create a website registration which requires a phone number. The number should be validated, and the client wants to use the OTP (one time password) as a solution to that problem. To put it short, you have to send an actual text message to that phone, with a generated code which should be inserted in a validation form.

Task requirements:

- Create a user registration page with the following fields -> an email, a phone, and a password.

- Make sure that we have a well formatted phone.  The customer may enter a number using special characters, e.g. 0885 (34-53-95). As you can see, the country code is missing, and since we need it, you have to prepend it if it is missing. Based on the previous example, the final and valid phone number should look like this: 359885345395.

- You have to create a validation of the phone number by sending a code to the provided number (How: You can mock an SMS provider by saving the messages in the database).

- Make sure that the user can only make 3 attempts of entering the validation code. If all three of them are wrong, add a 1 minute of “cooldown”.

- Create an option which can allow the system to send a new code to the customer. Make sure that this option is available only when at least 1 minute has passed since the original verification code was sent.

- Keep log of all verification attempts.

- After a successfully validated phone number, we have to send a confirmation SMS message, saying “Welcome to SMS System!”
=======================================================================================================
Относно решението: https://plovdiv-web.com/page/sms/

Малко се бях вманиачил - може да работи както с включен JavaScript (тогава прави AJAX), така и дори ако JavaScript е изключен (обикновени форми).

Акцентирах върху решението на задачата и това тя да работи, така както се очаква,
а не толкова върху абстракции като създаване на собствен framework, ORM и т.н., 

Умишлено съм  писал по различни начини, за да покажа, че съм гъвкав и мога да пиша както се наложи, и на каквото се наложи.
- За CRUD операциите съм ползвал функции разработени от мен преди години, базирани на процедурен mysqli (файл: includes/database.php). - select_query, insert_query, update_query 
Идеята е всичко да е централизирано, и да се обръщам към едни и същи функции, които да се модифицират, ако се наложи да се работи с друга база данни или технология.
- Специално по заданието на задачата използвах процедурно mysqli с prepare statements за да покажа и такъв стил, (те държаха да видят нещо такова)
- В папка other project / ProductsManager.php е използвано модифицирано PDO, ако погледнеш от ред 606 надолу ще видиш с какви чудовищни заявки съм се сблъсквал и модифицирал.... 
... аз не съм писал целия файл, аз обичам скобите една под друга, а и НЕ бих именовал функция като applyBrakuvaneQuantity() :) 
Сложил съм и файл с име 'спагети' от  друг проект, по който съм помагал, за да покажа, че дори такава каша не ме плаши :)