<?php
	// Задаваме времето за изчакване в минути
	define('COOLDOWN_TIME', 1, true);
	define('FREEZE_TIME', 1, true);

	// Дефинирани имена на таблици
	define('TABLE_PHONES', 'phones', true);
	define('TABLE_PHONES_OTP', 'phones_otp', true);
	define('TABLE_PHONES_SMS', 'phones_sms', true);
	define('TABLE_PHONES_LOG', 'phones_log', true);

	// Статуси използввани в таблица phones
	define('STATUS_REG_NEW', 'new', true);
	define('STATUS_REG_VERIFIED', 'verified', true);
	define('STATUS_LOG_BAN', 'banned', true);
	define('STATUS_LOG_ALLOWED', 'allowed', true);

	// Други дефиниции
	define('DEFAULT_PHONE_CODE', 359, true);
	define('ALLOWED_WRONG_LOGIN', 3, true);
?>