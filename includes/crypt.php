<?php
function encrypt($pure_string, $encryption_key = CRYPT_PASS)
	{
		$iv = mcrypt_create_iv(
			mcrypt_get_iv_size(CRYPT_ALGO, CRYPT_MODE),
			MCRYPT_DEV_URANDOM
		);

		$encrypted = base64_encode(
			$iv .
			mcrypt_encrypt(
				CRYPT_ALGO,
				hash('sha256', $encryption_key, true),
				$pure_string,
				CRYPT_MODE,
				$iv
			)
		);

		return $encrypted;
	}

	/**
	 * Returns decrypted original string
	 */
	function decrypt($encrypted_string, $encryption_key = CRYPT_PASS) {
		$data = base64_decode($encrypted_string);
		$iv = substr($data, 0, mcrypt_get_iv_size(CRYPT_ALGO, CRYPT_MODE));

		$decrypted = rtrim(
			mcrypt_decrypt(
				CRYPT_ALGO,
				hash('sha256', $encryption_key, true),
				substr($data, mcrypt_get_iv_size(CRYPT_ALGO, CRYPT_MODE)),
				CRYPT_MODE,
				$iv
			),
			"\0"
		);

		return $decrypted;
	}
	?>