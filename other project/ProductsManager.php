<?php

namespace App\Manager;

use App\Model\ProvidersProductsModel;
use App\Model\Products\ProductFiltersModel;
use App\Error;
use App\Validator;
use App\Service\ImageService;
use App\Model\Products\ProductsPricesModel;
use App\Model\Providers\RemoteProviderListModel;
use App\Model\Returns\ReturnTicketModel;

use Exception;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File;

use Imagine\Image\ImageInterface;
use Imagine\Image\Point as ImaginePoint;
use Imagine\Image\Box as ImagineBox;

/**
 * SECTIONS
 * - PRODUCTS
 * - PRODUCT CATEGORIES
 * - PRODUCT SUBCATEGORIES
 * - PRODUCT TAGS
 * - PRODUCT PROPERTIES
 * - PRODUCT IMAGES
 * - PRODUCT SIZES
 * - PRODUCT BRANDS
 * - PRODUCT VARIANTS
 * - PRODUCT PRICES
 * - PRODUCT DISCOUNTS
 * - RETURNS
 * - UNACTIVE
 */
class ProductsManager extends BaseManager {

	const	PRODUCT_TMP_FILE_PREFIX = 'tmp_',
			PRODUCT_DIR_PREFIX = 'product_',
			PRODUCTS_TEMP_DIR = 'tmp',
			MAX_PRODUCT_IMAGES = 4,
			MAX_IMAGE_SIZE = 1200,
			DEFAULT_IMAGE_EXTENSION = 'jpg',
			PRODUCT_DEFAULT_IMAGE = '/static/admin/img/placeholder.png',
			/**
			 * Dont change, оти чупя ръки!
			 * Used to md5 product hash_id
			 */
			PROPDUCT_HASH_ID_SALT = '5DGmbGA4';
	
	const	MAX_SIZE_BUY_QUANTITY = 12;

	const	SIZE_STATUS_ACTIVE = 1,
			SIZE_STATUS_HIDDEN = 2;
	
	protected $image_positions = array(
		1 => 'main_image',
		2 => 'hover_image'
	);
	
	protected $imageFormats = array(
		'box' => array(
			'size' => array(400, 447),
			'mode' => ImageInterface::THUMBNAIL_OUTBOUND
		)
	);

	/**
	 * Product cache
	 * @var ManagerCache
	 */
	protected $productCache;
	
	/**
	 *
	 * @var type Product active price
	 * @var ManagerCache
	 */
	protected $activePriceCache;
	
	public function __construct(\Silex\Application $app) {
		parent::__construct($app);
		$this->productCache = new Cache\SphinxManagerCache($app['sphinx.manager'], 'product.v2', $app['manager.cache']);
		$this->activePriceCache = $this->productCache->child('active_price.v2', 'product_id', false, 3600);
	}
	
	/**
     *----------------------------------------------------------------------------
     * PRODUCTS
     *----------------------------------------------------------------------------
     */
    
	/**
	 * Get product cache
     * 
	 * return ManagerCache
	 */
	public function getProductCache() {
		return $this->productCache;
	}
	
    /**
     * Chech if product is availiable
     * 
     * ProvidersProductsModel $product
     * Array(stdClass) $productSize
     * Decimal $amount
     * 
     * return nothing | \Exception
     */
	public function isAvailable(ProvidersProductsModel $product, $productSize, $amount = 0) {
		if (empty($amount) || !$product->available) {
			throw new Error\Products\ProductIsNotAvaiable();
		}
		if ($productSize->remaining_quantity < $amount) {
			throw new Error\Products\ProductIsNotAvaiable($amount, $productSize->remaining_quantity);
		}
		if($product->link_id) {
			$linkedProduct = $this->findProductById($product->link_id);
			if($linkedProduct->is_size_available($productSize->size_id, $amount)) {
				$product->replace($linkedProduct->toArray(true));
			}
		}
	}

    /**
     * Buy product
     * 
     * ProvidersProductsModel $product
     * stdClass $productSize
     * 
     * return nothing
     */
	public function buyProduct(ProvidersProductsModel $product, $productSize) {
		$productSize->buy += $productSize->cart_amount;
		$this->db->query(sprintf('UPDATE %s SET buy = buy + :buy WHERE product_id = :product AND size_id = :size',  self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array(
			'buy' => $productSize->cart_amount,
			'product' => $product->id,
			'size' => $productSize->size_id
		));
		$this->productCache->invalidate($product->id);
	}

    /**
     * Return product
     * 
     * ProvidersProductsModel $product
     * stdClass $productSize
     * 
     * return nothing
     */
	public function returnProduct(ProvidersProductsModel $product, $productSize) {
		$productSize->buy = max(0, $productSize->buy - $productSize->cart_amount);
		$this->db->query(sprintf('UPDATE %s SET buy = IF(buy-:buy > 0, buy-:buy, 0) WHERE product_id = :product AND size_id = :size',  self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array(
			'buy' => $productSize->cart_amount,
			'product' => $product->id,
			'size' => $productSize->size_id
		));
		$this->productCache->invalidate($product->id);
	}

    /**
     * Add product
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing | \Exception
     */
	public function addProduct(ProvidersProductsModel $product) {
		if ($product->has('provider')) {
			$product->provider_id = $product->provider->id;
		}
		$images = $product->images;
		foreach ($images as $key => $image) {
			if ($this->isProductImageTemp($product, $image)) {
				list($storagePath, $previewPath) = $this->getProductImagePaths(false, $image->filename);
			} else {
				list($storagePath, $previewPath) = $this->getProductImagePaths($product, $image->filename);
			}
			try {
				$product->image = $storagePath;
				$this->validateProduct($product, array('image'));
			} catch (Error\FormError $e) {
				$this->app['image.service']->deleteResponsiveImages($storagePath, true, $this->imageFormats);
				unset($images[$key]);
			}
		}
		$product->images = $images;
		$this->validateProduct($product);

		if (!$product->author_id) {
			$product->author_id = $this->app['user']->id;
		}
		$product->created_at = time();
		$product->brand = $this->app['brands.manager']->createIfNotExist($product->brand_name);
		$product->brand_id = $product->brand->id;

		$sizes = $product->sizes;
		foreach ($sizes as $index => $size) {
			if (!is_numeric($size->amount)) {
				unset($sizes[$index]);
			}
		}
		$product->sizes = $sizes;
		
		$isNewProduct = empty($product->id) ? true : false;

		try {

			$this->db->beginTransaction();

			if (!empty($product->id)) {
				$this->saveProduct($product);
			} else {
				$product->created_at = time();
				$row = $product->toArray();
				$row[] = '_load';
				$row[] = '_id';
				$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $row);
				$product->replace($row);

				$product->hash_id = substr(md5($product->id . self::PROPDUCT_HASH_ID_SALT), 0, 10);
				$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
					'hash_id' => $product->hash_id
						), array(
					'id' => $product->id
				));
			}
			
			$this->productCache->invalidate($product->id);

			/**
			 * Add product size
			 * Keep static values if size is already added
			 * Keep size values in use
			 */
			$rmSizeVars = array('product_size', 'in_use');
			if (!$isNewProduct) {
				$oldSizes = $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE product_id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), $product->id);
				foreach($oldSizes as $row) {
					$row->product_size = $row->product_id.'_'.$row->size_id;
				}
				$this->app['orders.manager']->mapSizeInUse($oldSizes);
				$temp = array();
				foreach($oldSizes as $row) {
					$temp[$row->size_id] = $row;
				}
				$oldSizes = $temp;
			} else {
				$oldSizes = array();
			}

			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, array(
				'product_id' => $product->id
			));
			
			$unique = array();
			foreach($product->sizes as $size) {
				$unique[$size->id] = $size;
			}
			foreach ($unique as $size) {
				if (isset($oldSizes[$size->id])) {
					$row = (array) $oldSizes[$size->id];
					foreach($rmSizeVars as $k) {
						if(isset($row[$k])) {
							unset($row[$k]);
						}
					}
					unset($oldSizes[$size->id]);
				} else {
					$row = array(
						'product_id' => $product->id,
						'size_id' => $size->id
					);
				}
                if($product->isWarehouseQuantities) {
                    if(!isset($row['amount'])) {
                        $row['amount'] = 0;
                    }
                } else {
                    $row['amount'] = $size->amount;
                }
				if (isset($size->barcode) && $size->barcode) {
					$row['barcode'] = $size->barcode;
				}
				if (isset($size->buy) && is_numeric($size->buy)) {
					$row['buy'] = $size->buy;
				}
				$row['status'] = self::SIZE_STATUS_ACTIVE;
				$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
                if($product->isWarehouseQuantities) {
                    $size->po_amount = $size->amount;
                }
			}
			/**
			 * Keep sizes in use
			 */
			foreach($oldSizes as $row) {
				if($row->in_use) {
					$row = (array)$row;
					foreach($rmSizeVars as $k) {
						if(isset($row[$k])) {
							unset($row[$k]);
						}
					}
					$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
				}
			}

			/* CHART TABLES */
			$tablesAdded = array();
			$this->clean_product_size_tables($product->id);
			foreach ($product->size_tables as $tableId) {
				if(!isset($tablesAdded[$tableId])) {
					$tablesAdded[$tableId] = true;
					$this->add_product_table($product->id, $tableId);
				}
			}
			
			/* Categories */
            $this->saveProductCategories($product, $product->categories);

			/* TAGS */
			$this->saveProductTags($product, $product->tags);

			/** PROPERTIES **/
			$this->saveProductProperties($product, $product->properties);
			
			/** SUB CATEGORIES **/
			$this->saveProductSubCategories($product, $product->sub_categories);
            
			/** VARIANTS **/
			$this->deleteProductVariants($product->id);
			foreach($product->variants as $variant) {
				$this->addProductVariant($product->id, $variant->variant_id);
			}
			
			/* IMAGES */
			$addedImages = count($product->images);
			
			$product->images = $this->addProductImages($product, true);

			$imagesCount = $this->db->fetch(sprintf('SELECT COUNT(*) AS count FROM %s WHERE product_id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES), $product->id);
			if ($imagesCount->count != $addedImages) {
				throw new Error\FormError('images', sprintf('Number of pictures does not meet. Expect %d, got %d', $addedImages, $imagesCount->count));
			}

			$cacheModel = new \App\Model\ModelCache($this->cache);
			foreach(array('product', 'product.sizes', 'product.images') as $key) {
				$cacheModel->invalidate($key.'.'.$product->id, $product);
			}
			
            /**
             * no longer automoderate
             */
//            if($product->provider->isSelf()) {
//                $this->app['products.mods.manager']->checkAndModifyProduct($product);
//            }
            
			$this->db->commit();
		} catch (Error\FormError $e) {
			$this->db->rollBack();
			throw new Error\FormError($e->getField(), $e->getMessage());
		} catch (Exception $e) {
			$this->db->rollBack();
			throw new Exception($e->getMessage());
		}
	}
    
    /**
     * Quick edit products
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing | \Exception
     */
    public function editQuickProduct(ProvidersProductsModel $product) {
		if ($product->has('provider')) {
			$product->provider_id = $product->provider->id;
		}
		$this->validateProduct($product, array('categories', 'sub_categories', 'color_id'));

		if (!$product->author_id) {
			$product->author_id = $this->app['user']->id;
		}
		
		try {
			$this->db->beginTransaction();

			if (!empty($product->id)) {
				$this->saveProduct($product, array('color_id', 'status'));
			}
			
			/* Categories */
			$this->saveProductCategories($product, $product->categories);
			/** SUB CATEGORIES **/
			$this->saveProductSubCategories($product, $product->sub_categories);
			
			$cacheModel = new \App\Model\ModelCache($this->cache);
			foreach(array('product') as $key) {
				$cacheModel->invalidate($key.'.'.$product->id, $product);
			}
            
			$this->productCache->invalidate($product->id);
            
            /**
             * no longer automoderate
             */
//            if($product->provider->isSelf()) {
//                $this->app['products.mods.manager']->checkAndModifyProduct($product);
//            }
            
			$this->db->commit();
		} catch (Error\FormError $e) {
			$this->db->rollBack();
			throw new Error\FormError($e->getField(), $e->getMessage());
		} catch (Exception $e) {
			$this->db->rollBack();
			throw new Exception($e->getMessage());
		}
	}
    
    /**
     * Quick edit products
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing | \Exception
     */
    public function editQuickProductDesctription(ProvidersProductsModel $product) {
		if ($product->has('provider')) {
			$product->provider_id = $product->provider->id;
		}
		$this->validateProduct($product, array('product_name', 'description', 'properties'));

		if (!$product->author_id) {
			$product->author_id = $this->app['user']->id;
		}

		try {
			$this->db->beginTransaction();

			if (!empty($product->id)) {
				$this->saveProduct($product, array('product_name', 'description', 'status'));
			} 
			
			$this->productCache->invalidate($product->id);

			/** PROPERTIES **/
			$this->saveProductProperties($product, $product->properties);
            
			$cacheModel = new \App\Model\ModelCache($this->cache);
			foreach(array('product') as $key) {
				$cacheModel->invalidate($key.'.'.$product->id, $product);
			}
            
			$this->productCache->invalidate($product->id);
            
			$this->db->commit();
		} catch (Error\FormError $e) {
			$this->db->rollBack();
			throw new Error\FormError($e->getField(), $e->getMessage());
		} catch (Exception $e) {
			$this->db->rollBack();
			throw new Exception($e->getMessage());
		}
	}
    
    /**
     * Save product
     * 
     * ProvidersProductsModel $product
     * Boolean $only
     * 
     * return nothing
     */
	public function saveProduct(ProvidersProductsModel $product, $only = false) {
		if ($product->status == ProvidersProductsModel::STATUS_WAIT) {
			$product->status = ProvidersProductsModel::STATUS_OK;
		}
        $row = $product->toArray();
        if(!empty($only) && is_array($only)) {
            $limited = array();
            foreach($row as $key => $value) {
                if(in_array($key, $only)) {
                    $limited[$key] = $value;
                }
            }
            $row = $limited;
        }
		$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $row, array(
			'id' => $product->id
        ), 1);

		if ($product->collection) {
			$this->app['collection.manager']->onProductUpdate($product->collection, $product);
		}
		$this->productCache->invalidate($product->id);
	}

    /**
     * Find product(s) by id(s)
     * 
     * Integer|Array(Integer) $id
     * String $modifier
     * 
     * return false|ProvidersProductsModel|Array(ProvidersProductsModel)
     */
	public function findProductById($id, $modifier='modifyProduct') {
		return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE id = :id', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS),
			sprintf('SELECT * FROM %s WHERE id IN (:id)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS)
		), 'id', array('id' => $id), 'convert_product_row', $modifier, $this->productCache);
	}

    /**
     * Find products by ids
     * 
     * Array $ids
     * false|Integer $useId
     * 
     * return Array(ProvidersProductsModel)
     */
	public function findProductsById(array $ids, $useId = false) {
		$result = $this->findProductById($ids);
		if($useId) {
			$list = array();
			foreach($result as $key=>$val) {
				$list[$val->id] = $val;
			}
			$result = $list;
		}
		return $result;
	}

    /**
     * Find product link(s) by link(s)
     * 
     * Integer|Array(Integer) $value
     * Boolean $modify
     * 
     * return false|ProvidersProductsModel|Array(ProvidersProductsModel)
     */
	public function findProductLink($value, $modify=false) {
		return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE link_id = :link', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS),
			sprintf('SELECT * FROM %s WHERE link_id IN (:link)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS)
		), 'link', array('link' => $value), ($modify ? 'convert_product_row' : null), ($modify ? 'modifyProduct' : null));
	}
	
    /**
     * Convert Integer to ProvidersProductsModel
     * 
     * Integer $id
     * 
     * return ProvidersProductsModel|null
     */
	public function convert_product($id) {
		$model = $this->findProductById($id);
		return $model ? $model : null;
	}
	
    /**
     * Convert stdClass to ProvidersProductsModel
     * 
     * stdClass|Array $values
     * 
     * return ProvidersProductsModel
     */
	public function convert_product_row($value) {
		return new ProvidersProductsModel($value);
	}

    /**
     * Convert integer to ProvidersProductsModel
     * 
     * Integer $id
     * 
     * return ProvidersProductsModel
     */
	public function convert_site_product($id) {
		$product = $this->convert_product($id);
		if ($product && empty($product->unactive_time)) {
			if (!empty($product->collection)) {
				if ($this->app['admin'] || !$this->app['admin'] && $product->collection->canUserView()) {
					return $product;
				}
			}
		}
		return null;
	}

    /**
     * Get limited number or counting of products by filters
     * 
     * ProductFiltersModel $filters
     * null|Array $paging
     * Boolean $count
     * String $modifier
     * 
     * return Integer | stdClass(GetPagingResult(rows => Array(ProvidersProductsModel)))
     */
	public function findProducts(ProductFiltersModel $filters, array $paging = null, $count = false, $modifier='modifyProduct') { 
		$fields = array();
		if ($filters->collection) {
			$fields[] = 'c_products.sort_index';
		}
        
		$joins = array();
		$leftJoins = array();
		$where = array();
		$params = array();
		$having = array();
		$sort = array();
                

		if($filters->id) {
			$where[] = 'products.id in (:product)';
            $id = $filters->id;
            if(!is_array($id)) {
                $id = array($id);
            }
			$params['product'] = $id;
		}
		
		if ($filters->has('provider')) {
			$where[] = '`products`.`provider_id` = :provider_id';
			$params['provider_id'] = $filters->provider;
		}
		if ($filters->has('brand')) {
			if (is_array($filters->brand)) {
				$where[] = '`products`.`brand_id` IN (:brand_id)';
			} else {
				$where[] = '`products`.`brand_id` = :brand_id';
			}
			$params['brand_id'] = $filters->brand;
		}
		if ($filters->has('tags')) {
			$joins['tags'] = true;
			if (is_array($filters->tags)) {
				$where[] = '`p_tags`.`tag_id` IN (:tag_id)';
			} else {
				$where[] = '`p_tags`.`tag_id` = :tag_id';
			}
			$params['tag_id'] = $filters->tags;
		}
		if ($filters->has('categories')) {
			$joins['categories'] = true;
			$where[] = '`p_categories`.`category_id` = :category_id';
			$params['category_id'] = $filters->categories;
		}
		if ($filters->has('size')) {
			$joins['sizes'] = true;
			$where[] = '`p_sizes`.`size_id` = :size_id';
			$params['size_id'] = $filters->size;
		}
		if($filters->sub_categories) {
			$joins['sub_categories'] = true;
			$where[] = '`sub_cats`.`category_id` IN(:sub_categories)';
			$params['sub_categories'] = (array)$filters->sub_categories;
		}
        
		if ($filters->has('collection')) {
			if (is_numeric($filters->collection)) {
				$joins['collections_products'] = true;
                if($filters->not_in_collection)
                { 
                    $where[] = 'products.id NOT IN (SELECT product_id FROM '.CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS.' WHERE `collection_id` = :collection_id)';
                }
                else
                {
                    $where[] = '`c_products`.`collection_id` = :collection_id';
                }
				$params['collection_id'] = $filters->collection;
			}
		}else if ('not_in' == $filters->not_in_collection) {
            $leftJoins[] = ' LEFT JOIN `' . CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS . '` AS `c_products` ON `products`.`id` = `c_products`.`product_id`';
            $where[] = '`c_products`.`collection_id` IS NULL';
        }
        
		if ($filters->has('amount')) {
			$value = $filters->amount;
			if (!is_array($value)) {
				$value = array($value, 0);
			}
			list($min, $max) = $value;
			if ($min > 0 && $max == 0) {
				$where[] = '`p_sizes`.`amount` > :amount';
				$params[':amount'] = $min;
			} else if ($min == 0 && $max > 0) {
				$where[] = '`p_sizes`.`amount` < :amount';
				$params[':amount'] = $max;
			} else if ($min > 0 && $max > 0) {
				$where[] = '`p_sizes`.`amount` BETWEEN :amount_min AND :amount_max';
				$params[':amount_min'] = $min;
				$params[':amount_max'] = $max;
			} else {
				$filters->set('amount', false);
			}

			if ($filters->amount) {
				$joins['sizes'] = true;
			}
		}
        
		if($filters->quantity || is_numeric($filters->out_of_stock)) {
			$fields['total_buy'] = 'SUM(p_sizes.buy) AS total_buy';
			$fields['total_amount'] = 'SUM(p_sizes.amount) AS total_amount';
			$joins['sizes'] = true; 
		}
		
		if($filters->quantity) {
			$having[] = 'GREATEST((total_amount - total_buy), 0) >= :quantity';
			$params['quantity'] = $filters->quantity;
		}
		
		if (is_numeric($filters->out_of_stock)) {
            if($filters->out_of_stock == 2) {
                $where[] = 'products.out_of_stock = 2';
            } else {
                if($count) {
                    if($filters->out_of_stock == 0) {
                        $where[] = 'products.out_of_stock = 0 AND p_sizes.out_of_stock = 0 AND products.unactive_time = 0 AND p_sizes.amount > p_sizes.buy';
                    }
                } else {
                    if($filters->only_query) {
                        if($filters->out_of_stock == 0) {
                            $where[] = 'products.out_of_stock = 0 AND p_sizes.out_of_stock = 0 AND products.unactive_time = 0';
                            $having[] = 'SUM(p_sizes.amount) > SUM(p_sizes.buy)';
                        } else {
                            $having[] = 'SUM(p_sizes.buy + IF(products.out_of_stock OR p_sizes.out_of_stock OR products.unactive_time > 0, p_sizes.amount,0)) >= SUM(p_sizes.amount)';
                        }
                    } else {
                        if($filters->out_of_stock == 0) {
                            $where[] = 'products.out_of_stock = 0 AND p_sizes.out_of_stock = 0 AND products.unactive_time = 0';
                            $having[] = 'total_amount > total_buy'; // Без този ред, числата се изравняват.
                        } else {
                            $fields['total_buy'] = 'SUM(p_sizes.buy + IF(products.out_of_stock OR p_sizes.out_of_stock OR products.unactive_time > 0, p_sizes.amount,0)) AS total_buy'; // Cheat
                            $having[] = 'total_buy >= total_amount';
                        }
                    }
                }
            }
		}

		if (is_numeric($filters->return_products)) {
			$joins['return_products'] = true;
		}
		
		if (is_numeric($filters->status)) {
			$where[] = '`products`.`status` = :status';
			$params['status'] = $filters->status;
		} else if($filters->status == 'for_moderation') {
            /*
             * Products for moderation
             */
//            $joins['moderation'] = true;
//            $where[] = ' moderation.product_id IS NOT NULL ';
            $where[] = 'products.modified_time = 0';
        } else if($filters->status == 'last_moderated') {
            /*
             * Last moderated products
             */
//            $joins['moderation_all'] = true;
//            $where[] = '(moderation_all.product_id IS NULL or moderation_all.`status` = 1)';
            $sort[] = 'products.modified_time DESC';
            $where[] = 'products.modified_time > 0';
        } else if($filters->status == 'moderated') {
            /*
             * Moderated products 
             */
//            $joins['moderation'] = true;
//            $where[] = 'moderation.product_id IS NULL ';
            $where[] = 'products.modified_time > 0';
        } else if($filters->status == 'unactive') {
           /*
            * Unactive products 
            */
           $where[] = 'products.unactive_time > 0 ';
        }
             
        if(isset($filters->remote_provider_label) && !empty($filters->remote_provider_label)){
            
            $joins['remote_provider_label'] = true;
            $where[] = 'provider_label.value_id IN (:remote_provider_label)';
			$params['remote_provider_label'] = (array)$filters->remote_provider_label;
        }
        
        if(isset($filters->season) && !empty($filters->season)){
            
            $joins['season'] = true;
            $where[] = 'product_season.value_id IN (:season)';
			$params['season'] = (array)$filters->season;
        }
        
        if(isset($filters->label) && !empty($filters->label)){
      
            $joins['label'] = true;
            $where[] = 'label.property_id IN (:label)';
			$params['label'] = (array)$filters->label;
        }
        
        if(isset($filters->remote_provider_percentage_translation) && !empty($filters->remote_provider_percentage_translation)){
            
            $joins['remote_provider_percentage_translation'] = true;
            $where[] = 'provider_percentage_translation.value_id IN (:provider_percentage_translation)';
			$params['remote_provider_percentage_translation'] = (array)$filters->remote_provider_percentage_translation;
        }
             
//        if(isset($filters->external_marketplace) && !empty($filters->external_marketplace)){
//
//
//            /**
//             * Marketplace joins
//             */
//
//             $joins['external_marketplaces_products'] = sprintf("LEFT JOIN %s as product_marketplaces ON products.id = product_marketplaces.product_id", 'providers__products_marketplaces');
//
//             $fields['external_marketplace'] = 'marketplaces.marketplace_name as marketplace_name';                  
//             $joins['external_marketplace'] = sprintf("LEFT JOIN %s as marketplaces ON marketplaces.id = product_marketplaces.marketplace_id", 'marketplaces');
//
//             $where[] = 'product_marketplaces.marketplace_id = :marketplace_id';
//             $params['marketplace_id'] = $filters->external_marketplace;
//
//        }

        if(isset($filters->total_sizes) && !empty($filters->total_sizes) && $filters->total_sizes >0 && !$filters->only_query ){
            /**
             * Marketplace joins
             */
             $joins['sizes'] = true;
             $fields['sizes_total_sizes'] = 'COUNT(IF(p_sizes.amount>0,1, NULL)) as total_sizes';                  
             $having['sizes_total_sizes'] = sprintf("total_sizes = %s",$filters->total_sizes ); // Cheat
        }
		
		if(is_numeric($filters->discounted)) {
			$where[] = 'products.visible_price = :visible_price';
			$params['visible_price'] = $filters->discounted;
		}
		
		if($filters->not_product) {
			$where[] = 'products.id NOT IN (:not_id)';
			$params['not_id'] = (array)$filters->not_product;
		}
		
		if($filters->has_search()) {
            $joins['sizes'] = true;
            $fields['sizes_barcode'] = 'p_sizes.barcode as product_size_barcode';
            $where[] = 'CONCAT(products.product_name, " ", products.product_temp_name, " ", p_sizes.barcode, " ", products.description) LIKE :search';
			$params['search'] = '%'.$filters->search.'%';
		}
		
		if($filters->has_marg()) {
			$where[] = 'ROUND(((products.discount_price - products.provider_price) * 100) / products.provider_price) = :marg';
			$params['marg'] = $filters->marg;
		}
		
        if($filters->price_from) {
            $joins['today_prices'] = true;
            $where[] = '((today_prices.price_from IS NULL AND products.discount_price >= :price_from) OR (today_prices.price_from IS NOT NULL AND today_prices.price_from >= :price_from))';
            $params['price_from'] = $filters->price_from;
        } 
        if($filters->price_to) {
            $joins['today_prices'] = true;
            $where[] = '((today_prices.price_to IS NULL AND products.discount_price <= :price_to) OR (today_prices.price_to IS NOT NULL AND today_prices.price_to <= :price_to))';
            $params['price_to'] = $filters->price_to;
        }
		
        if($filters->expected_price_from) {
            $joins['future_prices'] = true;
            $where[] = '((future_prices.price_to IS NULL AND products.discount_price >= :expected_price_from) OR (future_prices.price_from IS NOT NULL AND future_prices.price_to >= :expected_price_from))';
            $params['expected_price_from'] = $filters->expected_price_from;
        } 
        if($filters->expected_price_to) {
            $joins['future_prices'] = true;
            $where[] = '((future_prices.price_to IS NULL AND products.discount_price <= :expected_price_to) OR (future_prices.price_to IS NOT NULL AND future_prices.price_to <= :expected_price_to))';
            $params['expected_price_to'] = $filters->expected_price_to;
        }
        
        if($filters->not_return_products) {
            $joins['providers'] = true;
            $where[] = 'providers.return_products = 0';
        }
        
        if($filters->has('discount_period') && !empty($filters->discount_period)) {
            list($activeFrom, $activeTo) = explode('_', $filters->discount_period);
            $joins['prices'] = true;
            $where[] = 'prices.active_from = :active_from';
            $where[] = 'prices.active_to = :active_to';
            $params['active_from'] = $activeFrom;
            $params['active_to'] = $activeTo;
        }
        
        if(!empty($filters->remote_provider_list) && $filters->remote_provider_list instanceof RemoteProviderListModel) {
            $remoteProviderList = $filters->remote_provider_list;
            $joins['remote_provider_values'] = array();
            if(!empty($remoteProviderList->category)) {
                $joins['remote_provider_values'][] = 'category';
                $where[] = 'remote_values_category.value_id IN (:remote_category)';
                $params['remote_category'] = $remoteProviderList->category;
            }
            if(!empty($remoteProviderList->subcategory)) {
                $joins['remote_provider_values'][] = 'subcategory';
                $where[] = 'remote_values_subcategory.value_id IN (:remote_subcategory)';
                $params['remote_subcategory'] = $remoteProviderList->subcategory;
            }
            if(!empty($remoteProviderList->color)) {
                $joins['remote_provider_values'][] = 'color';
                $where[] = 'remote_values_color.value_id IN (:remote_color)';
                $params['remote_color'] = $remoteProviderList->color;
            }
        }
        
        if(!empty($filters->only_visible_price)) {
            $where[] = ' products.visible_price = :only_visible_price';
            $params['only_visible_price'] = 1;
        }
		
		if(!empty($filters->campaign)) {
			$joins['campaign'] = 'INNER JOIN products__prices AS prices ON prices.product_id = products.id AND prices.campaign_id = :campaign';
			$params['campaign'] = $filters->campaign;
		}
		
		if($filters->seller) {
			$joins['seller'] = 'INNER JOIN sellers ON sellers.provider_id = products.provider_id';
			if($filters->seller == 'yes') {
				$where[] = 'sellers.id IS NOT NULL';
			} else {
				$where[] = 'sellers.id IS NULL';
			}
		}
		
		if($filters->color) {
			$where[] = 'products.color_id IN (:colors)';
			$params['colors'] = (array)$filters->color;
		}
		
		if($filters->sub_collection) {
			$joins['sub_collection'] = 'LEFT JOIN collections__sub_products AS sub_coll ON sub_coll.product_id = products.id';
			if('yes' == $filters->sub_collection) {
				$where[] = 'sub_coll.product_id IS NOT NULL';
			} else {
				$where[] = 'sub_coll.product_id IS NULL';
			}
		}
        
        if(!empty($filters->barcode)) {
            $joins['sizes'] = true; 
            $where[] = " `p_sizes`.barcode = :barcode";
            $params['barcode'] = $filters->barcode;
        }
        
        if(!empty($filters->is_warehouse) && $filters->is_warehouse) {
            $joins['providers'] = true;
            $where[] = ' providers.type = :type_warehouse';
            $where[] = ' providers.unactive_time = 0';
            $params['type_warehouse'] = \App\Model\ProvidersModel::TYPE_WAREHOUSE;
        }
        
        if(!empty($filters->warehouse_only) && $filters->warehouse_only) {
            $joins['providers'] = true;
            $where[] = ' providers.type = :type_warehouse';
            $where[] = ' providers.unactive_time = 0';
            $params['type_warehouse'] = \App\Model\ProvidersModel::TYPE_WAREHOUSE;
        }
        
        if(!empty($filters->created_before) && $filters->created_before) {
            //$joins['providers'] = true;
            $x = $filters->created_before;
            $y = $x - 3;
            
            $where[] = ' (products.created_at > :created_before_x AND products.created_at <= :created_before_y)';
            $params['created_before_x'] = strtotime('-'.$x.' month');
            $params['created_before_y'] = strtotime('-'.$y.' month');
        }
        
        if(!empty($filters->warehouse_id) && $filters->warehouse_id) {
            $joins['providers'] = true;
            $where[] = ' providers.warehouse_id = :warehouse';
            $params['warehouse'] = $filters->warehouse_id;
        }
        
        if(!empty($filters->is_dropship) && $filters->is_dropship) {
            $joins['providers'] = true;
            $where[] = ' providers.type = :type_dropship';
            $where[] = ' providers.unactive_time = 0 and providers.id <> 155';
            $params['type_dropship'] = \App\Model\ProvidersModel::TYPE_STANDART;
        }
        
        if(!empty($filters->warehouse_differences) && $filters->warehouse_differences) {
            $joins['providers'] = true;
            $joins['sizes'] = true; 
            $where[] = " `p_sizes`.amount <> `p_sizes`.stock";
        }
        
        if(!empty($filters->no_wholesaler_collection) && $filters->no_wholesaler_collection) {
            $joins['wholesaler_collection'] = true;
            $where[] = ' wholesaler_cp.product_id IS NULL';
        }
        
        if(!empty($filters->active_provider)) {
            $joins['providers'] = true;
            switch($filters->active_provider) {
                case 1:
                    $where[] = 'providers.unactive_time = 0';
                    break;
                case 2:
                    $where[] = 'providers.unactive_time > 0';
                    break;
            }
        }
        
        if(!empty($filters->skip_outlet) && $filters->skip_outlet) {
            $joins['providers'] = true;
            $where[] = ' providers.is_private = 0';
        }
		
		switch($filters->sort) {
			case 'old':
				$sort[] = 'id ASC';
				break;
			case 'price_low':
				$sort[] = 'products.discount_price ASC';
				break;
			case 'price_high':
				$sort[] = 'products.discount_price DESC';
				break;
			case 'discount':
				$sort[] = 'products.visible_price DESC, 100 - (products.discount_price*100/products.regular_price) DESC';
				break;
			case 'sells';
			case 'popularity':
				$fields['buys'] = 'SUM(IF(op.id IS NOT NULL,1,0)) AS buys';
				$joins['orders'] = 'LEFT JOIN users__orders_products AS op ON op.product_id = products.id';
				$sort[]= 'buys DESC';
				break;
            case 'returns':
                $joins['returns'] = true;
                $sort[] = 'count_returns DESC';
                $sort[] = 'products.id DESC';
                break;
            case 'old_returns':
                $joins['old_returns'] = true;
                $sort[] = 'min_return_product_id ASC';
                break;
            case 'product_name':
                $sort[] = 'products.product_name ASC';
                break;
            case 'product_temp_name':
                $sort[] = 'products.product_temp_name ASC';
                break;
			default:
				if ($filters->collection) {
					$sort[] = 'c_products.sort_index ASC, products.id ASC';
				} else {
					$sort[] = 'products.id DESC';
				}
				break;
		}
		
		/**
		 * Create joins for given fields
		 */
        if(isset($joins['providers'])) {
            $joins['providers'] = sprintf('INNER JOIN %s as providers ON products.provider_id = providers.id', \App\Manager\ProvidersManager::ABSTRACT_TABLE_PROVIDERS);
        }
		if (isset($joins['tags'])) {
			$joins['tags'] = 'INNER JOIN ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS . ' AS `p_tags`';
			$where[] = '`p_tags`.`product_id` = `products`.`id`';
		}
		if (isset($joins['categories'])) {
			$joins['categories'] = 'INNER JOIN ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES . ' AS `p_categories`';
			$where[] = '`p_categories`.`product_id` = `products`.`id`';
		}
		if (isset($joins['collections_products'])) {
			$joins['collections_products'] = 'INNER JOIN `' . CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS . '` AS `c_products`';
			$where[] = '`c_products`.`product_id` = `products`.`id`';
		}
		if (isset($joins['sizes'])) {
			$joins['sizes'] = 'INNER JOIN ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE . ' AS `p_sizes`';
			$where[] = '`p_sizes`.`product_id` = `products`.`id`';
		}
		if (isset($joins['return_products'])) {
			$joins['return_products'] = 'INNER JOIN ' . ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS_PRODUCTS . ' AS `return_products`';
			$where[] = 'return_products.copy_product_id = products.id';
		}
		if(isset($joins['orders'])) {
			$joins['orders'] = '
			INNER JOIN users__orders_products AS op ON op.product_id = products.id 
			INNER JOIN users__orders AS orders ON orders.id = op.order_id 
			INNER JOIN users__orders_params AS orders_params ON orders_params.order_id = orders.id 
			';
		}
		if(isset($joins['sub_categories'])) {
			$joins['sub_categories'] = sprintf('INNER JOIN %s AS sub_cats ON sub_cats.product_id = products.id', 'providers__products_sub_categories_all');
		}
		if(isset($joins['products_data'])) {
			$joins['products_data'] = sprintf('LEFT JOIN %s AS products_data ON products_data.product_id = products.id AND products_data.type = "all"', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_DATA);
		}
        if(isset($joins['returns'])) {
            $joins['returns'] = 'LEFT JOIN users__orders_products as return_op ON products.id = return_op.product_id AND return_op.return_product_id > 0';
            $fields['count_returns'] = 'count(return_op.id) AS count_returns';
        }
        if(isset($joins['old_returns'])) {
            $joins['old_returns'] = 'INNER JOIN users__orders_products as return_op_old ON products.id = return_op_old.product_id AND return_op_old.return_product_id > 0';
            $fields['ticket_product_id'] = 'MIN(return_op_old.return_product_id) as min_return_product_id';
        }
        if(isset($joins['prices'])) {
            $joins['prices'] = sprintf('INNER JOIN %s as prices on products.id = prices.product_id', self::ABSTRACT_TABLE_PRODUCTS_PRICES);
        }
        if(isset($joins['today_prices'])) {
            // Change INNER JOIN to LEFT JOIN, because INNER JOIN require records in products_prices table, but WHERE clause allow today_prices to be NULL
            $joins['today_prices'] = sprintf('LEFT JOIN %s as today_prices on products.id = today_prices.product_id AND today_prices.active_from <= UNIX_TIMESTAMP() AND today_prices.active_to >= UNIX_TIMESTAMP()', self::ABSTRACT_TABLE_PRODUCTS_PRICES);
        }
        if(isset($joins['future_prices'])) {
            $joins['future_prices'] = sprintf('INNER JOIN %s as future_prices on products.id = future_prices.product_id AND future_prices.active_to >= UNIX_TIMESTAMP()', self::ABSTRACT_TABLE_PRODUCTS_PRICES);
        }
        if(isset($joins['wholesaler_collection'])) {
            $joins['wholesaler_collection'] = sprintf("LEFT JOIN %s as wholesaler_cp on wholesaler_cp.product_id = products.id AND wholesaler_cp.status = 'active'", \App\Manager\CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS);
        }
       
        if(!empty($filters->active_price_date)) {
            $joins['promotion'] = "LEFT JOIN " . self::ABSTRACT_TABLE_PRODUCTS_PRICES . " as promotion on promotion.product_id = products.id AND :promotion_active_to > promotion.active_from AND promotion.active_to > :promotion_active_from";
            $params['promotion_active_from'] = $filters->active_price_date['from'];
            $params['promotion_active_to'] = $filters->active_price_date['to'];
            $where[] = 'promotion.id IS NULL';
        }
//        if(isset($joins['moderation'])) {
//            $joins['moderation'] = sprintf("LEFT JOIN %s as moderation on products.id = moderation.product_id AND moderation.status = :moderation_status", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PRODUCTS_MODERATION);
//            $params['moderation_status'] = \App\Manager\ProductsModsManager::STATUS_UNMODERATED;
//        }
//        if(isset($joins['moderation_all'])) {
//            $joins['moderation_all'] = sprintf("LEFT JOIN %s as moderation_all on products.id = moderation_all.product_id", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PRODUCTS_MODERATION);
//        }
        
        if(isset($joins['remote_provider_label'])) {
            $joins['remote_provider_label'] = sprintf("INNER JOIN %s as provider_label on products.id = provider_label.product_id", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PROVIDERS_REMOTE_PRODUCTS_VALUES);
        }
        
        if(isset($joins['label'])) {
            $joins['label'] = sprintf("INNER JOIN %s as label on products.id = label.product_id", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES);
        }
        
        if(isset($joins['season'])) {
            $joins['season'] = sprintf("INNER JOIN %s as product_season on products.id = product_season.product_id", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES);
        }
            
        if(isset($joins['remote_provider_percentage_translation'])) {
            $joins['remote_provider_percentage_translation'] = sprintf("INNER JOIN %s as provider_percentage_translation on products.id = provider_percentage_translation.product_id", \App\Manager\ProductsModsManager::ABSTRACT_TABLE_PROVIDERS_REMOTE_PRODUCTS_VALUES);
        }
        
        if(isset($joins['remote_provider_values'])) {
            foreach($joins['remote_provider_values'] as $joinType) {
                $joinReplace = array(
                    '__REMOTE_VALUES__' => 'remote_values_' . $joinType
                );
                $remoteProviderJoin =  sprintf("JOIN %s as __REMOTE_VALUES__ ON products.id = __REMOTE_VALUES__.product_id",
                    \App\Manager\RemoteProviderManager::ABSTRACT_TABLE_PROVIDERS_REMOTE_PRODUCTS_VALUES);
                foreach($joinReplace as $key => $value) {
                    $remoteProviderJoin = str_replace($key, $value, $remoteProviderJoin);
                }
                $joins['remote_provider_values_' . $joinType] = $remoteProviderJoin;
            }
            unset($joins['remote_provider_values']);
        }
        /**
		 * Build query
		 */
		$query = sprintf('SELECT {fields} FROM %s AS `products`', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS);
		
		if (!empty($joins)) {
			$query .= ' ' . implode(" ", $joins);
		}
		if (!empty($leftJoins)) {
			$query .= implode(' ', $leftJoins);
		}
		if (!empty($where)) {
			$query .= ' WHERE ' . implode(' AND ', $where);
		}
        if(!$count) {
            $query .= ' GROUP BY products.id';
        }
		if(!empty($having) && !$count) {
			$query .= ' HAVING '.implode(' AND ', $having);
		}
        if(!$count && !$filters->only_query) {
            $query .= ' ORDER BY '.implode(', ', $sort);
        }
        if($filters->only_query) {
            return array($query, $params);
        }
        if($count) {
            $query = str_replace('{fields}', 'count(DISTINCT products.id) AS `count` ', $query);
            $data = $this->db->fetch($query, $params);
            return $data->count;
        }

		$baseQuery = $query; 
        //echo __LINE__.'<br>'.$baseQuery; exit;
        //echo __LINE__.'<br><pre>'.print_r($params, true).'</pre>'; exit;
        $query = str_replace('{fields}', 'products.* ' . (!empty($fields) ? ', '.implode(', ', $fields) : ''), $query);
		if ($paging) {
			list($current, $max) = $paging;
			$all = $this->db->getPagingResult($query, $params, $current, $max);
		} else {
			$all = $this->db->fetchAll($query, $params);
			$all = (object) array(
                'rows' => $all
			);
		}
		$all->rows = $this->modifyData($all->rows, true, 'convert_product_row', $modifier);
        //echo __LINE__.' '.$baseQuery; exit;        		
        $query = str_replace('{fields}', '{fields}' . (!empty($fields) ? ', '.implode(', ', $fields) : ''), $baseQuery);
        
//        echo $query; exit;
        $statsFields = !empty($fields) ? $fields : array();
        $statsFields[] = 'AVG(products.discount_price) AS avg_discount_price';
        $statsFields[] = 'AVG(products.discount_price - products.provider_price) AS avg_marg';
		$statsQuery = str_replace('{fields}', implode(', ', $statsFields), $baseQuery);
		$statsCacheKey = \App\Manager\ManagerCache::KEY_STATS_PRODUCTS . '.' . md5($statsQuery . serialize($params));
		$stats = $this->cache->get($statsCacheKey);
		if(false === $stats) {
			$stats = $this->db->fetch($statsQuery, $params);
			$this->cache->set($statsCacheKey, $stats, 3600*24);
		}
		$all->stats = $stats;
		        
        
		return $all;
	}

    /**
     * Get product filters
     * 
     * ProductFiltersModel $requestFilters
     * Array $extends
     * 
     * return Array
     */
	public function getProductsFilters(ProductFiltersModel $requestFilters, array $extends = array()) {
		$filtersData = $this->app['products.filters.manager']->getFilters($requestFilters, $extends);
		return $filtersData;
	}

    /*
     * Modify products with mappings
     * 
     * Array(ProvidersProductsModel) $products
     * 
     * return nothing
     */
	public function modifyProduct($products) {
        foreach ($products as $product) {
			if (false === ($product instanceof ProvidersProductsModel)) {
				throw new Exception('Product is not instance of ProvidersProductsModel');
			}
		}

		$app = $this->app;

		$app['collection.manager']->enableCollectionMeta(false);
		
		$app['provider.manager']->mapProvider($products, false);
		
		if ($this->app['admin']) {
			$app['user.manager']->mapUser($products, 'author_id', 'author');
		}
		
//		$this->mapUnactive($products);

		$app['brands.manager']->mapBrand($products, false);

		$this->mapData($products, 'id', array($this, 'findProductSizes'), 'sizes', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductSizeTables'), 'size_tables', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductImages'), 'images', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductTags'), 'tags', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductCategories'), 'categories', 'product_id', true);
		$this->mapData($products, 'id', array($app['collection.manager'], 'findProductCollection'), 'collection', 'product_id');
		$this->mapData($products, 'id', array($app['collection.manager'], 'findAllProductCollections'), 'all_collections', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductProperties'), 'properties', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductSubCategories'), 'sub_categories', 'product_id', true);
//        if($this->app['admin']) {
//            $this->mapData($products, 'id', array($app['products.mods.manager'], 'findProductModeration'), 'moderation', 'product_id', false);
//        }
		
		$this->app['colors.manager']->mapColor($products, 'color_id');
		
		$this->mapProductVariants($products);
		
		$this->mapProductsPrices($products, 'id');
		
		$app['collection.manager']->enableCollectionMeta(true);
        
		
		foreach ($products as $product) {
			$this->findProductBestImage($product);
			if ($this->app['admin']) {
				if ($product->collection) {
                    $editImageKey = 'box';
					foreach ($this->image_positions as $key) {
						if ($product->has($key)) {
							if ($product->collection->isNotStarted()) {
								$product->get($key)->formats[$editImageKey]['path'] .= '/_r/' . rand(1, 9999);
							}
							$product->edit_image_refresh = $product->get($key)->formats[$editImageKey]['path'];
							if ($product->collection->isStarted()) {
								$product->edit_image_refresh .= '?';
							}
						}
					}
				}
			}


			$categories = array();
			foreach ($product->categories as $category) {
				$categories[$category->id] = $category;
			}
			$product->categories = $categories;

			$product->available_amount = 0;
			$product->expected_amount = 0;
			$product->amount = 0;
			foreach ($product->sizes as $size) {
				if ($product->out_of_stock) {
					$size->available = false;
				} else {
					$product->amount += $size->amount;
					$product->available_amount += $size->remaining_quantity;
				}
                $product->expected_amount += $size->expected;
			}
			
			if($product->discount_price > 0) {
				$product->available = $product->available_amount > 0 ? true : false;
			} else {
				$product->available = false;
			}

			if ($product->collection && $product->collection->product_status == 'unactive') {
				$product->available = false;
			}
			
			foreach ($product->images as $image) {
                $image->collection_format = 'box';
				$image->collection_image = $image->formats[$image->collection_format]['path'];
			}

			if (count($product->sizes) == 1) {
				$product->select_size = true;
				foreach ($product->categories as $row) {
					if ($row->id == 4) {
						$product->select_size = false;
						break;
					}
				}
			} else {
				$product->select_size = true;
			}
			
			$product->blank_image = $this->app['url']['imgs'].'product_50.jpeg';
		}
	}

    /**
     *  Maping data to products
     * 
     *  array $data - array(object -> need to be instance of \App\Model\AbstractModel, need to have property "product_id")
     * 
     *  return nothing 
     */
	public function mapProducts($data, $rewrite = true, $pk='product_id') {
		$this->mapDatabaseData($data, $rewrite, $pk, array($this, 'findProductsById'), 'product', 'id');
	}
	
    /**
     * Map stdClasses with products
     * 
     * Array(stdClass) $data
     * Boolean $rewrite
     * String $pk
     * 
     * return nothing
     */
	public function mapProductsShort($data, $rewrite = true, $pk='product_id') {
		$that = $this;
		$this->mapDatabaseData($data, $rewrite, $pk, function($values)use($that){
			return $that->findProductById($values, 'shortProductsModifier');
		}, 'product', 'id');
	}

    /**
     * Validate product
     * 
     * ProvidersProductsModel $product
     * Array $allow
     * 
     * return nothing | \Exception
     */
	public function validateProduct(ProvidersProductsModel $product, array $allow = array()) {
		$validator = array(
			'allowExtraFields' => true,
			'fields' => array(
				'provider_id' => array(
					new Assert\NotBlank(array(
						'message' => 'Продукта няма доставчик'
					))
				),
				'product_name' => array(
					new Assert\NotBlank(array(
						'message' => 'Не сте въвели име на продукта'
					))
				),
				'regular_price' => array(
					new Validator\Price(array(
						'message' => 'Пазарната цена не е валидна'
					)),
					new Validator\Callback(array(
						'message' => 'Не сте въвели пазарна цена',
						'callback' => function($value)use($product) {
							if($product->visible_price && $value < 1) {
								return false;
							}
						}
					))
				),
				'provider_price' => array(
					new Validator\Price(array(
						'message' => 'Доставната цена не е валидна'
					)),
					new Validator\Callback(array(
						'message' => 'Не сте въвели цена на доставчик',
						'callback' => function($value) {
							return $value > 0 ? true : false;
						}
					)),
					new Validator\Callback(array(
						'message' => 'Цената от доставчика е по-висока от регулярната цена?',
						'callback' => function($value)use($product) {
							if($product->visible_price && $value > $product->regular_price) {
								return false;
							}
						}
					))
				),
				'discount_price' => array(
					new Validator\Price(array(
						'message' => 'Продажната цена не е валидна'
					)),
					new Validator\Callback(array(
						'message' => 'Не сте въвели цена в сайта',
						'callback' => function($value) {
							return $value > 0 ? true : false;
						}
					)),
					new Validator\Callback(array(
						'message' => 'Цената в сайта е по-висока от регулярната цена?',
						'callback' => function($value)use($product) {
							if($product->visible_price && $value > $product->regular_price) {
								return false;
							}
						}
					))
				),
				'weight' => array(
					new Validator\Numeric(array(
						'message' => 'Тежескта не е валидна цена'
					))
				),
				'advance' => array(
					new Validator\Numeric(array(
						'message' => 'Предплатата не е валидна цена'
					))
				),
				'brand_name' => array(
					new Assert\NotBlank(array(
						'message' => 'Не сте посочили марка'
					))
				),
				'sizes' => array(
					new Assert\Count(array(
						'min' => 1,
						'minMessage' => 'Продукта трябва да има поне един размер'
					))
				),
				'categories' => array(
					new Assert\Count(array(
						'min' => 1,
						'minMessage' => 'Продукта трябва да е в поне един раздел'
					))
				),
				'tags' => array(
					new Assert\Count(array(
						'min' => 1,
						'minMessage' => 'Продукта трябва да има поне 1 ключова дума'
					))
				),
				'description' => array(
					new Assert\NotBlank(array(
						'message' => 'Не сте въвели описание на продукта'
					))
				),
				'images' => array(
					new Assert\Count(array(
						'min' => 1,
						'minMessage' => 'Продукта трябва да има поне 1 снимка'
					))
				),
				'image' => array(
					new Assert\Image(array(
						'minWidth' => 100,
						'minHeight' => 100
					))
				),
				'size' => array(
					new Assert\NotBlank()
				)
			)
		);

		if($product->user_add) {
			$validator['fields']['product_temp_name'] = array(
				new Assert\NotBlank(array(
					'message' => 'Не сте въвели служебно име на продукта'
				))
			);
		}
		
		if($product->user_add || $product->validate_sub_categories) {
			$validator['fields']['sub_categories'] = array(
				new Assert\NotBlank(array(
					'message' => 'Не сте посочили под категории на продукта'
				))
			);
			if(isset($validator['fields']['tags'])) {
				unset($validator['fields']['tags']);
			}
		}
		
		if($product->validate_properties || ($product->user_add && !$product->skip_properties_validation)) {
			$validator['fields']['properties'] = array(
				new Assert\NotBlank(array(
					'message' => 'Не сте посочили описателни стойности към продукта'
				))
			);
			unset($validator['fields']['description']);
		}
        
		if($product->validate_color || ($product->user_add && !$product->skip_color)) {
			$validator['fields']['color_id'] = array(
				new Assert\NotBlank(array(
					'message' => 'Не сте посочили цвят на продукта'
				)),
                new Assert\GreaterThan(array(
                    'message' => 'Не сте посочили цвят на продукта',
                    'value' => 0
                ))
			);
		}
		
		if($product->validate_variants || ($product->user_add && !$product->provider->is_wholesaler)) {
			$maxVariants = 10;
			$validator['fields']['variants'] = array(
				new Assert\Count(array(
					'max' => $maxVariants,
					'maxMessage' => 'Посочените варианти са повече от '.$maxVariants
				))
			);
		}
		if ($allow) {
			foreach ($validator['fields'] as $key => $val) {
				if (!in_array($key, $allow)) {
                    unset($validator['fields'][$key]);
				}
			}
		} else {
			unset($validator['fields']['image'], $validator['fields']['size']);
		}
		
		$prodData = $product->toArray(true);    
		
		$validatorCollection = new Assert\Collection($validator);
		$errorList = $this->app['validator']->validateValue($prodData, $validatorCollection);

		foreach ($errorList as $error) {
			$name = substr($error->getPropertyPath(), 1, -1);
			throw new Error\FormError($name, $error->getMessage());
		}

		return $errorList;
	}

    /**
     * Map product with categories
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing
     */
	public function createProductDefaults(ProvidersProductsModel $product) {
		$product->all_categories = $this->app['tags.manager']->getCategories();
	}

    /**
     * Create product admin view data
     * 
     * ProvidersProductsModel $product
     * 
     * return Array
     */
	public function createProductAdminViewData(ProvidersProductsModel $product) {
		$allCategories = $product->all_categories;
		foreach ($allCategories as $category) {
			$category->selected = $product->has_category($category->id);
		}

		$productTags = array();
		foreach ($product->tags as $key => $val) {
			$productTags[] = $val->name;
		}
		$product->selected_tags = implode(',', $productTags);
		
		$charts = array();
		$sizes = array();
		foreach($product->sizes as $size) {
			$item = array(
				'amount' => $size->amount,
				'buy' => $size->buy,
				'expected' => empty($size->expected)?0:$size->expected,
				'stock' => empty($size->stock)?0:$size->stock,
				'barcode' => $size->barcode,
				'out_of_stock' => $size->out_of_stock
			);
			if(isset($size->size)) {
				$item['id'] = $size->size_id;
				$charts[] = $size->size->size_chart_id;
			} else {
				$item['id'] = $size->id;
			}
			if($product->id) {
				$item['product_size'] = $product->id.'_'.$item['id'];
			}
			$sizes[] = (object)$item;
		}
		
		if($product->id) {
			$this->app['orders.manager']->mapSizeInUse($sizes);
		}
		
		if(!is_array($product->charts)) {
			$product->charts = array();
		}
		$pCharts = $product->charts;
		foreach($charts as $chartId) {
			$pCharts[] = $chartId;
		}
		$product->charts = $pCharts;
		
		$sizeTables = array();
		foreach ($product->size_tables as $table) {
			if (!is_numeric($table)) {
				$id = $table->table_id;
			} else {
				$id = $table;
			}
			$sizeTables[] = $id;
		}
		
		$colors = $this->app['colors.manager']->findColors();

		if($product->id) {
			$parentProduct = $this->app['products.manager']->findProductLink($product->id);
		} else {
			$parentProduct = false;
		}

		
		$variants = $product->variants;
		$this->mapProducts($variants, false, 'variant_id');
		$variantsJson = array();
		foreach($variants as $var) {
			$row = $var->product->toArray();
			$row['edit_url'] = $var->product->edit_url;
			$row['main_image'] = $var->product->main_image;
			$variantsJson[$var->product->id] = $row;
		}
		
		$previewUrl = false;
		if($product->id) {
			$previewUrl = $this->app['url_generator']->generate('sales:product_short', array(
				'product' => $product->id
			));
		}

		$data = array(
			'id' => $product->id,
			'provider' => $product->provider,
			'product_name' => $product->product_name,
			'product_temp_name' => $product->product_temp_name,
			'provider_price' => $product->provider_price ? : '',
            'original_provider_price' => $product->original_provider_price ? $product->original_provider_price : "",
			'regular_price' => $product->regular_price ? : '',
			'discount_price' => $product->discount_price ? : '',
			'amount' => $product->amount,
			'weight' => $product->weight,
			'advance' => $product->advance > 0 ? $product->advance : false,
			'description' => $product->description,
			'brand_name' => $product->brand ? $product->brand->name : '',
			'main_image' => array(
				'src' => self::PRODUCT_DEFAULT_IMAGE,
				'default_src' => self::PRODUCT_DEFAULT_IMAGE
			),
			'images' => array(),
			'image_upload' => $this->app['url_generator']->generate('admin.providers:product:image:upload', array(
				'product' => $product->id
			)),
			'remote_provider_uid' => $product->remote_provider_uid,
			'barcode' => $product->barcode,
			'categories' => $allCategories,
			'selected_tags' => $product->selected_tags,
			'size_tables_ids' => $sizeTables,
			'sizes' => $sizes,
			'api_url' => array(
				'brands' => $this->app['url_generator']->generate('admin.brands:json'),
				'charts' => $this->app['url_generator']->generate('admin.providers:json_size_charts', array(
					'providerModel' => $product->provider->id,
					'charts' => $product->charts
				)),
				'tags' => $this->app['url_generator']->generate('admin.tags:json')
			),
			'link_product' => (
				$product->link_id ? array(
					'edit_url' => $this->app['url_generator']->generate('admin.providers:product:edit', array(
						'product' => $product->link_id
					))
				) : false
			),
			'parent_product' => (
				$parentProduct ? array(
					'edit_url' => $this->app['url_generator']->generate('admin.providers:product:edit', array(
						'product' => $parentProduct->id
					))
				) : false
			),
			'visible_price' => $product->visible_price ? true : false,
			'properties' => $product->properties,
			'sub_categories' => $product->sub_categories,
			'color_id' => $product->color_id,
			'colors' => $colors,
			'variants' => array(
				'count' => count($variants),
				'json' => json_encode($variantsJson)
			),
			'preview_url' => $previewUrl,
			'out_of_stock' => $product->out_of_stock,
			'unactive' => !empty($product->unactive_time),
			'unactive_time' => $product->unactive_time
		);
		

		$images = array();
		if (is_array($product->images)) {
			$images = $product->images;
		}
		$diff = self::MAX_PRODUCT_IMAGES - count($images);
		for ($i = 1; $i <= $diff; $i++) {
			$id = count($images) + 1;
			$images [] = (object) array(
						'id' => $id,
						'filename' => '',
						'upload' => false,
						'position' => 0
			);
		}
		foreach ($images as $key => $val) {
			$val->default_src = self::PRODUCT_DEFAULT_IMAGE;
			if (empty($val->filename)) {
				$val->preview = $val->default_src;
			} else {
				list($imageStoragePath, $imagePreviewPath) = $this->getProductImagePaths($product, $val->filename);
				$val->preview = $imagePreviewPath;
			}
			$images[$key] = $val;
		}
		$data['images'] = $images;
		
		return $data;
	}

    /**
     * Modify product buyer with mappings
     * 
     * Array(stdClass) $product
     * 
     * return nothing
     */
	public function modifyAdminBuyerMode($product) {
		if (!is_array($product)) {
			$products = array($product);
		} else {
			$products = $product;
		}

		$app = $this->app;

		$buyer = $app['user.manager']->convert_user_admin($app['session']->get('admin.products.buy'));
		$app['buyer'] = $buyer;
		if ($buyer) {
			$buyerCart = $buyer->getShoppingCart($app);
			foreach ($products as $product) {
				$product->refresh_url = $app['url_generator']->generate('admin.products:refresh_product', array(
					'product' => $product->id
						));

				if ($buyerCart->hasProduct($product)) {
					$product->shopping_cart_remove = $app['url_generator']->generate('admin.products:shopping_cart:remove', array(
						'user' => $buyer->id,
						'product' => $product->id,
						'size' => $product->variant ? $product->variant->size_id : null
							));
				} else {
					$product->shopping_cart_add = $app['url_generator']->generate('admin.products:shopping_cart:add', array(
						'user' => $buyer->id,
						'product' => $product->id
							));
				}
			}
		}
	}

    /**
     * Generate view url
     * 
     * Integer $id
     * String $name
     * 
     * return String
     */
	public function generateViewUrl($id, $name) {
		return $this->app['url_generator']->generate('sales:product', array(
            'name' => $this->app['text.service']->text2url($name),
            'product' => $id
        ));
	}
    
    /**
     * Archive products
     * 
     * Array(Integer) $products
     * 
     * return nothing
     */
	public function archive_products($products) {
		foreach($products as $product_id) {
			$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
				'status' => ProvidersProductsModel::STATUS_ARCHIVED),
            array(
				'id' => $product_id
			));
			$this->productCache->invalidate($product_id);
		}
	}
	
	/**
     * Copy product data
     * 
	 * Integer $productId
	 * Array $sizes
     * Boolean $providerId
     * Boolean $skipRestrictions
     * Boolean $skipLinked
     * 
     * return ProviderProducts | \Exception
     */
	public function copyProduct($productId, array $sizes=array(), $providerId=false, $skipRestrictions=false, $skipLinked = false) {
		$product = $this->db->fetch(sprintf('SELECT * FROM %s WHERE id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), $productId);
		if(empty($product)) {
			throw new \Exception('Product not found');
		}
		if(empty($sizes)) {
			throw new \Exception('Cant copy product without sizes');
		}
		
		$this->app['provider.manager']->mapProvider($product, false);
		if(empty($product->provider)) {
			throw new \Exception('Product has no provider');
		}
        if($product->provider->isTypeWarehouse()) {
            return $product;
        }
		if($product->provider->return_products && !$skipRestrictions) {
			return $product;
		}
		
		/**
		 * If product has already copy
		 */
		$hasLink = $this->db->fetch(sprintf('SELECT * FROM %s WHERE link_id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), $product->id);
		if($hasLink) {
			$product = $hasLink;
		}
		
        
		$checkSize = $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE product_id=? AND size_id IN (?)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array(
			$product->id,
			array_keys($sizes)
		));
		if(count($checkSize) != count($sizes)) {
            if($hasLink) {
                $checkSize = $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE product_id=? AND size_id IN (?)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array(
                    $product->link_id,
                    array_keys($sizes)
                ));
                if(count($checkSize) != count($sizes)) {
                    throw new \Exception('Sizes miss match');
                }
            } else {
                throw new \Exception('Sizes miss match');
            }
		}
		
		if(empty($providerId)) {
			$providerId = $this->app['app_config']['provider']['id'];
		}
		
		if($providerId == $this->app['app_config']['provider']['id']) {
			$linkProducts = true;
		} else {
			$linkProducts = false;
		}
		
		$resetProductFields = array(
			'provider_id' => $providerId,
			'created_at' => time(),
			'out_of_stock' => 0
		);
        if($skipLinked) {
            $resetProductFields['link_id'] = 0;
        }
		
		$resetSizeFields = array(
			'amount' => 0, 
			'buy' => 0, 
			'out_of_stock' => 0,
			'status' => 1
		);
		
		/**
		 * Just update sizes
		 * if product already has linked product
		 */
		if(!empty($product->link_id) && !$skipLinked) {
			try {
				$this->db->beginTransaction();
				
				$newProduct = $this->db->fetch(sprintf('SELECT * FROM %s WHERE id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), $product->link_id);
				foreach($sizes as $sizeId=>$quantity) {
					$row = $this->db->fetch(sprintf('SELECT * FROM %s WHERE product_id=? AND size_id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array($newProduct->id, $sizeId));
					if(empty($row)) {
						$row = $this->db->fetch(sprintf('SELECT * FROM %s WHERE product_id=? AND size_id=?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array($product->id, $sizeId));
						if(!empty($row)) {
							$row->product_id = $newProduct->id;
							$row = (array)$row;
							foreach($resetSizeFields as $key=>$val) {
								$row[$key] = $val;
							}
							$row['amount'] = 0;
                            $row['warehouse_code'] = $productId . '_' . $sizeId;
							$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
						} else {
							$row = array(
								'product_id' => $newProduct->id,
								'size_id' => $sizeId,
								'amount' => 0,
								'buy' => 0,
                                'warehouse_code' => $productId . '_' . $sizeId
							);
							$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
						}
					}
				}
				
				$this->productCache->invalidate($newProduct->id);
				
				$this->db->commit();
				return $newProduct;
			} catch(\Exception $e) {
				$this->db->rollBack();
				return $e;
			}
		}
		
		/**
		 * Create new linked product
		 * with associated data from old product
		 */
		try {
			$this->db->beginTransaction();
			
			$this->db->query(sprintf('CREATE TEMPORARY TABLE IF NOT EXISTS _tmpProducts SELECT * FROM %s WHERE id=? LIMIT 1', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), $product->id);
			$row = $this->db->fetch('SELECT * FROM _tmpProducts WHERE id=?', $product->id);
			unset($row->id);
			$newProduct = (array)$row;
			$newProduct[] = '_id';
			foreach($resetProductFields as $key=>$val) {
				$newProduct[$key] = $val;
			}
			$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $newProduct);
            
            $this->db->query('DROP TEMPORARY TABLE IF EXISTS _tmpProducts');
			if($linkProducts) {
				$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
					'link_id' => $newProduct->id
				), array(
					'id' => $product->id
				));
			}
            
			$srcPath = $this->getProductImagePaths($product->id, '');
			$newPath = $this->getProductImagePaths($newProduct->id, '');

			exec('cp -r '.$srcPath[0].' '.$newPath[0]);

			if(!is_dir($newPath[0])) {
				throw new \Exception($newPath[0].' - is not directory');
			}
			
			$tables = array(
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES
			);

			foreach($tables as $table) {
            	$tempTable = '_tmp_'.$table;

				switch($table) {
					case self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE:
						$this->db->query('CREATE TEMPORARY TABLE IF NOT EXISTS '.$tempTable.' SELECT * FROM '.$table.' WHERE product_id=:product AND size_id IN (:sizes)', array(
							'product' => $product->id,
							'sizes' => array_keys($sizes)
						));
						break;
					default:
						$this->db->query('CREATE TEMPORARY TABLE IF NOT EXISTS '.$tempTable.' SELECT * FROM '.$table.' WHERE product_id=:product', array(
							'product' => $product->id
						));
						break;
				}
				
				$rows = $this->db->fetchAll('SELECT * FROM '.$tempTable.' WHERE product_id=?', $product->id);
				foreach($rows as $row) {
					$row->product_id = $newProduct->id;
					$row = (array)$row;
					if(isset($row['id'])) {
						unset($row['id']);
						$row[] = '_id';
					}
					
					/**
					 * Reset size values
					 */
					if(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE == $table) {
						foreach($resetSizeFields as $key=>$val) {
							$row[$key] = $val;
						}
						if(isset($sizes[$row['size_id']])) {
							$row['amount'] = 0;//$sizes[$row['size_id']];
							unset($sizes[$row['size_id']]);
						}
                        $row['warehouse_code'] = $productId . '_' . $row['size_id'];
					}
					
					$this->db->insert($table, $row);
              	}
                
                $this->db->query('DROP TEMPORARY TABLE IF EXISTS ' . $tempTable);
			}
			
			if(!empty($sizes)) {
				foreach($sizes as $sizeId=>$quantity) {
					$row = array(
						'product_id' => $newProduct->id,
						'size_id' => $sizeId,
						'amount' => 0,//$quantity,
						'buy' => 0,
                        'warehouse_code' => $productId . '_' . $sizeId
					);
					$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
				}
			}
            
			$this->productCache->invalidate($newProduct->id);
			$this->productCache->invalidate($product->id);
			
			$this->db->commit();
            return $newProduct;
		} catch(\Exception $e) {
            $this->db->rollBack();
			return $e;
		}
	}

    /**
     * Delete product by id
     * 
     * Integer $productId
     * 
     * return nothing
     */
	public function deleteProduct($productId) {
		$this->db->delete('products__prices', array(
			'product_id' => $productId
		));
		$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
			'id' => $productId
		), 1);
		$this->db->delete('providers__remote_products', array(
			'product_id' => $productId
		), 1);
		$this->productCache->invalidate($productId);
        if(substr($productId, -1) >= 5) {
            $dirKey = 'products2';
        } else {
            $dirKey = 'products';
        }
        $storagePath = $this->app['paths'][$dirKey].self::PRODUCT_DIR_PREFIX . $productId . '/';
		exec('rm -rf '.$storagePath);
	}
	
    /**
     * Find product data(s) by product(s)
     * 
     * Integer|Array(Integer) $product
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductData($product) {
		return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE product_id = :product', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_DATA),
			sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_DATA)
		), 'product', array('product' => $product), null, 'modifyProductData', null, $this->productCache->child('data', 'product_id', false, 1800));
	}
	
    /**
     * Modify product data with mappings
     * 
     * Array(stdClass) $result
     * 
     * return nothing
     */
	public function modifyProductData($result) {
		foreach($result as $row) {
			$row->rating = ceil($row->popularity);
		}
	}
	
    /**
     * Map stdClasses with products
     * 
     * Array(stdClass) $data
     * String $pk
     * 
     * return nothing
     */
	public function mapProductData($data, $pk='id') {
		$this->mapData($data, $pk, array($this, 'findProductData'), 'data', 'product_id');
	}
	
    /**
     * Change products marg
     * 
     * Integer $newMarg
     * Array(Integer) $productIds
     * Array $margOptions
     * Decimal $newProviderPrice
     * 
     * return nothing
     */
	public function changeProductsMarg($newMarg, array $productIds, array $margOptions = array(), $newProviderPrice = 0) {
		$errors = array(
			'regular_price' => array(
				'count' => 0,
				'message' => 'Има продукти, на които регулярната цена е по-ниска от новата продажна цена!'
			)
		);
        $queryParams = array();
		$joins = array();
        $group = array();
        $where = "";
        if (isset($margOptions['price']) && $margOptions['price'] > 0) {
            if (isset($margOptions['type']) && isset($margOptions['symbol'])) {
                if ($margOptions['type'] == 'provider') {
                    if ($margOptions['symbol'] == 'more') {
                        $where .= ' AND pp.provider_price >= :price';
                    } else {
                        $where .= ' AND pp.provider_price <= :price';
                    }
                } else {
                    if ($margOptions['symbol'] == 'more') {
                        $where .= ' AND pp.discount_price >= :price';
                    } else {
                        $where .= ' AND pp.discount_price <= :price';
                    }
                }
                $queryParams['price'] = $margOptions['price'];
            }
        }
        if (isset($margOptions['sub_category']) && $margOptions['sub_category']) {
            $joins[] = sprintf('INNER JOIN %s as ppsc on pp.id = ppsc.product_id', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES);
            $group[] = "pp.id";
            $where .= " AND ppsc.category_id = :category";
            $queryParams['category'] = $margOptions['sub_category'];
        }
        if (isset($margOptions['brand']) && $margOptions['brand']) {
            $where .= ' AND pp.brand_id = :brand';
            $queryParams['brand'] = $margOptions['brand'];
        }
        if(!empty($group)) {
            $group = "GROUP BY " . implode(', ', $group);
        } else {
            $group = "";
        }
            
        $updates = array();

        $query = sprintf("SELECT pp.id, pp.provider_price, pp.discount_price, pp.regular_price, pp.visible_price FROM %s as pp %s WHERE pp.id IN (:products) %s %s", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, implode(" ", $joins), $where, $group);
        $queryParams['products'] = $productIds;
		$stmt = $this->db->query($query, $queryParams);
		while($row = $stmt->fetch()) {
            if(!empty($newProviderPrice) && $newProviderPrice > 0) {
                $row->provider_price = $newProviderPrice;
            }
			$row->discount_price = ceil($row->provider_price + ($row->provider_price * ($newMarg / 100)));
			if($row->visible_price && $row->regular_price < $row->discount_price) {
				$errors['regular_price']['count']++;
			} else {
				$updates[$row->id] = $row;
			}
		}
		foreach($errors as $key=>$err) {
			if($err['count'] > 0) {
				throw new Error\FormError($key, $err['message']);
			}
		}
		foreach($updates as $row) {
			$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
				'provider_price' => $row->provider_price,
				'discount_price' => $row->discount_price,
				'regular_price' => $row->regular_price
			), array(
				'id' => $row->id
			));
			$this->db->update(CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS, array(
				'discount_price' => $row->discount_price,
				'regular_price' => $row->regular_price,
				'discount' => round(100 - ($row->discount_price * 100 / $row->regular_price))
			), array(
				'product_id' => $row->id
			));
			$this->productCache->invalidate($row->id);
		}
	}
	
    /**
     * Get limited number of products by providers and filters
     * 
     * Array(Integer) $filters
     * Integer|false $current
     * Integer $max
     * Array $filters
     * 
     * return stdClass(GetPagingResult(rows => Array(ProvidersProductsModel)))
     */
    public function getProductsByProviders($providerIds, $current, $max, $filters) {
        $params = array(
            'providers' => $providerIds
        );
        $fields = array();
        $joins = array();
        $where = array();
        $having = array();
        
        if(isset($filters['provider']) && !empty($filters['provider'])) {
            $params['provider'] = $filters['provider'];
            $where[] = " AND products.provider_id = :provider";
        }
        if(isset($filters['status']) && !empty($filters['status'])) {
            $fields['total_buy'] = 'SUM(p_sizes.buy) AS total_buy';
            $fields['total_amount'] = 'SUM(p_sizes.amount) AS total_amount';
            $joins['sizes'] = true; 
            if($filters['status'] == 'in_stock') {
                $where[] = ' AND products.out_of_stock = 0 AND p_sizes.out_of_stock = 0';
				$having[] = 'total_amount > total_buy';
            } else {
                $fields['total_buy'] = 'SUM(p_sizes.buy + IF(products.out_of_stock OR p_sizes.out_of_stock, p_sizes.amount,0)) AS total_buy'; // Cheat
				$having[] = 'total_buy >= total_amount';
            }
        }
        if(isset($filters['exclude_total_out']) && !empty($filters['exclude_total_out'])) {
            $where[] = " AND 1 >= products.out_of_stock";
        }
        
		if(isset($filters['remote_id'])) {
			$where[] = 'AND products.remote_provider_uid IN (:remote_id)';
			$params['remote_id'] = (array)$filters['remote_id'];
		}
		
		if (isset($joins['sizes'])) {
			$joins['sizes'] = 'INNER JOIN ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE . ' AS `p_sizes`';
			$where[] = ' AND `p_sizes`.`product_id` = `products`.`id`';
		}
        
        $query = sprintf('SELECT products.* ' . (!empty($fields)?', ':'') . implode(', ', $fields) . '
                FROM %s as products ' .
                implode(" ", $joins) . '
                WHERE products.provider_id IN (:providers) AND product.unactive_time = 0 ' . implode('', $where) . '
                GROUP BY products.id ' .
                (!empty($having)? ' HAVING ':'') . implode('', $having) . '
                ORDER BY products.product_name ASC', 
            self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS);
        
        $result = $this->db->getPagingResult($query, $params, $current, $max);
        $result->rows = $this->modifyData($result->rows, true, 'convert_product_row', null);
        return $result;
    }
    
    /**
     * Build keywords for product
     * 
     * ProvidersProductsModel $product
     * 
     * return String
     */
	public function buildKeywords(ProvidersProductsModel $product) {
		$keywords = array($product->brand->name);
		foreach($product->categories as $row) {
			$keywords[] = $row->name;
		}
		foreach($product->categories as $row) {
			$keywords[] = $row->name;
		}
		foreach($product->sub_categories as $row) {
			$keywords[] = $row->category->name;
		}
		$color = $product->get_color_name();
		if($color) {
			$keywords[] = $color;
		}
		return implode(', ', $keywords);
	}
	
    /**
     * Build description for product
     * 
     * ProvidersProductsModel $product
     * 
     * return String
     */
	public function buildDescription(ProvidersProductsModel $product) {
		$color = $product->get_color_name();
		$categoryId = 0;
		foreach($product->categories as $row) {
			$categoryId = $row->id;
			break;
		}
		$str = $product->product_name;
		if(!empty($color)) {
			$str .= ' цвят '.  mb_convert_case($color, MB_CASE_LOWER);
		}
		$str .= ' марка '.$product->brand->name;
		if($categoryId) {
			$str .= ' подходящ '.mb_convert_case($this->app['translator']->trans('category_'.$categoryId), MB_CASE_LOWER);
		}
		foreach($product->sub_categories as $row) {
			$str .= ' раздел '.mb_convert_case($row->category->name, MB_CASE_LOWER);
			break;
		}
		$props = array();
		foreach($product->properties as $prop) {
			$props[] =  $prop->property->name.' '.$prop->value->value;
		}
		$str .= '. '.implode(', ', $props);
		$str .= '. Номер на продукта '.$product->id;
		return $str;
	}
	
    /**
     * Build json schema
     * 
     * ProvidersProductsModel $product
     * 
     * return Array
     */
	public function buildJsonSchema(ProvidersProductsModel $product) {
        
		$json = array(
			"@context" => "http://schema.org/",
			"@type" => "Product",
			"name" => $product->product_name,
			"description" => !empty($product->description) ? $product->description : '',
			"image" => $product->main_image ? $product->main_image->collection_image : null,
			"brand" => array(
				"@type" => "Thing",
				"name" => $product->brand->name
			),
			"offers" => array(
				"@type" => "Offer",
                "url" => "https://site.com/p/".$product->get_url_product_name().'_'.$product->id,
				"priceCurrency" => "BGN",
				"price" => $product->discount_price,
				"itemCondition" => "http://schema.org/NewCondition",
				"availability" => "http://schema.org/" . ($product->available ? 'InStock' : 'OutOfStock')
			)
		);
		
		$cats = array();
		foreach($product->categories as $row) {
			$cats[] = $row->name;
		}
		foreach($product->sub_categories as $row) {
			$cats[] = $row->category->name;
		}
		$json['category'] = implode(' / ', $cats);
		
		$props = array();
		foreach($product->properties as $prop) {
			$props[] = $prop->property->label.': '.$prop->value->value;
		}
		if(!empty($props)) {
			$json['description'] = implode('<br/>', $props).!empty($json['description']) ? '<br/>'.$json['description'] : '';
		}
		
		$color = $product->get_color_name();
		if(!empty($color)) {
			$json['color'] = $color;
		}
		
		return $json;
	}
	
    /**
     * Build product breadcrumb
     * 
     * ProvidersProductsModel $product
     * 
     * return Array
     */
	public function buildProductBreadcrumb(ProvidersProductsModel $product) {
		$allCategories = array();
		$superParents = array();
		$firstLevel = array();
		foreach($product->sub_categories as $c) {
			$firstLevel[] = $c->category;
		}
		
		$this->app['tags.manager']->mapSubCategoryParents($firstLevel);
		$this->app['tags.manager']->createSubCategoryTree($firstLevel, $allCategories, $superParents);
		
        $breadcrumbUrl = '';
		$breadcrumb = array();
		$breadcrumb_section = array();
		
		$filters = new \App\Model\Collections\CollectionFiltersMultiModel();
		
		foreach($product->categories as $cat) {
			$filters->category = $cat->id;
			$breadcrumb_section[] = array(
				'url' => $filters->toQuery($breadcrumbUrl),
				'text' => $cat->name
			);
			break;
		}
		
		foreach($allCategories as $category) {
			$filters->subcategory = $category->id;
			$category->filter_url = $filters->toQuery($breadcrumbUrl);
            
            $breadcrumb[] = array(
				'url' => $category->filter_url,
				'text' => $category->name
			);
		}
        
        krsort($breadcrumb);
        
        $breadcrumb_full = array_merge($breadcrumb_section, $breadcrumb);

		return $breadcrumb_full;
	}
	
    /**
     * Build json schema path
     * 
     * ProvidersProductsModel $product
     * 
     * return Array
     */
	public function buildJsonSchemaPath(ProvidersProductsModel $product) {
		$path = $this->buildProductBreadcrumb($product);
		$json = array(
			"@context" => "http://schema.org",
			"@type" => "BreadcrumbList",
			"itemListElement" => array()
		);
		$index = 0;
		foreach($path as $p) {
			$index++;
			$json['itemListElement'][] = array(
				"@type" => "ListItem",
				"position" => $index,
				"item" => array(
					"@id" => $p['url'],
					"name" => $p['text']
				)
			);
		}
		return $json;
	}
    
    /**
     * Provider edit quick bulk count
     * 
     * ProductFiltersModel $requestFilters
     * RemoteProviderListModel $searchModel
     * 
     * return Array(ProviderProductModel)
     */
    public function providerEditQuickBulkCount(ProductFiltersModel $requestFilters, RemoteProviderListModel $searchModel) {
        $requestFilters->search = $searchModel->search;
        $requestFilters->remote_provider_list = $searchModel;
        return $this->findProducts($requestFilters, null, true);
    }
    
    /**
     * Edit bulk, used in collections, products...
     * 
     * Array(Integer) $products
     * Array $replace
     * 
     * return nothing
     */
    public function EditQuickBulk($products, $replace) { // ProductFiltersModel $requestFilters, RemoteProviderListModel $searchModel
                
        // Add out_of_stock = '' to allow edit of all products no matter of out_of_stock status
        $requestFilters = new ProductFiltersModel(array('id' => explode(',', $products), 'out_of_stock' => '')); 
        //echo '<pre>'.print_r($requestFilters, true).'</pre>'; exit;
        
        //$this->validateEditQuickBulk($requestFilters, $searchModel, $replace);
        
        //$requestFilters->search = $searchModel->search;
        //$requestFilters->remote_provider_list = $searchModel;
        $requestFilters->only_query = true;
        list($baseQuery, $baseParams) = $this->app['products.manager']->findProducts($requestFilters);
        
        $this->db->beginTransaction();
        
        /**
         * Temp table
         */
        $tmpTableName = 'products_mod_'.md5(microtime(true). rand(1,99999));
//        $tmpTableName = 'products_mod_test';
        
        $query = "CREATE TEMPORARY TABLE IF NOT EXISTS " . $tmpTableName . " (id int NOT NULL, PRIMARY KEY (id))";
        $this->db->query($query);
        
        $query = sprintf('INSERT INTO %s __SELECT__', $tmpTableName);
        $query = str_replace('__SELECT__', $baseQuery, $query);
        $query = str_replace('{fields}', 'products.id as id', $query);
        $this->db->query($query, $baseParams);
        
        /**
         * update product names
         */
        if(isset($replace['product_name']) && !empty($replace['product_name'])) {
            $query = sprintf('UPDATE %s as products INNER JOIN %s as tmp ON products.id = tmp.id SET products.product_name = :product_name ',
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'product_name' => $replace['product_name']
            );
            $this->db->query($query, $params);
        }
        
        /**
         * update description
         */
        if(isset($replace['description']) && !empty($replace['description'])) {
            if(!preg_match('/^<!-- html -->/', $replace['description'])) {
                $replace['description'] = "<!-- html -->" . $replace['description'];
            }
            $query = sprintf('UPDATE %s as products INNER JOIN %s as tmp ON products.id = tmp.id SET products.description = :description ',
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'description' => $replace['description']
            );
            $this->db->query($query, $params);
        }
        
        /**
         * update categories
         */
        if(isset($replace['categories']) && !empty($replace['categories'])) {
            $query = sprintf("DELETE categories
                    FROM %s as categories
                    INNER JOIN %s as temp on categories.product_id = temp.id
                    WHERE 1",
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, $tmpTableName);
            $params = array();
            $this->db->query($query, $params);
            
            foreach ($replace['categories'] as $categoryId) {
                $query = sprintf('INSERT INTO %s SELECT id as product_id, :insert_category as category_id, :insert_created_at as created_at FROM %s', 
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, $tmpTableName);
                $params = array(
                    'insert_category' => $categoryId,
                    'insert_created_at' => time()
                );
                $this->db->query($query, $params);
            }
        }
        
        /**
         * update subcategories
         */
        if(isset($replace['subcategories']) && !empty($replace['subcategories'])) {
            $query = sprintf("DELETE subcategories 
                    FROM %s as subcategories
                    INNER JOIN %s as temp on subcategories.product_id = temp.id
                    WHERE 1",
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $tmpTableName);
            $params = array();
            $this->db->query($query, $params);
            
            foreach ($replace['subcategories'] as $subcategoryId) {
                $query = sprintf('INSERT INTO %s SELECT id as product_id, :insert_subcategory as category_id FROM %s', 
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $tmpTableName);
                $params = array(
                    'insert_subcategory' => $subcategoryId
                );
                $this->db->query($query, $params);
            }
        }
        
        /**
         * update color
         */
        if(isset($replace['color']) && !empty($replace['color'])) {
            $query = sprintf("UPDATE %s as products 
                JOIN %s as temp 
                SET products.color_id = :replace_color 
                WHERE products.id = temp.id",
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'replace_color' => $replace['color']
            );
            $this->db->query($query, $params);
        }
        
        /**
         *  Update properties
         */
        if(isset($replace['properties']) && !empty($replace['properties'])) {
            foreach($replace['properties'] as $propertyId => $value) {
                if(!empty($value)) {
                    $value = $this->app['properties.manager']->addValue($value);
                    $valueId = $value->id;
                    $query = $baseQuery;
                    $query = str_replace('{fields}', 'products.id as product_id, :property as property_id, :value as value_id', $query);
                    $query = sprintf("INSERT INTO %s ", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES) . $query . 
                            ' ON DUPLICATE KEY UPDATE value_id = VALUES(value_id)';
                    $params = $baseParams;
                    $params['property'] = $propertyId;
                    $params['value'] = $valueId;
                    $this->db->query($query, $params);
                    
                    $query = str_replace(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, $query);
                    $this->db->query($query, $params);
                }
                
            }   
        }
    
        /**
         * invalidate
         */
        $query = sprintf("SELECT * FROM %s WHERE 1", $tmpTableName);
        $params = array();
        $data = $this->db->fetchAll($query, $params);
        foreach($data as $row) {
            $this->app['products.manager']->invalidate($row->id);
        }
        
        $this->db->commit();
    }
    
    /**
     * Provider edit quick bulk
     * 
     * ProductFiltersModel $requestFilters
     * RemoteProviderListModel $searchModel
     * Array $replace
     * 
     * return nothing
     */
    public function providerEditQuickBulk(ProductFiltersModel $requestFilters, RemoteProviderListModel $searchModel, $replace) {
        $this->validateEditQuickBulk($requestFilters, $searchModel, $replace);
        
        $requestFilters->search = $searchModel->search;
        $requestFilters->remote_provider_list = $searchModel;
        $requestFilters->only_query = true;
        list($baseQuery, $baseParams) = $this->findProducts($requestFilters);
        
        $this->db->beginTransaction();
        
        /**
         * Temp table
         */
        $tmpTableName = 'products_mod_'.md5(microtime(true). rand(1,99999));
//        $tmpTableName = 'products_mod_test';
        
        $query = "CREATE TEMPORARY TABLE IF NOT EXISTS " . $tmpTableName . " (id int NOT NULL, PRIMARY KEY (id))";
        $this->db->query($query);
        
        $query = sprintf('INSERT INTO %s __SELECT__', $tmpTableName);
        $query = str_replace('__SELECT__', $baseQuery, $query);
        $query = str_replace('{fields}', 'products.id as id', $query);
        $this->db->query($query, $baseParams);
        
        /**
         * update product names
         */
        if(isset($replace['product_name']) && !empty($replace['product_name']) && !empty($requestFilters->provider)) {
            $query = sprintf('UPDATE %s as products INNER JOIN %s as tmp ON products.id = tmp.id SET products.product_name = :product_name 
                    WHERE products.provider_id = :provider',
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'product_name' => $replace['product_name'],
                'provider' => $requestFilters->provider
            );
            $this->db->query($query, $params);
        }
        
        /**
         * update description
         */
        if(isset($replace['description']) && !empty($replace['description']) && !empty($requestFilters->provider)) {
            if(!preg_match('/^<!-- html -->/', $replace['description'])) {
                $replace['description'] = "<!-- html -->" . $replace['description'];
            }
            $query = sprintf('UPDATE %s as products INNER JOIN %s as tmp ON products.id = tmp.id SET products.description = :description 
                    WHERE products.provider_id = :provider',
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'description' => $replace['description'],
                'provider' => $requestFilters->provider
            );
            $this->db->query($query, $params);
        }
        
        /**
         * update categories
         */
        if(isset($replace['categories']) && !empty($replace['categories'])) {
            $query = sprintf("DELETE categories
                    FROM %s as categories
                    INNER JOIN %s as temp on categories.product_id = temp.id
                    WHERE 1",
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, $tmpTableName);
            $params = array();
            $this->db->query($query, $params);
            
            foreach ($replace['categories'] as $categoryId) {
                $query = sprintf('INSERT INTO %s SELECT id as product_id, :insert_category as category_id, :insert_created_at as created_at FROM %s', 
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, $tmpTableName);
                $params = array(
                    'insert_category' => $categoryId,
                    'insert_created_at' => time()
                );
                $this->db->query($query, $params);
            }
        }
        
        /**
         * update subcategories
         */
        if(isset($replace['subcategories']) && !empty($replace['subcategories'])) {
            $query = sprintf("DELETE subcategories 
                    FROM %s as subcategories
                    INNER JOIN %s as temp on subcategories.product_id = temp.id
                    WHERE 1",
                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $tmpTableName);
            $params = array();
            $this->db->query($query, $params);
            
            foreach ($replace['subcategories'] as $subcategoryId) {
                $query = sprintf('INSERT INTO %s SELECT id as product_id, :insert_subcategory as category_id FROM %s', 
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $tmpTableName);
                $params = array(
                    'insert_subcategory' => $subcategoryId
                );
                $this->db->query($query, $params);
            }
        }
        
        /**
         * update color
         */
        if(isset($replace['color']) && !empty($replace['color'])) {
            $query = sprintf("UPDATE %s as products 
                JOIN %s as temp 
                SET products.color_id = :replace_color 
                WHERE products.id = temp.id",
                    self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $tmpTableName);
            $params = array(
                'replace_color' => $replace['color']
            );
            $this->db->query($query, $params);
        }
        
        /**
         *  Update properties
         */
        if(isset($replace['properties']) && !empty($replace['properties'])) {
            foreach($replace['properties'] as $propertyId => $value) {
                if(!empty($value)) {
                    $value = $this->app['properties.manager']->addValue($value);
                    $valueId = $value->id;
                    $query = $baseQuery;
                    $query = str_replace('{fields}', 'products.id as product_id, :property as property_id, :value as value_id', $query);
                    $query = sprintf("INSERT INTO %s ", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES) . $query . 
                            ' ON DUPLICATE KEY UPDATE value_id = VALUES(value_id)';
                    $params = $baseParams;
                    $params['property'] = $propertyId;
                    $params['value'] = $valueId;
                    $this->db->query($query, $params);
                    
                    $query = str_replace(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, $query);
                    $this->db->query($query, $params);
                }
                
            }   
        }
        
        /**
         * check and remove moderate
         */
//        $queryWhereColor = ' AND products.color_id > 0';
//        if(isset($replace['no_color']) && $replace['no_color']) {
//            $queryWhereColor = '';
//        }
//        $query = sprintf("SELECT products.id as id, COUNT(DISTINCT cat.category_id) as num_cat, COUNT(DISTINCT subcat.category_id) as num_subcat  
//            FROM %s as products 
//            JOIN %s as temp on products.id = temp.id 
//            JOIN %s as cat on cat.product_id = products.id AND cat.category_id <> :no_category_value 
//            JOIN %s as subcat ON subcat.product_id = products.id 
//            WHERE 1 " . $queryWhereColor . " 
//            GROUP BY products.id
//            HAVING num_cat > 0 AND num_subcat > 0",
//                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS,
//                $tmpTableName,
//                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES,
//                self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES);
//        $params = array(
//            'no_category_value' => 5
//        );
//        $data = $this->db->fetchAll($query, $params);
//        var_dump($data);exit;
//        foreach($data as $row) {
//            $singleProduct = $this->findProductById($row->id);
//            if($singleProduct->provider->isSelf()) {
//                $this->app['products.mods.manager']->approveProduct($singleProduct->id);
//            }
//        }
//        
        /**
         * invalidate
         */
        $query = sprintf("SELECT * FROM %s WHERE 1", $tmpTableName);
        $params = array();
        $data = $this->db->fetchAll($query, $params);
        foreach($data as $row) {
            $this->productCache->invalidate($row->id);
        }
        
        $this->db->commit();
    }
    
    /**
     * Invalidate product
     * 
     * Integer|Array(Integer) $product_id
     * 
     * return nothing
     */
    public function invalidate($product_id){
        $this->productCache->invalidate($product_id);
    }
    
    /**
     * Validate edit quick bulk
     * 
     * ProductFiltersModel $requestFilters
     * RemoteProviderListModel $searchModel
     * Array $replace
     * 
     * return nothing
     */
    public function validateEditQuickBulk(ProductFiltersModel $requestFilters, RemoteProviderListModel $searchModel, $replace) {
        $error = false;
        $emptyReplace = true;
        if(empty($searchModel->provider_id)) {
            $error = 'Не е избран доставчик.';
        }
        foreach($replace as $value) {
            if(!empty($value)) {
                $emptyReplace = false;
                break;
            }
        }
        if($emptyReplace) {
            $error = 'Не са попълнени данни за заместване.';
        }
        if(empty($error) && empty($requestFilters->id)) {
            if(!$searchModel->have_search_options()) {
                $error = 'Не са зададени условия за търсене';
            }
        }
        
        if(!empty($error)) {
            throw new Error\FormError('empty', $error);
        }
    }
    
    /**
     * Get product url title
     * 
     * Integer $id
     * 
     * return String
     */
    public function getProductUrlTitle($id) {
        $result = '';
        $product = $this->findProductById($id);
        if(!empty($product)) {
            $result = $product->get_url_product_name();
        }
        return $result;
    }
    
    /**
     * Find product by temp name
     * 
     * Integer $provider
     * Array(Integer) $productIds
     * 
     * return Array(stdClass)
     */
    public function findProductByTempName($provider, $productIds) {
        return $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE provider_id=? AND product_temp_name in (?)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), array($provider, $productIds));
    }
    
    /**
     * Modify products with mappings
     * 
     * Array(stdClass) $products
     * 
     * return nothing
     */
	public function shortProductsModifier($products) {
		$this->mapData($products, 'id', array($this, 'findProductImages'), 'images', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductCategories'), 'categories', 'product_id', true);
		$this->mapData($products, 'id', array($this, 'findProductSubCategories'), 'sub_categories', 'product_id', true);
		foreach($products as $product) {
			$this->findProductBestImage($product);
			foreach ($product->images as $image) {
				$image->collection_format = 'box';
				$image->collection_image = $image->formats[$image->collection_format]['path'];
			}
		}
	}
    
    /**
     * get all products from collections
     * 
     * array(Integer) $collectionIds
     * Integer|false $limit
     * 
     * return array(stdClass)
     */
    public function getCollectionsProducts($collectionIds, $limit = false) {
        $query = sprintf("SELECT products.id as id, MIN(cp.sort_index) as sort_index FROM %s as cp INNER JOIN %s as products ON cp.product_id = products.id WHERE cp.collection_id IN (:collections) GROUP BY cp.product_id ORDER BY cp.sort_index ASC", self::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS, self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS);
        if($limit) {
            $query .= ' LIMIT 0,' . $limit;
        }
        $params = array('collections' => $collectionIds);
        $rows = $this->db->fetchAll($query, $params);
        return $rows;
    }
    
    /**
     * set in cache filename
     * 
     * String $text
     * 
     * return nothing
     */
    public function setFilenameExportJson($filename) {
        $data = array(
            'created_at' => time(),
            'filename' => $filename
        );
        $cacheName = \App\Manager\ManagerCache::KEY_PRODUCTS_EXPORT_JSON;
        $this->cache->set($cacheName, $data, 86400);
    }
    
    /**
     * get filename from cache
     * 
     * return false or String
     */
    public function getFilenameExportJson() {
        $result = false;
        $cacheName = \App\Manager\ManagerCache::KEY_PRODUCTS_EXPORT_JSON;
        $data = $this->cache->get($cacheName);
        if(!empty($data)) {
            $result = $data['filename'];
        }
        return $result;
    }
    
    /**
     * Get business status options
     * 
     * return Array(Array(id, name))
     */
    public function getBusinessStatusOptions() {
        $result = array(
            array('id' => 0, 'name' => 'Статус на количеството'),
            array('id' => 'in_stock', 'name' => 'Налични продукти'),
            array('id' => 'out_of_stock', 'name' => 'Изчерпани продукти')
        );
        return $result;
    }
    
    /**
     * Update modify time for product and invalidate cache
     * 
     * Integer $productId
     * Integer $modifyTime
     * 
     * return nothing
     */
    public function updateProductModify($productId, $modifyTime) {
        $updateValues = array('modified_time' => $modifyTime);
        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $updateValues, array('id' => $productId), 1);
        $this->productCache->invalidate($productId);
    }
   
    /**
     * Get products data for export
     * 
     * Integer $providerId
     * 
     * return Array(stdClass)
     */
    public function getProductsForExport($providerId) {
        $params = array(
             'status' => \App\Model\CollectionsModel::STATUS_PUBLISH
        );
        if(!empty($providerId)) {
            $provider_clause = 'AND products.provider_id = :provider_id';
            $params['provider_id'] = $providerId;
        } else {
            $provider_clause = '';
        }


        $query = '
            SELECT 
                SQL_CALC_FOUND_ROWS 
                products.*,
                MAX(collections.is_outfit) AS outfit,
                collections.id AS collection_id,
                collections.title AS collection_title
            FROM ' . self::ABSTRACT_TABLE_COLLECTIONS . ' as collections
            INNER JOIN ' . self::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS . ' AS cp ON cp.collection_id = collections.id 
            INNER JOIN ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS . ' AS products ON products.id = cp.product_id 
            INNER JOIN ' . self::ABSTRACT_TABLE_BRANDS . ' ON brands.id = products.brand_id 
            WHERE 
                (
                    collections.is_outlet = 1 OR 
                    (collections.is_outfit AND UNIX_TIMESTAMP() <= collections.finish_at) OR 
                    (collections.status = :status AND UNIX_TIMESTAMP() BETWEEN collections.start_at AND collections.finish_at) 
                )
                AND cp.status = "active" '.$provider_clause.'
            GROUP BY products.id';
            
        return $this->db->fetchAll($query, $params);
    }
            
    /**
     * make all products popular_weight to 0 and invalidate those products
     * 
     * return nothing
     */
    public function resetPopularWeigth() {
        $query = sprintf("SELECT * FROM %s WHERE popular_weight > 0", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS);
        $rows = $this->db->fetchAll($query);
        
        foreach($rows as $row) {
            $updateValues = array(
                'popular_weight' => 0
            );
            $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $updateValues, array('id' => $row->id), 1);
            $this->invalidate($row->id);
        }
    }
            
    /**
     * update popular weight for product
     * 
     * Integer $productId
     * Integer $weigth
     * 
     * return nothing
     */
    public function updatePopularWeigth($productId, $weigth) {
        $updateValues = array(
            'popular_weight' => $weigth
        );

        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $updateValues, array('id' => $productId), 1);
        $this->invalidate($productId);
    }
    
	/**
     *----------------------------------------------------------------------------
     * END PRODUCTS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT CATEGORIES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Save product categories
     * 
     * ProvidersProductsModel $product
     * Array $categories
     * 
     * return nothing
     */
    public function saveProductCategories(ProvidersProductsModel $product, $categories) {
        $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, array(
            'product_id' => $product->id
        ));
        foreach ($categories as $category) {
            $row = array(
                'product_id' => $product->id,
                'category_id' => $category->id,
                'created_at' => time()
            );
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, $row);
        }
        
    }
    
    /**
     * Find product category(es) by product(s)
     * 
     * Integer|Array(Integer) $product
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductCategories($productId) {
		$all = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE `product_id` IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES), 'product', array(
			'product' => (array)$productId
		), null, null, $this->productCache->child('categories', 'product_id', true));
		$this->app['tags.manager']->mapCategories($all, false);
		foreach ($all as $key => $row) {
			$category = clone $row->category;
			$category->product_id = $row->product_id;
			$all[$key] = $category;
		}
		return $all;
	}

    /**
     * Update products categories
     * 
     * Array(Integer) $productIds
     * Array(Integer) $categories
     * 
     * return nothing
     */
	public function updateProductsCategories(array $productIds, array $categories) {
		$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES, array(
			'product_id' => $productIds
		));

		$insert = array();
		$params = array();
		foreach ($productIds as $id) {
			foreach ($categories as $cid) {
				$insert[] = '(?, ?, UNIX_TIMESTAMP())';
				$params[] = $id;
				$params[] = $cid;
			}
		}

		$insert = 'INSERT INTO ' . self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_CATEGORIES . ' (`product_id`, `category_id`, `created_at`) VALUES ' . implode(', ', $insert);
		$this->db->query($insert, $params);
		
		$this->productCache->invalidate($productIds);
	}

    /**
     *----------------------------------------------------------------------------
     * END PRODUCT CATEGORIES
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT SUBCATEGORIES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Save product subcategories
     * 
     * ProvidersProductsModel $product
     * Array(stdClass) $subcategories
     * 
     * return nothing
     */
    public function saveProductSubCategories(ProvidersProductsModel $product, $subcategories) {
        $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, array(
            'product_id' => $product->id
        ));
        foreach($subcategories as $cat) {
            $row = array(
                'product_id' => $product->id,
                'category_id' => $cat->category_id
            );
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $row);
        }
    }
    
    /**
     * Find product subcategory(es) by product(s)
     * 
     * Integer|Array(Integer) $product
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductSubCategories($value) {
		$result = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES), 'product', array(
			'product' => (array)$value
		), null, null, $this->productCache->child('subcategories', 'product_id', true));
		$this->app['tags.manager']->mapSubCategory($result, 'category_id');
		return $result;
	}
	
    /**
     * change product subcategories
     * 
     * Array(Integer) $oldCategories
     * Integer $newCategory
     * 
     * return nothing
     */
    public function changeSubCategories(array $oldCategories, $newCategory) {
		/**
		 * Find which products has already new category
		 */
		$result = $this->db->fetchAll(vsprintf('
		SELECT main.product_id, main.category_id  
		FROM %s AS main 
		JOIN ( SELECT product_id FROM %s WHERE category_id = :new ) AS c ON c.product_id = main.product_id 
		WHERE main.category_id IN (:old) 
		', array(
			self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES,
			self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES
		)), array(
			'new' => $newCategory,
			'old' => $oldCategories
		), 'product_id');
		
		foreach($result as $row) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, array(
				'product_id' => $row->product_id,
				'category_id' => $row->category_id
			));
		}
		
		$params = array('old' => $oldCategories, 'new' => $newCategory);
		if(!empty($result)) {
			$params['updated'] = array_keys($result);
			$query = sprintf('UPDATE %s SET category_id = :new WHERE category_id IN(:old) AND product_id NOT IN (:updated)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES);
		} else {
			$query = sprintf('UPDATE %s SET category_id = :new WHERE category_id IN(:old)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES);
		}
		$this->db->query($query, $params);
    }
	
    /**
     * Count products for subcategory
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function countSubCategoryProducts($value) {
		if(is_array($value)) {
			$result = $this->db->fetchAll(sprintf('SELECT COUNT(*) AS total, category_id FROM %s WHERE category_id IN (?) GROUP BY category_id', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES), array($value));
		} else {
			$result = $this->db->fetch(sprintf('SELECT COUNT(*) AS total, category_id FROM %s WHERE category_id = ?', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES), array($value));
		}
		return $result;
	}
	
    /**
     * get ids of products by subcategory
     * 
     * Integer $subcategoryId
     * 
     * return array(Integer)
     */
    public function getSubcategoryProducts($subcategoryId) {
        $query = sprintf('SELECT * FROM %s WHERE category_id = :subcategory', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES);
        $params = array('subcategory' => $subcategoryId);
        $rows = $this->db->fetchAll($query, $params);
        $result = array();
        foreach($rows as $row) {
            $result[] = $row->product_id;
        }
        return $result;
    }
    
    /**
     * for given products change the subcategory
     * 
     * array(Ingeger) $productIds
     * Integer $newSubcategory
     * 
     * return nothing or \Exception
     */
    public function changeProductsSubcategory($productIds, $newSubcategory, $user = null) {
        if(empty($newSubcategory)) {
            throw new \Exception('Не е избрана категория.');
        }
        
        foreach($productIds as $productId) {
            $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, array('product_id' => $productId));
            $insertArray = array(
                'product_id' => $productId,
                'category_id' => $newSubcategory
            );
            $onDuplicate = 'category_id = VALUES(category_id)';
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES, $insertArray, $onDuplicate);
            $this->productCache->invalidate($productId);
            
            if($user)
            {
                $log = new ProductLog();
                $log->editSubCategoryBulk($productId);
                $this->app['user.log']->log($user, $log);
            }
        }
    }
    
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT SUBCATEGORIES
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT TAGS
     *----------------------------------------------------------------------------
     */
    
    /**
     * Save product tags
     * 
     * ProvidersProductsModel $product
     * Array $tags
     * 
     * return nothing
     */
	public function saveProductTags(ProvidersProductsModel $product, array $tags) {
		$tags = $this->app['tags.manager']->createTags($tags);
		$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS, array(
			'product_id' => $product->id
		));
		foreach ($tags as $tag) {
			$row = array(
				'product_id' => $product->id,
				'tag_id' => $tag->id,
				'created_at' => time()
			);
			$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS, $row);
		}
	}
    
    /**
     * Find product tag(s) by product(s)
     * 
     * Integer|Array(Integer) $product
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductTags($productId) {
		$all = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE `product_id` IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS), 'product', array(
			'product' => (array)$productId
		), null, null, $this->productCache->child('tags', 'product_id', true));
		$this->app['tags.manager']->mapTags($all, false);
		foreach ($all as $key => $row) {
			$tag = clone $row->tag;
			$tag->product_id = $row->product_id;
			$all[$key] = $tag;
		}
		return $all;
	}

    /**
     * Count products for tag
     * 
     * Integer $tagId
     * false|Integer $collectionId
     * 
     * return Integer
     */
	public function countProductsForTag($tagId, $collectionId=false) {
		$params = array($tagId);
		if($collectionId) {
			$params[] = $collectionId;
			$query = sprintf('
			SELECT COUNT(DISTINCT ptags.product_id) AS count 
			FROM %s AS ptags 
			INNER JOIN %s AS cp ON cp.product_id = ptags.product_id 
			WHERE ptags.tag_id = ? AND cp.collection_id = ?
			', ProductsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS, CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS);
		} else {
			$query = sprintf('SELECT COUNT(DISTINCT product_id) AS count FROM %s WHERE tag_id = ?', ProductsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS);
		}
		$row = $this->db->fetch($query, $params);
		if ($row) {
			return $row->count;
		}
		return 0;
	}

    /**
     * Swap product tag
     * 
     * Integer $tagId
     * Integer $newTagId
     * false|Integer $collectionId
     * 
     * return nothing
     */
	public function swapProductTag($tagId, $newTagId, $collectionId=false) {
		$params = array(
			'new' => $newTagId,
			'old' => $tagId
		);
		if($collectionId) {
			$params['collection'] = $collectionId;
			$query = sprintf('
			UPDATE %s AS ptags, %s AS cp 
			SET ptags.tag_id=:new 
			WHERE cp.product_id = ptags.product_id  
			AND ptags.tag_id=:old 
			AND cp.collection_id = :collection 
			', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS, CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS);
		} else {
			$query = sprintf('UPDATE %s SET tag_id=:new WHERE tag_id=:old', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_TAGS);
		}
		$this->db->query($query, $params);
	}

    /**
     *----------------------------------------------------------------------------
     * END PRODUCT TAGS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT PROPERTIES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Save product properties
     * 
     * ProvidersProductsModel $product
     * Array(stdClass) $properties
     * 
     * return nothing
     */
    public function saveProductProperties(ProvidersProductsModel $product, $properties) {
        $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, array('product_id' => $product->id));
        $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, array('product_id' => $product->id));
        foreach($properties as $prop) {
            if(isset($prop->property_name)) {
                $propertyModel = $this->app['properties.manager']->createPropertyIfNotExist($prop->property_name);
                $prop->property_id = $propertyModel->id;
            }
            if(isset($prop->value)) {
                $value = $this->app['properties.manager']->addValue($prop->value);
                $prop->value_id = $value->id;
            }
            $row = array(
                'product_id' => $product->id,
                'property_id' => $prop->property_id,
                'value_id' => $prop->value_id
            );
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, $row);
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, $row);
        }
    }

    /**
     * Find product property(es) by product(s)
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductProperties($value) {
		$result = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES), 'product', array(
			'product' => (array)$value
		), null, null, $this->productCache->child('properties', 'product_id', true));
		$this->app['properties.manager']->mapProperties($result, 'property_id', 'value_id');
		return $result;
	}
	
    /**
     * Clear product properties
     * 
     * return nothing
     */
    public function clearProductsPropertiesTempTable() {
        $this->db->truncateTable(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP);
    }
    
    /**
     * Add property to product (if exists do not add and do not invalidate)
     * 
     * Integer $productId
     * Integer $propertyId
     * Integer $valueId
     * 
     * return nothing
     */
    public function addProductProperty($productId, $propertyId, $valueId) {
        try {
            $row = array(
                'product_id' => $productId,
                'property_id' => $propertyId,
                'value_id' => $valueId
            );
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, $row);
            $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, $row);
            $this->productCache->invalidate($productId);
        } catch(\Exception $e) {
            // do nothing
        }
        
    }
    
    /**
     * Remove property to product by product and property (value do not matter)
     * 
     * Integer $productId
     * Integer $propertyId
     * 
     * return nothing
     */
    public function removeProductProperty($productId, $propertyId) {
        try {
            $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES, array('product_id' => $productId, 'property_id' => $propertyId));
            $this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PROPERTIES_TEMP, array('product_id' => $productId, 'property_id' => $propertyId));
            $this->productCache->invalidate($productId);
        } catch(\Exception $e) {
            // do nothing
        }
        
    }
    
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT PROPERTIES
     *----------------------------------------------------------------------------
     */
	
    /**
     *----------------------------------------------------------------------------
     * PRODUCT IMAGES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Add product images
     * 
     * ProvidersProductsModel $product
     * Boolean $cleanUp
     * 
     * return Array(stdClass)
     */
	public function addProductImages(ProvidersProductsModel $product, $cleanUp) {
		if($cleanUp) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES, array(
				'product_id' => $product->id
			));
		}
		
		$newImages = array();
		foreach ($product->images as $image) {
		
            list($imagePath, $previewPath) = $this->getProductImagePaths($product, $image->filename);
			list($imageMainPath) = $this->getProductImagePaths(false, $image->filename);

            if (empty($image->id)) {
				if ($this->isProductImageTemp($product, $image)) {
					/**
					 * 1. Remove temp image prefix
					 * 2. Move file from storage/products/ to storage/products/product_{id}/
					 */
					$image->filename = str_replace(self::PRODUCT_TMP_FILE_PREFIX, '', $image->filename);
					list($imagePath, $previewPath) = $this->getProductImagePaths($product, $image->filename);

					$imageFile = new File\File($imageMainPath);
					$file = $imageFile->move(dirname($imagePath), $image->filename);
					chmod($file->getPathname(), 0777);
                    try {
                        chmod($file->getPath(), 0777);
                    } catch (\Exception $e) {
                        // do nothing
                    }
				} else {
					$file = new File\File($imagePath);
				}
				$size = @getimagesize($file->getRealPath());
				$imageRow = array(
					'product_id' => $product->id,
					'filename' => $image->filename,
					'width' => $size[0],
					'height' => $size[1],
					'position' => $image->position
				);
			} else {
				$imageRow = array(
					'product_id' => $image->product_id,
					'filename' => $image->filename,
					'width' => $image->width,
					'height' => $image->height,
					'position' => $image->position
				);
			}

			/**
			 * Create responsive image sizes
			 * @see \App\Service\ImageService for all formats
			 */
			$images = $this->app['image.service']->createResponsiveImages($imagePath, true, $this->imageFormats);

			/**
			 * Crop image if we provide external image coordinates
			 * Also we create new thumbnail base on this coords
			 */
            if (isset($image->coords) && !empty($image->coords)) {
				$coords = json_decode($image->coords, true);
				if (isset($coords['x'], $coords['y'], $coords['x2'], $coords['y2'], $coords['w'], $coords['h'])) {
                    $formatKey = 'box';
					foreach (array('x', 'y', 'w', 'h') as $key) {
						$coords[$key] = max($coords[$key], 0);
					}
					$box = new ImagineBox($this->imageFormats[$formatKey]['size'][0], $this->imageFormats[$formatKey]['size'][1]);
					$croppedImage = $this->app['imagine']->open($imagePath)
							->crop(new ImaginePoint($coords['x'], $coords['y']), new ImagineBox($coords['w'], $coords['h']))
							->resize($box)
							->save($images[$formatKey]['path'], array('format' => 'jpg'));
				}
			}
            
			$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES, $imageRow);

			$newImages[] = $imageRow;
		}
        
		return $newImages;
	}
	
    /**
     * Upload product image
     * 
     * ProvidersProductsModel $product
     * \Symfony\Component\HttpFoundation\File\File $file
     * 
     * return nothing | \Exception
     */
	public function uploadProductImage(ProvidersProductsModel $product, File\File $file) {
		if($file instanceof File\UploadedFile) {
			$originalFilename = $file->getClientOriginalName();
		} else {
			$originalFilename = $file->getFilename();
		}
		
		//$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
		$extension = self::DEFAULT_IMAGE_EXTENSION;
		$filename = 'product_' . substr(md5(microtime(true)), 0, 10) . '.' . $extension;
		if (empty($product->id)) {
			$filename = self::PRODUCT_TMP_FILE_PREFIX . $filename;
		}

		list($path, $previewUrl) = $this->getProductImagePaths($product, $filename);
		$targetFile = $file->move(dirname($path), $filename);
		chmod($targetFile->getPathname(), 0777);
		
		$product->image = $targetFile;
		try {
			$this->validateProduct($product, array('image'));
			$product->image = $targetFile->getFilename();
			$product->image_preview = $previewUrl;
			$product->image_path = $path;
			//$this->app['image.service']->resize($targetFile, self::MAX_IMAGE_SIZE);
			//-interlace line
			$png = '';
			if(preg_match('/\.png/ius', $originalFilename)) {
				$png = '-background white -flatten';
			}
			$this->app['image.service']->convert('-resize ' . self::MAX_IMAGE_SIZE . 'x\> '.$png.' -auto-orient -strip -quality 90', $targetFile);
		
			if($product->id) {
				$this->app['image.service']->createResponsiveImages($product->image_path, true, $this->imageFormats);
			}
			
		} catch (Error\FormError $e) {
			$this->deleteProductImage($product, $filename);
			throw new Error\FormError($e->getField(), $e->getMessage());
		}
	}

	/**
	 * Delete product image
     * 
	 * \App\Model\ProvidersProductsModel $product
	 * String $image
     * 
     * return Boolean
	 */
	public function deleteProductImage(ProvidersProductsModel $product, $image) {
		try {
			list($imageStoragePath) = $this->getProductImagePaths($product, $image);
			$this->app['image.service']->deleteResponsiveImages($imageStoragePath, true, $this->imageFormats);
			if ($product->id) {
				$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES, array(
					'product_id' => $product->id,
					'filename' => $image
						), 1);
			}
			return true;
		} catch (Exception $e) {
			
		}
		return false;
	}

    /**
     * Find product best image
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing
     */
	public function findProductBestImage(ProvidersProductsModel $product) {
		$list = array();
		foreach ($product->images as $index => $image) {
			if (isset($this->image_positions[$image->position])) {
				$kk = 'is_' . $this->image_positions[$image->position];
				$image->{$kk} = true;
				$list[$this->image_positions[$image->position]] = $index;
			}
			foreach ($this->image_positions as $k) {
				$k = 'is_' . $k;
				if (!isset($image->{$k})) {
					$image->{$k} = false;
				}
			}
		}
		
		/**
		 * For older products without main image
		 */
		if(!isset($list['main_image']) && isset($list['hover_image'])) {
			$images = $product->images;

			$list['main_image'] = $list['hover_image'];
			$images[$list['hover_image']]->is_hover_image = false;
			unset($list['hover_image']);

			$images[$list['main_image']]->is_main_image = true;
			$images[$list['main_image']]->position = 1;
		}
		if (empty($list)) {
			$keys = array_keys($product->images);
			$list['main_image'] = reset($keys);
		}
		foreach ($list as $key => $index) {
			if (isset($product->images[$index])) {
				$product->set($key, $product->images[$index]);
			}
		}
	}

    /**
     * Find product image(s) by product(s)
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductImages($productId) {
		$all = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE `product_id` IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES), 'product', array(
			'product' => (array)$productId
		), null, null, $this->productCache->child('images', 'product_id', true));
		foreach ($all as $image) {
			list($imageStoragePath, $imagePreviewPath) = $this->getProductImagePaths($image->product_id, $image->filename);
			$image->upload = true;
			$image->formats = $this->app['image.service']->getResponsiveImages($imagePreviewPath, $this->imageFormats);
			$image->formats['original'] = array(
				'path' => $imagePreviewPath
			);
			$image->preview = $image->formats['box']['path'];
			if ($image->width < 500) {
				$image->zoom = false;
			} else {
				$image->zoom = true;
			}
			if($image->position == 1) {
				$image->sort_index = 2;
			} else if($image->position == 2) {
				$image->sort_index = 1;
			} else {
				$image->sort_index = 0;
			}
		}
		uasort($all, function($a, $b){
			return $a->sort_index > $b->sort_index ? -1 : 1;
		});
		return $all;
	}

    /**
     * Is product image templorary
     * 
     * Any $product
     * String|stdClass $image
     * 
     * return Boolean
     */
	public function isProductImageTemp($product, $image) {
		if (is_object($image)) {
			$image = $image->filename;
		}
		return strstr($image, self::PRODUCT_TMP_FILE_PREFIX);
	}

    /**
     * Get product image paths
     * 
     * ProvidersProductsModel $product
     * String $filename
     * 
     * return Array(String, String)
     */
	public function getProductImagePaths($product, $filename) {
		if ($product) {
			if ($product instanceof ProvidersProductsModel) {
				$productId = $product->id;
			} else if (is_numeric($product)) {
				$productId = $product;
			}
		} else {
			$productId = false;
		}
		if ($productId) {
			if(substr($productId, -1) >= 5) {
				$dirKey = 'products2';
			} else {
				$dirKey = 'products';
			}
			$storagePath = $this->app['paths'][$dirKey].self::PRODUCT_DIR_PREFIX . $productId . '/';
			$urlPath = $this->app['url'][$dirKey].self::PRODUCT_DIR_PREFIX . $productId . '/';
		} else {
			$storagePath = $this->app['paths']['products'].self::PRODUCTS_TEMP_DIR . '/';
			$urlPath = $this->app['url']['products'].self::PRODUCTS_TEMP_DIR . '/';
		}
		$storagePath .= $filename;
		$urlPath .= $filename;
		return array($storagePath, $urlPath);
	}

    /**
     * Crop product image
     * 
     * ProvidersProductsModel $product
     * stdClass $image
     * Array $coords
     * 
     * return nothing
     */
	public function cropProductImage(ProvidersProductsModel $product, $image, array $coords) {
        $formatKey = 'box';

		foreach (array('x', 'y', 'w', 'h') as $key) {
			$coords[$key] = max($coords[$key], 0);
		}

		list($imagePath) = $this->getProductImagePaths($product, $image->filename);
		list($savePath, $previewPath) = $this->getProductImagePaths($product, $image->formats[$formatKey]['filename']);
		list($thumbPath) = $this->getProductImagePaths($product, $image->filename);

		$box = new ImagineBox($this->imageFormats[$formatKey]['size'][0], $this->imageFormats[$formatKey]['size'][1]);

		$croppedImage = $this->app['imagine']->open($imagePath)
				->crop(new ImaginePoint($coords['x'], $coords['y']), new ImagineBox($coords['w'], $coords['h']))
				->resize($box)
				->save($savePath, array('format' => 'jpg'))
		;

		$image->collection_image = $previewPath;
	}

    /**
     * Update images order
     * 
     * Integer $product_id
     * Array(Integer => Integer) $images
     * 
     * return nothing
     */
	public function updateImagesOrder($product_id, $images) {
		foreach ($images as $position => $image_id) {

			$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_IMAGES, array(
				'position' => $position
					), array(
				'product_id' => $product_id,
				'id' => $image_id
			));
		}
		
		$this->productCache->invalidate($product_id);
	}

    /**
     *----------------------------------------------------------------------------
     * END PRODUCT IMAGES
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT SIZES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Add product size table
     * 
     * Integer $product_id
     * Integer $table_id
     * 
     * return nothing
     */
	public function add_product_table($product_id, $table_id) {
		$row = array(
			'product_id' => $product_id,
			'table_id' => $table_id
		);
		$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, $row);
		$this->productCache->invalidate($product_id);
	}

    /**
     * Remove size tables for product
     * 
     * Integer $product_id
     * 
     * return nothing
     */
	public function clean_product_size_tables($product_id) {
		$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, array(
			'product_id' => $product_id
		));
		$this->productCache->invalidate($product_id);
	}

    /**
     * Update product size amounts
     * 
     * ProvidersProductsModel $product
     * Boolean $updateStatuses
     * Boolean $resetBuyProducts
     * 
     * return nothing
     */
	public function updateProductSizeAmounts(ProvidersProductsModel $product, $updateStatuses = false, $resetBuyProducts = true) {
        $invalidateCache = false;
        if($product->isWarehouseQuantities) {
            foreach($product->sizes as $size) {
                $size->po_amount = $size->amount;
                $size->amount = 0;
                $all = $this->findProductSizesSimple($product->id);
                $availiable = array();
                foreach($all as $enteredSize) {
                    $availiable[] = $enteredSize->size_id;
                }
                foreach($product->sizes as $size) {
                    if(!in_array($size->id, $availiable)) {
                        $row = array(
                            'product_id' => $product->id,
                            'size_id' => $size->id,
                            'amount' => 0
                        );
                        if(isset($size->barcode) && !empty($size->barcode)) {
                            $row['barcode'] = $size->barcode;
                        }
                        $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $row);
                        $invalidateCache = true;
                    }
                }
            }
        } else {
            $all = $this->findProductSizes($product->id);
            $available = array();
            foreach ($product->sizes as $size) {
                $available[$size->id] = true;
                $updateSize = array(
                    'amount' => $size->amount,
                    'out_of_stock' => 0//isset($size->out_of_stock) && $size->out_of_stock ? 1 : 0
                );
                if(isset($size->barcode) && !empty($size->barcode)) {
                    $updateSize['barcode'] = $size->barcode;
                }
                if($updateStatuses) {
//                    $updateSize['status'] = isset($size->out_of_stock) && $size->out_of_stock ? 0 : 1;
                }
                if($product->provider->is_wholesaler && $resetBuyProducts) {
                    $updateSize['buy'] = 0;
                }
                $affectedRows = $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $updateSize, array(
                    'product_id' => $product->id,
                    'size_id' => $size->id
                ), 1);
                if($affectedRows > 0) {
                    $invalidateCache = true;
                }
            }
            if (!empty($all)) {
                $outOfStock = array();
                foreach ($all as $size) {
                    if (!isset($available[$size->size_id])) {
                        $outOfStock[] = $size->size_id;
                    }
                }
                if (!empty($outOfStock)) {
                    $updateFields = array(
                        'out_of_stock' => 1
                    );
                    if($updateStatuses) {
//                        $updateFields['status'] = 0;
                    }
                    $affectedRows = $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $updateFields, array(
                        'product_id' => $product->id,
                        'size_id' => $outOfStock
                    ));
                    if($affectedRows > 0) {
                        $invalidateCache = true;
                    }
                }
            }

            $affectedRows = $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, array(
                'out_of_stock' => 0
            ), array(
                'id' => $product->id,
                'out_of_stock' => 1
            ));
            if($affectedRows > 0) {
                $invalidateCache = true;
            }
        }
        if($invalidateCache) {
            $this->productCache->invalidate($product->id);
        }
	}

    /**
     * Convert product size 
     * 
     * String $value
     * 
     * return ProvidersProductsModel|null
     */
	public function convert_product_size($value) {
		$exploded = explode('_', $value);
		if (count($exploded) == 2) {
			list($productId, $sizeId) = $exploded;
			$product = $this->convert_product($productId);
			if ($product) {
				$size = $product->find_size($sizeId);
				if ($size) {
					$product->variant = $size;
					return $product;
				}
			}
		}
		return null;
	}
    
    /**
     * Find product size(s) by product(s)
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductSizes($productId) {
		$sizes = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), 'product', array(
			'product' => (array)$productId
		), null, null);
        
		$app = $this->app;
		$this->db->map($sizes, 'size_id', function($provider, $sizeIds)use($app) {
			$sizes = $app['size.manager']->getSizeChartValuesById($sizeIds);
			return array('size', 'id', $sizes, false);
		});
		$sizeManager = $this->app['size.manager'];
		uasort($sizes, function($a, $b)use($sizeManager) {
			return $sizeManager->compareSizeValues($a->size, $b->size);
		});
		foreach ($sizes as $size) {
			$size->size = clone $size->size;
			if ($size->out_of_stock || $size->amount == 0) {
				$size->available = false;
				$size->remaining_quantity = 0;
				$size->max_buy_quantity = 0;
			} else {
				$size->remaining_quantity = max($size->amount - $size->buy, 0);
				$size->available = $size->amount - $size->buy > 0 ? true : false;
				$size->max_buy_quantity = min(self::MAX_SIZE_BUY_QUANTITY, $size->remaining_quantity);
			}
			if (empty($size->size->main)) {
				$size->size->main = $size->size->euro;
			} elseif(empty($size->size->euro)) {
				$size->size->euro = $size->size->main;
			}
			$size->delivered = false;
		}
		return $sizes;
	}
    
    /**
     *  return sizes of product (no cache) - 
     * 
     *  integer $productId
     * 
     *  return array(sql row object);
     */
    public function findProductSizesSimple($productId) {
        $query = sprintf('SELECT * FROM %s WHERE product_id = :product', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array(
            'product' => $productId
        );
		return $this->db->fetchAll($query, $params);
	}

    /**
     * Find product size table(s) by product(s)
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductSizeTables($productId) {
		$tables = $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES), 'product', array(
			'product' => (array)$productId
		), null, null, $this->productCache->child('tables', 'product_id', true));
		$this->app['size.manager']->mapChartTable($tables);
		return $tables;
	}

    /**
     * Find product sizes by size ids
     * 
     * Array(Integer) $sizeIds
     * 
     * return Array(stdClass)
     */
	public function findProductsSizes($sizeIds) {
		$rows = $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE size_id IN (?)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array($sizeIds));
		return $rows;
	}

    /**
     * Count sizes in products
     * 
     * Array(Integer) $sizeIds
     * 
     * return Array(stdClass)
     */
	public function countSizesInProducts($sizeIds) {
		$rows = $this->db->fetchAll(sprintf('SELECT COUNT(*) AS `count`, size_id FROM %s WHERE size_id IN(?) GROUP BY size_id', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array($sizeIds));
		return $rows;
	}

    /**
     * Map stdClasses to product sizes in use
     * 
     * Array(stdClass) $data
     * 
     * return nothing
     */
	public function mapProductSizesInUse($data) {
		$this->mapDatabaseData($data, false, 'id', array($this, 'countSizesInProducts'), 'products', 'size_id');
		foreach ($data as $row) {
			if ($row->products) {
				$row->products = $row->products->count;
			} else {
				$row->products = 0;
			}
		}
	}
    
    /**
     * Change size table in collection
     * 
     * Integer $collectionId
     * Integer $chartId
     * Integer $tableId
     * 
     * return nothing
     */
	public function changeSizeTableInCollection($collectionId, $chartId, $tableId) {
		if($chartId) {
			$query = vsprintf('
			SELECT DISTINCT cp.product_id FROM %s AS cp 
			INNER JOIN %s AS psize ON psize.product_id = cp.product_id 
			INNER JOIN %s AS chart_values ON chart_values.id = psize.size_id 
			INNER JOIN %s AS charts ON charts.id = chart_values.size_chart_id 
			WHERE cp.collection_id = :collection AND charts.id = :chart 
			', array(
				CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS,
				self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE,
				SizeManager::ABSTRACT_TABLE_SIZE_CHARTS_VALUES,
				SizeManager::ABSTRACT_TABLE_SIZE_CHARTS
			));
			$params = array(
				'collection' => $collectionId,
				'chart' => $chartId
			);
		} else {
			$query = vsprintf('
			SELECT DISTINCT cp.product_id FROM %s AS cp 
			WHERE cp.collection_id = :collection 
			', array(
				CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS,
			));
			$params = array(
				'collection' => $collectionId,
			);
		}
		
		$products = $this->db->fetchAll($query, $params);
		
		foreach($products as $row) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, array(
				'product_id' => $row->product_id
			));
			if($tableId) {
				$table = array(
					'product_id' => $row->product_id,
					'table_id' => $tableId
				);
				$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, $table);
			}
			$this->productCache->invalidate($row->product_id);
		}
	}
	
    /**
     * Change size table in collection subcategory
     * 
     * Integer $collectionId
     * Integer $subcategoryId
     * Integer $tableId
     * 
     * return nothing
     */
	public function changeSizeTableInCollectionSubcategory($collectionId, $subcategoryId, $tableId) {
		$query = vsprintf('
		SELECT DISTINCT cp.product_id FROM %s AS cp 
		INNER JOIN %s AS t ON t.product_id = cp.product_id 
		WHERE cp.collection_id = :collection  AND t.category_id = :subcategory
		', array(
			CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS,
			self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_SUB_CATEGORIES
		));
		
		$products = $this->db->fetchAll($query, array(
			'collection' => $collectionId,
			'subcategory' => $subcategoryId
		));
		
		foreach($products as $row) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, array(
				'product_id' => $row->product_id
			));
			if($tableId) {
				$table = array(
					'product_id' => $row->product_id,
					'table_id' => $tableId
				);
				$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, $table);
			}
			$this->productCache->invalidate($row->product_id);
		}
	}
	
    /**
     * Change size table in collection brand
     * 
     * Integer $collectionId
     * Integer $brandId
     * Integer $tableId
     * 
     * return nothing
     */
    public function changeSizeTableInCollectionBrand($collectionId, $brandId, $tableId) {
		$query = vsprintf('
		SELECT DISTINCT cp.product_id FROM %s AS cp 
		INNER JOIN %s AS products ON products.id = cp.product_id 
		WHERE cp.collection_id = :collection  AND products.brand_id = :brand
		', array(
			CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS,
            self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS
		));
		
		$products = $this->db->fetchAll($query, array(
			'collection' => $collectionId,
			'brand' => $brandId
		));
		
		foreach($products as $row) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, array(
				'product_id' => $row->product_id
			));
			if($tableId) {
				$table = array(
					'product_id' => $row->product_id,
					'table_id' => $tableId
				);
				$this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE_TABLES, $table);
			}
			$this->productCache->invalidate($row->product_id);
		}
	}
    
    /**
     * Update products quantities
     * 
     * Array(Integer => Array(Integer => Array()))) $products
     * 
     * return nothing
     */
	public function updateProductsQuantities($products) {
		foreach($products as $productId=>$sizes) {
			foreach($sizes as $sizeId=>$values) {
				$this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, array(
					'amount' => $values['quantity'],
					'buy' => $values['buy'],
				), array(
					'product_id' => $productId,
					'size_id' => $sizeId
				));
			}
			$this->productCache->invalidate($productId);
		}
	}
    
    /**
     *  Update product_size by product and size and invalidate cache
     * 
     *  integer $productId
     *  integer $sizeId
     *  integer $newAmount
     * 
     *  return nothing
     */
    public function updateProductSizeAmount($productId, $sizeId, $newAmount) {
        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, array('amount' => $newAmount), array('product_id' => $productId, 'size_id' => $sizeId));
        $this->productCache->invalidate($productId);
    }
    
    /**
     *  reduce products quantity ana bought quantity of product, only for product's providers that are warehouse type
     * 
     *  \App\Model\User\OrderModel $order
     * 
     *  return nothing
     */
    public function reduceWarehouseQuantities(\App\Model\User\OrderModel $order) {
        foreach($order->products() as $op) {
            if($op->product->provider->isTypeWarehouse()) {
                $productId = $op->product_id;
                $sizeId = $op->size_id;
                $quantity = $op->quantity;
                try {
                    $this->reduceWarehouseProductQuantities($productId, $sizeId, $quantity);
                } catch(\Exception $e) {
                    /**
                     * do nothing
                     */
                }
            }
        }
    }
    
    /**
     *  Reduce product quantity and bought quantity.
     * 
     *  integer $productId
     *  integer $sizeId
     *  integer $quantity
     * 
     *  return nothing or \Exception
     */
    private function reduceWarehouseProductQuantities($productId, $sizeId, $quantity) {
        $query = sprintf('SELECT * FROM %s WHERE product_id = :product AND size_id = :size', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array('product' => $productId, 'size' => $sizeId);
        $row = $this->db->fetch($query, $params);
        if($row->buy >= $quantity && $row->amount >= $quantity) {
            $updateValues = array(
                'amount' => $row->amount - $quantity,
                'buy' => $row->buy - $quantity
            );
            $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $updateValues, array('product_id' => $productId, 'size_id' => $sizeId), 1);
            $this->productCache->invalidate($productId);
        } else {
            throw new \Exception('Няма достатъчно закупени продукти.');
        }
    }
    
    /**
     *  update expected product size
     *  
     *  integer $productId
     *  integer $sizeId
     *  integer $newAmount
     * 
     *  return nothing
     */
    public function updateProductSizeExpected($productId, $sizeId, $newAmount) {
        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, array('expected' => $newAmount), array('product_id' => $productId, 'size_id' => $sizeId), 1);
        $this->productCache->invalidate($productId);
    }
    
    /**
     * reduce amount of product size and reduce delivery quantity of provider order product and increase broken quantity of provider order product
     * 
     * integer $providerOrderId
     * integer $productId
     * integer $sizeId
     * integer $quantity
     * 
     * return nothing or \Exception
     */
    public function applyBrakuvaneQuantity($providerOrderId, $productId, $sizeId, $quantity) {
        $query = sprintf("SELECT * FROM %s WHERE product_id = :product AND size_id = :size", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array('product' => $productId, 'size' => $sizeId);
        $row = $this->db->fetch($query, $params);
        if(!empty($row)) {
            $newAmount = $row->amount - $quantity;
            if($newAmount < 0) {
                $newAmount = 0;
            }
            $updateValues = array(
                'amount' => $newAmount
            );
            $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $updateValues, array('product_id' => $productId, 'size_id' => $sizeId), 1);
            $this->productCache->invalidate($productId);
        }
        
        $query = sprintf("SELECT * FROM %s WHERE provider_order_id = :provider_order AND product_id = :product AND size_id = :size", \App\Manager\ProvidersManager::ABSTRACT_TABLE_PROVIDERS_ORDERS_PRODUCTS);
        $params = array('provider_order' => $providerOrderId, 'product' => $productId, 'size' => $sizeId);
        $row = $this->db->fetch($query, $params);
        if(!empty($row)) {
            $newDeliveryQuantity = $row->delivery_quantity - $quantity;
            if($newAmount < 0) {
                $newDeliveryQuantity = 0;
            }
            $newBrokenQuantity = $row->broken_quantity + ($row->delivery_quantity - $newDeliveryQuantity);
            $updateValues = array(
                'delivery_quantity' => $newDeliveryQuantity,
                'broken_quantity' => $newBrokenQuantity
            );
            $this->db->update(\App\Manager\ProvidersManager::ABSTRACT_TABLE_PROVIDERS_ORDERS_PRODUCTS, $updateValues, array('id' => $row->id), 1);
        }
    }
    
    /**
     * reduce amount of product size and reduce delivery quantity of provider order product for all products in providerOrder
     * 
     * integer $providerOrderId
     * array $products - array('productId', 'sizeId')
     * 
     * return nothing or \Exception
     */
    public function applySnimaneQuantities($providerOrderId, $products) {
        foreach($products as $productData) {
            $query = sprintf("SELECT * FROM %s WHERE product_id = :product AND size_id = :size", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
            $params = array('product' => $productData['productId'], 'size' => $productData['sizeId']);
            $row = $this->db->fetch($query, $params);
            if(!empty($row)) {
                $newAmount = $row->amount - 1;
                if($newAmount < 0) {
                    $newAmount = 0;
                }
                $updateValues = array(
                    'amount' => $newAmount
                );
                $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE, $updateValues, array('product_id' => $productData['productId'], 'size_id' => $productData['sizeId']), 1);
                $this->productCache->invalidate($productData['sizeId']);
            }
            
            $query = sprintf("SELECT * FROM %s WHERE provider_order_id = :provider_order AND product_id = :product AND size_id = :size", \App\Manager\ProvidersManager::ABSTRACT_TABLE_PROVIDERS_ORDERS_PRODUCTS);
            $params = array('provider_order' => $providerOrderId, 'product' => $productData['productId'], 'size' => $productData['sizeId']);
            $row = $this->db->fetch($query, $params);
            if(!empty($row)) {
                $updateValues = array(
                    'bought_quantity' => $row->bought_quantity + 1
                );
                $this->db->update(\App\Manager\ProvidersManager::ABSTRACT_TABLE_PROVIDERS_ORDERS_PRODUCTS, $updateValues, array('id' => $row->id), 1);
            }
        }
    }
    
    /**
     * increase product amount
     * 
     * integer productId
     * integer sizeId
     * integer quantity
     * 
     * return nothing
     */
    public function inceaseProductQuantity($productId, $sizeId, $quantity) {
        $code = $productId . '_' . $sizeId;
        $query = sprintf("SELECT * FROM %s WHERE warehouse_code = :code", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array(
            'code' => $code
        );
        $row = $this->db->fetch($query, $params);
        if(empty($row)) {
            $query = sprintf("SELECT * FROM %s WHERE product_id = :product AND size_id = :size", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
            $params = array(
                'product' => $productId,
                'size' => $sizeId
            );
            $row = $this->db->fetch($query, $params);
        }
        if(!empty($row)) {
            $newQuantity = $row->amount + $quantity;
            $this->updateProductSizeAmount($row->product_id, $row->size_id, $newQuantity);
        }
    }
    
    /**
     * get DB row based on product and size
     * 
     * integer $productId
     * integer $sizeId
     * 
     * return stdClass|false
     */
    public function getSizeRow($productId, $sizeId) {
        $query = sprintf("SELECT * FROM %s WHERE product_id = :product AND size_id = :size", self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array('product' => $productId, 'size' => $sizeId);
        $result = $this->app['db']->fetch($query, $params);
        return $result;
    }
    
    /**
     * increase buy product for size
     * 
     * integer $productId
     * integer $sizeId
     * integer $quantity 
     * 
     * return nothing
     */
	public function buyProductSize($productId, $sizeId, $quantity) {
		$this->db->query(sprintf('UPDATE %s SET buy = buy + :buy WHERE product_id = :product AND size_id = :size',  self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE), array(
			'buy' => $quantity,
			'product' => $productId,
			'size' => $sizeId
		));
		$this->productCache->invalidate($productId);
	}
    
    /*
     * get record from providers__products_size
     * 
     * Integer $productId
     * Integer $sizeId
     * 
     * return stdClass or false
     */
    public function findProductSize($productId, $sizeId) {
        $query = sprintf('SELECT * FROM %s WHERE product_id = :product AND size_id = :size', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_PRODUCTS_SIZE);
        $params = array('product' => $productId, 'size' => $sizeId);
        return $this->db->fetch($query, $params);
    }
    
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT SIZES
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT BRANDS
     *----------------------------------------------------------------------------
     */
    
    /**
     * Count products for brand
     * 
     * Integer $brandId
     * Integer $collectionId
     * 
     * return Integer
     */
	public function countProductsForBrand($brandId, $collectionId=false) {
		$params = array($brandId);
		if($collectionId) {
			$params[] = $collectionId;
			$query = sprintf('
			SELECT COUNT(DISTINCT products.id) AS count 
			FROM %s AS products 
			INNER JOIN %s AS cp ON cp.product_id = products.id 
			WHERE products.brand_id = ? AND cp.collection_id = ?
			', ProductsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, CollectionsManager::ABSTRACT_TABLE_COLLECTIONS_PRODUCTS);
		} else {
			$query = sprintf('SELECT COUNT(id) AS count FROM %s WHERE brand_id = ?', ProductsManager::ABSTRACT_TABLE_PROVIDERS_PRODUCTS);
		}
		$row = $this->db->fetch($query, $params);
		if ($row) {
			return $row->count;
		}
		return 0;
	}

    /**
     * Swap product brand
     * 
     * Integer $brandId
     * Integer $newBrandId
     * Integer $collectionId
     * 
     * return nothing
     */
	public function swapProductBrand($brandId, $newBrandId, $collectionId=false) {
		$params = array(
			'old' => $brandId
		);
		$updateStmt = $this->db->prepare('UPDATE providers__products SET brand_id=:brand WHERE id=:product LIMIT 1');
		$productIds = array();
		
		$query = 'SELECT DISTINCT p.id FROM providers__products AS p';
		if($collectionId) {
			$query .= ' INNER JOIN collections__products AS cp ON cp.product_id = p.id AND cp.collection_id = :collection';
			$params['collection'] = $collectionId;
		}
		$query .= ' WHERE p.brand_id = :old';
		$stmt = $this->db->query($query, $params);
		while($row = $stmt->fetch()) {
			$productIds[] = $row->id;
			$updateStmt->execute(array(
				'brand' => $newBrandId,
				'product' => $row->id
			));
		}
		$this->productCache->invalidate($productIds);
	}
	
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT BRANDS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT VARIANTS
     *----------------------------------------------------------------------------
     */
    
    /**
     * Find product variant(s) by product(s)
     * 
     * Integer|Array(Integer) $value
     * 
     * return false|stdClass|Array(stdClass)
     */
	public function findProductVariants($value) {
		return $this->fetchQueryData(sprintf('SELECT * FROM %s WHERE product_id IN (:product)', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_VARIANS), 'product', array(
			'product' => (array)$value
		), null, null, $this->productCache->child('variants', 'product_id', true));
	}
	
    /**
     * Map stdClasses to product variants
     * 
     * Array(stdClass) $data
     * 
     * return nothing
     */
	public function mapProductVariants($data) {
		$this->mapDatabaseData($data, false, 'id', array($this, 'findProductVariants'), 'variants', 'product_id', true);
	}
	
    /**
     * Load variants
     * 
     * Array(stdClass) $data
     * 
     * return nothing
     */
	public function loadVariants($data) {
		if($data) {
			$variants = array();
			if(!is_array($data)) {
				$data = array($data);
			}
			foreach($data as $row) {
				$variants = array_merge($variants, $row->variants);
			}
			$this->mapProducts($variants, false, 'variant_id');
		}
	}
	
    /**
     * Load public variants
     * 
     * Array(stdClass) $data
     * 
     * return nothing
     */
	public function loadPublicVariants($data) {
		$this->loadVariants($data);
		if($data) {
			if(!is_array($data)) {
				$data = array($data);
			}
			foreach($data as $row) {
				$vars = $row->variants;
				foreach($vars as $key=>$val) {
					if(!$val->product || $val->product && !$val->product->collection) {
						unset($vars[$key]);
					}
				}
				$row->variants = $vars;
			}
		}
	}
	
    /**
     * Add product variants
     * 
     * Integer $productId
     * Integer $variantId
     * 
     * return nothing
     */
	public function addProductVariant($productId, $variantId) {
		$in = array(
			'product_id' => $productId,
			'variant_id' => $variantId
		);
		$inInsertResult = $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_VARIANS, $in, array('variant_id'));
		
		$out = array(
			'product_id' => $variantId,
			'variant_id' => $productId
		);
		$outInsertResult = $this->db->insert(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_VARIANS, $out, array('variant_id'));
		
        if(!empty($inInsertResult)) {
            $this->productCache->invalidate($productId);
        }
        if(!empty($outInsertResult)) {
            $this->productCache->invalidate($variantId);
        }
	}
	
    /**
     * Delete product variants for product
     * 
     * Integer $productId
     * 
     * return nothing
     */
	public function deleteProductVariants($productId) {
		$variants = $this->findProductVariants($productId);
		foreach($variants as $row) {
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_VARIANS, array(
				'product_id' => $row->product_id,
				'variant_id' => $row->variant_id
			));
			$this->db->delete(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS_VARIANS, array(
				'product_id' => $row->variant_id,
				'variant_id' => $row->product_id
			));
		}
	}
	
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT VARIANTS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT PRICES
     *----------------------------------------------------------------------------
     */
    
    /**
     * Find product price(s) by product(s)
     * 
     * Integer|Array(Integer) $products
     * 
     * return false|ProductsPricesModel|Array(ProductsPricesModel)
     */
	public function findProductsPrices($products) {        
		return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE product_id = :product AND UNIX_TIMESTAMP() BETWEEN active_from AND active_to', self::ABSTRACT_TABLE_PRODUCTS_PRICES),
			sprintf('SELECT * FROM %s WHERE product_id IN (:product) AND UNIX_TIMESTAMP() BETWEEN active_from AND active_to', self::ABSTRACT_TABLE_PRODUCTS_PRICES)
		), 'product', array('product' => $products), 'convertProductsPrices');
	}
    
    /**
     * Find product price(s) by product(s)
     * 
     * Integer|Array(Integer) $products
     * 
     * return false|ProductsPricesModel|Array(ProductsPricesModel)
     */
    public function findAllProductsPrices($products) {
        return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE product_id = :product ORDER BY active_from ASC', self::ABSTRACT_TABLE_PRODUCTS_PRICES),
			sprintf('SELECT * FROM %s WHERE product_id IN (:product) ORDER BY active_from ASC', self::ABSTRACT_TABLE_PRODUCTS_PRICES)
		), 'product', array('product' => $products), 'convertProductsPrices');
    }
	
    /**
     * Convert stdClass to ProductsPricesModel
     * 
     * stdClass|Array $values
     * 
     * return ProductsPricesModel
     */
	public function convertProductsPrices($values=array()) {
		return new ProductsPricesModel($values);
	}
	
    /**
     * Map product prices for stdClasses
     * 
     * Array(stdClass) $data
     * String $pk
     * 
     * return nothing
     */
	public function mapProductsPrices($data, $pk) {
		$promos = $this->app['active_promos'];
		foreach($promos as $index=>$coupon) {
			if(!$coupon->isActivePrice()) {
				unset($promos[$index]);
			}
		}
		if(!empty($promos)) {
			$exampleOrder = new \App\Model\User\OrderModel();
			if(!is_array($data)) {
				$data = array($data);
			}
			foreach($data as $row) {
				if(!$row->collection) {
					continue;
				}
				foreach($row->get_sizes() as $size) {
					$size->cart_amount = 1;
					$size->cart_price = $row->discount_price;
					$row->variant = $size;
					break;
				}
				$exampleOrder->products = array();
				$exampleOrder->loadProducts(array($row));
				unset($size->cart_amount, $size->cart_price, $row->variant);
				foreach($promos as $coupon) {
					try {
						$coupon->can_use($exampleOrder, $this->app);
						$row->active_price = new ProductsPricesModel(array(
							'product_id' => $row->id,
							'price_from' => $row->discount_price - ($row->discount_price * ($coupon->discount_amount / 100)),
							'price_to' => $row->discount_price - ($row->discount_price * ($coupon->discount_amount / 100)),
							'active_from' => $coupon->start_at,
							'active_to' => $coupon->expire_at
						));
						break;
					} catch (Error\CouponError $ex) {
						$row->active_price = false;
					}
				}
			}
		}
		
		$this->mapData($data, $pk, array($this, 'findProductsPrices'), 'active_price', 'product_id');
		$this->mapData($data, $pk, array($this, 'findAllProductsPrices'), 'all_prices', 'product_id', true);
        
		if($data) {
			foreach((array)$data as $p) {
				if($p->active_price && $p->active_price->provider_price > 0) {
                    $p->set_original_provider_price($p->provider_price);
					$p->provider_price = $p->active_price->provider_price;
				}
			}
		}
	}
	
    /**
     * Add active price for product
     * 
     * Integer $productId
     * Decimal $providerPrice
     * Decimal $discountPrice
     * 
     * return nothing
     */
	public function addActivePrice($productId, $providerPrice, $discountPrice) {
		$dates = array(
			array(strtotime(date("Y-04-29")), strtotime(date("Y-05-06 23:59:59")))
		);
		$check = $this->db->fetch('SELECT * FROM products__prices WHERE product_id=?', $productId);
		if(empty($check)) {
			foreach($dates as $d) {
				$insert = array(
					'product_id' => $productId,
					'provider_price' => toNumber($providerPrice, 2),
					'price_from' => toNumber($discountPrice, 2),
					'price_to' => toNumber($discountPrice, 2),
					'created_at' => time(),
					'active_from' => $d[0],
					'active_to' => $d[1],
					'reason' => 'campaign'
				);
				$this->db->insert('products__prices', $insert);
				$this->activePriceCache->invalidate($productId);
			}
		} elseif($check->price_from != $discountPrice) {
			$this->db->update('products__prices', array(
				'provider_price' => toNumber($providerPrice, 2),
				'price_from' => toNumber($discountPrice, 2),
				'price_to' => toNumber($discountPrice, 2),
			), array(
				'product_id' => $check->product_id
			));
			$this->activePriceCache->invalidate($productId);
		}
	}
    
    /**
     * Convert integer to ProductsPricesModel
     * 
     * Integer $id
     * 
     * return ProductsPricesModel
     */
    public function convert_product_price($id) {
        $model = $this->findProductPrice($id);
		return $model ? $model : null;
    }
    
    /**
     * Find product price(s) by id(s)
     * 
     * Integer|Array(Integer) $id
     * 
     * return false|ProductsPricesModel|Array(ProductsPricesModel)
     */
    public function findProductPrice($id) {
        return $this->fetchQueryData(array(
			sprintf('SELECT * FROM %s WHERE id = :id', self::ABSTRACT_TABLE_PRODUCTS_PRICES),
			sprintf('SELECT * FROM %s WHERE id IN (:id)', self::ABSTRACT_TABLE_PRODUCTS_PRICES)
		), 'id', array('id' => $id), 'convertProductsPrices');
    }
    
    /**
     * Delete product price
     * 
     * ProductsPricesModel $model
     * 
     * return nothing
     */
    public function deleteProductPrice(ProductsPricesModel $model) {
        $productId = $model->product_id;
        $this->db->delete(self::ABSTRACT_TABLE_PRODUCTS_PRICES, array('id' => $model->id));
        $this->app['sphinx.manager']->add(\App\Manager\SphinxManager::TYPE_PRODUCT, array($productId));
    }
    
    /**
     * Clear past product prices
     * 
     * return nothing
     */
    public function cleanPastProductPrices() {
        $query = sprintf("DELETE FROM %s WHERE campaign_id = 0 AND :last_week > active_to", self::ABSTRACT_TABLE_PRODUCTS_PRICES);
        $params = array(
            'last_week' => strtotime("-1 week")
        );
        $this->db->query($query, $params);
    }
    
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT PRICES
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * PRODUCT DISCOUNTS
     *----------------------------------------------------------------------------
     */
    
    /**
     * Get active offers
     * 
     * ProvidersProductsModel $product
     * 
     * return Array
     */
	public function getActiveOffers(ProvidersProductsModel $product) {
        $hidePaymentProviderOffers = false;
        if($product->provider->is_self == 1 && $product->discount_price < $this->app['app_config']['free_shipping_auto_split']) {
            $hidePaymentProviderOffers = true;
        }
		$exampleOrder = new \App\Model\User\OrderModel();
		foreach($product->get_sizes() as $size) {
			$size->cart_amount = 1;
			$size->cart_price = $product->discount_price;
			$product->variant = $size;
			break;
		}
		$exampleOrder->loadProducts(array($product));
		$offers = $this->app['orders.manager']->getOrderOffers($exampleOrder, $hidePaymentProviderOffers);
		return $offers;
	}
    
    /**
     * Add multiple prices
     * 
     * \App\Model\Products\ProductsPricesMultiModel $discount
     * 
     * return nothing
     */
    public function addMultiDiscounts(\App\Model\Products\ProductsPricesMultiModel $discount) {       
        $this->validateMultiDiscount($discount);
        $requestFilters = new ProductFiltersModel(unserialize($discount->request_filters));      
        $requestFilters->only_query = 1;
        $requestFilters->active_price_date = array(
            'from' => $discount->getActiveFrom(),
            'to' => $discount->getActiveTo()
        );
        
        if(empty($requestFilters->id) && empty($requestFilters->provider) && empty($requestFilters->collection)) {
             throw new \Exception('Не са избрани продукти, доставчик или колекция.');
        }
        if(empty($discount->is_new)) {
            $requestFilters->only_visible_price = 1;
        }
        list($productsQuery, $productsParams) = $this->findProducts($requestFilters);
        
        $params = array_merge($productsParams, array(
            'discount_now' => time(),
            'discount_reason' => 'campaign', 
            'discount_active_from' => $discount->getActiveFrom(), 
            'discount_active_to' => $discount->getActiveTo()
        ));
        if(!empty($discount->is_new)) {
            if($discount->type == 'percent') {
                $queryPrice = 'products.provider_price - (products.provider_price * :discount_provider_price)/100 as provider_price,
                    products.discount_price - (products.discount_price * :discount_price_from)/100 as price_from,
                    products.discount_price - (products.discount_price * :discount_price_to)/100 as price_to,';
            } else {
                $queryPrice = ':discount_provider_price as provider_price,
                    :discount_price_from as price_from, 
                    :discount_price_to as price_to,';
            }
            $params = array_merge($params, array(
                'discount_provider_price' => toNumber($discount->provider_price, 2),
                'discount_price_from' => toNumber($discount->discount_price_from, 2),
                'discount_price_to' => toNumber($discount->discount_price_to, 2)
            ));
        } else {
            $queryPrice = '0 as provider_price,
                    0 as price_from,
                    0 as price_to,';
        }
        $fields = 'products.id as product_id, 
            ' . $queryPrice . '
            :discount_now as created_at, 
            :discount_active_from as active_from, 
            :discount_active_to as active_to, 
            :discount_reason as reason';
        if(!empty($discount->campaign_id)) {
            $fields .= ', :campaign_id as campaign_id';
            $params['campaign_id'] = $discount->campaign_id;
        } else {
            $fields .= ', NULL as campaign_id';
        }
        $subQuery = str_replace('{fields}', $fields, $productsQuery);
        $sphinxFields = '"' . \App\Manager\SphinxManager::TYPE_PRODUCT . '" as type, products.id as product_id, UNIX_TIMESTAMP() as modified_at, "0" as updated_at';     
        $sphinxQuery = str_replace('{fields}', $sphinxFields, $productsQuery);
        $query = sprintf('INSERT INTO %s (product_id, provider_price, price_from, price_to,created_at, active_from, active_to, reason, campaign_id) ' . $subQuery, self::ABSTRACT_TABLE_PRODUCTS_PRICES);       
		$this->db->query($query, $params);
        $this->app['sphinx.manager']->addWithQuery($sphinxQuery, $productsParams);
    }
    
    /**
     * Add discount to product
     * 
     *  ProductsPricesModel $discount
     * 
     * return ProductsPricesModel | \Exception
     */
    public function addDiscount(ProductsPricesModel $discount) {
        if(!$this->checkDiscount($discount)) {
            return false;
        }
        try {
			$this->db->beginTransaction();
            $discount->created_at = time();
            $row = $discount->toArray();
            $row[] = '_load';
            $row[] = '_id';
            $this->db->insert(self::ABSTRACT_TABLE_PRODUCTS_PRICES, $row);
            $discount->replace($row);
			$this->db->commit();
            $this->app['sphinx.manager']->add(\App\Manager\SphinxManager::TYPE_PRODUCT, array($discount->product_id));
            return $discount;
        } catch(\Exception $e) {
			$this->db->rollBack();
			return $e;
		}
    }
    
    /**
     * Check discount
     * 
     * ProductsPricesModel $discount
     * 
     * return Boolean
     */
    public function checkDiscount(ProductsPricesModel $discount) {
        $result = true;
        $query = sprintf("SELECT COUNT(prices.id) as `count` "
                . "FROM %s as prices "
                . "WHERE prices.product_id = :product AND prices.active_to > :from AND prices.active_from < :to", 
            self::ABSTRACT_TABLE_PRODUCTS_PRICES);
        $params = array(
            'product' => $discount->product_id,
            'from' => $discount->active_from,
            'to' => $discount->active_to
        );
        $data = $this->db->fetch($query, $params);
        if(!empty($data->count)) {
            $result = false;
        }
        return $result;
    }
    
    /**
     * Validate discount
     * 
     * \App\Model\Products\ProductsPricesMultiModel $discount
     * 
     * return nothing | \Exception
     */
    public function validateMultiDiscount(\App\Model\Products\ProductsPricesMultiModel $discount) {
         $validator = array(
			'allowExtraFields' => true,
			'fields' => array(
                'active_from' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте въвели начална дата'
					))
                ),
                'active_to' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте въвели крайна дата'
					))
                ),
                
			)
		);
                    
        if(!empty($discount->is_new)) {
            $validator['fields'] = array_merge($validator['fields'], array(
                'type' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте избрали тип'
					))
                ),
                'provider_price' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте въвели доставна цена'
					)),
					new Validator\Price(array(
						'message' => 'Доставната цена не е валидна'
					))
                ),
                'discount_price_from' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте въвели цена в сайта от'
					)),
					new Validator\Price(array(
						'message' => 'Цена в сайта от не е валидна'
					))
                ),
                'discount_price_to' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте въвели цена в сайта до'
					)),
					new Validator\Price(array(
						'message' => 'Цена в сайта до не е валидна'
					)),
					new Validator\Callback(array(
						'message' => 'Цена в сайта от не може да е повече от цена в сайта до',
						'callback' => function($value)use($discount) {
							if($discount->discount_price_from > $value) {
								return false;
							}
						}
					))
                )
            ));
        } else {
            $validator['fields'] = array_merge($validator['fields'], array(
                'campaign_id' => array(
                    new Assert\NotBlank(array(
						'message' => 'Не сте избрали кампания.'
					)),
					new Assert\GreaterThan(array(
                        'value' => 0,
						'message' => 'Не сте избрали кампания.'
					))
                )
            ));
        }      
                    
        $validatorCollection = new Assert\Collection($validator);
		$errorList = $this->app['validator']->validateValue($discount->toArray(), $validatorCollection);

		foreach ($errorList as $error) {
			$name = substr($error->getPropertyPath(), 1, -1);
			throw new Error\FormError($name, $error->getMessage());
		}

		return $errorList;
    }

    /**
     * Find discounts
     * 
     * ProductFiltersModel $requestFilters
     * 
     * return Array(stdClass)
     */
    public function findDiscounts(ProductFiltersModel $requestFilters) {
        $requestFilters->only_query = 1;
        if(empty($requestFilters->id) && empty($requestFilters->provider) && empty($requestFilters->collection)) {
             return array();
        }
        list($productsQuery, $productsParams) = $this->findProducts($requestFilters);
        
        $productsQuery = str_replace('{fields}', 'products.id as id', $productsQuery);
        $query = sprintf("SELECT prices.active_from, prices.active_to, count(products.id) as products
                FROM %s as prices 
                JOIN (__join__) as products on prices.product_id = products.id
                GROUP BY prices.active_from, prices.active_to", 
            self::ABSTRACT_TABLE_PRODUCTS_PRICES);
                
        $query = str_replace('__join__', $productsQuery, $query);
        $params = $productsParams;
        
        $result = $this->db->fetchAll($query, $params);
        return $result;
    }
    
    /**
     * Remove discount groups
     * 
     * ProductFiltersModel $requestFilters
     * Array $discountGroups
     * 
     * return nothing
     */
    public function removeDiscountGroups(ProductFiltersModel $requestFilters, $discountGroups = array()) {
        $requestFilters->only_query = 1;
        if(empty($requestFilters->id) && empty($requestFilters->provider) && empty($requestFilters->collection)) {
             return;
        }
        list($productsQuery, $productsParams) = $this->findProducts($requestFilters);
        $productsQuery = str_replace('{fields}', 'products.id as id', $productsQuery);
        
        foreach($discountGroups as $discountGroup) {
            $active_from = substr($discountGroup, 0, 10);
            $active_to = substr($discountGroup, -10, 10);
            
            $params = array(
                'active_from' => $active_from,
                'active_to' => $active_to
            );
            $where = " prices.active_from = :active_from AND prices.active_to = :active_to";
            
            $select = "DELETE prices ";
            $baseQuery = sprintf("__select__
                    FROM %s as prices 
                    JOIN (__join__) as products on prices.product_id = products.id
                    WHERE __where__",
                self::ABSTRACT_TABLE_PRODUCTS_PRICES);
            $query = $baseQuery;
            $query = str_replace('__select__', $select, $query);
            $query = str_replace('__join__', $productsQuery, $query);
            $query = str_replace('__where__', $where, $query);
            $params = array_merge($params, $productsParams);
            
            $sphinxQuery = $baseQuery;
            $select = 'SELECT "' . \App\Manager\SphinxManager::TYPE_PRODUCT . '" as type, products.id as product_id, UNIX_TIMESTAMP() as modified_at, "0" as updated_at';
            $sphinxQuery = str_replace('__select__', $select, $sphinxQuery);
            $sphinxQuery = str_replace('__join__', $productsQuery, $sphinxQuery);
            $sphinxQuery = str_replace('__where__', $where, $sphinxQuery);
            $sphinxParams = $params;
            
            $this->app['sphinx.manager']->addWithQuery($sphinxQuery, $sphinxParams);
            
            $this->db->query($query, $params);
        }
    }
    
    /**
     *----------------------------------------------------------------------------
     * END PRODUCT DISCOUNTS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * RETURNS
     *----------------------------------------------------------------------------
     */
    
    /**
     * Map stdClasses with returns data
     * 
     * Array(stdClass) $products
     * 
     * return nothing
     */
    public function mapReturnData($products) {
        $this->mapData($products, 'id', array($this, 'findProductReturns'), 'returns_count', 'product_id', false);
        $this->mapData($products, 'id', array($this, 'findProductReturnReasons'), 'returns_reasons', 'product_id', true);
        $this->mapData($products, 'id', array($this, 'findProductReturnReasonTexts'), 'returns_reason_texts', 'product_id', true);
    }
    
    /**
     * Find product return(s) by product(s)
     * 
     * Integer|Array(Integer) $products
     * 
     * return false|stdClass|Array(stdClass)
     */
    public function findProductReturns($products) {
        $query = sprintf('SELECT op.product_id, count(DISTINCT return_products.id) as returns_count
                FROM %s as return_products
                JOIN %s as tickets on tickets.id = return_products.ticket_id
                JOIN %s as op on return_products.order_product_id = op.id
                WHERE tickets.status = :ticket_status AND op.product_id in (:product)
                GROUP BY op.product_id',
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS_PRODUCTS,
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS,
            \App\Manager\OrdersManager::ABSTRACT_TABLE_USERS_ORDERS_PRODUCTS);
        $params = array(
            'ticket_status' => ReturnTicketModel::STATUS_COMPLETED,
            'product' => $products
        );
        
        $all = $this->fetchQueryData($query, 'product', $params);
        return $all;
    }
    
    /**
     * Find product return reason(s) by product(s)
     * 
     * Integer|Array(Integer) $products
     * 
     * return false|stdClass|Array(stdClass)
     */
    public function findProductReturnReasons($products) {
        $query = sprintf('SELECT op.product_id, return_products.reason_id, IFNULL(reason.name, "") as reason_name, count(DISTINCT return_products.id) as reason_count
                FROM %s as return_products
                JOIN %s as tickets on tickets.id = return_products.ticket_id
                JOIN %s as op on return_products.order_product_id = op.id
                LEFT JOIN %s as reason on return_products.reason_id = reason.id
                WHERE tickets.status = :ticket_status AND op.product_id in (:product)
                GROUP BY op.product_id, return_products.reason_id
                ORDER BY reason_count DESC',
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS_PRODUCTS,
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS,
            \App\Manager\OrdersManager::ABSTRACT_TABLE_USERS_ORDERS_PRODUCTS,
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_REASONS);
        $params = array(
            'ticket_status' => ReturnTicketModel::STATUS_COMPLETED,
            'product' => $products
        );
        
        $all = $this->fetchQueryData($query, 'product', $params);
        return $all;
    }
       
    /**
     * Find product return reason text(s) by product(s)
     * 
     * Integer|Array(Integer) $products
     * 
     * return false|stdClass|Array(stdClass)
     */
    public function findProductReturnReasonTexts($products) {
        $query = sprintf('SELECT op.product_id, return_products.reason_text
                FROM %s as return_products
                JOIN %s as tickets on tickets.id = return_products.ticket_id
                JOIN %s as op on return_products.order_product_id = op.id
                WHERE tickets.status = :ticket_status AND op.product_id in (:product) AND return_products.reason_id = 2',
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS_PRODUCTS,
            \App\Manager\ReturnManager::ABSTRACT_TABLE_RETURN_TICKETS,
            \App\Manager\OrdersManager::ABSTRACT_TABLE_USERS_ORDERS_PRODUCTS);
        $params = array(
            'ticket_status' => ReturnTicketModel::STATUS_COMPLETED,
            'product' => $products
        );
        
        $all = $this->fetchQueryData($query, 'product', $params);
        return $all;
    }
    
    /**
     *----------------------------------------------------------------------------
     * END RETURNS
     *----------------------------------------------------------------------------
     */
    
    /**
     *----------------------------------------------------------------------------
     * UNACTIVE
     *----------------------------------------------------------------------------
     */
    
    /**
     * Map products with unactive data
     * 
     * Array(stdClass) $products
     * 
     * return nothing
     */
//    public function mapUnactive($products) {
////        $this->mapData($products, 'id', array($this, 'findUnactive'), 'unactive', 'product_id');
//    }
    
    /**
     * Find products
     * 
     * Array(Integer) $products
     * 
     * return Array(stdClass)
     */
    public function findUnactive($products) {
        if(is_array($products)) {
			$result = $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE id IN (?) AND unactive_time > 0', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), array($products));
		} else {
			$result = $this->db->fetch(sprintf('SELECT * FROM %s WHERE id = ? AND unactive_time > 0', self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS), array($products));
		}
		return $result;
    }
    
    /**
     * Make product unactive
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing
     */
    public function setProductUnactive(ProvidersProductsModel $product) {
        
        $product->unactive_time = time();
        $updateValues = array('unactive_time' => $product->unactive_time);
        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $updateValues, array('id' => $product->id), 1);
        
        $this->productCache->invalidate($product->id);
    }
    
    /**
     * Make unactive product - active
     * 
     * ProvidersProductsModel $product
     * 
     * return nothing
     */
    public function setProductActive(ProvidersProductsModel $product) {
        
        $product->unactive_time = 0;
        $updateValues = array('unactive_time' => $product->unactive_time);
        $this->db->update(self::ABSTRACT_TABLE_PROVIDERS_PRODUCTS, $updateValues, array('id' => $product->id), 1);
        
        $this->app['products.mods.manager']->addProduct($product);
        $this->productCache->invalidate($product->id);
    }
    
    /**
     *----------------------------------------------------------------------------
     * END UNACTIVE
     *----------------------------------------------------------------------------
     */
}
