<?php
$ajaxpage = true;
 include('includes/app_top.php');
$action = isset($_GET['action'])?$_GET['action']:'';

if($action == 'get_last_action'){
	$timesec=600;
	if($logged){
		$timesec = time()-$_SESSION['last_action'];
	}

	$result = array('sec'=>$timesec,'mins'=>$timesec/60);

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}


if(!$logged){
	exit;
}
if($role==3){
	exit;
}


if($action == 'bn2_deposit_modal'){
	include('includes/deposit_modal.php');
	exit;
}

if($action == 'get_serv_info'){
	$result = array();

	if(isset($_GET['serv_id'])){
		$servcherck = DB::get_row("select * from salon_services where id='".(int)$_GET['serv_id']."' and salon_id='".SALON_ID."' limit 1");
		if($servcherck) $result = $servcherck;
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'save_prov_cat_data'){
	$stylist = (int)$_GET['stl_id'];
	$cat_id = (int)$_GET['cat_id'];

	$check = DB::get_row("select * from provider_serv_cats where category_id='".$cat_id."' and provider_id='".$stylist."' limit 1");
	$upd_data = array(
		'availability' => $_POST['availability'],
		'commission_id' => (int)$_POST['commission_id']
	);

	if($check){
		DB::update('provider_serv_cats',$upd_data," category_id='".$cat_id."' and provider_id='".$stylist."' limit 1 ");
	}else{
		$upd_data['category_id']=$cat_id;
		$upd_data['provider_id']=$stylist;
		DB::insert('provider_serv_cats',$upd_data);
	}

	exit;
}
if($action == 'get_prov_cat_data'){

	$stylist = (int)$_GET['stl_id'];
	$cat_id = (int)$_GET['cat_id'];
	$result = array();

	$serv = DB::get_row("select sc.category_name, psc.availability, psc.commission_id from service_categories sc left join provider_serv_cats psc on psc.category_id=sc.id and psc.provider_id='".$stylist."'   where sc.id='".$cat_id."' and  sc.salon_id='".SALON_ID."' limit 1");

	if($serv){
		if($serv['availability']===null) $serv['availability']='';
		$result['category_name']=$serv['category_name'];
		$result['availability']=$serv['availability'];
		if($serv['commission_id']===null) $serv['commission_id']='';
		$result['commission_id']=$serv['commission_id'];
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'save_prov_serv_data'){

	$stylist = (int)$_GET['stl_id'];
	$serv_id = (int)$_GET['serv_id'];

	$equipments_list = null;
	$equipment_id = null;
	
	if($_POST['equipments_list']!=''){
		$serv = DB::get_row("select ss.* from salon_services ss where ss.id='".$serv_id."'  limit 1");
		if($serv && $serv['equipments_list']!=$_POST['equipments_list']){
			$equipments_list = (string)$_POST['equipments_list'];
			$eq_l = explode(',',$equipments_list);
			$equipment_id = (int) $eq_l[0];
		}
	}

	$upd_data = array(
		'availability' => $_POST['availability'],
		'timing_min' => $_POST['tot_serv_time']==''?null:(int)$_POST['tot_serv_time'],
		'service_time_1' => $_POST['appl_time']==''?null:(int)$_POST['appl_time'],
		'process_time' => $_POST['process_time']==''?null:(int)$_POST['process_time'],
		'service_time_2' => $_POST['fin_time']==''?null:(int)$_POST['fin_time'],
		'commission_id' => (int)$_POST['commission_id'],
		'equipment_id' => $equipment_id,
		'equipments_list' => $equipments_list
	);

	DB::update('provider_services',$upd_data," provider_id='".$stylist."' and service_id='".$serv_id."' limit 1 ");
	exit;
}


if($action == 'get_prov_serv_data'){

	$stylist = (int)$_GET['stl_id'];
	$serv_id = (int)$_GET['serv_id'];
	$result = array();

	$serv = DB::get_row("select ss.*, ps.provider_id,ps.pricing as prov_pricing,ps.timing_min as prov_timing_min,ps.service_time_1 as prov_service_time_1,ps.process_time as prov_process_time,ps.service_time_2 as prov_service_time_2,ps.fee_perc,ps.availability, ps.commission_id, sc.category_name, ps.equipments_list as prov_eq_list from salon_services ss left join provider_services ps on ps.service_id=ss.id  and ps.provider_id='".$stylist."' left join service_categories sc on sc.id=ss.service_category_id   where ss.id='".$serv_id."' and  ss.salon_id='".SALON_ID."' limit 1");

	if($serv){
		
		if($serv['prov_eq_list'] && $serv['prov_eq_list']!='') $serv['equipments_list']=$serv['prov_eq_list'];
		
		$result['is_enabled']=$serv['provider_id']?1:0;
		$result['service_name']=$serv['service_name'];
		$result['category_name']=$serv['category_name'];
		$result['prov_timing_min']=$serv['prov_timing_min'];
		$result['default_timing_min']=$serv['default_timing_min'];
		$result['prov_service_time_1']=$serv['prov_service_time_1'];
		$result['service_time_1']=$serv['service_time_1'];
		$result['prov_process_time']=$serv['prov_process_time'];
		$result['process_time']=$serv['process_time'];
		$result['prov_service_time_2']=$serv['prov_service_time_2'];
		$result['service_time_2']=$serv['service_time_2'];
		$result['availability']=$serv['availability'];
		$result['equipments_list']=$serv['equipments_list'];
		$result['commission_id']=($serv['commission_id']===null)?'':$serv['commission_id'];
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'get_cust_cc_status'){
	$gate = $salon_info['payment_gate'];
	if($gate=='evo'){
		include_once('../includes/functions_evo.php');
	}
	$cc_filter = "";

	if($gate=='evo'){
		$evoset = getEvoSettings();
		if(!isset($evoset['merch_id'])||$evoset['merch_id']=='') $evoset['merch_id']='none';
		$cc_filter = " and evo_merch_id='".addslashes($evoset['merch_id'])."'";
	}else{
		$cc_filter = "  and authnet_acc='".addslashes(AUTHNET_NAME)."' ";
	}

	$result = array('has_cc'=>0);
	$ccq = DB::get_row("select count(user_id) as c from customer_cc where user_id='".(int)$_GET['uid']."' ".$cc_filter."");
	if($ccq && $ccq['c']>0) $result['has_cc']=1;

	if(!$result['has_cc']){
		 $ccq2 = DB::get_row("select   cl.linked_user_id from customer_cc_linked cl inner join customer_cc c on c.id=cl.cc_id where cl.linked_user_id='".(int)$_GET['uid']."' ".$cc_filter." limit 1");
		if($ccq2 && $ccq2['linked_user_id']>0) $result['has_cc']=1;
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}
if($action == 'get_cust_mail'){

	$result = array('has_mail'=>0);
	$u = DB::get_row("select email from users where id='".(int)$_GET['uid']."'");
	if($u['email']!='') $result = array( 'has_mail'=>1, 'mail'=>$u['email'] );

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'get_cust_mail_by_serv'){

	$result = array('has_mail'=>0, 'email_cancellations'=>0);
	if( isset($salon_info['email_cancellations']) && $salon_info['email_cancellations']==1 ){ $result['email_cancellations'] = 1; } 
	$u = DB::get_row("select u.id, u.email from salon_appointments sa left join users u on u.id = sa.customer_id where sa.id='".(int)$_GET['sid']."' and sa.salon_id='".SALON_ID."' limit 1");
	if($u['email']!='') { $result['has_mail']=1; $result['mail']=$u['email']; }
	if($u['id']) $result['uid']=$u['id'];

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}


if($action == 'walkin_set_taken'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$taken = (int)$_GET['taken'];

	if($appt_id){
		if($taken){
			$loctime = tz_time(time());
			$wtime = round(($loctime-strtotime($appt['appt_date']))/60);
			log_event('taken_walkin', $appt_id, $appt['customer_id']);
			DB::update('salon_appointments',array('is_taken'=>1,'wait_time_min'=>$wtime),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1 ");
		}else{
			DB::update('salon_appointments',array('is_taken'=>0,'wait_time_min'=>0),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1 ");
		}
	}

	exit;
}

if($action == 'appt_get_avail_promos'){
	$promo_type = $_GET['promo_type'];
	$obj_id = (int)$_GET['obj_id'];
	$appt_id=0;

	$obj_data = array();

	if($promo_type=='service'){
		$obj_data = DB::get_row("select distinct sapp.appt_date, sapp.customer_id, sas.*, ss.service_name, ss.service_category_id, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ps.pricing, ss.employee_price, sc.sales_tax, ss.service_category_id from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join service_categories sc on sc.id=ss.service_category_id left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where sas.appt_serv_id='".(int)$obj_id."' limit 1");
		if($obj_data){
			$u_info = DB::get_row("select * from users where id='".$obj_data['customer_id']."' limit 1");
			$appt_id = $obj_data['appt_id'];
		}

	}else{
		$obj_data = DB::get_row("select sar.*, si.code, si.description, si.price as inv_price, si.employee_amt, si.brand,si.category from salon_appointments_retail sar left join salon_inventory si on si.id=sar.inv_id where sar.appt_retail_id='".(int)$obj_id."'");
		if($obj_data){
			$appt_id = $obj_data['appt_id'];
		}
	}
	if($appt_id && $obj_data){

		$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."'  and salon_id='".SALON_ID."'  limit 1");

		$promo_q = DB::query("select * from  salon_promotions where salon_id='".SALON_ID."' and status=1 and promo_type='".addslashes($promo_type)."' and (start_date is null or start_date<='".date('Y-m-d')."') and (end_date is null or end_date>='".date('Y-m-d')."') order by  id desc");
		$ic = 0;
		while($r=DB::fetch_assoc($promo_q)){

			$applied = get_promo_appldata($r,$promo_type,$obj_data,$appt['customer_id'],$appt_id);

			if($applied['status']){
			?><tr class="modlrw">
				<td style="padding-left:25px !important;"><?php echo $r['promo_name'].' ('.$r['promo_code'].')'; ?></td>
				<td style="text-align:center;text-transform:capitalize;" class="desk_only"><?php echo $r['promo_type']; ?></td>
				<td style="text-align:right;">-<?php
				if($r['promo_discount_type']=='amnt') echo '$';
				echo number_format($r['discount_perc'],2,'.',',');
				if($r['promo_discount_type']!='amnt') echo '%';
				?></td>
				<td style="text-align:center;"><a href="javascript:;" onclick="addPromoToItem(<?php echo $r['id']; ?>,'<?php echo addslashes($promo_type); ?>',<?php echo (int)$obj_id; ?>)" class="addserv"><i class="las la-plus-circle"></i></a></td>
			</tr> <?php
			$ic++;
			}
		}
	}
	if($ic==0){
		?><tr class="modlrw">
			<td style="text-align:center;" colspan="4">No applicable promotions found.</td>
		</tr> <?php
	}
	exit;
}


if($action == 'sms_not_resp_c'){
	$result = array('count'=>0);
	$unresponded_sms_c = DB::get_row("SELECT count(distinct `customer_id`) as c FROM `sms_notification_conv` WHERE salon_id='".SALON_ID."' and `is_unread`=1" );
	if($unresponded_sms_c) $result['count']=(int)$unresponded_sms_c['c'];
	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'enable_curbside'){
	$result = array('count'=>0);
	$co_count = DB::get_row("select count(oc.id) as c from order_curbside oc where oc.status='Arrived' and oc.salon_id='".SALON_ID."'");
	if($co_count) $result['count']=(int)$co_count['c'];
	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'bn2_serv_durations'){
	$result = array();
	$customer_id = (int)$_POST['customer_id'];


	if(isset($_POST['serv_pairs']) && is_array($_POST['serv_pairs'])){
		foreach($_POST['serv_pairs'] as $srv){
			if($srv['service']>0){
				$serv_ids[]=(int)$srv['service'];
				if(!in_array($srv['provider'],$prov_ids)) $prov_ids[]=$srv['provider'];
				$s_inf =  DB::get_row("select if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, sc.booking_order, ss.deposit from salon_services ss left join provider_services ps on ps.service_id=ss.id  and ps.provider_id='".(int)$srv['provider']."' left join customer_services cs on cs.customer_id='".(int)$customer_id."' and cs.service_id=ss.id left join service_categories sc on sc.id=ss.service_category_id  where ss.id='".(int)$srv['service']."' limit 1");
				if($s_inf){
					$duration = $s_inf['service_duration'];

					$has_parts = 0;
					if($s_inf['process_time'] || $s_inf['service_time_2']){
						$calcdur = $s_inf['service_time_1']+$s_inf['process_time']+$s_inf['service_time_2'];
						if($calcdur>$duration) $duration=$calcdur;
					}
					if($s_inf['process_time']>0 || $s_inf['service_time_2']>0){
						$duration=$s_inf['service_time_1'].','.$s_inf['process_time'].','.$s_inf['service_time_2'];
						$has_parts = 1;
					}
					$result[]=array(
						'duration'=>$duration,
						'has_parts'=>$has_parts,
						'serv_id'=>$srv['provider'],
						'num'=>$srv['num'],
						'bo'=>$s_inf['booking_order'],
						'deposit' => $s_inf['deposit']
					);
				}
			}
		}
	}
	if(count($result)>1){
		$sl = count($result);
		for($i=0;$i<$sl;$i++)
			for($j=$i+1;$j<$sl;$j++){
				if($result[$i]['bo']>$result[$j]['bo']){
					$tmp = $result[$i];
					$result[$i] = $result[$j];
					$result[$j] = $tmp;
				}
			}
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}


if($action == 'appt_serv_move'){
	$result = array('success'=>false,'errmsg'=>'', 'ask'=>false);

	$appt_serv_id = (int)$_POST['appt_serv_id'];
	$appt_id = (int)$_POST['appt_id'];
	$to_date = strtotime($_POST['to_date']);
	$to_time = strtoupper($_POST['to_time']);
	$to_stl = (int)$_POST['to_stl'];
	$do_checks = (int)$_POST['do_checks'];

	$book_date = date('Y-m-d',$to_date);

	//$result['errmsg']='no!';

	$appt_serv = DB::get_row("select sa.appt_date, sa.customer_id, sas.* from salon_appointments sa inner join salon_appointments_services sas on sas.appt_id=sa.id where sa.id='".(int)$appt_id."' and sa.salon_id='".SALON_ID."' and sas.appt_serv_id='".(int)$appt_serv_id."' limit 1");

	if($appt_serv){

		$client = DB::get_row("select u.* from users u left join salon_customers sc on sc.customer_id=u.id  and  sc.salon_id='".SALON_ID."'   where u.id='".$appt_serv['customer_id']."'  and u.status=1 ");

		$datechange = date('Y-m-d',$to_date)!=date('Y-m-d',strtotime($appt_serv['appt_date']));
		$has_changes = false;
		if($appt_serv['provider_id']!=$to_stl || $appt_serv['service_time_sec']!=getTimeSec($to_time) || $datechange) $has_changes = true;

		if($has_changes){
			if($datechange){
				$is_msa = DB::get_row("select count(*) as c from salon_appointments_services where appt_id='".(int)$appt_id."'");
				if($is_msa['c']>1){
					$result['errmsg']='Cannot change date of multi-service appointment!';
					header('Content-type: application/json');
					echo json_encode($result);
					exit;
				}
			}
			if($do_checks){
				$err_msg='';

				if($appt_serv['provider_id']!=$to_stl){
					$checkperform = DB::get_row("SELECT * FROM `provider_services` where provider_id='".(int)$to_stl."' and service_id='".(int)$appt_serv['service_id']."' limit 1");
					if(!$checkperform){
						$provinfo = DB::get_row("select * from users where id='".(int)$to_stl."' limit 1");
						$result['errmsg']=($provinfo?$provinfo['first_name']:"This provider").' does not offer this service. Please move to a different provider.';
						header('Content-type: application/json');
						echo json_encode($result);
						exit;
					}
				}

				$s_inf =  DB::get_row("select if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, ifnull(ps.equipment_id,ss.equipment_id) as equipment_id, sc.booking_order from salon_services ss inner join provider_services ps on ps.service_id=ss.id left join customer_services cs on cs.customer_id='".(int)$appt_serv['customer_id']."' and cs.service_id=ss.id left join service_categories sc on sc.id=ss.service_category_id  where ss.id='".(int)$appt_serv['service_id']."' and ps.provider_id='".(int)$to_stl."' limit 1");

				if($appt_serv['duration_changed']){
					$s_inf['service_duration'] = (int) $appt_serv['duration_changed'];
					$s_inf['service_time_1'] = (int)$appt_serv['dur_appl_chng'] ;
					$s_inf['process_time'] = (int)$appt_serv['dur_procs_chng'];
					$s_inf['service_time_2'] = (int)$appt_serv['dur_finsh_chng'];
				}
				
				if($appt_serv['equipment_id']) $s_inf['equipment_id'] = $appt_serv['equipment_id'];


				$w_day = date('w',$to_date);
				$w_num = date('W',$to_date);
				$d_start = 0;
				$d_end = 0;
				$prov_schedules = array();
				$prov_ids = array((int)$to_stl);


				$salon_closed = false;

				$salon_dates = array();
				$stylist_add_per = DB::query("select * from salon_adddays where salon_id='".SALON_ID."' and end_date>='".$book_date."'  ");
				while($r=DB::fetch_assoc($stylist_add_per)){
					$start_d = strtotime($r['start_date']);
					$end_d = strtotime($r['end_date']." 23:59:59");
					while($start_d<=$end_d){
						$salon_dates[date('Y-m-d',$start_d)]=$r;
						$start_d+=86400;
					}
				}




				$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");

				if(isset($salon_dates[$book_date])){
					$salon_h['is_open']=1;
					$salon_h['start_time']=$salon_dates[$book_date]['start_time'];
					$salon_h['end_time']=$salon_dates[$book_date]['end_time'];
				}
				$schq = null;
				if($salon_h){
					if($salon_h['is_open']){
						if($salon_h['end_time']=='00:00') $salon_h['end_time']='23:59';
						if($salon_h['end_time']=='12:00 AM') $salon_h['end_time']='11:59 PM';


					}else $salon_closed = true;
				}


				if(!$salon_closed){
					$salon_off_day = DB::get_row("select * from salon_offdays where salon_id='".SALON_ID."' and end_date>='".$book_date."' and start_date<='".$book_date."'");
					if($salon_off_day && !isset($salon_dates[$book_date]))  $salon_closed = true;
				}



				$salon_d_start = getTimeSec($salon_h['start_time']);
				$salon_d_end = getTimeSec($salon_h['end_time']);


				foreach($prov_ids as $provid){
					$stylist = DB::get_row("select u.*,pi.code_name, pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.transition_min from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$provid."' and pi.salon_id='".SALON_ID."' limit 1");
					if($stylist && $stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];
					$is_odd_week = $stylist['althern_sched'] && $w_num%2!=0;

					$prov_info[$provid]=$stylist;


					$add_dates = array();
					$add_dates_info = array();
					$stylist_add_per = DB::query("select * from provider_adddays where provider_id='".(int)$provid."' and end_date>='".date('Y-m-d')."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_add_per)){
						if($r['end_time']=='00:00') $r['end_time']='23:59';
						if($r['end_time']=='12:00 AM') $r['end_time']='11:59 PM';
						$start_d = strtotime($r['start_date']);
						$end_d = strtotime($r['end_date']." 23:59:59");
						while($start_d<=$end_d){
							$da = date('Y-m-d',$start_d);
							$add_dates[]=$da;
							$add_dates_info[$da]=$r;
							$start_d+=86400;
						}
					}

					$prov_avail = 1;


					if(!isset($add_dates_info[$book_date])){
						$stylist_off_per = DB::query("select * from provider_offdays where provider_id='".(int)$provid."' and end_date>='".$book_date."' and start_date<='".$book_date."' and salon_id='".SALON_ID."'");
						while($r=DB::fetch_assoc($stylist_off_per)){
							$prov_avail = 0;
						}
					}

					$schq = array();
					if($salon_h){
						if($salon_h['is_open']){
							$d_start = getTimeSec($salon_h['start_time']);
							$d_end = getTimeSec($salon_h['end_time']);

							$sch_index = ($is_odd_week?2:1);
							if($stylist['fut_enable_sched'] && strtotime($stylist['fut_date_sched'])<=strtotime($book_date)){
								if($stylist['fut_althern_sched'] && $w_num%2!=0) $sch_index = 4;
								else $sch_index = 3;
							}

							$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$provid."' and salon_id='".SALON_ID."' and sch_index='".$sch_index."' and weekday='".$w_day."'");

							if($schq){
							if($schq && ($schq['is_available'] || in_array($book_date,$add_dates))){
								if($schq['end_time']=='00:00') $schq['end_time']='23:59';
								if($schq['end_time']=='12:00 AM') $schq['end_time']='11:59 PM';

								if(isset($add_dates_info[$book_date])){
									if($add_dates_info[$book_date]['start_time']) $schq['start_time']=$add_dates_info[$book_date]['start_time'];
									if($add_dates_info[$book_date]['end_time']) $schq['end_time']=$add_dates_info[$book_date]['end_time'];
								}
								$s_start = getTimeSec($schq['start_time']);
								if($s_start>$d_start) $d_start=$s_start;
								$s_end = getTimeSec($schq['end_time']);
								if($s_end<$d_end) $d_end=$s_end;
							}else $prov_avail = 0;
							}else  if(isset($add_dates_info[$book_date])){
								$d_start = getTimeSec($salon_h['start_time']);
								$d_end = getTimeSec($salon_h['end_time']);
								if($add_dates_info[$book_date]['start_time']){
									$s_start = getTimeSec($add_dates_info[$book_date]['start_time']);
									if($s_start>$d_start) $d_start=$s_start;
								}
								if($add_dates_info[$book_date]['end_time']){
									$s_end = getTimeSec($add_dates_info[$book_date]['end_time']);
									if($s_end<$d_end) $d_end=$s_end;
								}
							}else $prov_avail = 0;

						}else $prov_avail = 0;
					}else $prov_avail = 0;

					$avail_times = array();
					$busy_times = array();
					$transit_times = array();
					$alert_times = array();
					if($prov_avail){
					foreach($times_list as $k=>$v){
						$t_sec = getTimeSec($k);
						if($t_sec>=$d_start && $t_sec<$d_end) $avail_times[$k]=$v;
					}

					if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
						$brk_check_change = DB::get_row("select * from provider_schedule_break_changes where salon_id='".SALON_ID."' and `date`='".addslashes($book_date)."' and provider_id='".(int)$provid."' limit 1");
						if($brk_check_change){
							if($brk_check_change['is_removed']){
								$schq['has_break']=0;
								$schq['brk_start_time']='';
								$schq['brk_end_time']='';
							}else{
								if($brk_check_change['end_time']=='00:00') $brk_check_change['end_time']='23:59';
								if($brk_check_change['end_time']=='12:00 AM') $brk_check_change['end_time']='11:59 PM';
								$schq['brk_start_time']=$brk_check_change['start_time'];
								$schq['brk_end_time']=$brk_check_change['end_time'];
							}
						}
						if($schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
							$brk_dur = (getTimeSec($schq['brk_end_time'])-getTimeSec($schq['brk_start_time']))/60;
							$extr_dur = getTimesArr($schq['brk_start_time'],$brk_dur);
							foreach($extr_dur as $stm) $busy_times[$stm]=1;
						}
					}


					$busyhours = DB::query("select sas.service_time,
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
					if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2,
					if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time,
					if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration
					from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id  left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id  where sas.provider_id='".(int)$provid."' and sapp.appt_date>='".addslashes($book_date)."' and sapp.appt_date<='".addslashes($book_date)." 23:59:59' and sas.appt_serv_id!='".(int)$appt_serv_id."'");
					while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!='' /*&& isset($avail_times[$r['service_time']])*/){
						$r['service_time']=timeFix($r['service_time']);
						$busy_times[$r['service_time']]=1;
						if($r['service_duration']>15){
							if($r['process_time'] || $r['service_time_2']){
								$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
								if($calcdur>$r['service_duration']) $r['service_duration']=$calcdur;
							}
							if(!$stylist['double_book']){
								$r['service_time_1']=$r['service_duration'];
								$r['process_time']=0;
							}
							$extr_dur = getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']);
							//print_r($r);
							//print_r($extr_dur);
							//echo '<hr/>';
							foreach($extr_dur as $stm) $busy_times[$stm]=1;

							if($prov_info[$provid] && $prov_info[$provid]['transition_min']>0){
								$extr_dur = getTimesArr($r['service_time'],$r['service_duration']+$prov_info[$provid]['transition_min'],$r['service_time_1'],$r['process_time']);
								foreach($extr_dur as $stm) if(!isset($busy_times[$stm])) $transit_times[$stm]=1;
							}

						}

					}

					$stylist_off_h = DB::query("select * from provider_offhours where  `date`='".addslashes($book_date)."'  and provider_id='".(int)$provid."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_off_h)){
						$extr_dur =getTimesArr($r['start_time'],$r['duration']);

						foreach($extr_dur as $stm) $busy_times[$stm]=1;
					}

					$prov_schedules[$provid] = array('avail'=>$avail_times,'busy'=>$busy_times,'transit'=>$transit_times);
					}

				}

				$equipment_schedules = array();

				if($s_inf['equipment_id']){

						$eq_info = DB::get_row("select * from salon_equipment where id='".$s_inf['equipment_id']."' and salon_id='".SALON_ID."'");
						if($eq_info){
							$equipment_num = $eq_info['qty'];
							$equipment_busy_times = array();
							$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
					if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2,
					if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time,
					if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration  from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id left join customer_services cs on cs.customer_id=sapp.customer_id where sapp.salon_id='".SALON_ID."' and sapp.appt_date>='".addslashes($book_date)."' and sapp.appt_date<='".addslashes($book_date)." 23:59:59' and ifnull(sas.equipment_id,ifnull(ps.equipment_id,ss.equipment_id))='".$s_inf['equipment_id']."' and sas.appt_serv_id!='".(int)$appt_serv_id."'");
							while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!=''){
								if(!isset($equipment_busy_times[$r['service_time']])) $equipment_busy_times[$r['service_time']]=0;

								if($r['service_duration']>15){
									$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
									if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
									$r['service_time_1'] = $r['service_duration'];
									$r['process_time'] = 0;
									$extr_dur = array_unique(getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']));
									foreach($extr_dur as $stm){
										if(!isset($equipment_busy_times[$stm])) $equipment_busy_times[$stm]=0;
										$equipment_busy_times[$stm]++;
									}
								}else $equipment_busy_times[$r['service_time']]++;

							}
							$equipment_busy_times_new = array();
							foreach($equipment_busy_times as $k=>$v){
								if($v>=$equipment_num) $equipment_busy_times_new[$k]=1;
							}
							$equipment_busy_times=$equipment_busy_times_new;

							$equipment_schedules[$s_inf['equipment_id']] = array(
								'busy' => $equipment_busy_times,
								'name' => $eq_info['equipment_name']
							);
						}

				}


			$can_fit = true;
			$is_overlap = false;
			$is_transit_overlap = false;
			$is_off_schedule = false;
			$is_equipment_overlap = false;

			$s_time = getSecTime(getTimeSec($to_time));
			$srv = $s_inf;


			$is_busy = isset($prov_schedules[$to_stl]['busy'][$s_time]);
			$is_avail = isset($prov_schedules[$to_stl]['avail'][$s_time]);
			if(!$is_avail) $is_off_schedule = true;
			if($is_busy) $is_overlap = true;

			if($srv['process_time'] || $srv['service_time_2']){
				$calcdur = $srv['service_time_1']+$srv['process_time']+$srv['service_time_2'];
				if($calcdur>$srv['service_duration']) $srv['service_duration']=$calcdur;
			}
			/*echo $s_time;
			print_r($prov_schedules[$to_stl]['avail']);
			print_r($prov_schedules[$to_stl]['busy']);
			print_r($srv);
			exit;*/

			if(!canFitInTime($srv['service_duration'],$s_time, $srv['service_time_1'],$srv['process_time'],$prov_schedules[$to_stl]['avail'],$prov_schedules[$to_stl]['busy'])) {
				$can_fit = false;

				if(!canFitInTime($srv['service_duration'],$s_time, $srv['service_time_1'],$srv['process_time'],$prov_schedules[$to_stl]['avail'],array())){
					 $is_off_schedule = true;
					 //exit('offshedule');
				}else{
					$is_overlap = true;
				}

			}elseif(count($prov_schedules[$to_stl]['transit'])){
				if(!canFitInTime($srv['service_duration'],$s_time, $srv['service_time_1'],$srv['process_time'],$prov_schedules[$to_stl]['avail'],$prov_schedules[$to_stl]['transit'])){ $is_transit_overlap = true;    }
			}

			if($srv['equipment_id'] && isset($equipment_schedules[$srv['equipment_id']])){

				if(!canFitInTime($srv['service_duration'],$s_time, $srv['service_time_1'],$srv['process_time'],$times_list,$equipment_schedules[$srv['equipment_id']]['busy'])){
					$is_equipment_overlap = true;
					$err_msg .= $equipment_schedules[$srv['equipment_id']]['name'].' is not available at this time! ';
				}
			}

			if( $is_overlap) $result['errmsg']='This booking overlaps another appointment!';
			elseif($is_transit_overlap) $result['errmsg']='This booking overlaps transition time!';
			elseif($is_off_schedule) $result['errmsg']='Provider is off schedule. Book anyway?';
			elseif($is_equipment_overlap) $result['errmsg']=$err_msg;

				if($result['errmsg']!=''){
					$result['success']=false;
					$result['ask']=true;
				}else $result['success']=true;
			} else $result['success']=true;
			if($result['success']){

				$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price,ss.ep_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$to_stl."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$appt_serv['service_id']."' limit 1");
				$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
				$is_employee = 0;
				$is_ep=0;
				if( $client['is_fnf'] && $serv_q['employee_price'] > 0 ){ $price = $serv_q['employee_price']; $is_employee=1; }
				if(isset($client['role_id']) &&  $client['role_id']!=3 && $serv_q['ep_price'] > 0 ){ $price = $serv_q['ep_price']; $is_ep=1; }


				$s_time = getSecTime(getTimeSec($to_time));

				$serv_data = array(
					'provider_id' => (int)$to_stl,
					'service_time' => $s_time,
					'book_price' => (float) $price,
					'service_time_sec' => getTimeSec($s_time),
					'is_employee' => $is_employee,
					'is_ep' =>$is_ep
				);
				DB::update('salon_appointments_services',$serv_data," appt_serv_id='".(int)$appt_serv_id."' limit 1 ");
				log_event('edit_appt', $appt_id, $appt_serv['customer_id'], (int)$appt_serv_id, ($datechange?'appt_date = '.date('m/d/Y',strtotime($book_date)):''));

				include_once('includes/appointment_alerts.php');

				$client=DB::get_row("select * from users where id='".$appt_serv['customer_id']."' limit 1");
				$client_name = $client['first_name'].' '.$client['last_name'];

				$stylist = DB::get_row("select pi.appointment_alerts, u.first_name from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$to_stl."' and pi.salon_id='".SALON_ID."' limit 1");

				if($datechange){

					DB::update('salon_appointments',array('appt_date' => $book_date)," id='".(int)$appt_id."' and  salon_id='".SALON_ID."' limit 1 ");

					edited_appt_date_alert( (int)$to_stl, (int)$appt_id,  $appt_serv['appt_date'],$book_date, $client_name, $salon_info['main_location'] );

					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'date_change',
						'details' => 'Appointment date changed from '.date('m/d/Y',strtotime($appt_serv['appt_date'])).' to '.date('m/d/Y',strtotime($book_date))

					));
				}


				if($appt_serv['provider_id']!=$to_stl  ){
					$stylist_old = DB::get_row("select pi.appointment_alerts, u.first_name from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$appt_serv['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");

					edited_appt_providerchange_alert( (int)$to_stl, $appt_serv['provider_id'], (int)$appt_id,  $client_name, $salon_info['main_location'], $stylist['appointment_alerts'], $stylist_old['appointment_alerts'] );

					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'stylist_change',
						'details' => 'Service '.$serv_q['service_name'].' provider changed from '.$stylist_old['first_name'].' to '.$stylist['first_name']

					));
				}
				if($appt_serv['service_time_sec']!=getTimeSec($s_time) ){

					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'time_change',
						'details' => 'Service '.$serv_q['service_name'].' time changed from '.$appt_serv['service_time'].' to '.$s_time

					));
					edited_appt_servicetime_alert( (int)$to_stl, (int)$appt_id, $serv_q['service_name'],$appt_serv['service_time'], $s_time, $client_name,  $salon_info['main_location'] );



				}


			}
		}else{
			$result['success']=true;
		}
	}else{
		$result['errmsg']='Appointment unavailable!';
	}




	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}

if($action == 'bn2_book_appt'){
	$customer_id = (int)$_POST['customer_id'];
	$notes = $_POST['notes'];
	$services = $_POST['services'];
	$prebook = isset($_POST['prebook'])&&$_POST['prebook']?1:0;
	$booking_date =  date('Y-m-d',strtotime($_POST['date']));
	$deposit_amt = 0;
	$deposit_note = '';
	$deposit_waived = isset($_POST['deposit_status']) && $_POST['deposit_status']=='waived'?1:0;
	$deposit_paymethod = isset($_POST['deposit_paymethod'])?$_POST['deposit_paymethod']:'';
	$deposit_cc_id = isset($_POST['deposit_cc_id'])?(int)$_POST['deposit_cc_id']:0;
	
	if($customer_id){
		$client = DB::get_row("select u.* from users u left join salon_customers sc on sc.customer_id=u.id  and  sc.salon_id='".SALON_ID."'   where u.id='".$customer_id."'  and u.status=1 ");
		if($client){
			
			
			foreach($services as $k=>$serv){
				if($serv['service'] && $serv['provider']  ){ 
					if($serv['deposit']>0) $deposit_amt+=round((float)$serv['deposit'],2);
					if($serv['deposit_notes']!='')$deposit_note.=($deposit_note!=''?', ':'').$serv['deposit_notes'];
				}
			}
			
			$pay_success = false;
			$response = array();
			$deposit_id = 0;
			
			if($deposit_amt>0){
				if($deposit_waived){
					$deposit_paymethod = 'waived';
					$pay_success = true;
					$deposit_amt=0;
				}else{
					if($deposit_paymethod=='cash' || $deposit_paymethod=='check'){
						$pay_success = true;
					}else{
						$gate = $salon_info['payment_gate'];
						$cc_filter = "";

						if($gate=='evo'){
							include('../includes/functions_evo.php');
							$evoset = getEvoSettings();
							if(!isset($evoset['merch_id'])||$evoset['merch_id']=='') $evoset['merch_id']='none';
							$cc_filter = " and evo_merch_id='".addslashes($evoset['merch_id'])."'";
							
						}else{
							$cc_filter = "  and authnet_acc='".addslashes(AUTHNET_NAME)."' ";
							include('../includes/functions_authnet.php');
						}
						$uinf = DB::get_row("select * from users where id='".$customer_id."' limit 1");
						
						$cc = DB::get_row("select * from customer_cc where (user_id='".$customer_id."' or id in (select cc_id from customer_cc_linked where linked_user_id='".$customer_id."')) and id='".(int)$deposit_cc_id."'   ".$cc_filter." order by is_default desc limit 1");
						
						if(!$cc) exit('Deposit payment failed: invalid card');
						
						
					
									 
						$errmsg = '';
						$response = array();
						
						if($gate=='evo'){
							$pay_success = evoProcessPayment($customer_id, $customer_id,$cc,$deposit_amt,$errmsg, $response  );
						}else{
						$uinf['authnet_profile_id']=authNetCreateUserProfile($cc['user_id']);
						if($cc){
							$authnet_url = AUTHNET_TESTMODE?AUTHNET_TEST_URL:AUTHNET_URL;
							$authnet_name = AUTHNET_NAME;
							$authnet_key = AUTHNET_KEY;
							
							/*$req = array(
								'authenticateTestRequest' => array(
									'merchantAuthentication' => array(
										'name' => $authnet_name,
										'transactionKey' => $authnet_key
									)
								)
							);*/
							
							$req = array(
								'createTransactionRequest' => array(
									'merchantAuthentication' => array(
										'name' => $authnet_name,
										'transactionKey' => $authnet_key
									),
									'refId' => 'dp'.time(),
									'transactionRequest' => array(
										'transactionType' => 'authCaptureTransaction',
										'amount' => $deposit_amt,
										'profile' => array(
											'customerProfileId' => $uinf['authnet_profile_id'],
											'paymentProfile' =>array('paymentProfileId'=>$cc['authnet_pp_id'])
											 
										)
									)
								
								)
							); 
							 

							$ch = curl_init( $authnet_url );
							# Setup request to send json via POST.
							$payload = json_encode( $req  );
							curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
							curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
							# Return response instead of printing.
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							# Send request.
							$result = trim(curl_exec($ch));
							curl_close($ch);
							
							$result=explode('{',$result,2);
							$result='{'.$result[1];
							# Print response.
							/*echo "<pre>$payload</pre>";
							 echo "<pre>$result</pre>";
							 exit;*/
							$response = json_decode($result,true);
							 
							if(strtoupper($response['messages']['resultCode'])=='OK'){
								if((strtoupper($response['transactionResponse']['responseCode'])=='1' || strtoupper($response['transactionResponse']['responseCode'])=='4')  && empty($response['transactionResponse']['errors'])){
									$pay_success = true;
								}else{
									
									foreach($response['transactionResponse']['errors'] as $msg){
										 $errmsg.=$msg['errorText'].'<br/>';
									}
									if($errmsg=='') $errmsg='Error: Transaction declined.';
								}
							}else{
								
								foreach($response['transactionResponse']['errors'] as $msg){
									 $errmsg.=$msg['errorText'].'<br/>';
								}
								foreach($response['messages']['message'] as $msg){
									 $errmsg.=$msg['text'].'<br/>';
								}
							}
						}else $errmsg.='Error: Unknown credit card<br/>';
							
						}	
						if($errmsg!='') exit($errmsg);
					}
				}
				
				
				if($pay_success){ 
					
					DB::insert('salon_appointments_deposits',array(
						'salon_id' => SALON_ID,
						'user_id' => $customer_id,
						'appt_id' => 0,
						'deposit_date' => date('Y-m-d H:i:s'),
						'amount' => $deposit_amt,
						'transaction_info' => json_encode($response),
						'payment_cc_id' => $deposit_cc_id,
						'pay_method' => $deposit_paymethod,
						'notes' => $deposit_note
					));
					
					$deposit_id = DB::insert_id();
				
				}else{
					exit('Deposit payment failed!');
				}
				
			}

			DB::insert('salon_appointments',array(
				'salon_id' => SALON_ID,
				'customer_id' => $customer_id,
				'appt_date' => date('Y-m-d',strtotime($_POST['date'])),
				'checkout_book' => isset($_POST['checkout_book'])&&$_POST['checkout_book']?1:0,
				'visit_type_id' => isset($_POST['visit_type'])&&$_POST['visit_type']?(int)$_POST['visit_type']:0,
				'is_prebook' => $prebook,
				'deposit_amt' => $deposit_amt,
				'is_dpst_waved' => (int)$deposit_waived
			));
			$appt_id = DB::insert_id();
			$mail_add = '';
			log_event('create_appt', $appt_id, $customer_id, 0, 'on '.date('m/d/Y',strtotime($_POST['date'])));
			if($appt_id){
				if($deposit_id) DB::update('salon_appointments_deposits',array('appt_id'=>$appt_id)," id='".$deposit_id."' limit 1");

				if(isset($_POST['client_notes']) && $_POST['client_notes']!=''){

					DB::update('users',array(
						'guest_notes' => $_POST['client_notes']
					)," id='".(int)$customer_id."' limit 1");
				}

				$servlist = array();

				foreach($services as $k=>$serv){
					if($serv['service'] && $serv['provider']  ){
						$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price,ss.ep_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$serv['provider']."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$serv['service']."' limit 1");
						$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
						$is_employee = 0;
						$is_ep=0;
						if( $client['is_fnf'] && $serv_q['employee_price'] > 0 ){ $price = $serv_q['employee_price']; $is_employee=1; }
						if(isset($client['role_id']) &&  $client['role_id']!=3 && $serv_q['ep_price'] > 0 ){ $price = $serv_q['ep_price']; $is_ep=1; }
						//if(!getTimeSec($times[$k])) exit('An error occurred!');

						$serv_data = array(
							'appt_id' => $appt_id,
							'provider_id' => (int)$serv['provider'],
							'service_id'=> (int)$serv['service'],
							'service_time' => $serv['time'],
							'book_price' => (float) $price,
							'service_time_sec' => getTimeSec($serv['time']),
							'notes' => $notes,
							'is_employee' => $is_employee,
							'is_ep' =>$is_ep,
							'is_request' => (isset($_POST['is_request']) && $_POST['is_request'] ? 1 : 0 )
						);
						
						if($serv['equipment_id']>0) $serv_data['equipment_id'] = $serv['equipment_id'];
						
						if($serv['deposit_notes']!='') { 
							$serv_data['notes']+=($serv_data['notes']!=''?' ':'').$serv['deposit_notes'];
							
						}

						if($serv['dur']!=''){
							if(strpos($serv['dur'],',')!==false){
								$d = explode(',',$serv['dur']);
								if($d[0]>0){
									$serv_data['duration_changed'] = (int) $d[0] + (int) $d[1] + (int) $d[2];
									$serv_data['dur_appl_chng'] = (int) $d[0] ;
									$serv_data['dur_procs_chng'] = (int) $d[1];
									$serv_data['dur_finsh_chng'] = (int) $d[2];
								}
							}else{
								if($serv['dur']>0){
									$serv_data['duration_changed'] = (int) $serv['dur'];
								}
							}

							if($serv['dur_save'] && $serv_data['duration_changed']){

								$updata = array(
									'timing_min' => $serv_data['duration_changed'],
									'service_time_1' => (isset($serv_data['dur_appl_chng'])?$serv_data['dur_appl_chng']:0),
									'process_time' => (isset($serv_data['dur_procs_chng'])?$serv_data['dur_procs_chng']:0),
									'service_time_2' =>(isset($serv_data['dur_finsh_chng'])?$serv_data['dur_finsh_chng']:0),
								);
								$check = DB::get_row("select * from customer_services where customer_id='".(int)$customer_id."' and service_id='".(int)$serv['service']."' limit 1");
								if($check){
									DB::update('customer_services',$updata, " customer_id='".(int)$customer_id."' and service_id='".(int)$serv['service']."' limit 1");
								} else {
									$updata['customer_id']=(int)$customer_id;
									$updata['service_id']=(int)$serv['service'];
									DB::insert('customer_services',$updata);
								}
								log_event('updated_smart_booking', $appt_id, (int)$customer_id, (int)$serv['service']);
							}
						}

						$servlist[]=$serv_data;

						DB::insert('salon_appointments_services',$serv_data);

						$apt_serv_id = DB::insert_id();


						$stylist = DB::get_row("select u.*,pi.code_name,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$serv['provider']."' limit 1");
						if($stylist && $stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];

						log_event('add_service', $appt_id, $customer_id, $serv['service'], $serv['time'].' with '.$stylist['first_name']);

						$appt_serv_c = DB::get_row("select sas.service_time,ss.default_timing_min,ps.timing_min, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration,
						if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
						if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time, if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0)))) as process_time,
						if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0)))) as service_time_2 from  salon_appointments_services sas inner join salon_appointments sapp on sas.appt_id=sapp.id   inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id    where  sas.appt_serv_id='".(int)$apt_serv_id."' order by sas.service_time_sec ");


						$duration = $appt_serv_c['duration_changed'];
						if(!$duration) $duration = $appt_serv_c['timing_min'];
						if(!$duration) $duration = $appt_serv_c['default_timing_min'];
						if($appt_serv_c['process_time'] || $appt_serv_c['service_time_2']) $duration = $appt_serv_c['service_time_1']+$appt_serv_c['process_time']+$appt_serv_c['service_time_2'];

						$end = getEndTime_AMPM($appt_serv_c['service_time'],$duration);

						$mail_add.= "<br/>
Service Type: ".$serv_q['service_name']."<br/>
Stylist: ".$stylist['first_name']."<br/>
Time: ".$serv['time'].($end!=''?' - '.$end :'')."<br/>
Date: ".date('M d, Y',strtotime($_POST['date']))."<br/>";

						 
						include_once('includes/appointment_alerts.php');
						booked_appt_alert( $stylist['id'], $client['first_name'] . ' ' . $client['last_name'], $salon_info['name'], strtotime($_POST['date']), $serv['time'], $appt_id );
						 

					}
				}

				if($salon_info['enable_covid_form_notif'] && $salon_info['covid_form_notif_h']=='booking'){

					DB::insert('covid_forms', array( 'user_id' =>$customer_id,
										'appt_id' => $appt_id,
										'sent_time' => time() ) );
				}

				if(isset($_POST['stndappts']) && $_POST['stndappts']!=''){
					$stndappts = json_decode( $_POST['stndappts'], true);
					if($stndappts && is_array($stndappts) && isset($stndappts['bdates']) && count($stndappts['bdates'])>0){

						DB::update('salon_appointments',array(
							'is_standing' => $appt_id
						)," id='".(int)$appt_id."' limit 1");


						foreach($stndappts['bdates'] as $st_date){

							DB::insert('salon_appointments',array(
								'salon_id' => SALON_ID,
								'customer_id' => $customer_id,
								'appt_date' => date('Y-m-d',strtotime($st_date)),
								'checkout_book' => 0,
								'is_prebook' => $prebook,
								'is_standing' => $appt_id
							));
							$st_appt_id = DB::insert_id();

							log_event('create_appt', $st_appt_id, $customer_id, 0, '[standing] on '.date('m/d/Y',strtotime($st_date)));

							foreach($servlist as $servrow){
								$servrow['appt_id'] = $st_appt_id;
								DB::insert('salon_appointments_services',$servrow);
							}
						}

					}
				}

				$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
				$uinf = DB::get_row("select * from users where id='".$customer_id."' limit 1");

				$subj = 'Your appointment is booked!';

				$body = "Hi ".$uinf['first_name'].",<br/>
<br/>
Your Appointment at ".$salon_info['name']." has been booked:<br/>
".$mail_add."
<br/>
<a href='".B_URL."/booked'>Click here to view your Booked Services.</a>";

		if( $uinf['parent_user_id'] ){
			$p_u_info = DB::get_row("select * from users where id='".$uinf['parent_user_id']."' limit 1");
			$uinf['email']=$p_u_info['email'];
		}

		//mail($uinf['email'],$subj,$body,"Content-Type: text/html; charset=UTF-8\r\n".(MAIL_FROM?'From: '.MAIL_FROM:''));
		if(isset($_POST['mail_client']) && $_POST['mail_client']) sendEmail($uinf['email'],$subj,$body,true);
		//if(defined(APPT_BCC_EMAIL) && APPT_BCC_EMAIL!='') mail(APPT_BCC_EMAIL,$subj,$body,"Content-Type: text/html; charset=UTF-8\r\n".(MAIL_FROM?'From: '.MAIL_FROM:''));
		//if(defined(APPT_BCC_EMAIL) && APPT_BCC_EMAIL!='') sendEmail(APPT_BCC_EMAIL,$subj,$body,true);

					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'book',
						'details' => 'Appointment Booked'

					));

				if(isset($_REQUEST['wl_id'])){
					DB::delete('waitlist'," id='".(int)$_REQUEST['wl_id']."' and salon_id='".SALON_ID."' limit 1");
				}

				echo $appt_id;
				exit;
			}
		}
	}


	exit('An error occurred!');
}


if($action == 'bn2_time_selection'){

	$date_tm = strtotime($_POST['date']);
	$customer_id = (int)($_POST['customer_id']);

	$w_day = date('w',$date_tm);
	$w_num = date('W',$date_tm);
	$d_start = 0;
	$d_end = 0;
	$book_date = date('Y-m-d',$date_tm);

	$services = array();
	$serv_ids = array();
	$prov_ids = array();
	$prov_info = array();

	$prov_schedules = array();
	$equipment_ids = array();

	if(isset($_POST['serv_pairs']) && is_array($_POST['serv_pairs'])){
		foreach($_POST['serv_pairs'] as $srv){
			if($srv['service']>0){
				$serv_ids[]=(int)$srv['service'];
				if(!in_array($srv['provider'],$prov_ids)) $prov_ids[]=$srv['provider'];
				$s_inf =  DB::get_row("select if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, ifnull(ps.equipment_id,ss.equipment_id) as equipment_id,ifnull(ps.equipments_list,ss.equipments_list) as equipments_list, sc.booking_order from salon_services ss inner join provider_services ps on ps.service_id=ss.id left join customer_services cs on cs.customer_id='".(int)$customer_id."' and cs.service_id=ss.id left join service_categories sc on sc.id=ss.service_category_id  where ss.id='".(int)$srv['service']."' and ps.provider_id='".(int)$srv['provider']."' limit 1");
				if($s_inf){

					if($srv['dur']!=''){
						if(strpos($srv['dur'],',')!==false){
							$d = explode(',',$srv['dur']);
							if($d[0]>0){
								$s_inf['service_duration'] = (int) $d[0] + (int) $d[1] + (int) $d[2];
								$s_inf['service_time_1'] = (int) $d[0] ;
								$s_inf['process_time'] = (int) $d[1];
								$s_inf['service_time_2'] = (int) $d[2];
							}
						}else{
							if($srv['dur']>0){
								$s_inf['service_duration'] = (int) $srv['dur'];
								$s_inf['service_time_1'] = 0;
								$s_inf['process_time'] = 0;
								$s_inf['service_time_2'] = 0;
							}
						}
					}
					$eq_list = array();
					if($s_inf['equipment_id']){
						$eq_list = array($s_inf['equipment_id']);
						if($s_inf['equipments_list']!=''){
							$eq_list=explode(',',$s_inf['equipments_list']);
							foreach($eq_list as $k=>$v) $eq_list[$k]=(int)$v;
						}
						foreach($eq_list as $eqid) if(!in_array($eqid,$equipment_ids))$equipment_ids[]=$eqid;
						
					}
					$services[]=array(
						'info'=>$s_inf,
						'prov_id'=>$srv['provider'],
						'num'=>$srv['num'],
						'order'=>$s_inf['booking_order'],
						'equipment_id'=>$s_inf['equipment_id'],
						'equipments_list'=>$eq_list
					);
					
					
				}
			}
		}
	}

	if(count($services)>1){
		$sl = count($services);
		for($i=0;$i<$sl;$i++)
			for($j=$i+1;$j<$sl;$j++){
				if($services[$i]['order']>$services[$j]['order']){
					$tmp = $services[$i];
					$services[$i] = $services[$j];
					$services[$j] = $tmp;
				}
			}
	}

	$salon_closed = false;

	$salon_dates = array();
	$stylist_add_per = DB::query("select * from salon_adddays where salon_id='".SALON_ID."' and end_date>='".$book_date."'  ");
	while($r=DB::fetch_assoc($stylist_add_per)){
		$start_d = strtotime($r['start_date']);
		$end_d = strtotime($r['end_date']." 23:59:59");
		while($start_d<=$end_d){
			$salon_dates[date('Y-m-d',$start_d)]=$r;
			$start_d+=86400;
		}
	}




	$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");

	if(isset($salon_dates[$book_date])){
		$salon_h['is_open']=1;
		$salon_h['start_time']=$salon_dates[$book_date]['start_time'];
		$salon_h['end_time']=$salon_dates[$book_date]['end_time'];
	}
	$schq = null;
	if($salon_h){
		if($salon_h['is_open']){
			if($salon_h['end_time']=='00:00') $salon_h['end_time']='23:59';
			if($salon_h['end_time']=='12:00 AM') $salon_h['end_time']='11:59 PM';


		}else $salon_closed = true;
	}


	if(!$salon_closed){
		$salon_off_day = DB::get_row("select * from salon_offdays where salon_id='".SALON_ID."' and end_date>='".$book_date."' and start_date<='".$book_date."'");
		if($salon_off_day && !isset($salon_dates[$book_date]))  $salon_closed = true;
	}



	$salon_d_start = getTimeSec($salon_h['start_time']);
	$salon_d_end = getTimeSec($salon_h['end_time']);


	foreach($prov_ids as $provid){
		$stylist = DB::get_row("select u.*,pi.code_name, pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.transition_min from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$provid."' and pi.salon_id='".SALON_ID."' limit 1");
		if($stylist && $stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];
		$is_odd_week = $stylist['althern_sched'] && $w_num%2!=0;

		$prov_info[$provid]=$stylist;


		$add_dates = array();
		$add_dates_info = array();
		$stylist_add_per = DB::query("select * from provider_adddays where provider_id='".(int)$provid."' and end_date>='".date('Y-m-d')."' and salon_id='".SALON_ID."'");
		while($r=DB::fetch_assoc($stylist_add_per)){
			if($r['end_time']=='00:00') $r['end_time']='23:59';
			if($r['end_time']=='12:00 AM') $r['end_time']='11:59 PM';
			$start_d = strtotime($r['start_date']);
			$end_d = strtotime($r['end_date']." 23:59:59");
			while($start_d<=$end_d){
				$da = date('Y-m-d',$start_d);
				$add_dates[]=$da;
				$add_dates_info[$da]=$r;
				$start_d+=86400;
			}
		}

		$prov_avail = 1;


		if(!isset($add_dates_info[$book_date])){
			$stylist_off_per = DB::query("select * from provider_offdays where provider_id='".(int)$provid."' and end_date>='".$book_date."' and start_date<='".$book_date."' and salon_id='".SALON_ID."'");
			while($r=DB::fetch_assoc($stylist_off_per)){
				$prov_avail = 0;
			}
		}

		$schq = array();
		if($salon_h){
			if($salon_h['is_open']){
				$d_start = getTimeSec($salon_h['start_time']);
				$d_end = getTimeSec($salon_h['end_time']);

				$sch_index = ($is_odd_week?2:1);
				if($stylist['fut_enable_sched'] && strtotime($stylist['fut_date_sched'])<=strtotime($book_date)){
					if($stylist['fut_althern_sched'] && $w_num%2!=0) $sch_index = 4;
					else $sch_index = 3;
				}

				$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$provid."' and salon_id='".SALON_ID."' and sch_index='".$sch_index."' and weekday='".$w_day."'");

				if($schq){
				if($schq && ($schq['is_available'] || in_array($book_date,$add_dates))){
					if($schq['end_time']=='00:00') $schq['end_time']='23:59';
					if($schq['end_time']=='12:00 AM') $schq['end_time']='11:59 PM';

					if(isset($add_dates_info[$book_date])){
						if($add_dates_info[$book_date]['start_time']) $schq['start_time']=$add_dates_info[$book_date]['start_time'];
						if($add_dates_info[$book_date]['end_time']) $schq['end_time']=$add_dates_info[$book_date]['end_time'];
					}
					$s_start = getTimeSec($schq['start_time']);
					if($s_start>$d_start) $d_start=$s_start;
					$s_end = getTimeSec($schq['end_time']);
					if($s_end<$d_end) $d_end=$s_end;
				}else $prov_avail = 0;
				}else  if(isset($add_dates_info[$book_date])){
					$d_start = getTimeSec($salon_h['start_time']);
					$d_end = getTimeSec($salon_h['end_time']);
					if($add_dates_info[$book_date]['start_time']){
						$s_start = getTimeSec($add_dates_info[$book_date]['start_time']);
						if($s_start>$d_start) $d_start=$s_start;
					}
					if($add_dates_info[$book_date]['end_time']){
						$s_end = getTimeSec($add_dates_info[$book_date]['end_time']);
						if($s_end<$d_end) $d_end=$s_end;
					}
				}else $prov_avail = 0;

			}else $prov_avail = 0;
		}else $prov_avail = 0;

		$avail_times = array();
		$busy_times = array();
		$transit_times = array();
		$alert_times = array();
		if($prov_avail){
		foreach($times_list as $k=>$v){
			$t_sec = getTimeSec($k);
			if($t_sec>=$d_start && $t_sec<$d_end) $avail_times[$k]=$v;
		}

		if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
			$brk_check_change = DB::get_row("select * from provider_schedule_break_changes where salon_id='".SALON_ID."' and `date`='".addslashes($book_date)."' and provider_id='".(int)$provid."' limit 1");
			if($brk_check_change){
				if($brk_check_change['is_removed']){
					$schq['has_break']=0;
					$schq['brk_start_time']='';
					$schq['brk_end_time']='';
				}else{
					if($brk_check_change['end_time']=='00:00') $brk_check_change['end_time']='23:59';
					if($brk_check_change['end_time']=='12:00 AM') $brk_check_change['end_time']='11:59 PM';
					$schq['brk_start_time']=$brk_check_change['start_time'];
					$schq['brk_end_time']=$brk_check_change['end_time'];
				}
			}
			if($schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
				$brk_dur = (getTimeSec($schq['brk_end_time'])-getTimeSec($schq['brk_start_time']))/60;
				$extr_dur = getTimesArr($schq['brk_start_time'],$brk_dur);
				foreach($extr_dur as $stm) $busy_times[$stm]=1;
			}
		}


		$busyhours = DB::query("select sas.service_time,
		if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
		if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2,
		if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time,
		if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration
		from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id  left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id  where sas.provider_id='".(int)$provid."' and sapp.appt_date>='".addslashes($book_date)."' and sapp.appt_date<='".addslashes($book_date)." 23:59:59'");
		while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!='' /*&& isset($avail_times[$r['service_time']])*/){
			$r['service_time']=timeFix($r['service_time']);
			$busy_times[$r['service_time']]=1;
			if($r['service_duration']>15){
				if($r['process_time'] || $r['service_time_2']){
					$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
					if($calcdur>$r['service_duration']) $r['service_duration']=$calcdur;
				}
				if(!$stylist['double_book']){
					$r['service_time_1']=$r['service_duration'];
					$r['process_time']=0;
				}
				$extr_dur = getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']);
				//print_r($r);
				//print_r($extr_dur);
				//echo '<hr/>';
				foreach($extr_dur as $stm) $busy_times[$stm]=1;

				if($prov_info[$provid] && $prov_info[$provid]['transition_min']>0){
					$extr_dur = getTimesArr($r['service_time'],$r['service_duration']+$prov_info[$provid]['transition_min'],$r['service_time_1'],$r['process_time']);
					foreach($extr_dur as $stm) if(!isset($busy_times[$stm])) $transit_times[$stm]=1;
				}

			}

		}

		$stylist_off_h = DB::query("select * from provider_offhours where  `date`='".addslashes($book_date)."'  and provider_id='".(int)$provid."' and salon_id='".SALON_ID."'");
		while($r=DB::fetch_assoc($stylist_off_h)){
			$extr_dur =getTimesArr($r['start_time'],$r['duration']);

			foreach($extr_dur as $stm) $busy_times[$stm]=1;
		}

		$prov_schedules[$provid] = array('avail'=>$avail_times,'busy'=>$busy_times,'transit'=>$transit_times);
		}

	}

	$equipment_schedules = array();

	if(count($equipment_ids)){
		foreach($equipment_ids as $eq_id){

			$eq_info = DB::get_row("select * from salon_equipment where id='".$eq_id."' and salon_id='".SALON_ID."'");
			if($eq_info){
				$equipment_num = $eq_info['qty'];
				$equipment_busy_times = array();
				$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
		if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2,
		if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time,
		if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration  from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id left join customer_services cs on cs.customer_id=sapp.customer_id where sapp.salon_id='".SALON_ID."' and sapp.appt_date>='".addslashes($book_date)."' and sapp.appt_date<='".addslashes($book_date)." 23:59:59' and ifnull(sas.equipment_id,ifnull(ps.equipment_id,ss.equipment_id))='".$eq_id."'");
				while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!=''){
					if(!isset($equipment_busy_times[$r['service_time']])) $equipment_busy_times[$r['service_time']]=0;

					if($r['service_duration']>15){
						$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
						if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
						$r['service_time_1'] = $r['service_duration'];
						$r['process_time'] = 0;
						$extr_dur = array_unique(getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']));
						foreach($extr_dur as $stm){
							if(!isset($equipment_busy_times[$stm])) $equipment_busy_times[$stm]=0;
							$equipment_busy_times[$stm]++;
						}
					}else $equipment_busy_times[$r['service_time']]++;

				}
				$equipment_busy_times_new = array();
				foreach($equipment_busy_times as $k=>$v){
					if($v>=$equipment_num) $equipment_busy_times_new[$k]=1;
				}
				$equipment_busy_times=$equipment_busy_times_new;

				$equipment_schedules[$eq_id] = array(
					'busy' => $equipment_busy_times,
					'name' => $eq_info['equipment_name']
				);
			}
		}
	}


	if(count($services)>0){

		$def_time = $_POST['def_time'];

		function canServicesFit($start_time,&$serv_start_times,&$flag,&$err_msg){
			global $services,$prov_schedules,$salon_d_start,$salon_d_end,$equipment_schedules,$times_list,$eq_fit_ids;

			$can_fit = true;
			$is_overlap = false;
			$is_transit_overlap = false;
			$is_off_schedule = false;
			$is_equipment_overlap = false;
			$is_equipment_fit = false;
			$eq_fit_id = 0;
			$info_msg = '';

			$s_time = $start_time;

			foreach($services as $srv){
				$is_busy = isset($prov_schedules[$srv['prov_id']]['busy'][$s_time]);
				$is_avail = isset($prov_schedules[$srv['prov_id']]['avail'][$s_time]);
				if(!$is_avail) $is_off_schedule = true;
				if(!$is_busy) $is_overlap = true;

				if($srv['info']['process_time'] || $srv['info']['service_time_2']){
					$calcdur = $srv['info']['service_time_1']+$srv['info']['process_time']+$srv['info']['service_time_2'];
					if($calcdur>$srv['info']['service_duration']) $srv['info']['service_duration']=$calcdur;
				}

				if(!canFitInTime($srv['info']['service_duration'],$s_time, $srv['info']['service_time_1'],$srv['info']['process_time'],$prov_schedules[$srv['prov_id']]['avail'],$prov_schedules[$srv['prov_id']]['busy'])) $can_fit = false;
				elseif(count($prov_schedules[$srv['prov_id']]['transit'])){
					if(!canFitInTime($srv['info']['service_duration'],$s_time, $srv['info']['service_time_1'],$srv['info']['process_time'],$prov_schedules[$srv['prov_id']]['avail'],$prov_schedules[$srv['prov_id']]['transit'])){ $is_transit_overlap = true;    }
				}

				if($srv['equipment_id'] && count($srv['equipments_list'])>1){
					$eq_fit_id = 0;
					foreach($srv['equipments_list'] as $eqid){
						if(canFitInTime($srv['info']['service_duration'],$s_time, $srv['info']['service_time_1'],$srv['info']['process_time'],$times_list,$equipment_schedules[$eqid]['busy'])){
							$is_equipment_fit = true;
							$info_msg .= $equipment_schedules[$eqid]['name'].' is available at this time! ';
							$eq_fit_id = $eqid;
							break;
						}
					}
					if(!$is_equipment_fit){
						$is_equipment_overlap = true;
						$err_msg .= 'No equipment is available at this time! ';
					}
				}elseif($srv['equipment_id']){

					if(!canFitInTime($srv['info']['service_duration'],$s_time, $srv['info']['service_time_1'],$srv['info']['process_time'],$times_list,$equipment_schedules[$srv['equipment_id']]['busy'])){
						$is_equipment_overlap = true;
						$err_msg .= $equipment_schedules[$srv['equipment_id']]['name'].' is not available at this time! ';
					}else{
						$eq_fit_id = $eq_fit_id;
						$is_equipment_fit = true;
						$info_msg .= $equipment_schedules[$srv['equipment_id']]['name'].' is available at this time! ';
					}
				}

				$serv_start_times[$srv['num']]=$s_time;
				$eq_fit_ids[$srv['num']]=$eq_fit_id;

				$s_time = getSecTime(getTimeSec($s_time) + $srv['info']['service_duration']*60);
			}
			if($is_equipment_fit) $flag=8;
			if($is_equipment_overlap) $flag=7;
			if($is_transit_overlap) $flag=6;
			
			
			if($can_fit){
				if($flag=8 && $err_msg=='') $err_msg=$info_msg;
				return 1;
			}
			if($is_off_schedule) $flag=3;
			else if($is_overlap) $flag=5;
			else if($is_transit_overlap) $flag=6;
			else $flag=5;
			return 0;
		} //equipment_id


		$prov_names = array();
		foreach($prov_info as $r){
			if($r['code_name']!='') $r['first_name']=$r['code_name'];
			$stl_name = ucwords(strtolower($r['first_name']));
			$prov_names[] = $stl_name;
		}
		$provnum = count($prov_names);

		if($salon_closed){
			echo '<h3>Business is closed on this day.</h3>';
		}else{
			
			

		?>
		<h3><?php if($provnum==1) echo $prov_names[0]."'s Availability"; else echo implode(' & ',$prov_names); ?>
		<span class="provico" style="background:none;<?php /*if($provnum>1) echo 'margin-left:-'.($provnum*18).'px;'; */ ?>"><?php
		foreach($prov_info as $r){
			?><img src="<?php echo IMG_URL; ?>/<?php if($r['image_filename']){ ?>avatars/thumbs/<?php echo $r['image_filename']; ?><?php } else echo 'default_photo.png'; ?>" class="img-circle img-sm" <?php if($r['image_filename']){ echo 'style="border: 3px solid transparent;" '; } ?> alt=""><?php
		}
		?></span><div class="clear"></div></h3>
		<div>
			<div class="bn2_aval_btns">
				<?php
				foreach($times_list as $k=>$v){
					$t_sec = getTimeSec($k);
					if($t_sec<43200 && $t_sec>=$salon_d_start && $t_sec<$salon_d_end){

					$serv_start_times = array();
					$eq_fit_ids = array();
					$flag = 0;
					$err_msg = '';
					
					$isFit = canServicesFit($k,$serv_start_times,$flag,$err_msg);

				?><div class="time_itm<?php echo $isFit?' active':' busy'; ?>"><a href="javascript:;" data-flag="<?php echo $flag; ?>" onclick="setStartTime(this)"  data-time="<?php echo $k ; ?>" data-servtimes="<?php echo htmlspecialchars(json_encode($serv_start_times)); ?>" data-eqips="<?php echo htmlspecialchars(json_encode($eq_fit_ids)); ?>" class="<?php if($def_time==$k) echo 'selected'; if($err_msg!='' && $flag != 8) echo ' notav'; ?>" <?php if($err_msg!=''){ echo ' data-errmsg="'.htmlspecialchars($err_msg).'" title="'.htmlspecialchars($err_msg).'" data-ttip="1" '; } ?>><?php echo $k ; /*if($err_msg!='') echo '<b style="color:red">*</b>';*/ ?></a></div> <?php
					}

				}
		?>
				<?php
				foreach($times_list as $k=>$v){
					$t_sec = getTimeSec($k);
					if($t_sec>=43200 && $t_sec>=$salon_d_start && $t_sec<$salon_d_end){
					$serv_start_times = array();
					$flag = 0;
					$err_msg = '';
					$isFit = canServicesFit($k,$serv_start_times,$flag,$err_msg);
				?><div class="time_itm<?php echo $isFit?' active':' busy'; if($err_msg!='' && $flag != 8) echo ' notav'; ?>"><a href="javascript:;"  data-flag="<?php echo $flag; ?>"  onclick="setStartTime(this)"  data-time="<?php echo $k ; ?>" data-servtimes="<?php echo htmlspecialchars(json_encode($serv_start_times)); ?>" data-eqips="<?php echo htmlspecialchars(json_encode($eq_fit_ids)); ?>" class="<?php if($def_time==$k) echo 'selected'; if($err_msg!='' && $flag != 8) echo ' notav'; ?>" <?php if($err_msg!=''){ echo ' data-errmsg="'.htmlspecialchars($err_msg).'" title="'.htmlspecialchars($err_msg).'" data-ttip="1" '; } ?>><?php echo $k; /*if($err_msg!='') echo '<b style="color:red">*</b>';*/ ?></a></div> <?php
					}

				}
		?>
			</div>

			<div style="clear:both;"></div>
		</div><?php
		/*echo '<pre>';
		print_r($prov_schedules);
		echo '</pre>';*/

		}
	}
	exit;
}
if($action == 'bn2_get_stylists_for_service'){
	$serv_id = (int)$_GET['serv_id'];
	$curr_stylist = (int)$_GET['curr_stylist'];
	$curr_stylist_lvl = (int)$_GET['curr_stylist_lvl'];
	$resp = array('html'=>'','avail_levels'=>array());
	ob_start();
	if($serv_id){
		$s_inf = DB::get_row("select ss.* from salon_services ss  where ss.id='".(int)$serv_id."' and ss.salon_id='".SALON_ID."' limit 1");
		$service_def_price = $s_inf['default_pricing'];

		$levels_price = array();

		$stylists = DB::query("select u.*, ps.pricing,pi.code_name,pi.pricing_level from users u inner join provider_info pi on pi.user_id=u.id inner join provider_services ps on ps.provider_id=u.id where pi.salon_id='".SALON_ID."' and  u.role_id in (".BOOKABLE_ROLE_IDS.") and ps.service_id='".(int)$serv_id."'  and u.status=1 "/*.($role==2?" and u.id='".$user_id."' ":"")*/ /*. ($curr_stylist_lvl>-1?" and pi.pricing_level='".(int)$curr_stylist_lvl."' ":"")*/ ." order by pi.position_num");
		if(!$salon_info['solo_salon']) echo '<option value="">Provider</option>';
		while($r=DB::fetch_assoc($stylists)){
			$r['pricing_level']=(int)$r['pricing_level'];
			if(!in_array($r['pricing_level'],$resp['avail_levels'])) $resp['avail_levels'][]=$r['pricing_level'];
			if($curr_stylist_lvl>-1 && $r['pricing_level']!=$curr_stylist_lvl) continue;
			$serv_price = $service_def_price;
			if($r['pricing']>0){
				$serv_price = $r['pricing'];
			}elseif($r['pricing_level']){
				if(isset($levels_price[$r['pricing_level']])){
					$serv_price = $levels_price[$r['pricing_level']];
				}else{
					$prlvl = DB::get_row("select * from salon_services_prices where service_id='".(int)(int)$serv_id."' and level_id='".(int)$r['pricing_level']."' limit 1");
					if($prlvl && $prlvl['level_price']!=0){
						$serv_price = $prlvl['level_price'];
						$levels_price[$r['pricing_level']] = $prlvl['level_price'];
					}
				}
			}
			if($r['code_name']!='') $r['first_name']=$r['code_name'];
			?><option value="<?php echo $r['id']; ?>" <?php if($curr_stylist==$r['id']) echo ' selected '; ?> ><?php echo ucwords(strtolower($r['first_name'] )); echo ' ($'.number_format($serv_price,0).')'; ?></option> <?php
		}

	}
	$resp['html']=ob_get_contents();
	ob_end_clean();

	header('Content-Type: application/json');
	echo json_encode($resp);
	exit;
}
if($action == 'bn2_get_stylists_list'){
	$curr_stylist = (int)$_GET['curr_stylist'];
	$curr_stylist_lvl = (int)$_GET['curr_stylist_lvl'];
	if(1){

		$levels_price = array();

		$stylists = DB::query("select u.*,  pi.code_name,pi.pricing_level from users u inner join provider_info pi on pi.user_id=u.id  where pi.salon_id='".SALON_ID."' and  u.role_id in (".BOOKABLE_ROLE_IDS.")   and u.status=1 "/*.($role==2?" and u.id='".$user_id."' ":"")*/ . ($curr_stylist_lvl>-1?" and pi.pricing_level='".(int)$curr_stylist_lvl."' ":"") ." order by pi.position_num");
		echo '<option value="">Provider</option>';
		while($r=DB::fetch_assoc($stylists)){

			if($r['code_name']!='') $r['first_name']=$r['code_name'];
			?><option value="<?php echo $r['id']; ?>" <?php if($curr_stylist==$r['id']) echo ' selected '; ?> ><?php echo ucwords(strtolower($r['first_name'] ));   ?></option> <?php
		}

	}
	exit;
}
if($action == 'bn2_provider_selection'){

	$services = array();
	$serv_ids = array();

	$date = $_POST['date'];
	$time = $_POST['time'];
	$customer_id = (int)($_POST['customer_id']);

	if(isset($_POST['serv_pairs']) && is_array($_POST['serv_pairs'])){
		foreach($_POST['serv_pairs'] as $srv){
			if($srv['service']>0){
				$serv_ids[]=(int)$srv['service'];
				//$s_inf = DB::get_row("select ss.*,sc.booking_order from salon_services ss left join service_categories sc on sc.id=ss.service_category_id where ss.id='".(int)$srv['service']."' and ss.salon_id='".SALON_ID."' limit 1");
				$s_inf =  DB::get_row("select ss.*,if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, sc.booking_order from salon_services ss left join provider_services ps on ps.service_id=ss.id and ps.provider_id='".(int)$srv['provider']."' left join customer_services cs on cs.customer_id='".(int)$customer_id."' and cs.service_id=ss.id left join service_categories sc on sc.id=ss.service_category_id  where ss.id='".(int)$srv['service']."'  limit 1");
				if($s_inf){
					if($srv['dur']!=''){
						if(strpos($srv['dur'],',')!==false){
							$d = explode(',',$srv['dur']);
							if($d[0]>0){
								$s_inf['service_duration'] = (int) $d[0] + (int) $d[1] + (int) $d[2];
								$s_inf['service_time_1'] = (int) $d[0] ;
								$s_inf['process_time'] = (int) $d[1];
								$s_inf['service_time_2'] = (int) $d[2];
							}
						}else{
							if($srv['dur']>0){
								$s_inf['service_duration'] = (int) $srv['dur'];
								$s_inf['service_time_1'] = 0;
								$s_inf['process_time'] = 0;
								$s_inf['service_time_2'] = 0;
							}
						}
					}
					$services[]=array(
						'info'=>$s_inf,
						'prov_id'=>$srv['provider'],
						'num'=>$srv['num'],
						'dur_cust'=>$srv['dur'],
						'order'=>$s_inf['booking_order'],
						'level'=>(int)$srv['level'],
						'desired_start_time' => ''
					);
				}
			}
		}
	}

	if(count($services)>1){
		$sl = count($services);
		for($i=0;$i<$sl;$i++)
			for($j=$i+1;$j<$sl;$j++){
				if($services[$i]['order']>$services[$j]['order']){
					$tmp = $services[$i];
					$services[$i] = $services[$j];
					$services[$j] = $tmp;
				}
			}
	}
	if(count($services)>0){

		if($time!=''){
			$start_time = $time;

			foreach($services as $k=>$srv){
				$services[$k]['desired_start_time'] = $start_time;
				//echo $srv['desired_start_time'].', ';

				$start_time = getSecTime(getTimeSec($start_time) + $srv['info']['service_duration']*60);
			}

		}

		$d_start = 0;
		$d_end = 0;
		$date_tm = 0;
		$w_day = '';
		$w_num = '';
		$book_date = '';
		$prov_ids = array();
		$prov_data = array();

		$prov_schedules = array();
		$salon_closed = false;
		$salon_d_start = 0;
		$salon_d_end = 0;
		$salon_h = false;

		if($date!=''){

			$date_tm = strtotime($date);


			$w_day = date('w',$date_tm);
			$w_num = date('W',$date_tm);
			$book_date = date('Y-m-d',$date_tm);



			$salon_dates = array();
			$stylist_add_per = DB::query("select * from salon_adddays where salon_id='".SALON_ID."' and end_date>='".$book_date."'  ");
			while($r=DB::fetch_assoc($stylist_add_per)){
				$start_d = strtotime($r['start_date']);
				$end_d = strtotime($r['end_date']." 23:59:59");
				while($start_d<=$end_d){
					$salon_dates[date('Y-m-d',$start_d)]=$r;
					$start_d+=86400;
				}
			}




			$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");

			if(isset($salon_dates[$book_date])){
				$salon_h['is_open']=1;
				$salon_h['start_time']=$salon_dates[$book_date]['start_time'];
				$salon_h['end_time']=$salon_dates[$book_date]['end_time'];
			}
			$schq = null;
			if($salon_h){
				if($salon_h['is_open']){
					if($salon_h['end_time']=='00:00') $salon_h['end_time']='23:59';
					if($salon_h['end_time']=='12:00 AM') $salon_h['end_time']='11:59 PM';


				}else $salon_closed = true;
			}
			if(!$salon_closed){
				$salon_off_day = DB::get_row("select * from salon_offdays where salon_id='".SALON_ID."' and end_date>='".$book_date."' and start_date<='".$book_date."'");
				if($salon_off_day && !isset($salon_dates[$book_date]))  $salon_closed = true;
			}



			$salon_d_start = getTimeSec($salon_h['start_time']);
			$salon_d_end = getTimeSec($salon_h['end_time']);


		}
	?>
	<h3>Provider</h3>
	<div class="serv_code_pills">
		<?php
		$scp_c = 0;
		foreach($services as $serv){
			if($serv['prov_id']) continue;
			echo '<span '. ($scp_c==0?'class="active"':'') .' onclick="showServProvs(this, \'#providers_list_serv_'. $serv['info']['id'] .'\')">' . $serv['info']['service_code'] . '</span>';
			$scp_c = 1;
		} ?>
	</div>
	<div class="clear"></div>
	<?php

	if($salon_closed && $date_tm){
		echo '<h4>Business is closed on '.date('D m/d/Y',$date_tm).'</h4>';
	}else{
	$scpl_c = 0;
	foreach($services as $serv){
		if($serv['prov_id']) continue;
	?>
	<div class="provider_list <?php if($scpl_c==0){ echo 'active'; $scpl_c = 1; }  ?>" id="providers_list_serv_<?php echo $serv['info']['id']; ?>">
		<?php
		$stylists = DB::query("select u.*, ps.pricing,pi.code_name,pi.title,pi.pricing_level, pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id inner join provider_services ps on ps.provider_id=u.id where pi.salon_id='".SALON_ID."' and ps.service_id='".(int)$serv['info']['id']."'  and u.status=1 ".($serv['level']>-1?" and pi.pricing_level='".(int)$serv['level']."' ":"")." order by pi.position_num, u.id");

		while($stl=DB::fetch_assoc($stylists)){

			if(!isset($prov_data[$stl['id']])){
				$prov_data[$stl['id']] = array('active'=>1,'errmsg'=>'');
				$stylist=$stl;
				$is_odd_week = $stylist['althern_sched'] && $w_num%2!=0;
				$provid = $stl['id'];


				$add_dates = array();
				$add_dates_info = array();
				$stylist_add_per = DB::query("select * from provider_adddays where provider_id='".(int)$provid."' and end_date>='".date('Y-m-d')."' and salon_id='".SALON_ID."'");
				while($r=DB::fetch_assoc($stylist_add_per)){
					if($r['end_time']=='00:00') $r['end_time']='23:59';
					if($r['end_time']=='12:00 AM') $r['end_time']='11:59 PM';
					$start_d = strtotime($r['start_date']);
					$end_d = strtotime($r['end_date']." 23:59:59");
					while($start_d<=$end_d){
						$da = date('Y-m-d',$start_d);
						$add_dates[]=$da;
						$add_dates_info[$da]=$r;
						$start_d+=86400;
					}
				}

				$schq = array();
				if($salon_h){
					if($salon_h['is_open']){
						$d_start = getTimeSec($salon_h['start_time']);
						$d_end = getTimeSec($salon_h['end_time']);

						$sch_index = ($is_odd_week?2:1);
						if($stylist['fut_enable_sched'] && strtotime($stylist['fut_date_sched'])<=strtotime($book_date)){
							if($stylist['fut_althern_sched'] && $w_num%2!=0) $sch_index = 4;
							else $sch_index = 3;
						}

						$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$provid."' and salon_id='".SALON_ID."' and sch_index='".$sch_index."' and weekday='".$w_day."'");

						if($schq){
						if($schq && ($schq['is_available'] || in_array($book_date,$add_dates))){
							if($schq['end_time']=='00:00') $schq['end_time']='23:59';
							if($schq['end_time']=='12:00 AM') $schq['end_time']='11:59 PM';

							if(isset($add_dates_info[$book_date])){
								if($add_dates_info[$book_date]['start_time']) $schq['start_time']=$add_dates_info[$book_date]['start_time'];
								if($add_dates_info[$book_date]['end_time']) $schq['end_time']=$add_dates_info[$book_date]['end_time'];
							}
							$s_start = getTimeSec($schq['start_time']);
							if($s_start>$d_start) $d_start=$s_start;
							$s_end = getTimeSec($schq['end_time']);
							if($s_end<$d_end) $d_end=$s_end;
						}else{
							$prov_data[$stl['id']]['active']=0;
							$prov_data[$stl['id']]['errmsg']='Off schedule';
						}
						}else  if(isset($add_dates_info[$book_date])){
							$d_start = getTimeSec($salon_h['start_time']);
							$d_end = getTimeSec($salon_h['end_time']);
							if($add_dates_info[$book_date]['start_time']){
								$s_start = getTimeSec($add_dates_info[$book_date]['start_time']);
								if($s_start>$d_start) $d_start=$s_start;
							}
							if($add_dates_info[$book_date]['end_time']){
								$s_end = getTimeSec($add_dates_info[$book_date]['end_time']);
								if($s_end<$d_end) $d_end=$s_end;
							}
						}else{
							$prov_data[$stl['id']]['active']=0;
							$prov_data[$stl['id']]['errmsg']='Off schedule';
						}

					} else{
						$prov_data[$stl['id']]['active']=0;
						$prov_data[$stl['id']]['errmsg']='Salon is closed';
					}
				}else{
					$prov_data[$stl['id']]['active']=0;
					$prov_data[$stl['id']]['errmsg']='Salon is closed';
				}


				if($time!=''){
					$avail_times = array();
					$busy_times = array();
					$alert_times = array();

					foreach($times_list as $k=>$v){
						$t_sec = getTimeSec($k);
						if($t_sec>=$d_start && $t_sec<$d_end) $avail_times[$k]=$v;
					}

					if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
						$brk_check_change = DB::get_row("select * from provider_schedule_break_changes where salon_id='".SALON_ID."' and `date`='".addslashes($book_date)."' and provider_id='".(int)$provid."' limit 1");
						if($brk_check_change){
							if($brk_check_change['is_removed']){
								$schq['has_break']=0;
								$schq['brk_start_time']='';
								$schq['brk_end_time']='';
							}else{
								if($brk_check_change['end_time']=='00:00') $brk_check_change['end_time']='23:59';
								if($brk_check_change['end_time']=='12:00 AM') $brk_check_change['end_time']='11:59 PM';
								$schq['brk_start_time']=$brk_check_change['start_time'];
								$schq['brk_end_time']=$brk_check_change['end_time'];
							}
						}
						if($schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
							$brk_dur = (getTimeSec($schq['brk_end_time'])-getTimeSec($schq['brk_start_time']))/60;
							$extr_dur = getTimesArr($schq['brk_start_time'],$brk_dur);
							foreach($extr_dur as $stm) $busy_times[$stm]=1;
						}
					}


					$busyhours = DB::query("select sas.service_time,
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
					if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2,
					if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time,
					if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration
					from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id  left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id  where sas.provider_id='".(int)$provid."' and sapp.appt_date>='".addslashes($book_date)."' and sapp.appt_date<='".addslashes($book_date)." 23:59:59'");
					while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!='' /*&& isset($avail_times[$r['service_time']])*/){
						$r['service_time']=timeFix($r['service_time']);
						$busy_times[$r['service_time']]=1;
						if($r['service_duration']>15){
							if($r['process_time'] || $r['service_time_2']){
								$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
								if($calcdur>$r['service_duration']) $r['service_duration']=$calcdur;
							}
							if(!$stylist['double_book']){
								$r['service_time_1']=$r['service_duration'];
								$r['process_time']=0;
							}
							$extr_dur = getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']);
							//print_r($r);
							//print_r($extr_dur);
							//echo '<hr/>';
							foreach($extr_dur as $stm) $busy_times[$stm]=1;
						}

					}

					$stylist_off_h = DB::query("select * from provider_offhours where  `date`='".addslashes($book_date)."'  and provider_id='".(int)$provid."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_off_h)){
						$extr_dur =getTimesArr($r['start_time'],$r['duration']);

						foreach($extr_dur as $stm) $busy_times[$stm]=1;
					}

					$prov_schedules[$provid] = array('avail'=>$avail_times,'busy'=>$busy_times);
				}
			}


			if($stl['code_name']!='') $stl['first_name']=$stl['code_name'];
			$stl_name = ucwords(strtolower($stl['first_name']));
			$stl_price = $serv['info']['default_pricing'];
			if($stl['pricing']>0) $stl_price = $stl['pricing'];
			elseif($stl['pricing_level']){
				$prlvl = DB::get_row("select * from salon_services_prices where service_id='".(int)$serv['info']['id']."' and level_id='".(int)$stl['pricing_level']."' limit 1");
				if($prlvl && $prlvl['level_price']!=0){
					$stl_price = $prlvl['level_price'];
				}
			}
			if( $user['is_fnf'] && $serv['info']['employee_price'] > 0 ) $stl_price = $serv['info']['employee_price'];

			$is_avail = $prov_data[$stl['id']]['active'];
			$errmsg = $prov_data[$stl['id']]['errmsg'];

			if($serv['desired_start_time']!=''){
				$servdur = $serv['info']['service_duration'];
				$service_time_1 = $serv['info']['service_time_1'];
				$process_time = $serv['info']['process_time'];
				if($serv['dur_cust']==''){
					$s_inf =  DB::get_row("select ss.*,if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, sc.booking_order from salon_services ss left join provider_services ps on ps.service_id=ss.id and ps.provider_id='".(int)$stl['id']."' left join customer_services cs on cs.customer_id='".(int)$customer_id."' and cs.service_id=ss.id left join service_categories sc on sc.id=ss.service_category_id  where ss.id='".(int)$serv['service']."'  limit 1");

					if($s_inf){
						$newdur = $s_inf['service_duration'];
						if($s_inf['process_time']>0 || $s_inf['service_time_2']>0) $newdur = $s_inf['service_time_1']+$s_inf['process_time']+$s_inf['service_time_2'];
						$servdur = $newdur;
						$service_time_1 =  $s_inf['service_time_1'];
						$process_time =  $s_inf['process_time'];
					}
				}

				/*if($stl['id']==84344) {
					echo $servdur.','.$serv['desired_start_time'].', '.$service_time_1.','.$process_time.'<br/>';
					print_r($prov_schedules[$stl['id']]['busy']);

				}*/
				if(!canFitInTime($servdur,$serv['desired_start_time'], $service_time_1,$process_time,$prov_schedules[$stl['id']]['avail'],$prov_schedules[$stl['id']]['busy'])){
					$is_avail = 0;
					$errmsg = 'Not available at this time.';
				}
			}
		?>
		<a href="javascript:;" class="<?php if($stl['id']==$serv['prov_id']) echo 'selected'; if(!$is_avail) echo ' prov_disbl ttip'; ?>" <?php if(!$is_avail) echo ' title="'.htmlspecialchars($errmsg).'" '; ?> onclick="selectProvider(this,<?php echo $stl['id']; ?>,'<?php echo $serv['num']; ?>','<?php echo $serv['info']['id']; ?>')"><div class="img-circle img-lg"><?php if($stl['image_filename']){ ?><img src="<?php echo IMG_URL; ?>/<?php if($stl['image_filename']){ ?>avatars/thumbs/<?php echo $stl['image_filename']; ?><?php } else echo 'default_photo.png'; ?>" class="img-circle img-lg" alt=""><?php } else echo '<div class="icoinitials">'.($stl['first_name']!=''?$stl['first_name'][0]:'').($stl['last_name']!=''?$stl['last_name'][0]:'').'</div>'; ?></div><strong><?php echo $stl_name; ?></strong><br/>$<?php echo number_format($stl_price,2); ?></a>
		<?php } ?>

		<div style="clear:both;"></div>
	</div><?php

		if($time!='')  break;
	}
	}

	}
exit;
}

if($action == 'save_cat_bookorder'){
	$cats = $_POST['cats'];

	if(is_array($cats) && count($cats)){
		foreach($cats as $catid=>$catorder){
			DB::update('service_categories',array('booking_order'=>(int)$catorder),"  salon_id='".SALON_ID."' and id='".(int)$catid."' limit 1");
		}
	}

	exit;
}

if($action == 'send_bulk_sms'){
	$sms_info = DB::get_row("select * from sms_bulk where  id='".(int)$_GET['mid']."' and salon_id='".SALON_ID."' limit 1");
	if($sms_info){
		include('includes/functions_sms.php');
		if($sms_info['text_content']=='' && $sms_info['image_content']=='') exit('No text message content!');

		$sql = get_bulk_sms_users($sms_info,'count(distinct u.phone_clean) as cnt');
		$receivers = DB::get_row($sql);
		if(!$receivers['cnt']) exit('No clients selected!');

		$send_dt = time();

		if(isset($_GET['dt']) && $_GET['dt']!=''){
			$tmp = rtz_time(strtotime($_GET['dt'].' '.$_GET['time']));
			if($tmp<$send_dt) exit('Please select date and time in the future.');
			$send_dt=$tmp;
		}

		DB::update('sms_bulk',array(
			'status'=>'scheduled',
			'send_estimate'=>$receivers['cnt'],
			'send_datetime'=>date('Y-m-d H:i:s',$send_dt)

		)," id='".(int)$_GET['mid']."' and salon_id='".SALON_ID."' limit 1");

	}else exit('Error occured.');

	exit;
}
if($action == 'send_test_sms'){
	$sms_info = DB::get_row("select * from sms_bulk where  id='".(int)$_GET['mid']."' and salon_id='".SALON_ID."' limit 1");

	if($sms_info && $_GET['testphone']!=''){
		include('includes/functions_sms.php');
		$phones = explode(',',$_GET['testphone']);
		foreach($phones as $phone){
			$phone=trim($phone);
			sendSingleSms($phone, $sms_info, $salon_info);
		}
	}

	exit;
}


if($action == 'umodal_load_purch_services'){
	$uid = (int)$_GET['user_id'];
	$response = array('upcoming'=>'','history'=>'');
	if($uid){
		ob_start();
		$last_ticket_id = 0;
		//
		$appt_serv_q = DB::query("select sapp.appt_date, sapp.customer_id, sas.*, u.first_name, u.image_filename,ss.service_name, ss.default_pricing, ps.pricing, s.name as location_name, ss.default_timing_min, ps.timing_min  from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join salons s on s.id=sapp.salon_id where sapp.customer_id='".(int)$uid."' and sapp.is_paid=0 and sapp.appt_date>'".date('Y-m-d')."' and " . ( ( defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1 )? "sapp.salon_id in (".implode(',',ALL_SALON_IDS).")" : "sapp.salon_id='".SALON_ID."'" ) . " order by sapp.appt_date asc limit 50 ");
		while($r = DB::fetch_assoc($appt_serv_q)){

			$duration = $r['duration_changed'];
			if(!$duration) $duration = $r['timing_min'];
			if(!$duration) $duration = $r['default_timing_min'];

			$end = getEndTime_AMPM($r['service_time'],$duration);

		?>
			<?php if($last_ticket_id!=$r['appt_id']){
				if($last_ticket_id!=0) echo '<p>&nbsp;</p>';
				$last_ticket_id=$r['appt_id'];
			?>
			<span class="smlgrey"><a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" class="desk_only">Ticket # <?php echo $r['appt_id']; ?></a><span class="mobile_only_i" >Ticket # <?php echo $r['appt_id']; ?></span></span><br/>
			<?php } ?>
			<div class="serv_itm">
				<div class="btns_div"><?php
				?></div>
				<h2 style="font-size:20px;"><?php echo ucwords($r['service_name']); ?></h2>
				<?php echo date('M d, Y',strtotime($r['appt_date'])); ?> <span style="font-size: 32px; font-weight: bold;  line-height: 12px; position: relative; top: 5px;">&middot;</span> <?php echo $r['service_time']; if($end) echo ' - '.$end; ?><br/>
				with <a href="stylist_appointments.php?id=<?php echo $r['provider_id']; ?>" class="desk_only" ><?php echo $r['first_name']; ?></a><span class="mobile_only_i" ><strong><?php echo $r['first_name']; ?></strong> at <strong><?php echo $r['location_name']; ?></strong></span><br/>


			</div>
		<?php }

		$response['upcoming'] = ob_get_contents();
		ob_end_clean();



		ob_start();
		$last_ticket_id = 0;
		$appt_serv_q = DB::query("select sapp.appt_date, sapp.customer_id, sapp.discount, sapp.total_service, sapp.series_total, sapp.discount_promos,sapp.promos_breakdown, sas.*,sas.discount as item_discount, u.first_name, s.name as location_name, u.image_filename,ss.service_name, ss.default_pricing, ps.pricing, ss.default_timing_min, ps.timing_min  from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join salons s on s.id=sapp.salon_id  where  ((sapp.customer_id='".(int)$uid."' and sas.merged_cust_id is null) or sas.merged_cust_id='".(int)$uid."') and sapp.is_paid=1   and " . ( ( defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1 )? "sapp.salon_id in (".implode(',',ALL_SALON_IDS).")" : "sapp.salon_id='".SALON_ID."'" ) . " order by sapp.appt_date desc limit 10 ");
		while($r = DB::fetch_assoc($appt_serv_q)){

			$duration = $r['duration_changed'];
			if(!$duration) $duration = $r['timing_min'];
			if(!$duration) $duration = $r['default_timing_min'];

			$end = getEndTime_AMPM($r['service_time'],$duration);

			$net_price = $r['book_price'];

			if($r['used_series_id']>0){
				$ser_inf = DB::get_row("select * from salon_appointments_series where appt_series_id='".(int)$r['used_series_id']."' limit 1");
				if($ser_inf  ) {
					$net_price = $ser_inf['discount_price'];
					$price = $ser_inf['discount_price'];
				}
			}

			if($r['discount']>0 && $r['total_service']>0){
				//$perc = ($r['discount']/($r['total_service']+$r['series_total'] )) ;
				$s_disc = $r['item_discount'];
				$net_price-=$s_disc;
			}
			if($r['discount_promos']>0 && $r['promos_breakdown']!=''){
				$promos_breakdown = json_decode($r['promos_breakdown'],true);
				if(isset($promos_breakdown['s']) && $promos_breakdown['s'][$r['service_id']]){
					$s_disc += $promos_breakdown['s'][$r['service_id']];
					$net_price-=$promos_breakdown['s'][$r['service_id']];
				}
			}



		?>
			<?php if($last_ticket_id!=$r['appt_id']){
				if($last_ticket_id!=0) echo '<p>&nbsp;</p>';
				$last_ticket_id=$r['appt_id'];

			?>
			<span class="smlgrey"><a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" class="desk_only">Ticket # <?php echo $r['appt_id']; ?></a><span class="mobile_only_i" >Ticket # <?php echo $r['appt_id']; ?></span></span><br/>
			<?php } ?>
			<div class="serv_itm">
				<div class="btns_div"><?php
				?></div>
				<h2 style="font-size:20px;"><?php echo ucwords($r['service_name']); ?></h2>
				<?php echo date('M d, Y',strtotime($r['appt_date'])); ?> <span style="font-size: 32px; font-weight: bold;  line-height: 12px; position: relative; top: 5px;">&middot;</span> <?php echo $r['service_time']; if($end) echo ' - '.$end; ?><br/>
				with <a href="stylist_appointments.php?id=<?php echo $r['provider_id']; ?>" class="desk_only" ><?php echo $r['first_name']; ?></a><span class="mobile_only_i" ><strong><?php echo $r['first_name']; ?></strong> at <strong><?php echo $r['location_name']; ?></strong></span><br/>
				<?php if($net_price) echo '<span style="font-weight:bold;font-size:20px;">$'.number_format($net_price,2).'</span><br/>'; ?>


			</div>
		<?php }

		$response['history'] = ob_get_contents();
		ob_end_clean();

	}
	header('Content-type: application/json');
	echo json_encode($response);
	exit;
}
if($action == 'umodal_load_purch_products'){
	$uid = (int)$_GET['user_id'];
	if($uid){
		echo '<table class="umodal_services">';

		$appt_ret_q = DB::query("select sar.*, si.code, si.description, si.price as inv_price, sa.appt_date, sa.salon_id,  u.first_name from salon_appointments_retail sar inner join salon_appointments sa on sa.id=sar.appt_id left join salon_inventory si on si.id=sar.inv_id left join users u on u.id=sar.stylist_id where " . ( ( defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1 )? "sa.salon_id in (".implode(',',ALL_SALON_IDS).")" : "sa.salon_id='".SALON_ID."'" ) . " and ((sa.customer_id='".$uid ."' and sar.merged_cust_id is null)  or sar.merged_cust_id='".$uid ."') and sa.is_paid=1 order by sar.appt_retail_id desc limit 50");
		while($r = DB::fetch_assoc($appt_ret_q)){
			$price = $r['price'];
			if(!$price) $price = $r['inv_price'];
		?>
			<tr>
				 <td width="30%"><h2><?php echo date('m/d/Y',strtotime($r['appt_date'])); ?></h2><?php if($r['stylist_id']){ ?><a href="stylist_appointments.php?id=<?php echo $r['stylist_id']; ?>" ><?php echo $r['first_name']; ?></a><?php } ?> </td>
				<td width="67%"><h2><?php echo $r['description']; ?></h2><?php if($r['qty']>1) echo $r['qty'].'x '; ?>$<?php echo number_format($price,2);?> </td>
				<td width="3%" style="vertical-align: middle;" class="umod_apb"><?php if(isset($_GET['appt_id'])&&$_GET['appt_id'] && $r['salon_id'] == SALON_ID ){ ?><a href="javascript:;" onclick="umodal_add_product(<?php echo $r['inv_id']; ?>,<?php echo (int)$_GET['appt_id']; ?>,'<?php echo addslashes($r['description']); ?>')" class="umod_addprod"><i class="las la-plus-circle"></i></a><?php } ?></td>
			</tr>
		<?php }






		echo '</table>';
	}
	exit;
}

if($action == 'do_appt_merge'){
	$main_appt_id = (int) $_POST['main_appt_id'];
	$appt_ids = $_POST['appt_ids'];
	if($main_appt_id>0 && $appt_ids!=''){
		$appt_ids=explode(',',$appt_ids);

		$main_appt = DB::get_row("select * from salon_appointments sa  where sa.id='".(int)$main_appt_id ."' and sa.salon_id='".SALON_ID."' and sa.is_paid=0 limit 1");

		if($main_appt){
			$merged_ids = array();
			$discount = $main_appt['discount']>0?$main_appt['discount']:0;
			$discount_retail = $main_appt['discount_retail'];
			$discount_promos = $main_appt['discount_promos'];
			$promos_breakdown = array();
			if($main_appt['promos_breakdown']!='') $promos_breakdown = json_decode($main_appt['promos_breakdown'],true);
			$deposit_amt = $main_appt['deposit_amt'];
			if($main_appt['merged_id_list']!='') $merged_ids =explode(',',$main_appt['merged_id_list']);
			foreach($appt_ids as $aid){
				if($aid==$main_appt_id) continue;
				$appt = DB::get_row("select * from salon_appointments sa  where sa.id='".(int)$aid ."' and sa.salon_id='".SALON_ID."' and sa.is_paid=0 limit 1");
				if($appt){
					$servq = DB::query("select * from salon_appointments_services where appt_id='".$appt['id']."' ");
					while($r=DB::fetch_assoc($servq)){
						unset($r['appt_serv_id']);
						$remkeys=array();
						foreach($r as $k=>$v) if($v===null) $remkeys[]=$k;
						foreach($remkeys as $k) unset($r[$k]);
						$r['appt_id'] = $main_appt_id;
						$r['merged_appt'] =$appt['id'];
						$r['merged_cust_id'] =$appt['customer_id'];
						DB::insert('salon_appointments_services',$r);
					}
					$servq = DB::query("select * from salon_appointments_retail where appt_id='".$appt['id']."' ");
					while($r=DB::fetch_assoc($servq)){
						unset($r['appt_retail_id']);
						$remkeys=array();
						foreach($r as $k=>$v) if($v===null) $remkeys[]=$k;
						foreach($remkeys as $k) unset($r[$k]);
						$r['appt_id'] = $main_appt_id;
						$r['merged_appt'] =$appt['id'];
						$r['merged_cust_id'] =$appt['customer_id'];
						DB::insert('salon_appointments_retail',$r);
					}

					$servq = DB::query("select * from gift_cards where appt_id='".$appt['id']."' ");
					while($r=DB::fetch_assoc($servq)){
						$gs_id = $r['id'];
						DB::update('gift_cards',array('card_no'=>$r['card_no'].'--merged'.rand(1000,10000))," id='".$gs_id."' limit 1");
						unset($r['id']);
						$remkeys=array();
						foreach($r as $k=>$v) if($v===null) $remkeys[]=$k;
						foreach($remkeys as $k) unset($r[$k]);
						$r['appt_id'] = $main_appt_id;
						$r['merged_appt'] =$appt['id'];
						$r['merged_cust_id'] =$appt['customer_id'];
						DB::insert('gift_cards',$r);


					}

					$servq = DB::query("select * from salon_appointments_promos where appt_id='".$appt['id']."' ");
					while($r=DB::fetch_assoc($servq)){
						$check = DB::get_row("select * from salon_appointments_promos where appt_id='".(int)$main_appt_id."' and promo_id='".$r['promo_id']."' limit 1 ");
						if(!$check){
							unset($r['appt_promo_id']);
							$r['appt_id'] = $main_appt_id;
							$remkeys=array();
							foreach($r as $k=>$v) if($v===null) $remkeys[]=$k;
							foreach($remkeys as $k) unset($r[$k]);
							DB::insert('salon_appointments_promos',$r);
						}
					}

					$servq = DB::query("select * from salon_appointments_tips where appt_id='".$appt['id']."' ");
					while($r=DB::fetch_assoc($servq)){
						$check = DB::get_row("select * from salon_appointments_tips where appt_id='".(int)$main_appt_id."' and stylist_id='".$r['stylist_id']."' limit 1 ");
						if(!$check){
							unset($r['appt_tip_id']);
							$r['appt_id'] = $main_appt_id;
							$remkeys=array();
							foreach($r as $k=>$v) if($v===null) $remkeys[]=$k;
							foreach($remkeys as $k) unset($r[$k]);
							DB::insert('salon_appointments_tips',$r);
						}else{
							DB::update('salon_appointments_tips',array('amount'=>$r['amount']+$check['amount']), " appt_id='".(int)$main_appt_id."' and stylist_id='".$r['stylist_id']."' limit 1");
						}
					}

					if($appt['promos_breakdown']!=''){
						$pr_br = json_decode($appt['promos_breakdown'],true);
						if(is_array($pr_br)){
							if(isset($pr_br['r'])){
								if(!isset($promos_breakdown['r'])) $promos_breakdown['r']=array();
								foreach($pr_br['r'] as $i_id=>$i_val){
									if(!isset($promos_breakdown['r'][$i_id])) $promos_breakdown['r'][$i_id]=0;
									$promos_breakdown['r'][$i_id]+=$i_val;
								}
							}
							if(isset($pr_br['s'])){
								if(!isset($promos_breakdown['s'])) $promos_breakdown['s']=array();
								foreach($pr_br['s'] as $i_id=>$i_val){
									if(!isset($promos_breakdown['s'][$i_id])) $promos_breakdown['s'][$i_id]=0;
									$promos_breakdown['s'][$i_id]+=$i_val;
								}
							}
						}
					}


					$discount += $appt['discount']>0?$appt['discount']:0;
					$discount_retail += $appt['discount_retail'];
					$discount_promos += $appt['discount_promos'];
					$merged_ids[]=$appt['id'];
					$deposit_amt += $appt['deposit_amt'];

					DB::update('salon_appointments',array('merged_with'=>(int)$main_appt_id )," id='".(int)$appt['id']."' and salon_id='".SALON_ID."' limit 1");
				}
			}


			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1,'discount'=>$discount,'discount_retail'=>$discount_retail,'discount_promos'=>$discount_promos,'promos_breakdown'=>json_encode($promos_breakdown),'deposit_amt'=>$deposit_amt,'merged_id_list'=>implode(',',$merged_ids))," id='".(int)$main_appt_id."' and salon_id='".SALON_ID."' limit 1");
		}

	}


	exit;
}
if($action == 'get_merge_text'){
	$appt_ids = $_POST['appt_ids'];

	$clients = array();
	$appts = array();
	$appts_h = array();
	$appt_dates = array();
	$appt_cust = array();

	foreach($appt_ids as $aid){
		$appt = DB::get_row("select sa.id,sa.customer_id, u.first_name,u.last_name, sa.appt_date from salon_appointments sa inner join users u on u.id=sa.customer_id where sa.id='".(int)$aid."' and sa.salon_id='".SALON_ID."' and sa.is_paid=0 limit 1");
		if($appt){
			$appts[] = $appt['id'];
			$appts_h[] = '#'.$appt['id'];
			if(!isset($clients[ $appt['customer_id']])){
				$clients[ $appt['customer_id']]=$appt['first_name'].' '.$appt['last_name'];
				$appt_cust[ $appt['customer_id']]=$appt['id'] ;
			}
			$appt_dates[date('Y-m-d',strtotime( $appt['appt_date']))]=1;
		}
	}

	if(count($appt_dates)>1){
		echo '<p style="color:red;">You cannot merge appointments from different dates.</p>';
		exit;
	}
	if(count($appts)<=1){
		echo '<p style="color:red;">Nothing to merge.</p>';
		exit;
	}

	echo '<p style="font-size:20px;">'.implode(', ',$appts_h).'</p>';
	echo '<p><b>THE PAYING CUSTOMER IS</b></p>';
	echo '<p><span style="display:inline-block;width:250px;"><select class="mrg_main_appt">';
	foreach($clients as $cid=>$cname){
		echo '<option value="'.$appt_cust[$cid].'">'.$cname.'</option>';
	}
	echo '</select></span></p>';
	echo '<input type="hidden" class="mrg_appt_list" value="'.implode(',',$appts).'" />';

	exit;
}
if($action == 'save_cat_name'){
	$errors = [];
	
	// Repairing user mistakes in real number format (if use ',' instead '.')
	$_POST['tax'] = str_ireplace(',', '.', $_POST['tax']);
	
	if( $_POST['catid'] <= 0 ){ $errors[] = 'Missing category ID'; }
	if( $_POST['name'] == '' ){ $errors[] = 'Category name is required field!'; }
	if( $_POST['tax'] == ''  || $_POST['tax'] >= 100 || $_POST['tax'] < 0){ $errors[] = 'Tax must be from 0 to 99.999'; }
	
	if(empty($errors)){
		$cat = DB::get_row("select * from service_categories where id='".(int)$_POST['catid']."' and  salon_id='".SALON_ID."' limit 1");
		if($cat){
			DB::update('service_categories',array('category_name'=>$_POST['name'], 'sales_tax'=>$_POST['tax']),"id='".(int)$_POST['catid']."' and  salon_id='".SALON_ID."' limit 1");
			echo 'success';
		} else { echo 'Unknown Error #'.__LINE__; }
	} else { echo implode(PHP_EOL, $errors); }
	exit;
}

if($action == 'get_inv_stats'){

	$inv_stats=array();
	$stats_q = DB::query("select `type`, sum(cost*on_hand) as total from salon_inventory where salon_id='".SALON_ID."' and active=1 group by `type`");
	while($r=DB::fetch_assoc($stats_q )){
		$key = strtolower(trim($r['type']));
		if(!isset($inv_stats[$key])) $inv_stats[$key]=0;
		$inv_stats[$key]+=$r['total'];
	}

	$response=array(
		'retail' => '0.00',
		'professional' => '0.00'
	);

	if(isset($inv_stats['retail']) && $inv_stats['retail']) $response['retail'] = number_format($inv_stats['retail'],2);
	if(isset($inv_stats['professional']) && $inv_stats['professional']) $response['professional'] = number_format($inv_stats['professional'],2);

	header('Content-type: application/json');
	echo json_encode($response);
	exit;
}

if($action == 'get_supplier_products'){
	$response = array(
		'products'=>array(),
		'brands' => array(),
		'lines' => array(),
		'categories' => array()
	);

	$brandsq = DB::query("select distinct brand from salon_inventory where salon_id='".SALON_ID."' and supplier_id='".addslashes($_POST['supplier'])."' order by brand");
	while($rb = DB::fetch_assoc($brandsq)){
		if(trim($rb['brand'])=='') continue;
		$response['brands'][] = $rb['brand'];
	}
	$brandsq = DB::query("select distinct category from salon_inventory where salon_id='".SALON_ID."' and supplier_id='".addslashes($_POST['supplier'])."' order by category");
	while($rb = DB::fetch_assoc($brandsq)){
		if(trim($rb['category'])=='') continue;
		$response['categories'][] = $rb['category'];
	}
	$brandsq = DB::query("select distinct line from salon_inventory where salon_id='".SALON_ID."' and supplier_id='".addslashes($_POST['supplier'])."' order by line");
	while($rb = DB::fetch_assoc($brandsq)){
		if(trim($rb['line'])=='') continue;
		$response['lines'][] = $rb['line'];
	}

	// UPC	BRAND	PRODUCT	UNIT COST	OH	MAX	MIN
	$productq = DB::query("select id,description,code,altern_codes,sku,altern_skus,brand,line,category,price,cost,on_hand,minimum,maximum,type from salon_inventory where salon_id='".SALON_ID."' and supplier_id='".addslashes($_POST['supplier'])."' and active=1 order by description");
	while($r = DB::fetch_assoc($productq)){
		$r['type']=ucwords($r['type']);
		$r['type_l']=strtolower($r['type']);
		$response['products'][] = $r;
	}

	header('Content-type: application/json');
	echo json_encode($response);
	exit;
}
if($action == 'appt_resend_receipt'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if($appt){
		$u_info = DB::get_row("select * from users where id='".$appt['customer_id']."' limit 1");
		$appt_serv_q = DB::query("select sapp.appt_date, sapp.customer_id, sas.*, u.first_name, u.image_filename,ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ps.pricing from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id  left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where  sas.appt_id='".(int)$appt_id."' ");

		$retail_prods = DB::query("select sar.*, si.code, si.description, si.price as inv_price from salon_appointments_retail sar left join salon_inventory si on si.id=sar.inv_id where sar.appt_id='".(int)$appt_id."'");
		ob_start();
		include('includes/emailreceipt.php');
		$mail_cont = ob_get_contents();
		ob_end_clean();



		if($appt['payment_cc_id']){
			$cc = DB::get_row("select * from customer_cc where id='".(int)$appt['payment_cc_id']."' limit 1");
			if($cc && $cc['user_id']!=$u_info['id']){
				$cc_u_info = DB::get_row("select email from users where id='".$cc['user_id']."' limit 1");
				if($cc_u_info && $cc_u_info['email']!='') $u_info['email']=$cc_u_info['email'];
			}
		}
		//mail($u_info['email'],'Your receipt',$mail_cont,"Content-type: text/html"."\r\nFrom: ".MAIL_FROM);
		if($u_info['email']!=''){
			sendEmail($u_info['email'],'Your receipt',$mail_cont,true);

		}
	}
	exit;
}

if($action == 'set_client_notes'){

	$uinfo = DB::get_row("select * from users where id='".(int)$_GET['client_id']."' limit 1");
	if($uinfo){
		$loc_ids = array(SALON_ID);
		if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1){
			$loc_ids = ALL_SALON_IDS;
		}
		$check = DB::get_row("select * from salon_customers where salon_id in (".implode(',',$loc_ids).") and customer_id='".(int)$_GET['client_id']."' limit 1");
		if($check){
			DB::update('users',array('guest_notes'=>$_POST['notes']),"  id='".(int)$_GET['client_id']."' limit 1 ");
			log_event('updated_client_note', 0, (int)$_GET['client_id'], 0, $uinfo['first_name'].' '.$uinfo['last_name']);
		}
		//echo $uinfo['guest_notes'];
	}
	exit;
}
if($action == 'get_client_notes'){

	$uinfo = DB::get_row("select * from users where id='".(int)$_GET['client_id']."' limit 1");
	if($uinfo){
		echo $uinfo['guest_notes'];
	}
	exit;
}
if($action == 'set_refund_info'){

	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit(); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit(); }

	$data = $_POST['data'];
	log_event('refund_ticket', $appt['id'],  (int) $appt['customer_id'], (int) $appt['service_id']);
	DB::update('salon_appointments',array('refund_last_data'=>$data)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
}

if($action == 'giftcard_usage'){

	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;

	if(isset($_GET['gcid']) && $_GET['gcid']>0){
		$gcard = DB::get_row("select gc.* from gift_cards gc inner join salon_appointments sa on sa.id=gc.appt_id  where gc.salon_id in (".implode(',',$salon_ids).") and sa.is_paid=1  and gc.id='".(int)$_GET['gcid']."'");
		if($gcard){


			$log_changes = array();
			$balance_changes = array();
			$log = DB::query("select l.*,u.first_name,u.last_name from giftcard_log l left join users u on u.id=l.user_id where l.giftcard_id='".$gcard['id']."' order by l.id asc ");

			if(DB::num_rows($log)){
				while($r=DB::fetch_assoc($log)){
					$log_changes[]=$r;

					$event_type = $r['event_action'];
					if($event_type == 'change_balance'){
						$balance_changes[strtotime($r['event_time'])]=$r['gf_balance'];
					}
				}
			}

			$output_arr = array();
			$output_arr['heading1'] = '<thead>
				<tr>
					<th class="ltr">Ticket #</th>
					<th  class="ltr">Date</th>
					<th  class="ltr">Client</th>
					<th  class="rtr">Amount</th>
					<th  class="rtr">Balance</th>
				</tr></thead><tbody class="gccontent">';
			$usage = DB::query("select sa.id,sa.appt_date,sa.redeemed_details,sa.date_paid, u.first_name,u.last_name from salon_appointments sa inner join users u on u.id=sa.customer_id where sa.salon_id in (".implode(',',$salon_ids).") and sa.is_paid=1 and sa.redeemed_amount_total>0 and redeemed_details like '%\"gc_id\":\"".$gcard['id']."\"%' order by sa.appt_date asc");
			if(DB::num_rows($usage)){
				$curr_balance = $gcard['card_amount'];
				while($r=DB::fetch_assoc($usage)){
					if(!empty($balance_changes)){
						$captdate = strtotime($r['date_paid']?$r['date_paid']:$r['appt_date']);
						$unsetkeys = array();
						foreach($balance_changes as $btime=>$balance){
							if($btime<=$captdate){
								$curr_balance=$balance;
								$unsetkeys[]=$btime;
								//$output_arr[$btime] = '<tr><td colspan="5">apply '.$balance.' - '.$r['date_paid'].'/'.$r['appt_date'].'</td></tr>';
							}
						}
						if(!empty($unsetkeys)){
							foreach($unsetkeys as $k) unset($balance_changes[$k]);
						}
					}
					$amt = 0;
					$redeemed_details = json_decode($r['redeemed_details'],true);
					foreach($redeemed_details as $k=>$v){
						if(strpos($k,'giftcard')!==false && $v['gc_id']==$gcard['id']){
							$amt+=$v['amount'];
						}
					}
					$key = date('YmdHis',strtotime($r['appt_date'])).rand();
					$curr_balance-=$amt;
					$output_arr[$key]= '<tr>
					<td class="ltr"><a href="transactions.php?appt_id='.$r['id'].'" target="_blank">'.$r['id'].'</a></td>
					<td  class="ltr">'.date('m/d/Y',strtotime($r['appt_date'])).'</td>
					<td  class="ltr">'.$r['first_name'].' '.$r['last_name'].'</td>
					<td  class="rtr">$'.number_format($amt,2).'</td>
					<td  class="rtr">$'.number_format($curr_balance,2).'</td>
				</tr>';
				}

			}else $output_arr['notickets']='<tr><td colspan="5"><br/>No usage information.<br/><br/></td></tr>';
			$output_arr['heading12'] = '</tbody>';

			if(!empty($log_changes)){
				$output_arr['heading2'] = '<thead>
				<tr>
					<th class="ltr">Action</th>
					<th  class="ltr">Date</th>
					<th  class="ltr">Employee</th>
					<th  class="ltr">Details</th>
					<th  class="rtr">Balance</th>
				</tr></thead><tbody class="gccontent">';
				foreach($log_changes as $r){
					$ttip = '';
					if(strlen($r['details'])>16) {
						$ttip = $r['details'];
						$r['details']=substr($r['details'],0,16).'...';
					}
					$key = date('YmdHis',strtotime($r['event_time'])).rand();
					$event_type = $r['event_action'];
					if($event_type == 'change_comment') $event_type='Comment Change';
					elseif($event_type == 'change_balance') $event_type='Balance Updated';
					$output_arr[$key]= '<tr>
					<td class="ltr">'.$event_type.'</td>
					<td  class="ltr">'.date('m/d/Y',strtotime($r['event_time'])).'</td>
					<td  class="ltr">'.$r['first_name'].' '.$r['last_name'].'</td>
					<td  class="ltr"><span'.($ttip!=''?' class="ttip" title="'.htmlspecialchars($ttip).'"':'').'>'.$r['details'].'</span></td>
					<td  class="rtr">$'.number_format($r['gf_balance'],2).'</td>
				</tr>';
				}
				$output_arr['heading22'] = '</tbody>';
			}

			if(!empty($output_arr)){
				//ksort($output_arr);

				foreach($output_arr as $v) echo $v;

				exit;
			}
		}
	}

	echo '<tr><td colspan="5"><br/>No usage information.</td></tr>';
	exit;
}

if($action == 'get_ticket_activity'){

	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit(); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit(); }

	$last_activity = array();
	if($appt['last_activity']!='') $last_activity = json_decode($appt['last_activity'],1);
	if(!is_array($last_activity)) $last_activity=array();

	$last_activity[$user_id]=array('t'=>time(),'n'=>$user['first_name']);

	DB::update('salon_appointments',array('last_activity'=>json_encode($last_activity))," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");

	$tlim = time()-5;
	$last_active = array();
	foreach($last_activity as $uids=>$dt){
		if($uids==$user_id) continue;
		if($dt['t']>=$tlim ) $last_active[]=$dt['n'];
	}

	if(!empty($last_active)){
		if(count($last_active)==1) exit('<strong>'.$last_active[0].'</strong> is still working.');
		else exit('<strong>'.implode(', ',$last_active).'</strong> are still working.');
	}



	exit;
}
if($action == 'set_station'){
	$_SESSION['station']=$_GET['station'];
	exit;
}
if($action == 'create_category'){
	if($role==1 && $_POST['name']!=''){
		$catname = trim($_POST['name']);
		$parent = (int)$_POST['parent'];
		if($catname!=''){
			$check = DB::get_row("select * from service_categories where salon_id='".SALON_ID."' and category_name='".addslashes($catname)."' and parent_id='".(int)$parent."' limit 1");
			if(!$check){
				DB::insert('service_categories',array(
					'category_name'=>$catname,
					'salon_id' => SALON_ID,
					'parent_id' => $parent,
					'visible' => 0,
					'disabled' => 1
				));
			}else exit('Category "'.$catname.'" already exists!');
		}
	}
	exit;
}
if($action == 'create_service'){
	//pre_print($_POST); exit;  
	if($_POST['category_id'] && $_POST['name']!=''){
		$_POST['name']=trim($_POST['name']);
		$check = DB::get_row("select * from salon_services where salon_id='".SALON_ID."' and service_name='".addslashes($_POST['name'])."' limit 1");
		if($check) exit("Service ".$_POST['name']." already exists!");
		if($_POST['code']!=''){
			$check = DB::get_row("select * from salon_services where salon_id='".SALON_ID."' and service_code='".addslashes($_POST['code'])."' limit 1");
			if($check) exit("Service with code ".$_POST['code']." already exists!");
		}
		
		$equipment_id = null;
		$equipments_list = null;
		if($_POST['equipments_list'] && $_POST['equipments_list']!=''){
			$equipments_list = (string) $_POST['equipments_list'];
			$eq_l = explode(',',$equipments_list);
			$equipment_id = (int) $eq_l[0];
		}

		$serv_data = array(
			'salon_id' => SALON_ID,
			'service_category_id' => (int)$_POST['category_id'],
			'service_name' => $_POST['name'],
			'service_code' => $_POST['code'],
			'default_pricing' => (float)$_POST['price'],
			'default_timing_min' => (int)$_POST['duration'],
			'labor_fee' => (float)$_POST['labor_fee'],
			'tax' => (float)$_POST['tax'],
			'default_fee_perc' => (float)$_POST['fee'],
			'deposit' => (float)$_POST['deposit'],
			'service_time_1' =>(int) $_POST['application_time'],
			'process_time' => (int)$_POST['processing_time'],
			'service_time_2' => (int)$_POST['finishing_time'],
			'service_usell_id' => (int)$_POST['service_upsell'],
			'retail_usell_id' => (int)$_POST['retail_upsell'],
			//'equipment_id' =>(int)$_POST['equipment'],
			'equipment_id' =>$equipment_id,
			'equipments_list' => $_POST['equipments_list'],
			'first_book_enbl' =>(int)$_POST['first_book_enbl'],
			'dashboard_report' =>(int)$_POST['dashboard_report'],
			'pinned_service' =>(int)$_POST['pinned_service'],
			'description' => $_POST['description'],
			'hide_on_front' =>0,
			'order_number'=>1000
		);
		DB::insert('salon_services',$serv_data);
		$serv_id = DB::insert_id();


		$levels = DB::query("select id from price_levels where salon_id='".SALON_ID."'");
		while($lev = DB::fetch_assoc($levels)){

			DB::insert('salon_services_prices',array(
				'service_id'=>$serv_id,
				'level_id'=>(int)$lev['id'],
				'salon_id'=>SALON_ID,
				'level_price'=>$_POST['price']
			) );
		}



		if( isset($_POST['add_to_all_locs']) && $_POST['add_to_all_locs'] && defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1 ){
			$cat_name = '';
			$equipment_name = '';
			$service_usell_name = '';
			$service_usell_code = '';
			$retail_usell_name = '';

			if($serv_data['service_category_id']>0){
				$cat_info = DB::get_row("select * from service_categories where id='".(int)$serv_data['service_category_id']."' and salon_id='".SALON_ID."' limit 1");
				if($cat_info) $cat_name = $cat_info['category_name'];
			}
			if($serv_data['equipment_id']>0){
				$eq_info = DB::get_row("select * from salon_equipment where id='".(int)$serv_data['equipment_id']."' and salon_id='".SALON_ID."' limit 1");
				if($eq_info) $equipment_name = $eq_info['equipment_name'];
			}
			if($serv_data['service_usell_id']>0){
				$s_info = DB::get_row("select * from salon_services where id='".(int)$serv_data['service_usell_id']."' and salon_id='".SALON_ID."' limit 1");
				if($s_info) {
					$service_usell_name = $s_info['service_name'];
					$service_usell_code = $s_info['service_code'];
				}
			}
			if($serv_data['retail_usell_id']>0){
				$eq_info = DB::get_row("select * from salon_inventory where id='".(int)$serv_data['retail_usell_id']."' and salon_id='".SALON_ID."' limit 1");
				if($eq_info) $retail_usell_name = $eq_info['description'];
			}

			foreach(ALL_SALON_IDS as $salid){
				if($salid!=SALON_ID){
					$loc_info = DB::get_row("select * from salons where id='".(int)$salid."' limit 1");
					$servcheck = false;

					if($serv_data['service_code']!=''){
						$servcheck = DB::get_row("select * from salon_services where service_code='".addslashes($serv_data['service_code'])."' and salon_id='".(int)$salid."' limit 1");
					}
					if(!$servcheck){
						$servcheck = DB::get_row("select * from salon_services where service_name='".addslashes($serv_data['service_name'])."' and salon_id='".(int)$salid."' limit 1");
					}
					if(!$servcheck){
						$service_category_id = 0;
						$service_usell_id = 0;
						$retail_usell_id = 0;
						$equipment_id = 0;

						if($cat_name!=''){
							$cat_info = DB::get_row("select * from service_categories where category_name='".addslashes($cat_name)."' and salon_id='".(int)$salid."' limit 1");
							if($cat_info) $service_category_id = $cat_info['id'];
							else{
								DB::insert('service_categories',array(
									'salon_id' => (int)$salid,
									'category_name' => $cat_name,
									'parent_id' => $loc_info['main_cat_id'],
									'visible' => 1,
									'position_num' => 1000,
									'disabled' => 0,
									'booking_order' => 999
								));
								$service_category_id = DB::insert_id();
							}
						}
						if($equipment_name!=''){
							$eq_info = DB::get_row("select * from salon_equipment where equipment_name='".addslashes($equipment_name)."' and salon_id='".(int)$salid."' limit 1");
							if($eq_info) $equipment_id = $eq_info['id'];
						}
						if($retail_usell_name!=''){
							$r_info = DB::get_row("select * from salon_inventory where description='".addslashes($retail_usell_name)."' and salon_id='".(int)$salid."' limit 1");
							if($r_info) $retail_usell_id = $r_info['id'];
						}
						if($service_usell_code!=''){
							$r_info = DB::get_row("select * from salon_services where service_code='".addslashes($service_usell_code)."' and salon_id='".(int)$salid."' limit 1");
							if($r_info) $service_usell_id = $r_info['id'];
						}
						if(!$service_usell_id && $service_usell_name!=''){
							$r_info = DB::get_row("select * from salon_services where service_name='".addslashes($service_usell_name)."' and salon_id='".(int)$salid."' limit 1");
							if($r_info) $service_usell_id = $r_info['id'];
						}



						$serv_data_loc = array(
							'salon_id' => (int)$salid,
							'service_category_id' => (int)$service_category_id,
							'service_name' => $serv_data['service_name'],
							'service_code' => $serv_data['service_code'],
							'default_pricing' => (float)$serv_data['default_pricing'],
							'default_timing_min' => (int)$serv_data['default_timing_min'],
							'tax' => (float)$serv_data['tax'],
							'default_fee_perc' => (float)$serv_data['default_fee_perc'],
							'deposit' => (float)$serv_data['deposit'],
							'service_time_1' =>(int) $serv_data['service_time_1'],
							'process_time' => (int)$serv_data['process_time'],
							'service_time_2' => (int)$serv_data['service_time_2'],
							'service_usell_id' => (int)$service_usell_id,
							'retail_usell_id' => (int)$retail_usell_id,
							'equipment_id' =>(int)$equipment_id,
							'hide_on_front' =>0,
							'order_number'=>1000
						);
						DB::insert('salon_services',$serv_data_loc);
						$serv_loc_id = DB::insert_id();


						$levels = DB::query("select id from price_levels where salon_id='".(int)$salid."'");
						while($lev = DB::fetch_assoc($levels)){

							DB::insert('salon_services_prices',array(
								'service_id'=>$serv_loc_id,
								'level_id'=>(int)$lev['id'],
								'salon_id'=>(int)$salid,
								'level_price'=>(float)$serv_data['default_pricing']
							) );
						}

					}
				}
			}
		}

	}else echo 'Please enter service name.';

	exit;
}
if($action == 'get_next_timeslot'){
	$data = array('time'=>'');

	$stylist_id = isset($_POST['stylist_id'])?$_POST['stylist_id']:0;
	$service_id = isset($_POST['service_id'])?$_POST['service_id']:0;
	$time = isset($_POST['time'])?$_POST['time']:0;
	$customer_id = isset($_POST['customer_id'])?$_POST['customer_id']:0;

	if($stylist_id && $service_id && $customer_id && $time!=''){

		$c_serv = DB::get_row("select if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0))) as service_time_2 from salon_services ss inner join provider_services ps on ps.service_id=ss.id left join customer_services cs on cs.customer_id='".(int)$customer_id."' and cs.service_id=ss.id where ss.id='".(int)$service_id."' and ps.provider_id='".(int)$stylist_id."' limit 1");
		if($c_serv){
			$checkdur = $c_serv['service_time_1']+$c_serv['process_time']+$c_serv['service_time_2'];
			if($checkdur>$c_serv['service_duration']) $c_serv['service_duration']=$checkdur;

			$beg = getTimeSec($time);
			$next_time_sec = $beg+($c_serv['service_duration']*60);

			$m = (int)($next_time_sec/60);
			$h=0;
			while($m>=60){
				$h++; $m-=60;
			}

			if($m==0) $m='00';
			$ampm = 'AM';
			if($h>=12){
				$ampm = 'PM';
				if($h>12) $h-=12;
			}
			$t_key = $h.':'.$m.' '.$ampm;
			if($t_key){
				$data['time']=$t_key;
			}
		}
	}


	header('Content-type: application/json');
	echo json_encode($data);
	exit;
}

if($action == 'save_block_time'){

	$res = DB::get_row("select * from provider_offhours where id='".(int)$_GET['btid']."' and salon_id='".SALON_ID."' limit 1");
	if($res){
		DB::update('provider_offhours',array(
			'start_time' => $_POST['start_time'],
			'start_time_sec' => getTimeSec($_POST['start_time']),
			'duration' => $_POST['duration'],
			'reason' => $_POST['reason']
		)," id='".(int)$_GET['btid']."' and salon_id='".SALON_ID."' limit 1 ");
	}

	exit;
}
if($action == 'get_block_time'){
	$data = array('id'=>0);
	$res = DB::get_row("select * from provider_offhours where id='".(int)$_GET['btid']."' and salon_id='".SALON_ID."' limit 1");
	if($res){
		$data['id'] = $res['id'];
		$data['start_time'] = $res['start_time'];
		$data['duration'] = $res['duration'];
		$data['reason'] = $res['reason'];
	}

	header('Content-type: application/json');
	echo json_encode($data);
	exit;
}
if($action == 'remove_break_time'){
	if($_GET['stl_id'] && $_GET['date']){
		DB::query("delete from provider_schedule_break_changes where salon_id='".SALON_ID."' and provider_id='".(int)$_GET['stl_id']."' and date='".addslashes($_GET['date'])."' limit 1");
		DB::insert('provider_schedule_break_changes',array(
			'salon_id' => SALON_ID,
			'provider_id' => (int)$_GET['stl_id'],
			'date' => $_GET['date'],
			'start_time' => '',
			'end_time' => '',
			'reason' => '',
			'is_removed' => 1
		));
	}
	exit;
}
if($action == 'save_break_time'){
	if($_GET['stl_id'] && $_GET['date']){
		DB::query("delete from provider_schedule_break_changes where salon_id='".SALON_ID."' and provider_id='".(int)$_GET['stl_id']."' and date='".addslashes($_GET['date'])."' limit 1");

		if(isset($times_list_24h_rev[$_POST['start_time']])) $_POST['start_time']=$times_list_24h_rev[$_POST['start_time']];
		if(isset($times_list_24h_rev[$_POST['end_time']])) $_POST['end_time']=$times_list_24h_rev[$_POST['end_time']];

		DB::insert('provider_schedule_break_changes',array(
			'salon_id' => SALON_ID,
			'provider_id' => (int)$_GET['stl_id'],
			'date' => $_GET['date'],
			'start_time' => $_POST['start_time'],
			'end_time' => $_POST['end_time'],
			'reason' => $_POST['reason'],
			'is_removed' => 0
		));
	}
	exit;
}
if($action == 'remove_block_time'){
	$r = DB::get_row("select * from  `provider_offhours` where  id='".(int)$_GET['btid']."' and salon_id='".SALON_ID."' limit 1");
	if($r){
		DB::query("delete from provider_offhours where id='".(int)$_GET['btid']."' and salon_id='".SALON_ID."' limit 1");
		log_event("sched_sch_r_block", 0, 0, 0, '', '', '', $r['provider_id'], json_encode( array( 'range' => $r['date'] . ' ' . $r['start_time'] . ' for ' . $r['duration'] . 'min.'  ) )  );
	}
	exit;
}

if($action == 'void_transaction'){

	$can_void = ($role==1 && !$frontdesk);
	if($frontdesk && $salon_info['frontdesk_can_void']) $can_void = true;
	if(!$can_void) exit;

	$appt_id = (int) $_GET['appt_id'];
	$appt_info = DB::get_row("select * from salon_appointments where id='".$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt_info) exit('An error occurred!');

	if($appt_info['payment_info']!=''){

		if(is_numeric($appt_info['payment_info'])){
			$pinf = DB::get_row("SELECT * FROM `salon_transactions` WHERE `trxId`='".addslashes($appt_info['payment_info'])."' limit 1");
			if($pinf){
				//$url = 'https://aurasalonware.com/dev/admin/dj.php?method=creditvoid&account='.$pinf['salonid'].'&sn='.$pinf['trxMachine'].'&transactionid='.$pinf['trxId'];
				//file_get_contents($url);
				$data = array('method'=>'creditvoid', 'account'=>$pinf['salonid'],'sn'=>$pinf['trxMachine'],'transactionid'=>$pinf['trxId'],'merchant'=>$pinf['trxMerchatID'],'amount'=>($appt_info['total']-$appt_info['partial_payment_amt']-$appt_info['deposit_amt']));
				$data_string = json_encode($data);

				$ch = curl_init(ADM_URL."/dj.php");
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);

				$result = curl_exec($ch);
				if($result!=''){
					$req = date('m/d/Y H:i:s') . ' -- '.$data_string.' - ' . $result . "\n\n";

					$fp = fopen('voidreport.txt', "a");
					fwrite($fp, $req);
					fclose($fp);

					$resdata=json_decode($result,true);
					if(isset($resdata['ResponseCode']) && $resdata['ResponseCode']==0){

					}else{
						exit('Error: '.$resdata['ResponseMsg']);
					}
				}else exit('An error occured.');
			}else{
				exit('Invalid transaction ID.');
			}
			/* https://aurasalonware.com/dev/admin/dj.php?method=creditvoid&account=[Salon Id]&sn=[device serial number]&transactionid=[transaction id from the credit sale] */
		}else{
			$pinfo = json_decode($appt_info['payment_info'],true);
			include('../includes/functions_authnet.php');
			if(is_array($pinfo)){
				$res = voidTransaction($appt_id);
				if($res!='') exit($res);
			}

		}
	}


	$prods = DB::query("select * from salon_appointments_retail where appt_id='".(int)$appt_id."'");
	while($r=DB::fetch_assoc($prods)){
		DB::query("update salon_inventory set on_hand=on_hand+".(int)$r['qty']." where id='".$r['inv_id']."' limit 1");
	}


	DB::query("update order_curbside set status='Void' where appt_id='".(int)$appt_id."'");

	DB::update('salon_appointments',array(
		'is_paid' => 0,
		'payment_info' => '',
		'pay_method' => '',
		'partial_payment_amt' =>0,
		'partial_payment_inf' =>'',
		'is_voided'=>1
	)," id='".(int)$appt_id."' limit 1 ");

	log_event('void_appt', $appt_id, $appt_info['customer_id'], 0, isset($_POST['reason'])?$_POST['reason']:'',date('Y-m-d',strtotime($appt_info['appt_date'])));

	exit('ok');
}

if($action == 'stylist_block_time'){
	if(isset($_POST['stylist_id']) && $_POST['stylist_id']>0 && $_POST['start_time']!='' && $_POST['end_time']!='' ){
		$stime_sec = getTimeSec($_POST['start_time']);
		$etime_sec = getTimeSec($_POST['end_time']);
		$duration = (int)(($etime_sec-$stime_sec)/60);
		DB::insert('provider_offhours',array(
			'provider_id' => (int)$_POST['stylist_id'],
			'salon_id' => SALON_ID,
			'date' => date('Y-m-d',strtotime($_POST['date'])),
			'start_time' => $_POST['start_time'],
			'start_time_sec' => $stime_sec,
			'duration' => $duration,
			'reason' => $_POST['reason'],
			'created_by' => $user_id,
			'created_time' => date('Y-m-d H:i:s')
		));


		log_event("sched_sch_block", 0, 0, 0, '', '', '',(int)$_POST['stylist_id'], json_encode( array( 'provider_offhours' => DB::insert_id(), 'range' => date('Y-m-d',strtotime($_POST['date'])) . ' ' . $_POST['start_time'] . ' for ' . $duration. 'min.' ) )  );
	}
	exit;
}

if($action == 'save_employee_notes'){
	if($_POST['note']!=''){
		DB::insert('provider_notes',array(
			'salon_id' => SALON_ID,
			'provider_id' => $_GET['id'],
			'note_by_id' => $user_id,
			'note_dt' => time(),
			'note_date' => date('Y-m-d'),
			'note_text' => $_POST['note']
		));
	}
	exit;
}
if($action == 'get_employee_notes'){
	$stylist = DB::get_row("select u.*, pi.code_name,pi.title from users u inner join provider_info pi on pi.user_id=u.id   where u.id='".(int)$_GET['id']."' and pi.salon_id='".SALON_ID."' and  u.role_id!=3   and u.status=1   order by pi.position_num, u.id");
	if(!$stylist){
		?><div class="panel panel-body pan" style="box-shadow: none !important;">
				<button type="button" class="close" data-dismiss="modal"  onclick=""><i class="icon-x"></i></button>
			<b>Employee Not Found</b>
			</div><?php
		exit;
	}
	?><div class="panel panel-body pan" style="box-shadow: none !important;">
				<button type="button" class="close" data-dismiss="modal"  onclick=""><i class="icon-x"></i></button>
			<b><?php echo $stylist['first_name']; ?>'s Notes for Today</b>
			</div>

			<div class="modal-body mod_body" style="max-height:300px;overflow-y:auto; padding: 20px">
				<?php
				$notes = DB::query("select n.*, u.first_name,u.last_name from provider_notes n left join users u on u.id=n.note_by_id where n.salon_id='".SALON_ID."' and n.provider_id='".(int)$_GET['id']."' and n.note_date='".date('Y-m-d')."' order by n.note_dt desc");
				$lnotedt = 0;
				while($r=DB::fetch_assoc($notes)){
					echo '<div style="padding-bottom:5px;">
						<b>'.$r['first_name'].' '.$r['last_name'].' ('.date('h:ia',$r['note_dt']).'):</b> '.htmlspecialchars($r['note_text']).'
					</div>';
					if( $lnotedt == 0 ) $lnotedt = $r['note_dt'];
				}
				if( $user_id == (int)$_GET['id'] ) echo '<script>document.cookie = "enotes'.$user_id.'='.$lnotedt.'"; jQuery(".hu_has_notes").removeClass("hu_has_notes");</script>';

				?>
			</div>
			 <div class="modal-footer">
				<input type="text" class="form-control emp_note_txt" placeholder="Add a note..." style=" border: 1px solid #ddd; border-radius: 3px;  padding: 0 10px;" />
				<button type="button" class="btn " data-dismiss="modal" style="margin-top:10px !important;">Cancel</button>
				<button type="button" class="btn btn-primary" style="margin-top:10px !important; background: #000;" onclick="saveEmployeeNotes(<?php echo (int)$_GET['id']; ?>);">Save</button>
			</div>
	<?php
	exit;
}

if($action == 'get_team_box'){
	include('includes/team_box.php');
	exit;
}


if($action == 'update_last_action'){
	$_SESSION['last_action']=time();
}

if($action == 'get_series'){
	$serv_q = DB::query("select ss.*, ser.service_name, ser.default_pricing from salon_series ss left join salon_services ser on ser.id=ss.service_id where ss.salon_id='".SALON_ID."' and ss.status=1  order by ss.id desc");

	?>
	<div class="form-group has-feedback">
		<select name="series_id" class="form-control styleselect " onchange="change_series(this)" >
		<option value="">Select a series</option>
		<?php while($r=DB::fetch_assoc($serv_q)){
			$disc_price_srv = (float)$r['discounted_price'];
			if(!$disc_price_srv && $r['discount']>0) $disc_price_srv = ($r['default_pricing'])*(1-$r['discount']/100);
			echo '<option value="'.$r['id'].'" data-price="'.number_format(($disc_price_srv*$r['qty']),2,'.','').'">'.$r['series_name'].'</option>';
		} ?>
		</select>
	</div>
	<div class="form-group has-feedback">
		$<input type="number" name="series_amt" step="0.01" pattern="[0-9\.]*" class="form-control " style="max-width: 94%; display: inline-block; margin-left: 3%;" readonly placeholder="Amount">
	</div>
	<?php

	exit;
}
if($action == 'inventory_edit_field'){
	$allow_fields = array('on_hand','minimum','price','maximum','employee_amt','cost','subcategory','category','type','color','size_vol','supplier','description','brand','line','sku','code','supplier_id');

	if($frontdesk && !$salon_info['desc_full_inv']) exit;

	if(isset($_POST['name']) && in_array($_POST['name'],$allow_fields) && $_POST['pk']>0){
		$upd_arr = array();
		$upd_arr[$_POST['name']]=$_POST['value'];
		if( $_POST['name'] == 'description' ){
			$p = DB::get_row("select description from salon_inventory where id='".(int)$_POST['pk']."' limit 1");
			log_event("inv_edit_p", 0, 0, 0, '', '', '', 0, json_encode( array( 'prod_id' => (int)$_POST['pk'], 'name' =>$_POST['value'] , 'old_val' => $p['description'], 'new_val' => $_POST['value'], 'edt' => $_POST['name'] ) ) );
		}else if( $_POST['name'] == 'supplier_id' ){
			$p = DB::get_row("select description, " . $_POST['name'] . " from salon_inventory where id='".(int)$_POST['pk']."' limit 1");
			$so = DB::get_row("select supplier_name from suppliers where id='".(int)$p[$_POST['name']]."' limit 1");
			$sn = DB::get_row("select supplier_name from suppliers where id='".(int)$_POST['value']."' limit 1");
			log_event("inv_edit_p", 0, 0, 0, '', '', '', 0, json_encode( array( 'prod_id' => (int)$_POST['pk'], 'name' => $p['description'] , 'old_val' => $so['supplier_name'], 'new_val' => $sn['supplier_name'], 'edt' => $_POST['name'] ) ) );

		}else{
			$p = DB::get_row("select description, " . $_POST['name'] . " from salon_inventory where id='".(int)$_POST['pk']."' limit 1");
			log_event("inv_edit_p", 0, 0, 0, '', '', '', 0, json_encode( array( 'prod_id' => (int)$_POST['pk'], 'name' => $p['description'] , 'old_val' =>$p[$_POST['name']], 'new_val' => $_POST['value'], 'edt' => $_POST['name'] ) ) );
		}
		DB::update('salon_inventory',$upd_arr," id='".(int)$_POST['pk']."' and salon_id='".SALON_ID."' limit 1");
	}
	exit;
}
if($action == 'check_prod_exists'){
	if(isset($_GET['upc']) && trim($_GET['upc'])!=''){
		$check = DB::get_row("select * from salon_inventory where salon_id='".SALON_ID."' and code='".addslashes(trim($_GET['upc']))."' limit 1");
		if($check){
			echo 'Product with UPC "'.trim($_GET['upc']).'" already exists, update will be performed.';
			exit;
		}
	}
	if(isset($_GET['sku']) && trim($_GET['sku'])!=''){
		$check = DB::get_row("select * from salon_inventory where salon_id='".SALON_ID."' and sku='".addslashes(trim($_GET['sku']))."' limit 1");
		if($check){
			echo 'Product with SKU "'.trim($_GET['sku']).'" already exists, update will be performed.';
			exit;
		}
	}
	exit;
}


if($action == 'get_refund_modal_manual'){
	$appt_id = (int)$_GET['appt_id'];
	if(!$appt_id){
		DB::insert('salon_appointments',array(
			'salon_id'=>SALON_ID,
			'appt_date' =>date('Y-m-d H:i:s',tz_time(time())),
			'is_manual_rfnd'=>1,
			'customer_id' => null,
			'total' => 0,
			'tip' => 0,
			'tax' => 0,
			'discount' => 0
		));
		$appt_id = DB::insert_id();

		log_event('closeout', $appt_id, $appt['customer_id'], 0, '',$appt['appt_date']);
	}

	if(isset($_GET['set_customer']) && $_GET['set_customer']>0){
		DB::update('salon_appointments',array('customer_id' => (int)$_GET['set_customer'])," id='".(int)$appt_id."' limit 1");
	}

	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' limit 1");
	if($appt){
		$u_info=array();
		if($appt['customer_id']) $uinfo = DB::get_row("select * from users where id='".(int)$appt['customer_id']."' limit 1");
		include('includes/refund_manual_modal.php');
	}

}
if($action == 'get_refund_modal'){
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$_GET['appt_id']."' limit 1");
	if($appt){
		$uinfo = DB::get_row("select * from users where id='".(int)$appt['customer_id']."' limit 1");
		include('includes/refund_modal.php');
	}
	exit;
}
if($action == 'save_user_modal'){
	$uinfo = DB::get_row("select * from users where id='".(int)$_GET['uid']."' limit 1");
	if($uinfo){
		DB::update('users',array(
			'guest_notes' => $_POST['guest_notes'],
			'guest_formulas' => $_POST['guest_formulas']
		)," id='".(int)$_GET['uid']."' limit 1");

		//pre_print($uinfo); exit;
		if($uinfo['guest_formulas'] != $_POST['guest_formulas']){ log_event('updated_formula', 0, (int)$_GET['uid'], 0, $uinfo['first_name'].' '.$uinfo['last_name']); }
		if($uinfo['guest_notes'] != $_POST['guest_notes']){ log_event('updated_client_note', 0, (int)$_GET['uid'], 0, $uinfo['first_name'].' '.$uinfo['last_name']); }
	}
	exit;
}
if($action == 'get_user_modal'){
	if($_GET['appt_id']){
		$appt = DB::get_row("select * from salon_appointments where id='".(int)$_GET['appt_id']."' and salon_id='".SALON_ID."' limit 1");
		if(!$_GET['uid'] && $appt) $_GET['uid']=$appt['customer_id'];
	}
	$uinfo = DB::get_row("select * from users where id='".(int)$_GET['uid']."' limit 1");
	//pre_print($uinfo, true);
	include('includes/user_modal.php'); 
	exit;
}

if($action == 'search_clients'){
	$results = array("results"=>array(), "pagination"=>array("more"=>false) );
	$pnum = (int)$_POST['pnum'];
	if($pnum<1) $pnum = 1;
	$clients_per_page=20;
	$where = '';

	if(isset($_POST['search']) && $_POST['search']!=''){


		if(isset($_POST['search']) && $_POST['search']!=''){
			$addtourl ='&kw='.urlencode($_POST['search']);
			$kws= explode(' ',trim($_POST['search']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$where .= " and (u.first_name like '%".$k."%' or u.last_name like '%".$k."%'  or u.email like '%".$k."%' or u.phone like '%".$k."%'  or u.phone_clean like '%".$k."%') ";
				}
			}
		}
	}else{

		header('Content-type: application/json');
		echo json_encode($results);
		exit;
	}

	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;

	$clients = DB::query("select distinct u.id,u.first_name,u.last_name,u.phone,u.phone_clean from users u inner join salon_customers sc on sc.customer_id=u.id   where sc.salon_id in (".implode(',',$salon_ids).")  and u.status=1 ".$where." order by u.first_name,u.last_name limit ".($clients_per_page*($pnum-1)).",".$clients_per_page);

	$ic=0;

	 if($role==2 &&  $salon_info['hide_client_data'])$hide_contact_inf=true;

	while($r=DB::fetch_assoc($clients)){

		 if($role==2 &&  $salon_info['hide_client_data']){
			 $r['phone']=phoneMask($r['phone']);
		 }

		if( $r['phone'] && !preg_match('^[0-9]{3}.[0-9]{3}.[0-9]{4}', $r['phone']) ) { $r['phone'] = substr( $r['phone_clean'], 0, 3 ) . '.' . substr( $r['phone_clean'], 3, 3 ) . '.' . substr( $r['phone_clean'], 6 ); }
		$results['results'][]=array("id"=>$r['id'],"text"=>ucwords(strtolower($r['first_name'].' '.$r['last_name'])) .($hide_contact_inf?'':" / ".$r['phone'] ));
		$ic++;
	}

	if($ic==$clients_per_page) $results['pagination']['more']=true;

	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}
if($action == 'search_sms_users'){
	$results = array("results"=>array(), "pagination"=>array("more"=>false) );
	$pnum = (int)$_POST['pnum'];
	if($pnum<1) $pnum = 1;
	$clients_per_page=20;
	$where = '';

	if(isset($_POST['search']) && $_POST['search']!=''){


		if(isset($_POST['search']) && $_POST['search']!=''){
			$addtourl ='&kw='.urlencode($_POST['search']);
			$kws= explode(' ',trim($_POST['search']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$where .= " and (u.first_name like '%".$k."%' or u.last_name like '%".$k."%'  or u.email like '%".$k."%' or u.phone like '%".$k."%'  or u.phone_clean like '%".$k."%') ";
				}
			}
		}
	}

	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;
	if($role==2 &&  $salon_info['hide_client_data'])$hide_contact_inf=true;

	if(isset($_POST['search']) && $_POST['search']!='' && $pnum == 1){

		$employees = DB::query("select distinct u.id,u.first_name,u.last_name,u.phone,u.phone_clean,u.role_id, r.name as role_name from users u inner join provider_info pi on pi.user_id=u.id inner join roles r on r.id=u.role_id  where pi.salon_id in (".implode(',',$salon_ids).")  and u.status=1 and u.phone_clean!='' and u.phone_clean is not null ".$where." order by u.first_name,u.last_name limit 5");

		while($r=DB::fetch_assoc($employees)){

			 if($role==2 &&  $salon_info['hide_client_data']){
				 $r['phone']=phoneMask($r['phone']);
			 }

			if( $r['phone'] && !preg_match('^[0-9]{3}.[0-9]{3}.[0-9]{4}', $r['phone']) ) { $r['phone'] = substr( $r['phone_clean'], 0, 3 ) . '.' . substr( $r['phone_clean'], 3, 3 ) . '.' . substr( $r['phone_clean'], 6 ); }
			$results['results'][]=array("id"=>$r['id'],"text"=>ucwords(strtolower($r['first_name'].' '.$r['last_name'])) .($hide_contact_inf?'':" / ".$r['phone'] )." / ".$r['role_name']);
			$ic++;
		}
	}

	$clients = DB::query("select distinct u.id,u.first_name,u.last_name,u.phone,u.phone_clean from users u inner join salon_customers sc on sc.customer_id=u.id   where sc.salon_id in (".implode(',',$salon_ids).")  and u.status=1 and u.phone_clean!='' and u.phone_clean is not null ".$where." order by u.first_name,u.last_name limit ".($clients_per_page*($pnum-1)).",".$clients_per_page);

	$ic=0;
	while($r=DB::fetch_assoc($clients)){

		 if($role==2 &&  $salon_info['hide_client_data']){
			 $r['phone']=phoneMask($r['phone']);
		 }

		if( $r['phone'] && !preg_match('^[0-9]{3}.[0-9]{3}.[0-9]{4}', $r['phone']) ) { $r['phone'] = substr( $r['phone_clean'], 0, 3 ) . '.' . substr( $r['phone_clean'], 3, 3 ) . '.' . substr( $r['phone_clean'], 6 ); }
		$results['results'][]=array("id"=>$r['id'],"text"=>ucwords(strtolower($r['first_name'].' '.$r['last_name'])) .($hide_contact_inf?'':" / ".$r['phone'] ));
		$ic++;
	}

	if($ic==$clients_per_page) $results['pagination']['more']=true;

	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}

if($action == 'search_clients_text'){
	$results = array( );
	$pnum=1;
	$clients_per_page=10;
	$where = '';

	if(isset($_GET['term']) && $_GET['term']!=''){

		if(isset($_GET['term']) && $_GET['term']!=''){
			$addtourl ='&kw='.urlencode($_GET['term']);
			$kws= explode(' ',trim($_GET['term']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$where .= " and (u.first_name like '%".$k."%' or u.last_name like '%".$k."%'  or u.email like '%".$k."%' or u.phone like '%".$k."%'  or u.phone_clean like '%".$k."%') ";
				}
			}
		}
	}

	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;

	$clients = DB::query("select distinct u.id,u.first_name,u.last_name,u.phone from users u inner join salon_customers sc on sc.customer_id=u.id   where sc.salon_id in (".implode(',',$salon_ids).")  and u.status=1 ".$where." order by u.first_name,u.last_name limit ".($clients_per_page*($pnum-1)).",".$clients_per_page);

	$ic=0;
	while($r=DB::fetch_assoc($clients)){
		$names = ucwords(strtolower($r['first_name'].' '.$r['last_name']));
		$results[]=array("id"=>$r['id'],"label"=>$names,"value"=>$names   );
		$ic++;

	}



	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}

if($action == 'set_category_status'){
	if($_GET['cat_id']){
		DB::update('service_categories',array('visible'=>(isset($_GET['status'])&&$_GET['status']?1:0))," id='".(int)$_GET['cat_id']."' and salon_id='".SALON_ID."' limit 1");
	}

	exit;
}
if($action == 'set_category_disabled'){
	if($_GET['cat_id']){
		DB::update('service_categories',array('disabled'=>(isset($_GET['status'])&&$_GET['status']?1:0))," id='".(int)$_GET['cat_id']."' and salon_id='".SALON_ID."' limit 1");
	}

	exit;
}
if($action == 'get_stylist_avail_times'){
	$curr_stylist = (int)$_GET['curr_stylist'];
	$curr_date =  $_GET['curr_date'];
	$curr_date_time = strtotime($curr_date);
	$avail_times = $times_list;
	if($curr_date_time){
		$stylist = DB::get_row("select u.*,pi.code_name, pi.althern_sched from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$curr_stylist."' limit 1");


		$w_day = date('w',$curr_date_time);
		$w_num = date('W',$curr_date_time);
		$d_start = 0;
		$d_end = 0;
		$is_odd_week = $stylist['althern_sched'] && $w_num%2!=0;

		$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");

		if($salon_h){
			if($salon_h['is_open']){
				$d_start = getTimeSec($salon_h['start_time']);
				$d_end = getTimeSec($salon_h['end_time']);
				/*if($curr_stylist){
					$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$curr_stylist."' and salon_id='".SALON_ID."' and sch_index='".($is_odd_week?2:1)."' and weekday='".$w_day."'");
					if($schq){
					if($schq && $schq['is_available']){
						$s_start = getTimeSec($schq['start_time']);
						if($s_start>$d_start) $d_start=$s_start;
						$s_end = getTimeSec($schq['end_time']);
						if($s_end<$d_end) $d_end=$s_end;
					}
					}
				}*/

			}
		}

		/*if($errmsg=='' && $d_start && $d_end){
			$avail_times = array();
			foreach($times_list as $k=>$v){
				$t_sec = getTimeSec($k);
				if($t_sec>=$d_start && $t_sec<$d_end) $avail_times[$k]=$v;
			}
		}*/

	}
	$def_time = isset($_GET['b_time'])&&$_GET['b_time']?strtoupper($_GET['b_time']):'';
	if($def_time=='' || $def_time=='12:00 AM') $def_time=(((int)date('h')+1)).':00 '.date('A');
	foreach($avail_times as $k=>$v){
		$t_sec = getTimeSec($k);
	?><option value="<?php echo $k; ?>" <?php if($def_time==$k) echo ' selected '; ?> ><?php echo $k; ?></option> <?php


	}
	exit;
}

if($action == 'get_stylist_dates_times'){
	$result = array();
	$curr_stylist = (int)$_GET['curr_stylist'];
	$curr_date =  $_GET['curr_date'];

	if($curr_stylist){
		$stylist = DB::get_row("select u.*,pi.code_name, pi.althern_sched from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$curr_stylist."' limit 1");
		$disable_dates = array();
		$salon_off_per = DB::query("select * from salon_offdays where salon_id='".SALON_ID."' and end_date>='".date('Y-m-d')."'");
		while($r=DB::fetch_assoc($salon_off_per)){
			$start_d = strtotime($r['start_date']);
			$end_d = strtotime($r['end_date']." 23:59:59");
			while($start_d<=$end_d){
				$da = date('Y-m-d',$start_d);
				$disable_dates[]=$da;
				$start_d+=86400;
			}
		}
		if($curr_stylist){
			$stylist_off_per = DB::query("select * from provider_offdays where provider_id='".(int)$curr_stylist."' and end_date>'".date('Y-m-d')."' and salon_id='".SALON_ID."'");
			while($r=DB::fetch_assoc($stylist_off_per)){
				$start_d = strtotime($r['start_date']);
				$end_d = strtotime($r['end_date']." 23:59:59");
				while($start_d<=$end_d){
					$disable_dates[]=date('Y-m-d',$start_d);
					$start_d+=86400;
				}
			}
		}

		$off_w_days=array();
		$off_w_odd_days=array();
		if($curr_stylist){
			$schq = DB::query("select * from provider_schedule where provider_id='".(int)$curr_stylist."' and salon_id='".SALON_ID."' and sch_index='1' and is_available=0");
			while($r=DB::fetch_assoc($schq)) $off_w_days[$r['weekday']]=1;

			if($stylist['althern_sched']){
				$schq = DB::query("select * from provider_schedule where provider_id='".(int)$curr_stylist."' and salon_id='".SALON_ID."' and sch_index='2' and is_available=0");
				while($r=DB::fetch_assoc($schq)) $off_w_odd_days[$r['weekday']]=1;
			}else{
				$off_w_odd_days=$off_w_days;
			}
		}
		$schq = DB::query("select * from salon_hours where salon_id='".SALON_ID."' and is_open=0");
		while($r=DB::fetch_assoc($schq)) { $off_w_days[$r['weekday']]=1;  $off_w_odd_days[$r['weekday']]=1;   }

		if(count($off_w_days)) $off_w_days=array_keys($off_w_days);
		if(count($off_w_odd_days)) $off_w_odd_days=array_keys($off_w_odd_days);

		$result['off_w_days']=$off_w_days;
		$result['off_w_odd_days']=$off_w_odd_days;
		$result['disable_dates']=$disable_dates;
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}
if($action == 'get_stylists_for_service'){
	$serv_id = (int)$_GET['serv_id'];
	$curr_stylist = (int)$_GET['curr_stylist'];
	if($serv_id){
		$stylists = DB::query("select u.*, ps.pricing,pi.code_name from users u inner join provider_info pi on pi.user_id=u.id inner join provider_services ps on ps.provider_id=u.id where pi.salon_id='".SALON_ID."' and  u.role_id in (".BOOKABLE_ROLE_IDS.") and ps.service_id='".(int)$serv_id."'  and u.status=1 "/*.($role==2?" and u.id='".$user_id."' ":"")*/." order by pi.position_num");
		echo '<option value="">Provider</option>';
		while($r=DB::fetch_assoc($stylists)){
			if($r['code_name']!='') $r['first_name']=$r['code_name'];
			?><option value="<?php echo $r['id']; ?>" <?php if($curr_stylist==$r['id']) echo ' selected '; ?> ><?php echo ucwords(strtolower($r['first_name'] )); ?></option> <?php
		}

	}
	exit;
}
if($action == 'book_appt'){
	$customer_id = (int)$_POST['customer_id'];
	$notes = $_POST['notes'];
	$service_ids = $_POST['service_ids'];
	$stylist_ids = $_POST['stylist_ids'];
	$times = $_POST['times'];
	$prebook = isset($_POST['prebook'])&&$_POST['prebook']?1:0;
	$booking_date =  date('Y-m-d',strtotime($_POST['date']));
	if($customer_id){
		$client = DB::get_row("select u.* from users u left join salon_customers sc on sc.customer_id=u.id   and  sc.salon_id='".SALON_ID."'  where u.id='".$customer_id."' and u.status=1 ");
		if($client){

			if(isset($_POST['check_overlaps']) && $_POST['check_overlaps']){


				$booking_datetime=strtotime($booking_date);
				$w_day = date('w',$booking_datetime);
				$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");
				$salon_dates = array();
				$stylist_add_per = DB::query("select * from salon_adddays where salon_id='".SALON_ID."' and end_date>='".$booking_date."'  ");
				while($r=DB::fetch_assoc($stylist_add_per)){
					$start_d = strtotime($r['start_date']);
					$end_d = strtotime($r['end_date']." 23:59:59");
					while($start_d<=$end_d){
						$salon_dates[date('Y-m-d',$start_d)]=$r;
						$start_d+=86400;
					}
				}
				if(isset($salon_dates[$booking_date])){

					$salon_h['is_open']=1;
					$salon_h['start_time']=$salon_dates[$booking_date]['start_time'];
					$salon_h['end_time']=$salon_dates[$booking_date]['end_time'];
					if($salon_h['end_time']=='00:00') $salon_h['end_time']='23:59';
					if($salon_h['end_time']=='12:00 AM') $salon_h['end_time']='11:59 PM';
				}
				$d_start = 0;
				$d_end = 0;
				if($salon_h){
					if(!$salon_h['is_open']){  exit('SERVICE_OVERLAP|This booking is outside of regular hours. Proceed?' ); }
					else{
						$d_start = getTimeSec($salon_h['start_time']);
						$d_end = getTimeSec($salon_h['end_time']);

					}

				}


				$prov_info = array();
				$is_overlap = false;
				$is_block_overlap = false;
				$is_transit_overlap = false;
				$is_equip_overlap = false;
				$overlap_msg = '';
				$equip_overlap = array();
				foreach($service_ids as $k=>$serv_id){
					$provid = (int)$stylist_ids[$k];
					$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.transition_min from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stylist_ids[$k]."' and pi.salon_id='".SALON_ID."' limit 1");

					$prov_info[$provid]=$stylist;

					$stylist_schedule_index = 1;
					if($stylist['fut_enable_sched'] && strtotime($stylist['fut_date_sched'])<=$booking_datetime){

						if($stylist['fut_althern_sched']){
							$is_odd_week = date('W',$booking_datetime)%2!=0;
							if($is_odd_week) $stylist_schedule_index = 4;
							else $stylist_schedule_index = 3;
						}else $stylist_schedule_index = 3;
					}else{
						if($stylist['althern_sched']){
							$is_odd_week = date('W',$booking_datetime)%2!=0;
							if($is_odd_week) $stylist_schedule_index = 2;
						}
					}

					$stylist_adday = false;
					$add_day_info = array();
					$stylist_add_per = DB::query("select * from provider_adddays where provider_id='".(int)$stylist_ids[$k]."' and end_date>='".$booking_date."' and start_date<='".$booking_date."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_add_per)){
						$stylist_adday=true;
						$add_day_info=$r;
					}
					$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$stylist_ids[$k]."' and salon_id='".SALON_ID."' and sch_index='".$stylist_schedule_index."' and weekday='".$w_day."'");
					$stl_d_start = $d_start;
					$stl_d_end = $d_end;
					if($schq){
						if($schq && ($schq['is_available'] || $stylist_adday)){
							if($add_day_info['start_time']) $schq['start_time']=$add_day_info['start_time'];
							if($add_day_info['end_time']) $schq['end_time']=$add_day_info['end_time'];
							if($schq['end_time']=='00:00') $schq['end_time']='23:59';
							if($schq['end_time']=='12:00 AM') $schq['end_time']='11:59 PM';

							$s_start = getTimeSec($schq['start_time']);
							if($s_start>$stl_d_start) $stl_d_start=$s_start;
							$s_end = getTimeSec($schq['end_time']);
							if($s_end<$stl_d_end) $stl_d_end=$s_end;
						}  else {  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?;' ); }
					}elseif($add_day_info){
						$stl_d_start = getTimeSec($salon_h['start_time']);
						$stl_d_end = getTimeSec($salon_h['end_time']);
						if($add_day_info['start_time']){
							$s_start = getTimeSec($add_day_info['start_time']);
							if($s_start>$stl_d_start) $stl_d_start=$s_start;
						}
						if($add_day_info['end_time']){
							if($add_day_info['end_time']=='00:00') $add_day_info['end_time']='23:59';
							if($add_day_info['end_time']=='12:00 AM') $add_day_info['end_time']='11:59 PM';
							$s_end = getTimeSec($add_day_info['end_time']);
							if($s_end<$stl_d_end) $stl_d_end=$s_end;
						}
					} //else  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?' );

					$busy_times = array();
					$block_times = array();
					$transit_times = array();

					/*if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
						$brk_dur = (getTimeSec($schq['brk_end_time'])-getTimeSec($schq['brk_start_time']))/60;
						$extr_dur = getTimesArr($schq['brk_start_time'],$brk_dur);
						foreach($extr_dur as $stm) $busy_times[$stm]=1;
					}*/

					$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0))) as service_time_2, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0))) as process_time, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id where sas.provider_id='".(int)$stylist_ids[$k]."' and sapp.appt_date='".addslashes($booking_date)."'");
					while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!='' ){
						$busy_times[$r['service_time']]=1;
						$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
						if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
						if($r['service_duration']>15){
							$extr_dur = getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']);
							foreach($extr_dur as $stm) $busy_times[$stm]=1;
						}
						if($prov_info[$provid] && $prov_info[$provid]['transition_min']>0){
							$extr_dur = getTimesArr($r['service_time'],$r['service_duration']+$prov_info[$provid]['transition_min'],$r['service_time_1'],$r['process_time']);
							foreach($extr_dur as $stm) if(!isset($busy_times[$stm])) $transit_times[$stm]=1;
						}

					}


					$stylist_off_h = DB::query("select * from provider_offhours where  `date`='".addslashes($booking_date)."'  and provider_id='".(int)$stylist_ids[$k]."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_off_h)){
						$extr_dur =getTimesArr($r['start_time'],$r['duration']);
						foreach($extr_dur as $stm){ $busy_times[$stm]=1; $block_times[$stm]=1; }
					}

					$c_serv = DB::get_row("select if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)) as service_duration, if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)) as service_time_1, if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0)) as process_time, ss.equipment_id from salon_services ss inner join provider_services ps on ps.service_id=ss.id where ss.id='".(int)$serv_id."' and ps.provider_id='".(int)$stylist_ids[$k]."' limit 1");

					$service_times = array();
					$service_times[]=$times[$k];
					if($c_serv['service_duration']>15){
						$extr_dur = getTimesArr($times[$k],$c_serv['service_duration'],$c_serv['service_time_1'],$c_serv['process_time']);
						foreach($extr_dur as $stm) $service_times[]=$stm;
					}

					if($d_end){
						foreach($service_times  as $tm){
							$serv_time = getTimeSec($tm);
							if($serv_time<$d_start || $serv_time>$d_end) exit('SERVICE_OVERLAP|This booking is outside of regular hours. Proceed?' );
						}
					}
					if($stl_d_end){
						foreach($service_times  as $tm){
							$serv_time = getTimeSec($tm);
							if($serv_time<$stl_d_start || $serv_time>$stl_d_end)  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?.' );
						}
					}


					if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
						$brk_check_change = DB::get_row("select * from provider_schedule_break_changes where salon_id='".SALON_ID."' and `date`='".addslashes($booking_date)."' and provider_id='".(int)$stylist_ids[$k]."' limit 1");
						if($brk_check_change){
							if($brk_check_change['is_removed']){
								$schq['has_break']=0;
								$schq['brk_start_time']='';
								$schq['brk_end_time']='';
							}else{
								$schq['brk_start_time']=$brk_check_change['start_time'];
								$schq['brk_end_time']=$brk_check_change['end_time'];
							}
						}
						if($schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
							$brk_start_time = getTimeSec($schq['brk_start_time']);
							$brk_end_time = getTimeSec($schq['brk_end_time']);
							foreach($service_times  as $tm){
								$serv_time = getTimeSec($tm);
								if($serv_time>=$brk_start_time && $serv_time<$brk_end_time)  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is on a break. Book anyway?.' );
							}
						}
					}


					foreach($service_times as $stime){
						if(isset($block_times[$stime])) $is_block_overlap = true;
						else  if(isset($busy_times[$stime])) $is_overlap = true;
						else  if(isset($transit_times[$stime])) $is_transit_overlap = true;
					}

					if($c_serv['equipment_id']){
						$eq_info = DB::get_row("select * from salon_equipment where id='".$c_serv['equipment_id']."' and salon_id='".SALON_ID."'");
						if($eq_info){
							$equipment_num = $eq_info['qty'];

							$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1, if(sas.dur_finsh_chng is not null and sas.dur_finsh_chng>0,sas.dur_finsh_chng,if(ps.service_time_2>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, if(sas.dur_procs_chng is not null and sas.dur_procs_chng>0,sas.dur_procs_chng,if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id where sapp.salon_id='".SALON_ID."' and sapp.appt_date='".addslashes($booking_date)."' and ifnull(sas.equipment_id,ifnull(ps.equipment_id,ss.equipment_id))='".$c_serv['equipment_id']."'");
							while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!=''){
								if(!isset($equipment_busy_times[$r['service_time']])) $equipment_busy_times[$r['service_time']]=0;

								if($r['service_duration']>15){
									$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
									if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
									$r['service_time_1'] = $r['service_duration'];
									$r['process_time'] = 0;
									$extr_dur = array_unique(getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']));
									foreach($extr_dur as $stm){
										if(!isset($equipment_busy_times[$stm])) $equipment_busy_times[$stm]=0;
										$equipment_busy_times[$stm]++;
									}
								}else $equipment_busy_times[$r['service_time']]++;

							}
							$equipment_busy_times_new = array();
							foreach($equipment_busy_times as $k=>$v){
								if($v>=$equipment_num) $equipment_busy_times_new[$k]=1;
							}
							$equipment_busy_times=$equipment_busy_times_new;

							foreach($service_times as $stime) if(isset($equipment_busy_times[$stime])){ $is_equip_overlap = true; $equip_overlap[] = $eq_info['equipment_name']; }
						}
					}
				}

				if($is_block_overlap) $overlap_msg .= 'This booking overlaps a block! ';
				else if($is_overlap) $overlap_msg .= 'This booking overlaps another appointment! ';
				else if($is_transit_overlap) $overlap_msg .= 'This booking overlaps transition time! ';
				if($is_equip_overlap)  $overlap_msg .= implode(', ',array_unique($equip_overlap)).' is not available at this time! ';

				if($is_overlap || $is_block_overlap ||  $is_equip_overlap || $is_transit_overlap) exit('SERVICE_OVERLAP|'.$overlap_msg);
			}

			DB::insert('salon_appointments',array(
				'salon_id' => SALON_ID,
				'customer_id' => $customer_id,
				'appt_date' => date('Y-m-d',strtotime($_POST['date'])),
				'checkout_book' => isset($_POST['checkout_book'])&&$_POST['checkout_book']?1:0,
				'visit_type_id' => isset($_POST['visit_type'])&&$_POST['visit_type']?(int)$_POST['visit_type']:0,
				'is_prebook' => $prebook
			));
			$appt_id = DB::insert_id();
			$mail_add = '';
			log_event('create_appt', $appt_id, $customer_id, 0, 'on '.date('m/d/Y',strtotime($_POST['date'])));
			if($appt_id){

				if(isset($_POST['client_notes']) && $_POST['client_notes']!=''){

					DB::update('users',array(
						'guest_notes' => $_POST['client_notes']
					)," id='".(int)$customer_id."' limit 1");
				}


				foreach($service_ids as $k=>$serv_id){
					if($service_ids[$k] && $stylist_ids[$k] && $times[$k]!==''){
						$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price,ss.ep_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$stylist_ids[$k]."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$service_ids[$k]."' limit 1");
						$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
						$is_employee = 0;
						$is_ep = 0;
						if( $client['is_fnf'] && $serv_q['employee_price'] > 0 ){ $price = $serv_q['employee_price']; $is_employee=1; }
						if(isset($client['role_id']) &&  $client['role_id']!=3 && $serv_q['ep_price'] > 0 ){ $price = $serv_q['ep_price']; $is_ep=1; }

						if(!getTimeSec($times[$k])) exit('An error occurred!');
						DB::insert('salon_appointments_services',array(
							'appt_id' => $appt_id,
							'provider_id' => (int)$stylist_ids[$k],
							'service_id'=> (int)$service_ids[$k],
							'service_time' => $times[$k],
							'book_price' => (float) $price,
							'service_time_sec' => getTimeSec($times[$k]),
							'notes' => $notes,
							'is_employee' => $is_employee,
							'is_ep' =>$is_ep,
							'is_request' => (isset($_POST['is_request']) && $_POST['is_request'] ? 1 : 0 )
						));


						$stylist = DB::get_row("select u.*,pi.code_name,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stylist_ids[$k]."' limit 1");
						if($stylist && $stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];

						log_event('add_service', $appt_id, $customer_id, $service_ids[$k], $times[$k].' with '.$stylist['first_name']);

						$mail_add.= "<br/>
Service Type: ".$serv_q['service_name']."<br/>
Stylist: ".$stylist['first_name']."<br/>
Time: ".$times[$k]."<br/>
Date: ".date('M d, Y',strtotime($_POST['date']))."<br/>";

						 
						include_once('includes/appointment_alerts.php');
						booked_appt_alert( $stylist['id'], $client['first_name'] . ' ' . $client['last_name'], $salon_info['name'], strtotime($_POST['date']), $times[$k], $service_ids[$k] );
						 

					}
				}
				if($salon_info['enable_covid_form_notif'] && $salon_info['covid_form_notif_h']=='booking'){

					DB::insert('covid_forms', array( 'user_id' =>$customer_id,
										'appt_id' => $appt_id,
										'sent_time' => time() ) );
				}

				$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
				$uinf = DB::get_row("select * from users where id='".$customer_id."' limit 1");

				$subj = 'Your appointment is booked!';

				$body = "Hi ".$uinf['first_name'].",<br/>
<br/>
Your Appointment at ".$salon_info['name']." has been booked:<br/>
".$mail_add."
<br/>
<a href='".B_URL."/booked'>Click here to view your Booked Services.</a>";

		if( $uinf['parent_user_id'] ){
			$p_u_info = DB::get_row("select * from users where id='".$uinf['parent_user_id']."' limit 1");
			$uinf['email']=$p_u_info['email'];
		}

		//mail($uinf['email'],$subj,$body,"Content-Type: text/html; charset=UTF-8\r\n".(MAIL_FROM?'From: '.MAIL_FROM:''));
		if(isset($_POST['mail_client']) && $_POST['mail_client']) sendEmail($uinf['email'],$subj,$body,true);
		//if(defined(APPT_BCC_EMAIL) && APPT_BCC_EMAIL!='') mail(APPT_BCC_EMAIL,$subj,$body,"Content-Type: text/html; charset=UTF-8\r\n".(MAIL_FROM?'From: '.MAIL_FROM:''));
		//if(defined(APPT_BCC_EMAIL) && APPT_BCC_EMAIL!='') sendEmail(APPT_BCC_EMAIL,$subj,$body,true);


				if($_POST['booked_by']){
					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $_POST['booked_by'],
						'dtime' => time(),
						'event_type' => 'book',
						'details' => 'Appointment Booked'

					));
				}

				exit('ok');
			}
		}
	}
	/*
	customer_id: 5
service_ids[]: 22
service_ids[]: 25
service_ids[]: 22
stylist_ids[]: 23
stylist_ids[]: 3
stylist_ids[]: 23
times[]: 8:00 AM
times[]: 10:15 AM
times[]: 8:15 AM
date: 03/23/2018
	*/
	exit('An error occurred!');
}

if($action == 'get_serv_equipment'){
	$equipments = array();
	$bookdate = date('Y-m-d',strtotime($_POST['date']));
	$start_time = $_POST['time'];
	$stylist_id = (int)$_POST['stylist_id'];
	$service_id = (int)$_POST['service_id'];
	$appt_s_id = (int)$_POST['appt_s_id'];
	$customer_id = (int)$_POST['customer_id'];
	
	$apptserv = array();
	if($appt_s_id) $apptserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$appt_s_id."' limit 1");
	
	
	
	$c_serv = DB::get_row("select if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, ifnull(ps.equipment_id,ss.equipment_id) as equipment_id,ifnull(ps.equipments_list,ss.equipments_list) as equipments_list from salon_services ss inner join provider_services ps on ps.service_id=ss.id left join customer_services cs on cs.customer_id='".$customer_id."' and cs.service_id=ss.id where ss.id='".(int)$service_id."' and ps.provider_id='".(int)$stylist_id."' and ss.salon_id='".SALON_ID."' limit 1");
	
	if($c_serv){
		
		if($apptserv && $apptserv['duration_changed']){
			$c_serv['service_duration'] = $apptserv['duration_changed'];
			$c_serv['service_time_1'] = $apptserv['dur_appl_chng'];
			$c_serv['process_time'] = $apptserv['dur_procs_chng'];
			$c_serv['service_time_2'] = $apptserv['dur_finsh_chng'];
		}
		
		$calcdur = $c_serv['service_time_1']+$c_serv['process_time']+$c_serv['service_time_2'];
		if($calcdur>$c_serv['service_duration']) $c_serv['service_duration']=$calcdur;
		
		$service_tlist =getTimesArr($start_time,$c_serv['service_duration']);
		
		if($c_serv['equipment_id']){
			
			
			$def_eq_id = $c_serv['equipment_id'];
			if($apptserv && $apptserv['equipment_id']) $def_eq_id = $apptserv['equipment_id'];
			
			$eq_list = array($c_serv['equipment_id']);
			if($c_serv['equipments_list']!=''){
				$eq_list=explode(',',$c_serv['equipments_list']);
				foreach($eq_list as $k=>$v) $eq_list[$k]=(int)$v;
			}
			
			
			$equipment_busy_times = array();
			//foreach($eq_list as $k=>$v) loadEquipmentBusy($v, $bookdate);
			
			foreach($eq_list as $eqid){
				
				$eqinfo = DB::get_row("select * from salon_equipment where id='".(int)$eqid."' limit 1");
				if($eqinfo){
					$equipment_busy_times = array();
					
					$equipment_num = $eqinfo['qty'];
					
					$busyhours = DB::query("select sas.service_time, 
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1, 
				if(sas.dur_finsh_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2, if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_1>0,ss.service_time_2,0)))) as service_time_2, 
				if(sas.dur_procs_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time,if(ps.service_time_1>0, ps.process_time, if(ss.service_time_1>0,ss.process_time,0)))) as process_time, 
				if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration 
					from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id  where sapp.salon_id='".SALON_ID."' and sapp.appt_date>='".addslashes(date('Y-m-d',strtotime($bookdate)))."' and sapp.appt_date<='".addslashes(date('Y-m-d',strtotime($bookdate)))." 23:59:59' and ifnull(sas.equipment_id,ifnull(ps.equipment_id,ss.equipment_id))='".(int)$eqinfo['id']."'".($appt_s_id?" and sas.appt_serv_id!='".$appt_s_id."' ":""));
					while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!=''){
						$r['service_time']=timeFix($r['service_time']);
						if(!isset($equipment_busy_times[$r['service_time']])) $equipment_busy_times[$r['service_time']]=0;
						
						if($r['service_duration']>15){
							$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
							if($calcdur>$r['service_duration']) $r['service_duration']=$calcdur;
							$r['service_time_1'] = $r['service_duration'];
							$r['process_time'] = 0;
							$extr_dur = array_unique(getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']));
							foreach($extr_dur as $stm){
								if(!isset($equipment_busy_times[$stm])) $equipment_busy_times[$stm]=0;
								$equipment_busy_times[$stm]++;
							}
						}else $equipment_busy_times[$r['service_time']]++;
					
					}
					$equipment_busy_times_new = array();
					foreach($equipment_busy_times as $k=>$v){
						if($v>=$equipment_num) $equipment_busy_times_new[$k]=1;
					}
					$equipment_busy_times=$equipment_busy_times_new;
					
					$isav = true;
					foreach($service_tlist as $tm){
						if(isset($equipment_busy_times[$tm])){
							$isav = false;
							break;
						}
					}
					
					
					if($isav) $equipments[]=array('id'=>$eqid,'name'=>$eqinfo['equipment_name']);
				}
			}
			
			if(empty($equipments)) $equipments[]=array('id'=>$def_eq_id,'name'=>'No equipment available!');
		}
	}
	
	header('Content-type: applcation/json');
	echo json_encode($equipments);
	exit;
}

if($action == 'edit_appt'){
	$appt_id = (int) $_POST['appt_id'];
	$appt_info = DB::get_row("select * from salon_appointments where id='".$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt_info) exit('An error occurred!');

	$appt_date = date('Y-m-d',strtotime($_POST['date']));

	if($appt_info['is_paid']) exit('Cannot edit paid appointments!');
	$customer_id = (int)$_POST['customer_id'];
	$notes = $_POST['notes'];
	$service_ids = $_POST['service_ids'];
	$stylist_ids = $_POST['stylist_ids'];
	$serv_notes = $_POST['serv_notes'];
	$appt_serv_ids = $_POST['appt_serv_ids'];
	$times = $_POST['times'];
	$equipment_ids = $_POST['equipment_ids'];
	$booking_date =  date('Y-m-d',strtotime($_POST['date']));
	$prebook = isset($_POST['prebook'])&&$_POST['prebook']?1:0;
	$is_request = isset($_POST['is_request'])&&$_POST['is_request']?1:0;
	if($customer_id){
		$client = DB::get_row("select u.* from users u left join salon_customers sc on sc.customer_id=u.id and  sc.salon_id='".SALON_ID."'    where u.id='".$customer_id."'  and u.status=1 ");
		if($client){


			if(isset($_POST['check_overlaps']) && $_POST['check_overlaps']){

				$booking_datetime=strtotime($booking_date);
				$w_day = date('w',$booking_datetime);
				$salon_h = DB::get_row("select * from salon_hours where salon_id='".SALON_ID."' and weekday='".$w_day."' ");
				$salon_dates = array();
				$stylist_add_per = DB::query("select * from salon_adddays where salon_id='".SALON_ID."' and end_date>='".$booking_date."'  ");
				while($r=DB::fetch_assoc($stylist_add_per)){
					$start_d = strtotime($r['start_date']);
					$end_d = strtotime($r['end_date']." 23:59:59");
					while($start_d<=$end_d){
						$salon_dates[date('Y-m-d',$start_d)]=$r;
						$start_d+=86400;
					}
				}
				if(isset($salon_dates[$booking_date])){

					$salon_h['is_open']=1;
					$salon_h['start_time']=$salon_dates[$booking_date]['start_time'];
					$salon_h['end_time']=$salon_dates[$booking_date]['end_time'];
					if($salon_h['end_time']=='00:00') $salon_h['end_time']='23:59';
					if($salon_h['end_time']=='12:00 AM') $salon_h['end_time']='11:59 PM';
				}
				$d_start = 0;
				$d_end = 0;
				if($salon_h){
					if(!$salon_h['is_open']){  exit('SERVICE_OVERLAP|This booking is outside of regular hours. Proceed?' ); }
					else{
						$d_start = getTimeSec($salon_h['start_time']);
						$d_end = getTimeSec($salon_h['end_time']);

					}

				}



				$is_overlap = false;
				$is_block_overlap = false;
				$is_equip_overlap = false;
				$overlap_msg = '';
				$equip_overlap = array();
				foreach($service_ids as $k=>$serv_id){


					$stylist = DB::get_row("select u.*,pi.code_name,pi.appointment_alerts,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stylist_ids[$k]."' and pi.salon_id='".SALON_ID."' limit 1");

					$stylist_schedule_index = 1;
					if($stylist['fut_enable_sched'] && strtotime($stylist['fut_date_sched'])<=$booking_datetime){

						if($stylist['fut_althern_sched']){
							$is_odd_week = date('W',$booking_datetime)%2!=0;
							if($is_odd_week) $stylist_schedule_index = 4;
							else $stylist_schedule_index = 3;
						}else $stylist_schedule_index = 3;
					}else{
						if($stylist['althern_sched']){
							$is_odd_week = date('W',$booking_datetime)%2!=0;
							if($is_odd_week) $stylist_schedule_index = 2;
						}
					}

					$stylist_adday = false;
					$add_day_info = array();
					$stylist_add_per = DB::query("select * from provider_adddays where provider_id='".(int)$stylist_ids[$k]."' and end_date>='".$booking_date."' and start_date<='".$booking_date."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_add_per)){
						$stylist_adday=true;
						$add_day_info=$r;
					}
					$schq = DB::get_row("select * from provider_schedule where provider_id='".(int)$stylist_ids[$k]."' and salon_id='".SALON_ID."' and sch_index='".$stylist_schedule_index."' and weekday='".$w_day."'");
					$stl_d_start = $d_start;
					$stl_d_end = $d_end;
					if($schq){
						if($schq && ($schq['is_available'] || $stylist_adday)){
							if($add_day_info['start_time']) $schq['start_time']=$add_day_info['start_time'];
							if($add_day_info['end_time']) $schq['end_time']=$add_day_info['end_time'];
							if($schq['end_time']=='00:00') $schq['end_time']='23:59';
							if($schq['end_time']=='12:00 AM') $schq['end_time']='11:59 PM';

							$s_start = getTimeSec($schq['start_time']);
							if($s_start>$stl_d_start) $stl_d_start=$s_start;
							$s_end = getTimeSec($schq['end_time']);
							if($s_end<$stl_d_end) $stl_d_end=$s_end;
						}  else {  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?;' ); }
					}elseif($add_day_info){
						$stl_d_start = getTimeSec($salon_h['start_time']);
						$stl_d_end = getTimeSec($salon_h['end_time']);
						if($add_day_info['start_time']){
							$s_start = getTimeSec($add_day_info['start_time']);
							if($s_start>$stl_d_start) $stl_d_start=$s_start;
						}
						if($add_day_info['end_time']){
							if($add_day_info['end_time']=='00:00') $add_day_info['end_time']='23:59';
							if($add_day_info['end_time']=='12:00 AM') $add_day_info['end_time']='11:59 PM';
							$s_end = getTimeSec($add_day_info['end_time']);
							if($s_end<$stl_d_end) $stl_d_end=$s_end;
						}
					} //else  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?' );


					$busy_times = array();
					$block_times = array();
					$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id where sas.provider_id='".(int)$stylist_ids[$k]."' and sapp.id!='".$appt_id."' and sapp.appt_date='".addslashes($booking_date)."'");
					while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!='' ){
						$busy_times[$r['service_time']]=1;
						$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
						if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
						if($r['service_duration']>15){
							$extr_dur = getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']);
							foreach($extr_dur as $stm) $busy_times[$stm]=1;
						}

					}
					$stylist_off_h = DB::query("select * from provider_offhours where  `date`='".addslashes($booking_date)."'  and provider_id='".(int)$stylist_ids[$k]."' and salon_id='".SALON_ID."'");
					while($r=DB::fetch_assoc($stylist_off_h)){
						$extr_dur =getTimesArr($r['start_time'],$r['duration']);
						foreach($extr_dur as $stm){ $busy_times[$stm]=1; $block_times[$stm]=1; }
					}
					//if(isset($busy_times[$times[$k]])) $is_overlap = true;
					$c_serv = DB::get_row("select if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)) as service_duration, if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)) as service_time_1, if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0)) as process_time, ifnull(ps.equipment_id,ss.equipment_id) as equipment_id,ifnull(ps.equipments_list,ss.equipments_list) as equipments_list from salon_services ss inner join provider_services ps on ps.service_id=ss.id where ss.id='".(int)$serv_id."' and ps.provider_id='".(int)$stylist_ids[$k]."' limit 1");
					
					if(isset($equipment_ids[$k]) && $equipment_ids[$k]) $c_serv['equipment_id'] = $equipment_ids[$k];

					$service_times = array();
					$service_times[]=$times[$k];
					if($c_serv['service_duration']>15){
						$extr_dur = getTimesArr($times[$k],$c_serv['service_duration'],$c_serv['service_time_1'],$c_serv['process_time']);
						foreach($extr_dur as $stm) $service_times[]=$stm;
					}


					if($d_end){
						foreach($service_times  as $tm){
							$serv_time = getTimeSec($tm);
							if($serv_time<$d_start || $serv_time>$d_end) exit('SERVICE_OVERLAP|This booking is outside of regular hours. Proceed?' );
						}
					}
					if($stl_d_end){
						foreach($service_times  as $tm){
							$serv_time = getTimeSec($tm);
							if($serv_time<$stl_d_start || $serv_time>$stl_d_end)  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is off schedule. Book anyway?.' );
						}
					}


					if($schq && $schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
						$brk_check_change = DB::get_row("select * from provider_schedule_break_changes where salon_id='".SALON_ID."' and `date`='".addslashes($booking_date)."' and provider_id='".(int)$stylist_ids[$k]."' limit 1");
						if($brk_check_change){
							if($brk_check_change['is_removed']){
								$schq['has_break']=0;
								$schq['brk_start_time']='';
								$schq['brk_end_time']='';
							}else{
								$schq['brk_start_time']=$brk_check_change['start_time'];
								$schq['brk_end_time']=$brk_check_change['end_time'];
							}
						}
						if($schq['has_break']  && $schq['brk_start_time']!='' && $schq['brk_end_time']!=''){
							$brk_start_time = getTimeSec($schq['brk_start_time']);
							$brk_end_time = getTimeSec($schq['brk_end_time']);
							foreach($service_times  as $tm){
								$serv_time = getTimeSec($tm);
								if($serv_time>=$brk_start_time && $serv_time<$brk_end_time)  exit('SERVICE_OVERLAP|'.$stylist['first_name'].' is on a break. Book anyway?.' );
							}
						}
					}
					foreach($service_times as $stime){ if(isset($block_times[$stime])) $is_block_overlap = true; else if(isset($busy_times[$stime])) $is_overlap = true; }

					if($c_serv['equipment_id']){
						$eq_info = DB::get_row("select * from salon_equipment where id='".$c_serv['equipment_id']."' and salon_id='".SALON_ID."'");
						if($eq_info){
							$equipment_num = $eq_info['qty'];

							$busyhours = DB::query("select sas.service_time, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration from salon_appointments_services sas inner join salon_appointments sapp on sapp.id=sas.appt_id inner join salon_services ss on ss.id=sas.service_id inner join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id where sapp.salon_id='".SALON_ID."' and sapp.appt_date='".addslashes($booking_date)."' and sapp.id!='".$appt_id."'  and ifnull(sas.equipment_id,ifnull(ps.equipment_id,ss.equipment_id))='".$c_serv['equipment_id']."'");
							while($r=DB::fetch_assoc($busyhours)) if($r['service_time']!=''){
								if(!isset($equipment_busy_times[$r['service_time']])) $equipment_busy_times[$r['service_time']]=0;

								if($r['service_duration']>15){
									$calcdur = $r['service_time_1']+$r['process_time']+$r['service_time_2'];
									if($calcdur>$r['service_duration'] && ($r['process_time']>0 || $r['service_time_2']>0)) $r['service_duration']=$calcdur;
									$r['service_time_1'] = $r['service_duration'];
									$r['process_time'] = 0;
									$extr_dur = array_unique(getTimesArr($r['service_time'],$r['service_duration'],$r['service_time_1'],$r['process_time']));
									foreach($extr_dur as $stm){
										if(!isset($equipment_busy_times[$stm])) $equipment_busy_times[$stm]=0;
										$equipment_busy_times[$stm]++;
									}
								}else $equipment_busy_times[$r['service_time']]++;

							}
							$equipment_busy_times_new = array();
							foreach($equipment_busy_times as $k=>$v){
								if($v>=$equipment_num) $equipment_busy_times_new[$k]=1;
							}
							$equipment_busy_times=$equipment_busy_times_new;

							foreach($service_times as $stime) if(isset($equipment_busy_times[$stime])){ $is_equip_overlap = true; $equip_overlap[] = $eq_info['equipment_name']; }
						}
					}
				}

				if($is_block_overlap) $overlap_msg .= 'This booking overlaps a block! ';
				else if($is_overlap) $overlap_msg .= 'This booking overlaps another appointment! ';
				if($is_equip_overlap)  $overlap_msg .= implode(', ',array_unique($equip_overlap)).' is not available at this time! ';

				if($is_overlap || $is_block_overlap || $is_equip_overlap) exit('SERVICE_OVERLAP|'.$overlap_msg);
			}
			include_once('includes/appointment_alerts.php');

			DB::update('salon_appointments',array(
				'appt_date' => date('Y-m-d',strtotime($_POST['date'])),
				'customer_id' => $customer_id,
				'tip' => -1,
				'visit_type_id' => isset($_POST['visit_type'])&&$_POST['visit_type']?(int)$_POST['visit_type']:0,
				'is_prebook' =>$prebook
			)," id='".$appt_id."' and salon_id='".SALON_ID."' limit 1 ");
			log_event('edit_appt', $appt_id, $customer_id, 0, 'appt_date = '.date('m/d/Y',strtotime($_POST['date'])));

			if(strtotime($appt_info['appt_date'])!=strtotime($_POST['date'])){
				DB::update('salon_appointments_services',array('is_sms_notified'=>0)," appt_id='".$appt_id."' ");
				DB::insert('salon_appointments_log',array(
					'appt_id' => $appt_id,
					'user_id' => $user_id,
					'dtime' => time(),
					'event_type' => 'date_change',
					'details' => 'Appointment date changed from '.date('m/d/Y',strtotime($appt_info['appt_date'])).' to '.date('m/d/Y',strtotime($_POST['date']))

				));

				$stl_notif = array();
				foreach($stylist_ids as $stlid){
					if(isset($stl_notif[$stlid])) continue;
					$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stlid."' and pi.salon_id='".SALON_ID."' limit 1");
					
					edited_appt_date_alert( $stlid, $appt_id, $appt_info['appt_date'], $_POST['date'], $client['first_name'] . ' ' . $client['last_name'],$salon_info['main_location'] );
					
					$stl_notif[$stlid]=$stlid;
				}


			}

			$appt_info['appt_date'] = date('Y-m-d',strtotime($_POST['date']));

			$mail_add = '';

			if($appt_id){

				if(isset($_POST['client_notes']) && $_POST['client_notes']!=''){

					DB::update('users',array(
						'guest_notes' => $_POST['client_notes']
					)," id='".(int)$customer_id."' limit 1");
				}


				if(isset($_POST['deleted']) && is_array($_POST['deleted']) && count($_POST['deleted'])){
					foreach($_POST['deleted'] as $sid){
						if($sid>0){
							$bookedserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$sid."' and appt_id='".$appt_id."'  limit 1");
							DB::delete('salon_appointments_services', " appt_serv_id='".(int)$sid."' and appt_id='".$appt_id."' ");
							log_event('delete_service', $appt_id, $customer_id, 0, '', date('Y-m-d',strtotime($appt_info['appt_date'])),$bookedserv['service_time'],$bookedserv['provider_id']);
						}
					}
				}

				foreach($service_ids as $k=>$serv_id){
					if($service_ids[$k] && $stylist_ids[$k] && $times[$k]!==''){
						$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$stylist_ids[$k]."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$service_ids[$k]."' limit 1");
						$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
						$is_employee = 0;
						$is_ep = 0;
						if( $client['is_fnf'] && $serv_q['employee_price'] > 0 ){ $price = $serv_q['employee_price']; $is_employee=1; }
						if( isset($client['role_id']) &&  $client['role_id']!=3 && $serv_q['ep_price'] > 0 ){ $price = $serv_q['ep_price']; $is_ep=1; }

						if(isset($appt_serv_ids[$k]) && $appt_serv_ids[$k]>0){
							$bookedserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$appt_serv_ids[$k]."' and appt_id='".$appt_id."'  limit 1");
							//if(!getTimeSec($times[$k])) exit('An error occurred!');
							$upd_data = array(
								'provider_id' => (int)$stylist_ids[$k],
								'service_id'=> (int)$service_ids[$k],
								//'service_time' => $times[$k],
								'book_price' => $price,
								'is_request' => $is_request
								//'service_time_sec' => getTimeSec($times[$k])
							);
							
							if(isset($equipment_ids[$k]) && $equipment_ids[$k]) $upd_data['equipment_id'] = $equipment_ids[$k];
							if(getTimeSec($times[$k])){
								$upd_data['service_time']=$times[$k];
								$upd_data['service_time_sec']=getTimeSec($times[$k]) ;
								if($upd_data['service_time_sec']!=$bookedserv['service_time_sec']){
									DB::update('salon_appointments_services',array('is_sms_notified'=>0)," appt_id='".$appt_id."' ");
									DB::insert('salon_appointments_log',array(
										'appt_id' => $appt_id,
										'user_id' => $user_id,
										'dtime' => time(),
										'event_type' => 'time_change',
										'details' => 'Service '.$serv_q['service_name'].' time changed from '.$bookedserv['service_time'].' to '.$times[$k]

									));
									$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$upd_data['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
									 
									edited_appt_servicetime_alert( (int)$upd_data['provider_id'], $appt_id, $serv_q['service_name'], $bookedserv['service_time'], $upd_data['service_time'], $client['first_name'] . ' ' . $client['last_name'],$salon_info['main_location'] );
									 
								}

								if($upd_data['provider_id']!=$bookedserv['provider_id']){
									$oldstylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$bookedserv['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
									if($oldstylist['code_name']!='') $oldstylist['first_name']=$oldstylist['code_name'];
									$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$upd_data['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
									if($stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];
									DB::insert('salon_appointments_log',array(
										'appt_id' => $appt_id,
										'user_id' => $user_id,
										'dtime' => time(),
										'event_type' => 'stylist_change',
										'details' => 'Service '.$serv_q['service_name'].' provider changed from '.$oldstylist['first_name'].' to '.$stylist['first_name']

									));

									edited_appt_providerchange_alert( $stylist['id'], $oldstylist['id'], $appt_id,  $client['first_name'] . ' ' . $client['last_name'],$salon_info['main_location'],$stylist['appointment_alerts'], $oldstylist['appointment_alerts']  );
								}
							}
							$upd_data['notes']=$serv_notes[$k];

							if($bookedserv['notes']!=$upd_data['notes'] && $upd_data['notes']!=''){
								$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book,pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$upd_data['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
								
								edited_appt_memo_alert( $upd_data['provider_id'], $appt_id,  $times[$k],  $client['first_name'] . ' ' . $client['last_name'],$salon_info['main_location'] );
							}

							DB::update('salon_appointments_services',$upd_data," appt_serv_id='".(int)$appt_serv_ids[$k]."' and appt_id='".$appt_id."' ");

							//if(!getTimeSec($times[$k])) exit('An error occurred!');
							$stylist = DB::get_row("select u.*,pi.code_name,pi.appointment_alerts,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stylist_ids[$k]."' and pi.salon_id='".SALON_ID."' limit 1");
						$mail_add.= "<br/>
Service Type: ".$serv_q['service_name']."<br/>
Stylist: ".$stylist['first_name']."<br/>
Time: ".$times[$k]."<br/>
Date: ".date('M d, Y',strtotime($appt_info['appt_date']))."<br/>";


						 
							//include_once('includes/appointment_alerts.php');
							//edited_appt_alert( $stylist['id'], $appt_id );
						 


						}else{
							//if(!getTimeSec($times[$k])) exit('An error occurred!');
							$stylist = DB::get_row("select u.*,pi.code_name,pi.appointment_alerts,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$stylist_ids[$k]."' and pi.salon_id='".SALON_ID."' limit 1");
							if($stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];

							DB::insert('salon_appointments_services',array(
								'appt_id' => $appt_id,
								'provider_id' => (int)$stylist_ids[$k],
								'service_id'=> (int)$service_ids[$k],
								'service_time' => $times[$k],
								'book_price' => $price,
								'service_time_sec' => getTimeSec($times[$k]),
								'notes' => $serv_notes[$k],
								'is_request' => $is_request,
								'is_employee'=>$is_employee,
								'is_ep'=>$is_ep
							));
						$mail_add.= "<br/>
Service Type: ".$serv_q['service_name']."<br/>
Stylist: ".$stylist['first_name']."<br/>
Time: ".$times[$k]."<br/>
Date: ".date('M d, Y',strtotime($appt_info['appt_date']))."<br/>";
							log_event('add_service', $appt_id, $customer_id, $service_ids[$k], 'service_time = '.getTimeSec($times[$k]).', provider_id='.$stylist_ids[$k]);

							//booked_appt_alert( $stylist['id'], $client['first_name'] . ' ' . $client['last_name'], $salon_info['name'], strtotime($appt_info['appt_date']), $times[$k], $appt_id );

							edited_appt_newservice_alert($stylist['id'], $appt_id, $serv_q['service_name'], $times[$k], $client['first_name'] . ' ' . $client['last_name'],  $salon_info['main_location'] );

						}
					}
				}


				$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
				$uinf = DB::get_row("select * from users where id='".$customer_id."' limit 1");

				$subj = 'Your appointment is changed!';

				$body = "Hi ".$uinf['first_name'].",<br/>
<br/>
Your Appointment at ".$salon_info['name']." has been changed:<br/>
".$mail_add."
<br/>
<a href='".B_URL."/booked'>Click here to view your Booked Services.</a>";

		//mail($uinf['email'],$subj,$body,"Content-Type: text/html; charset=UTF-8\r\n".(MAIL_FROM?'From: '.MAIL_FROM:''));
		if(isset($_POST['mail_client']) && $_POST['mail_client']) sendEmail($uinf['email'],$subj,$body,true);




					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user['id'],
						'dtime' => time(),
						'event_type' => 'edit',
						'details' => 'Appointment Edited'

					));

					DB::delete('salon_appointments_tips', "  appt_id='".(int)$appt_id."' ");

				exit('ok');
			}
		}
	}
	/*
	customer_id: 5
service_ids[]: 22
service_ids[]: 25
service_ids[]: 22
stylist_ids[]: 23
stylist_ids[]: 3
stylist_ids[]: 23
times[]: 8:00 AM
times[]: 10:15 AM
times[]: 8:15 AM
date: 03/23/2018
	*/
	exit('An error occurred!');
}

if($action == 'add-client'){
	try {
		$all_salons= array(SALON_ID);
		if(defined('ALL_SALON_IDS')) $all_salons=ALL_SALON_IDS;
		/*if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			exit('Provide valid email address.');
		}*/
		if($_POST['email']) {
			$checkuser = DB::get_row("select distinct u.* from users u inner join salon_customers sc on sc.customer_id=u.id and sc.salon_id in (".implode(',',$all_salons).") where email='".addslashes($_POST['email'])."' limit 1");
			if($checkuser){
				exit('The entered email is associated with another client account, '.$checkuser['first_name'].' '.$checkuser['last_name'].'. Please enter a new email or leave the email field blank.');
				if($checkuser['role_id']==3){
					$checkown = DB::get_row("select * from salon_customers where customer_id='".$checkuser['id']."' and salon_id='".SALON_ID."' limit 1");
					if(!$checkown){
						DB::insert('salon_customers',array('customer_id'=>$checkuser['id'],'salon_id'=>SALON_ID));
					}
					$upddata = array(
						'first_name' => $_POST['first_name'],
						'last_name' => $_POST['last_name']
					);
					if($checkuser['phone_clean']==''){
						$upddata['phone']=$_POST['mobile_num'];
						$upddata['phone_clean']=preg_replace("/[^0-9]/", "", $_POST['mobile_num'] );
					}
					DB::update('users',$upddata, " id='".$checkuser['id']."' limit 1" );
					//exit('There is already someone else registered with this email.');
					exit('ok||'.$checkuser['id']);
				}else $_POST['email']='';
			}
		}
		if(!empty($_POST['mobile_num'])){
			$checkuser = DB::get_row("select distinct u.* from users u inner join salon_customers sc on sc.customer_id=u.id and sc.salon_id in (".implode(',',$all_salons).") where phone_clean='".addslashes(preg_replace("/[^0-9]/", "", $_POST['mobile_num'] ))."' limit 1");
			if($checkuser){
				exit('The entered phone number is associated with another client account, '.$checkuser['first_name'].' '.$checkuser['last_name'].'. Please enter a new phone number or leave the phone number field blank.');
				if($checkuser['role_id']==3){
					//exit('There is already someone else registered with this phone.');
					$checkown = DB::get_row("select * from salon_customers where customer_id='".$checkuser['id']."' and salon_id='".SALON_ID."' limit 1");
					if(!$checkown){
						DB::insert('salon_customers',array('customer_id'=>$checkuser['id'],'salon_id'=>SALON_ID));
					}
					$upddata = array(
						'first_name' => $_POST['first_name'],
						'last_name' => $_POST['last_name']
					);
					if($checkuser['email']==''){
						$upddata['email']=$_POST['email'];
					}
					DB::update('users',$upddata, " id='".$checkuser['id']."' limit 1" );
					exit('ok||'.$checkuser['id']);
				}else $_POST['mobile_num']='';
			}
		} else if(empty($_POST['mobile_num'])) {
			//exit('Provide valid phone.');
		}
		
		$upddata = array(
			'first_name' => $_POST['first_name'],
			'last_name' => $_POST['last_name'],
			'phone' => $_POST['mobile_num'],
			'phone_clean' => preg_replace("/[^0-9]/", "", $_POST['mobile_num'] ),
			'email' => $_POST['email'],
			'role_id' => 3
		);
		if($_POST['referrer_id']>0) $upddata['referrer_id'] = $_POST['referrer_id'];
		if($_POST['parent_id']>0) $upddata['parent_user_id'] = $_POST['parent_id'];
		DB::insert('users',$upddata );

		$uid = DB::insert_id();
		log_event('add_client', 0, $uid, 0, '');
		if($uid){
			DB::insert('salon_customers',array('customer_id'=>$uid,'salon_id'=>SALON_ID));
			exit('ok||'.$uid);
		}
		exit("An Error Occurred.");
	} catch(\Exception $e) {
		exit($e->getMessage());
	}
}

if($action == 'book_edit_form'){

	include('includes/book_edit.php');
	exit;
}
if($action == 'book_new_form_v2'){

	include('includes/book_new_v2.php');
	exit;
}
if($action == 'book_new_form'){

	include('includes/book_new.php');
	exit;
}
if($action == 'appt_set_empl_ret_price'){

	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)){
		$sinf = DB::get_row("select * from salon_appointments_retail where appt_retail_id='".(int)$_GET['appt_retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
		if($sinf){
			if($_GET['is_employee']){
				$upd_data = array(
					'is_employee' => 1,
					'bef_empl_pr' => $sinf['price'],
					'price' => (float) $_GET['price']
				);
				DB::update('salon_appointments_retail',$upd_data," appt_retail_id='".$sinf['appt_retail_id']."' limit 1");
			}else{
				$upd_data = array(
					'is_employee' => 0,
					'bef_empl_pr' => 0,
					'price' => (float) $_GET['price']
				);
				DB::update('salon_appointments_retail',$upd_data," appt_retail_id='".$sinf['appt_retail_id']."' limit 1");
			}
			if(!$appt['is_paid']) DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
	}

	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_set_empl_price'){

	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)){
		$sinf = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['appt_serv_id']."' and  appt_id='".(int)$appt_id."' limit 1");
		if($sinf){
			if($_GET['is_employee']){
				$upd_data = array(
					'is_employee' => 1,
					'bef_empl_pr' => $sinf['book_price'],
					'book_price' => (float) $_GET['price']
				);
				DB::update('salon_appointments_services',$upd_data," appt_serv_id='".$sinf['appt_serv_id']."' limit 1");
			}else{
				if(!$_GET['price']){
					$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price,ss.ep_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$sinf['provider_id']."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$sinf['service_id']."' limit 1");
					if($serv_q){
						$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
						if($price)$_GET['price'] = $price;
					}
				}
				$upd_data = array(
					'is_employee' => 0,
					'bef_empl_pr' => 0,
					'book_price' => (float) $_GET['price']
				);
				DB::update('salon_appointments_services',$upd_data," appt_serv_id='".$sinf['appt_serv_id']."' limit 1");
			}
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
	}

	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_update'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)){
	if(isset($_GET['field'])){
		if($_GET['field']=='tax' || $_GET['field']=='tip' || $_GET['field'] == 'discount' || $_GET['field'] == 'discount_retail'){
			$val = (float)$_GET['val'];
			if($_GET['field'] == 'discount' && $val<0) $val*=-1;
			if($_GET['field'] == 'discount_retail' && $val<0) $val*=-1;
			if($val<0) $val=0;
			$upd_data=array();
			$upd_data[$_GET['field']]=$val;
			DB::update('salon_appointments',$upd_data," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if( $_GET['field'] == 'discount_perc'){
			$val = (float)$_GET['val'];
			if($val<0) $val*=-1;
			if($val<0) $val=0;

			$subtotal = $appt['total_service']+$appt['series_total'] ;

			$upd_data=array();
			$upd_data['discount']=($subtotal/100)* $val;
			DB::update('salon_appointments',$upd_data," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if( $_GET['field'] == 'discount_retail_perc'){
			$val = (float)$_GET['val'];
			if($val<0) $val*=-1;
			if($val<0) $val=0;

			$subtotal =  $appt['total_retail'];

			$upd_data=array();
			$upd_data['discount_retail']=($subtotal/100)* $val;
			DB::update('salon_appointments',$upd_data," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if( $_GET['field'] == 'ref_credits_rdm'){
			$val = (float)$_GET['val'];
			if($val<0) $val=0;

			$redeemed_details = array();
			if($appt['redeemed_details']!='') $redeemed_details = json_decode($appt['redeemed_details'],true);
			$redeemed_details['ref_credits_rdm'] = array(
				'amount'=>$val
			);

			$upd_data=array();
			$upd_data['redeemed_details']=json_encode($redeemed_details);
			DB::update('salon_appointments',$upd_data," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if( $_GET['field'] == 'loyalty_points'){
			$val = (float)$_GET['val'];
			if($val<0) $val=0;

			$redeemed_details = array();
			if($appt['redeemed_details']!='') $redeemed_details = json_decode($appt['redeemed_details'],true);
			$redeemed_details['loyalty_points'] = array(
				'amount'=>0,
				'points'=>$val
			);

			$upd_data=array();
			$upd_data['redeemed_details']=json_encode($redeemed_details);
			DB::update('salon_appointments',$upd_data," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}

		if( $_GET['field'] == 'discount' || $_GET['field'] == 'discount_perc') $reset_tips=1;
	}

	if(isset($_GET['retail_id'])){
		if(isset($_GET['qty'])){
			$_GET['qty']=(int)$_GET['qty'];
			if($_GET['qty']>0){
				DB::update('salon_appointments_retail',array('qty'=>$_GET['qty'])," appt_retail_id='".(int)$_GET['retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			}else{
				DB::delete('salon_appointments_retail', " appt_retail_id='".(int)$_GET['retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			}
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if(isset($_GET['price'])){
			DB::update('salon_appointments_retail',array('price'=>$_GET['price'])," appt_retail_id='".(int)$_GET['retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		if(isset($_GET['stylist_id'])){
			$_GET['stylist_id']=(int)$_GET['stylist_id'];
			DB::update('salon_appointments_retail',array('stylist_id'=>$_GET['stylist_id'])," appt_retail_id='".(int)$_GET['retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
		}
	}
	if(isset($_GET['discount_retail_id'])){

		if(isset($_GET['price'])){
			$s_discount = 0;
			$sinf = DB::get_row("select * from salon_appointments_retail where appt_retail_id='".(int)$_GET['discount_retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			if($sinf){
				if($_GET['disc_type']=='amnt'){
					$s_discount = (float) $_GET['price'];
					if($s_discount<0) $s_discount=0;
					if($s_discount>$sinf['price']) $s_discount=$sinf['price'];
				}else{
					$s_perc = (float) $_GET['price'];
					if($s_perc>0 && $sinf['price']>0){
						$s_discount = $s_perc*($sinf['price']/100);
						if($s_discount>$sinf['price']) $s_discount=$sinf['price'];
					}
				}

			}

			DB::update('salon_appointments_retail',array('discount'=>$s_discount,'disc_type'=>$_GET['disc_type'])," appt_retail_id='".(int)$_GET['discount_retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
	}
	if(isset($_GET['remove_service']) && $_GET['remove_service']){
		$sinf = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['remove_service']."' and  appt_id='".(int)$appt_id."' limit 1");
		if($sinf) DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_id."' and stylist_id='".(int)$sinf['provider_id']."' ");

		log_event('delete_service', $appt_id, $sinf['customer_id'], $sinf['service_id'], 'removed from ticket',date('Y-m-d',strtotime($appt['appt_date'])),$sinf['service_time'],$sinf['provider_id']);

		DB::delete('salon_appointments_services'," appt_serv_id='".(int)$_GET['remove_service']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['remove_series']) && $_GET['remove_series']){
		DB::delete('salon_appointments_series'," appt_series_id='".(int)$_GET['remove_series']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['remove_giftcard']) && $_GET['remove_giftcard']){
		DB::delete('gift_cards'," id='".(int)$_GET['remove_giftcard']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['remove_promotion']) && $_GET['remove_promotion']){
		DB::delete('salon_appointments_promos'," appt_promo_id='".(int)$_GET['remove_promotion']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['remove_cust_fee']) && $_GET['remove_cust_fee'] && $_POST['fee_name']!=''){
		$removed_fees = array();
		if($appt['removed_fees']!='')$removed_fees = json_decode($appt['removed_fees'],true);
		$removed_fees[]=$_POST['fee_name'];
		DB::update('salon_appointments',array('removed_fees'=>json_encode($removed_fees))," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	}
	if(isset($_GET['add_service_series']) && $_GET['add_service_series']){

		$salon_ids = array(SALON_ID);
		if(defined('ALL_SALON_IDS') && is_array(ALL_SALON_IDS) && in_array(SALON_ID,ALL_SALON_IDS)) $salon_ids =ALL_SALON_IDS;
		$sinf = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['add_service_series']."' and  appt_id='".(int)$appt_id."' limit 1");
		$serie = DB::get_row("select sasr.*,ser.series_name  FROM `salon_appointments_series` sasr inner join salon_appointments sa on sa.id=sasr.appt_id left join salon_series ser on ser.id=sasr.series_id   WHERE sasr.appt_series_id='".(int)$_GET['appt_series_id']."' and sa.customer_id='".$appt['customer_id']."' and sa.is_paid=1 and sa.salon_id in (".implode(',',$salon_ids).") and sasr.qty>sasr.qty_used");
		if($serie && $sinf /*&& $sinf['service_id']==$serie['service_id']*/){
			DB::update('salon_appointments_services',array('used_series_id'=>($_GET['add']?$serie['appt_series_id']:-1),'discount'=>0)," appt_serv_id='".$sinf['appt_serv_id']."' limit 1");
		}


		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['update_service_price']) && $_GET['update_service_price']){
		DB::update('salon_appointments_services',array('book_price'=>$_GET['price']), " appt_serv_id='".(int)$_GET['update_service_price']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['update_service_discount']) && $_GET['update_service_discount']){
		$s_discount = 0;
		$sinf = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['update_service_discount']."' and  appt_id='".(int)$appt_id."' limit 1");
		if($sinf){
			if($_GET['disc_type']=='amnt'){
				$s_discount = (float) $_GET['price'];
				if($s_discount<0) $s_discount=0;
				if($s_discount>$sinf['book_price']) $s_discount=$sinf['book_price'];
			}else{
				$s_perc = (float) $_GET['price'];
				if($s_perc>0 && $sinf['book_price']>0){
					$s_discount = $s_perc*($sinf['book_price']/100);
					if($s_discount>$sinf['book_price']) $s_discount=$sinf['book_price'];
				}
			}

		}

		DB::update('salon_appointments_services',array('discount'=>$s_discount,'disc_type'=>$_GET['disc_type']), " appt_serv_id='".(int)$_GET['update_service_discount']."' and  appt_id='".(int)$appt_id."' ");
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		$reset_tips=1;
	}
	if(isset($_GET['update_service_qty']) && $_GET['update_service_qty']){
		$sas = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['update_service_qty']."' and  appt_id='".(int)$appt_id."' limit 1");
		$_GET['qty']=(int)$_GET['qty'];
		if($_GET['qty']<=0) $_GET['qty']=1;
		if($sas && $_GET['qty']!=$sas['service_qty']){
			$discount = $sas['discount'];
			if($sas['service_qty']>1) $discount = $sas['discount']/$sas['service_qty'];
			$unit_price = $sas['book_price']/$sas['service_qty'];
			DB::update('salon_appointments_services',array('book_price'=>$unit_price*$_GET['qty'], 'discount'=>$discount*$_GET['qty'], 'service_qty'=>$_GET['qty']), " appt_serv_id='".(int)$_GET['update_service_qty']."' and  appt_id='".(int)$appt_id."' ");
			DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
		$reset_tips=1;
	}
	}else if($role==1 && $appt['is_paid']){

		if(isset($_GET['retail_id'])){
			if(isset($_GET['stylist_id'])){
				$_GET['stylist_id']=(int)$_GET['stylist_id'];
				DB::update('salon_appointments_retail',array('stylist_id'=>$_GET['stylist_id'])," appt_retail_id='".(int)$_GET['retail_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			}
		}
	}
	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_check_giftcard'){
	$result = array('amount'=>'$0.00');

	$gc_num = strtoupper(trim($_GET['gc_num']));
	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && is_array(ALL_SALON_IDS) && in_array(SALON_ID,ALL_SALON_IDS)) $salon_ids =ALL_SALON_IDS;
	//if(isset($_SESSION['SALON_SELECTED_CUST']) && in_array((int)$_SESSION['SALON_SELECTED_CUST'],ALL_SALON_IDS)) $salon_ids =ALL_SALON_IDS;
	$checkcode = DB::get_row("select g.* from gift_cards g  inner join salon_appointments sa on sa.id=g.appt_id where g.card_no='".addslashes($gc_num)."' and g.salon_id in (".implode(',',$salon_ids).") and sa.is_paid=1  and sa.is_voided=0 and (sa.refunded is null or sa.refunded<sa.total) limit 1");
	if($checkcode){
		if($checkcode['card_balance']<=0){
			$result['err'] = 'Gift Card is empty!';
		}else{
			$result['amount']='$'.number_format($checkcode['card_balance'],2);
		}
	}else{
		$result['err'] = 'Gift Card '.$gc_num.' not found!';
	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}
if($action == 'remove_cust_redeem'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Error: Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	$redeemed_details = array();
	if($appt['redeemed_details']!='') $redeemed_details = json_decode($appt['redeemed_details'],true);

	if(isset($_GET['label']) && isset($redeemed_details[$_GET['label']])) unset($redeemed_details[$_GET['label']]);

	$redeemed_amount_total = 0;
	foreach($redeemed_details as $v) if($v['amount']>0) $redeemed_amount_total+=$v['amount'];
	if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)) DB::update('salon_appointments',array('redeemed_details'=>json_encode($redeemed_details),'redeemed_amount_total'=>$redeemed_amount_total),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");

	$contents_only=1;
	include('includes/checkout_modal.php');
	exit;
}
if($action == 'appt_redeem_giftcard'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Error: Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	$redeemed_details = array();
	if($appt['redeemed_details']!='') $redeemed_details = json_decode($appt['redeemed_details'],true);

	$gc_num = strtoupper(trim($_GET['gc_num']));
	$amount = (float) $_GET['amount'];
	$salon_ids = array(SALON_ID);
	if(defined('ALL_SALON_IDS') && is_array(ALL_SALON_IDS) && in_array(SALON_ID,ALL_SALON_IDS)) $salon_ids =ALL_SALON_IDS;

	$checkcode = DB::get_row("select g.* from gift_cards g inner join salon_appointments sa on sa.id=g.appt_id where card_no='".addslashes($gc_num)."' and g.salon_id in (".implode(',',$salon_ids).") and g.card_balance>0 and sa.is_paid=1  and sa.is_voided=0 and (sa.refunded is null or sa.refunded<sa.total) limit 1");

	if($checkcode && !$appt['is_paid']){
		$card_amt_left = $checkcode['card_balance'];
		if($card_amt_left<$amount) $amount=$card_amt_left;

		$appt_avamt = $appt['total'];//-$appt['tip'];
		if($appt_avamt<$amount) $amount=$appt_avamt;
		if($amount>0){
			$redeemed_details['giftcard_'.$checkcode['id']]=array(
				'amount' => $amount,
				'gc_id' => $checkcode['id'],
				'gc_num' => $gc_num
			);

			$redeemed_amount_total = 0;
			foreach($redeemed_details as $v) if($v['amount']>0) $redeemed_amount_total+=$v['amount'];

			DB::update('salon_appointments',array('redeemed_details'=>json_encode($redeemed_details),'redeemed_amount_total'=>$redeemed_amount_total),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
	}

	$contents_only=1;
	include('includes/checkout_modal.php');
	exit;
}
if($action == 'appt_add_giftcard'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Error: Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	$contents_only=1;

	$gc_num = strtoupper(trim($_GET['gc_num']));
	$amount = (float)$_GET['amount'];

	$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");


	$all_salons= array(SALON_ID);
	if(defined('ALL_SALON_IDS')) $all_salons=ALL_SALON_IDS;


	$giftcard_settings = array();
	if($salon_info['giftcard_settings']!='') $giftcard_settings = json_decode($salon_info['giftcard_settings'],true);
	if($giftcard_settings['min']<0) $giftcard_settings['min']=0;
	if(empty($giftcard_settings)) exit('Error: Gift card settings not set!');
	if(strlen($gc_num)<5)  exit('Error: Invalid card number!');
	if($amount<0 || $amount<$giftcard_settings['min'])  exit('Error: Minimum amount $'.number_format($giftcard_settings['min'],2).'!');
	if($giftcard_settings['max']>0 && $amount>$giftcard_settings['max'])  exit('Error: Maximum amount $'.number_format($giftcard_settings['max'],2).'!');

	$checknum = DB::get_row("select * from gift_cards where card_no='".addslashes($gc_num)."' and salon_id in (".implode(',',$all_salons).") limit 1");
	if($checknum) exit('Error: card '.$gc_num.' is already registered!');

	$card_type = 1;
	$is_paid_card = 1;
	if(isset($_GET['card_type']) && $_GET['card_type']>1 && $_GET['card_type']<=3) $card_type = (int)$_GET['card_type'];
	if($card_type==2 || $card_type==3) $is_paid_card = 0;
	if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)){
		//include('includes/checkout_modal.php');
		$gcdata = array(
			'card_no' => $gc_num,
			'card_status' => 'INACTIVE',
			'card_activated' => date('Y-m-d H:i:s'),
			'salon_id' => SALON_ID,
			'card_location' => $giftcard_settings['location'],
			'card_amount' => $amount,
			'card_balance' => $amount,
			'appt_id' => $appt_id,
			'card_type' => $card_type,
			'is_paid_card' => $is_paid_card
		);
		if($giftcard_settings['term']>0) $gcdata['date_expire'] = date('Y-m-d', time() + $giftcard_settings['term']*24*3600 );

		DB::insert('gift_cards',$gcdata);
	}
	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_tip'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	if($_GET['stylist_id']>0 && $_GET['tip']>0 && (!$appt['is_paid'] || ( $is_manager || $is_owner  || ($frontdesk && $salon_info['fdesk_edit_tipproc']) ) /* || $role==1 */)){
		if($_GET['tip']<0) $_GET['tip']=0;
		if($appt['is_paid'] && $appt['tip']>0){
			$othertip = DB::get_row("select sum(amount) as tipamt from salon_appointments_tips where  appt_id='".(int)$appt_id."' and stylist_id!='".(int)$_GET['stylist_id']."' ");
			$totaltips = $othertip['tipamt']+(float)$_GET['tip'];
			if($totaltips>$appt['tip']) $_GET['tip'] =$appt['tip']- $othertip['tipamt'];
		}
		$check = DB::get_row("select * from salon_appointments_tips where appt_id='".(int)$appt_id."' and stylist_id='".(int)$_GET['stylist_id']."' limit 1");
		if($check){
			DB::update('salon_appointments_tips',array('amount'=>$check['amount']+(float)$_GET['tip']),"  appt_id='".(int)$appt_id."' and stylist_id='".(int)$_GET['stylist_id']."' limit 1");
		}else{
			DB::insert('salon_appointments_tips',array(
				'appt_id'=>(int)$appt_id,
				'stylist_id' => (int)$_GET['stylist_id'],
				'amount'=>(float)$_GET['tip']
			));
		}
	}
	$contents_only=1;
	include('includes/checkout_modal.php');
}
if($action == 'appt_get_refundable_list'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	
	$lim1Y = time()-365*24*3600;
	
	$mobile = isset($_GET['mobile'])&&$_GET['mobile']?1:0;
	
	$retail_itms = DB::query("SELECT sa.appt_date, si.description, u.first_name, u.last_name, sar.*, sar.discount as item_discount, sar.tax as item_tax FROM `salon_appointments` sa inner join salon_appointments_retail sar on sar.appt_id=sa.id left join salon_inventory si on si.id=sar.inv_id left join users u on u.id=sar.stylist_id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".$appt['customer_id']."' and sa.is_paid=1 and (sa.total>sa.`refunded` or sa.`refunded` is null) and sar.is_refunded=0 and date_paid>='".date('Y-m-d H:i:s',$lim1Y)."' order by date_paid desc");
	$service_itms = DB::query("SELECT sa.appt_date, ss.service_name, u.first_name, u.last_name, sas.*, sas.discount as item_discount, sas.tax as item_tax  FROM `salon_appointments` sa inner join salon_appointments_services sas on sas.appt_id=sa.id left join salon_services ss on ss.id=sas.service_id left join users u on u.id=sas.provider_id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".$appt['customer_id']."' and sa.is_paid=1 and (sa.total>sa.`refunded` or sa.`refunded` is null) and sas.is_refunded=0 and date_paid>='".date('Y-m-d H:i:s',$lim1Y)."' order by date_paid desc");
	
	if(!$mobile){
	?>
	<div id="ref_retail">
		<div class="table-responsive">
		<table class="table table-lg">
		<thead>
		<tr>
			<th>Ticket</th>
			<th>Product</th>
			<th>Provider</th>
			<th>Cost</th>
			<th>Qty</th>
			<th>RC</th>
			<th>RS</th>
			<th>&nbsp;</th> 
		</tr>
		</thead>
		<tbody>
		<?php 
	}else{
		echo '<div class="chref_retail">';
	}
		if(DB::num_rows($retail_itms)){
			while($r=DB::fetch_assoc($retail_itms)){ 
				$item_price = $r['price'];
				if($r['item_discount']>0){ 
					$disc = $r['item_discount'];
					$item_price = $item_price-$disc;
				}

				$price_qty = $item_price*$r['qty'];

				if($r['promo_amnt']>0 ){
					$price_qty -= $r['promo_amnt'];
				}

				$ref_comms_price = $price_qty;

				if($r['item_tax']) $price_qty+=$r['item_tax'];
			if(!$mobile){
			?>
		<tr>
			<td><a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" target="_blank"><?php echo date('m/d/y',strtotime($r['appt_date'])); ?></a></td>
			<td><?php echo $r['description']; ?></td>
			<td><?php echo $r['first_name'].' '.$r['last_name']; ?></td>
			<td>$<?php echo number_format($price_qty,2,'.',','); ?></td>
			<td><?php echo $r['qty']; ?></td>
			<td><input type="checkbox" class="check_rc" <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked '; 
				if(isStylist() && !$salon_info['prov_chg_comm_ref']) echo 'disabled'; 
				if($frontdesk && !$salon_info['fdesk_chg_comm_ref']) echo 'disabled'; 
			?> /></td> 
			<td><input type="checkbox" class="check_rs" /></td>
			<td><a href="javascript:;" onclick="addRefundItem('retail','<?php echo $r['appt_retail_id']; ?>',this);" class="addserv"><i class="las la-plus-circle"></i></a></td>
		</tr>
			<?php 
			}else{
				?>
				<div class="ref_item_mob" data-type="retail" data-itemid="<?php echo $r['appt_retail_id']; ?>">
					<div class="rim_head"><span class="tr_s">$<?php echo number_format($price_qty,2,'.',','); ?> <a href="javascript:;" onclick="markrefund_togglecheckbox(this);" class="addserv"><i class="las la-plus-circle"></i></a><span style="display:none;"><input type="checkbox" class="check_ref_itm"   /></span></span>
					<a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" target="_blank"><?php echo date('m/d/y',strtotime($r['appt_date'])); ?></a>
					</div>
					<div class="rim_prod">
					<span class="tr_s">
						<span class="qtybx"><?php echo $r['qty']; ?></span>
						<span style="display:none;"><input type="checkbox" class="check_rs"   /></span>
						<a href="javascript:;" onclick="markrefund_togglecheckbox(this);" class="addserv"><i class="las la-undo-alt"></i></a>
					</span>
					<?php echo $r['description']; ?>
					</div>
					<div class="rim_stl">
					<span class="tr_s <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked '; ?>">
						<span style="display:none;"><input type="checkbox" class="check_rc" <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked '; ?> /></span>
						<span class="show_checked" onclick="markrefundchecked(this,0)">Commission Reversed <i class="las la-check-circle"></i></span>
						<span class="show_unchecked" onclick="markrefundchecked(this,1)">Commission Not Reversed <i class="las la-times-circle"></i></span>
					</span>
					<?php echo $r['first_name'].' '.$r['last_name']; ?>&nbsp;
					</div>
				</div>
				<?php 
			}
				
			}
		}else{
			?>
		<tr>
			<td colspan="8">No products to refund.</td>
		</tr>
			<?php 
		}
		if(!$mobile){
		?>
		</tbody>
		</table>
		</div>
	</div>
	<div id="ref_service" style="display:none;">
		<div class="table-responsive">
		<table class="table table-lg">
		<thead>
		<tr>
			<th>Ticket</th>
			<th>Service</th>
			<th>Provider</th>
			<th>Cost</th>
			<th>Qty</th>
			<th>RC</th>
			<th>&nbsp;</th> 
		</tr>
		</thead>
		<tbody>
		<?php 
		}else{
			echo '</div><div class="chref_service" style="display:none;">';
		}
		if(DB::num_rows($service_itms)){
			while($r=DB::fetch_assoc($service_itms)){ 
				$price = $r['book_price'];
				
				$ref_price = $price; 


				if($r['used_series_id']>0){
					$ser_inf = DB::get_row("select * from salon_appointments_series where appt_series_id='".(int)$r['used_series_id']."' limit 1");
					if($ser_inf  ) $ref_price = $ser_inf['discount_price'];
				}



				if($r['item_discount']>0){ 
					$s_disc = $r['item_discount'];
					$ref_price-=$s_disc;
				}
				if($r['promo_amnt']>0 ){
					$ref_price -= $r['promo_amnt'];
				}

				if($r['item_tax']){ 
					$ref_price +=$r['item_tax'];
				}

			if(!$mobile){
			?>
		<tr>
			<td><a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" target="_blank"><?php echo date('m/d/y',strtotime($r['appt_date'])); ?></a></td>
			<td><?php echo $r['service_name']; ?></td>
			<td><?php echo $r['first_name'].' '.$r['last_name']; ?></td>
			<td>$<?php echo number_format($ref_price,2,'.',','); ?></td>
			<td><?php echo $r['service_qty']; ?></td>
			<td><input type="checkbox" class="check_rc" <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked ';
				if(isStylist() && !$salon_info['prov_chg_comm_ref']) echo 'disabled'; 
				if($frontdesk && !$salon_info['fdesk_chg_comm_ref']) echo 'disabled'; 
			?> /></td>
			<td><a href="javascript:;" onclick="addRefundItem('service','<?php echo $r['appt_serv_id']; ?>',this);" class="addserv"><i class="las la-plus-circle"></i></a></td>
		</tr>
			<?php 
			}else{
				?>
				<div class="ref_item_mob" data-type="service" data-itemid="<?php echo $r['appt_serv_id']; ?>">
					<div class="rim_head"><span class="tr_s">$<?php echo number_format($ref_price,2,'.',','); ?> <a href="javascript:;" onclick="markrefund_togglecheckbox(this);" class="addserv"><i class="las la-plus-circle"></i></a><span style="display:none;"><input type="checkbox" class="check_ref_itm"   /></span></span>
					<a href="transactions.php?appt_id=<?php echo $r['appt_id']; ?>" target="_blank"><?php echo date('m/d/y',strtotime($r['appt_date'])); ?></a>
					</div>
					<div class="rim_prod">
					<span class="tr_s"><span class="qtybx"><?php echo $r['service_qty']; ?></span></span>
					<?php echo $r['service_name']; ?>
					</div>
					<div class="rim_stl">
					<span class="tr_s <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked '; ?>">
						<span style="display:none;"><input type="checkbox" class="check_rc" <?php if(!$salon_info['no_rev_comm_ref']) echo ' checked '; ?> /></span>
						<span class="show_checked" onclick="markrefundchecked(this,0)">Commission Reversed <i class="las la-check-circle"></i></span>
						<span class="show_unchecked" onclick="markrefundchecked(this,1)">Commission Not Reversed <i class="las la-times-circle"></i></span>
					</span>
					<?php echo $r['first_name'].' '.$r['last_name']; ?>
					</div>
				</div>
				<?php 
			}
				
			}
		}else{
			if(!$mobile){
			?>
		<tr>
			<td colspan="7">No services to refund.</td>
		</tr>
			<?php 
			}else{
				?>
				<div class="ref_item_mob">
				No services to refund.
				</div>
				<?php 
			}
		}
		if(!$mobile){
		?>
		</tbody>
		</table>
		</div>
	</div>
	<?php  
		}else{
			echo '</div>';
		}
	exit; 
}

if($action == 'appt_remove_refund_itm'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	
	
	$item_id = (int) $_GET['item_id'];
	
	if($item_id){
		DB::delete('salon_appointments_linerefund'," apt_ref_id='".(int)$item_id."' and appt_id='".(int)$appt_id."' limit 1");
	}
	
	
	$contents_only=1;
	include('includes/checkout_modal.php');
	
	exit;
}
if($action == 'appt_add_refund_itm_multi'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	
	$data = $_POST['data'];
	if($data!=''){
		$data = json_decode($data,1);
		if(is_array($data)) foreach($data as $refitm){
			
			$type = $refitm['type'];
			$item_id = (int) $refitm['item_id'];
			$rc = (int) $refitm['rc'];
			$rs = (int) $refitm['rs'];
			
			if($item_id && ($type=='service' || $type=='retail')){
				$check = DB::get_row("select * from salon_appointments_linerefund where appt_id='".(int)$appt_id."' and ref_item_id='".(int)$item_id."' limit 1");
				
				if(!$check){
					$cost_total = 0;
					$cost_item = 0;
					$cost_tax = 0;
					$comm_amt = 0;
					$qty = 1;
					$skip = false;
					
					if($type=='service'){
						$itm_info = DB::get_row("SELECT sa.appt_date, sas.*, sas.discount as item_discount, sas.tax as item_tax  FROM `salon_appointments` sa inner join salon_appointments_services sas on sas.appt_id=sa.id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".(int)$appt['customer_id']."' and sa.is_paid=1 and sas.appt_serv_id='".(int)$item_id."' and sas.is_refunded=0 limit 1");
						if($itm_info){ 
							$price = $itm_info['book_price'];
							
							$cost_total = $price; 


							if($itm_info['used_series_id']>0){
								$ser_inf = DB::get_row("select * from salon_appointments_series where appt_series_id='".(int)$itm_info['used_series_id']."' limit 1");
								if($ser_inf  ) $cost_total = $ser_inf['discount_price'];
							}



							if($itm_info['item_discount']>0){ 
								$s_disc = $itm_info['item_discount'];
								$cost_total-=$s_disc;
							}
							if($itm_info['promo_amnt']>0 ){
								$cost_total -= $itm_info['promo_amnt'];
							}

							$cost_item = $cost_total;
							$comm_amt = $cost_total;
							$qty =  $itm_info['service_qty'];
							
							if($itm_info['item_tax']){ 
								$cost_total +=$itm_info['item_tax'];
								$cost_tax =$itm_info['item_tax'];
							}
						
						
						}else $skip = true;
					}
					if($type=='retail'){
						$itm_info = DB::get_row("SELECT sa.appt_date,  sar.*, sar.discount as item_discount, sar.tax as item_tax FROM `salon_appointments` sa inner join salon_appointments_retail sar on sar.appt_id=sa.id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".(int)$appt['customer_id']."' and sa.is_paid=1 and sar.appt_retail_id='".(int)$item_id."' and sar.is_refunded=0 limit 1");
						
						if($itm_info){ 
							$item_price = $itm_info['price'];
							if($itm_info['item_discount']>0){ 
								$disc = $itm_info['item_discount'];
								$item_price = $item_price-$disc;
							}

							$cost_total = $item_price*$itm_info['qty'];

							if($itm_info['promo_amnt']>0 ){
								$cost_total -= $itm_info['promo_amnt'];
							}

							$comm_amt = $cost_total;
							$cost_item = $cost_total;
							$qty =  $itm_info['qty'];

							if($itm_info['item_tax']){
								$cost_total+=$itm_info['item_tax'];
								$cost_tax = $itm_info['item_tax'];
							}
							
						}else $skip = true;
					}
					
					if(!$skip){
						$lineref = array(
							'appt_id' => (int)$appt_id,
							'refund_type' => $type,
							'ref_item_id' => (int)$item_id,
							'cost_total' => $cost_total, 
							'cost_item' =>  $cost_item,
							'cost_tax' => $cost_tax,
							'comm_amt' => $comm_amt,
							'qty' => $qty,
							'ref_comm' => $rc,
							'restock' => $rs
						);
						
						DB::insert('salon_appointments_linerefund',$lineref);
					}
					
				}
				
			}
		}
	}
	
	$contents_only=1;
	include('includes/checkout_modal.php');
	exit;
	
}
if($action == 'appt_add_refund_itm'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	
	$type = $_GET['type'];
	$item_id = (int) $_GET['item_id'];
	$rc = (int) $_GET['rc'];
	$rs = (int) $_GET['rs'];
	
	if($item_id && ($type=='service' || $type=='retail')){
		$check = DB::get_row("select * from salon_appointments_linerefund where appt_id='".(int)$appt_id."' and ref_item_id='".(int)$item_id."' limit 1");
		
		if(!$check){
			$cost_total = 0;
			$cost_item = 0;
			$cost_tax = 0;
			$comm_amt = 0;
			$qty = 1;
			$skip = false;
			
			if($type=='service'){
				$itm_info = DB::get_row("SELECT sa.appt_date, sas.*, sas.discount as item_discount, sas.tax as item_tax  FROM `salon_appointments` sa inner join salon_appointments_services sas on sas.appt_id=sa.id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".(int)$appt['customer_id']."' and sa.is_paid=1 and sas.appt_serv_id='".(int)$item_id."' and sas.is_refunded=0 limit 1");
				if($itm_info){ 
					$price = $itm_info['book_price'];
					
					$cost_total = $price; 


					if($itm_info['used_series_id']>0){
						$ser_inf = DB::get_row("select * from salon_appointments_series where appt_series_id='".(int)$itm_info['used_series_id']."' limit 1");
						if($ser_inf  ) $cost_total = $ser_inf['discount_price'];
					}



					if($itm_info['item_discount']>0){ 
						$s_disc = $itm_info['item_discount'];
						$cost_total-=$s_disc;
					}
					if($itm_info['promo_amnt']>0 ){
						$cost_total -= $itm_info['promo_amnt'];
					}

					$cost_item = $cost_total;
					$comm_amt = $cost_total;
					$qty =  $itm_info['service_qty'];
					
					if($itm_info['item_tax']){ 
						$cost_total +=$itm_info['item_tax'];
						$cost_tax =$itm_info['item_tax'];
					}
				
				
				}else $skip = true;
			}
			if($type=='retail'){
				$itm_info = DB::get_row("SELECT sa.appt_date,  sar.*, sar.discount as item_discount, sar.tax as item_tax FROM `salon_appointments` sa inner join salon_appointments_retail sar on sar.appt_id=sa.id WHERE sa.`salon_id`='".SALON_ID."' and sa.`customer_id`='".(int)$appt['customer_id']."' and sa.is_paid=1 and sar.appt_retail_id='".(int)$item_id."' and sar.is_refunded=0 limit 1");
				
				if($itm_info){ 
					$item_price = $itm_info['price'];
					if($itm_info['item_discount']>0){ 
						$disc = $itm_info['item_discount'];
						$item_price = $item_price-$disc;
					}

					$cost_total = $item_price*$itm_info['qty'];

					if($itm_info['promo_amnt']>0 ){
						$cost_total -= $itm_info['promo_amnt'];
					}

					$comm_amt = $cost_total;
					$cost_item = $cost_total;
					$qty =  $itm_info['qty'];

					if($itm_info['item_tax']){
						$cost_total+=$itm_info['item_tax'];
						$cost_tax = $itm_info['item_tax'];
					}
					
				}else $skip = true;
			}
			
			if(!$skip){
				$lineref = array(
					'appt_id' => (int)$appt_id,
					'refund_type' => $type,
					'ref_item_id' => (int)$item_id,
					'cost_total' => $cost_total, 
					'cost_item' =>  $cost_item,
					'cost_tax' => $cost_tax,
					'comm_amt' => $comm_amt,
					'qty' => $qty,
					'ref_comm' => $rc,
					'restock' => $rs
				);
				
				DB::insert('salon_appointments_linerefund',$lineref);
			}
			
		}
		
	}
	
	
	$contents_only=1;
	include('includes/checkout_modal.php');
}

if($action == 'appt_update_tip'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }
	if($_GET['tip']<0) $_GET['tip']=0;
	if($_GET['stylist_id']>0 && ((!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)) || ( $is_manager || $is_owner || ($frontdesk && $salon_info['fdesk_edit_tipproc']) ) /* || $role==1 */)){
		if($appt['is_paid'] && $appt['tip']>0){
			$othertip = DB::get_row("select sum(amount) as tipamt from salon_appointments_tips where  appt_id='".(int)$appt_id."' and stylist_id!='".(int)$_GET['stylist_id']."' ");
			$totaltips = $othertip['tipamt']+(float)$_GET['tip'];
			if($totaltips>$appt['tip']) $_GET['tip'] =$appt['tip']- $othertip['tipamt'];
			log_event("proct_tip", 0, 0, 0, '', '', '', $user_id, json_encode( array( 'appt_id' => (int)$appt_id , 'new_val' => number_format((float)$_GET['tip'],2), 'old_val' => number_format($appt['tip'],2) ) ) );
		}
		DB::update('salon_appointments_tips',array('amount'=>(float)$_GET['tip']),"  appt_id='".(int)$appt_id."' and stylist_id='".(int)$_GET['stylist_id']."' limit 1");
		if($_GET['tip']==0){
			$check = DB::get_row("select * from salon_appointments_services where provider_id='".(int)$_GET['stylist_id']."' and  appt_id='".(int)$appt_id."' limit 1");
			if(!$check) DB::delete('salon_appointments_tips', "  appt_id='".(int)$appt_id."' and stylist_id='".(int)$_GET['stylist_id']."' limit 1");
		}
	}
	$contents_only=1;
	include('includes/checkout_modal.php');
}
if($action == 'appt_edit_service'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	$appt_service = isset($_GET['appt_service'])?$_GET['appt_service']:0;
	if(!$appt_id || !$appt_service){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$serv_id = isset($_GET['serv_id'])?(int)$_GET['serv_id']:0;
	$stylist = isset($_GET['stylist'])?(int)$_GET['stylist']:0;

	if($serv_id && $stylist){
		$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$stylist."'  left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$serv_id."' limit 1");
		$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
		$bef_empl = $price;
		$is_empl = 0;
		if( DB::fetch_assoc(DB::query("select u.* from users u inner join salon_customers sc on sc.customer_id=u.id   where u.id='".$appt['customer_id']."' and  sc.salon_id='".SALON_ID."'  and u.status=1 "))['is_fnf'] && $serv_q['employee_price'] > 0 ){
			$price = $serv_q['employee_price'];
			$is_empl = 1;
		}
		$bookedserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$appt_service."' and appt_id='".$appt_id."'  limit 1");
		//if(!getTimeSec($_GET['time'])) exit('An error occurred!');
		$servdata =  array(
			//'appt_id' => $appt_id,
			'provider_id' => (int)$stylist,
			//'service_id'=> (int)$serv_id,
			//'service_time' => $_GET['time'],
			//'book_price' => $price,
			//'service_time_sec' => getTimeSec($_GET['time']),
			'is_hidden' =>0
		);
		if(!$appt['is_paid'] && ($appt['pay_lock']==0 || $appt['pay_lock']==$user_id)){
			log_event('edit_service', $appt_id, $appt['customer_id'], $serv_id, '');
			if($serv_id!=$bookedserv['service_id']){
				$servdata['service_id']=$serv_id;
				$servdata['book_price']=(float)$price;
				$servdata['is_employee']=$is_empl;
				$servdata['bef_empl_pr']=$bef_empl;

				DB::query("update salon_appointments_services set duration_changed=null, dur_appl_chng=null, dur_procs_chng=null, dur_finsh_chng=null,used_series_id=0 where appt_serv_id='".(int)$appt_service."' and appt_id='".$appt_id."'  limit 1");
				DB::insert('salon_appointments_log',array(
					'appt_id' => $appt_id,
					'user_id' => $user_id,
					'dtime' => time(),
					'event_type' => 'service_change',
					'details' => 'Service changed to '.$serv_q['service_name']

				));
			}
			if(getTimeSec($_GET['time'])){
				$servdata['service_time']=$_GET['time'];
				$servdata['service_time_sec']=getTimeSec($_GET['time']);
				if($servdata['service_time_sec']!=$bookedserv['service_time_sec']){

					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'time_change',
						'details' => 'Service '.$serv_q['service_name'].' time changed from '.$bookedserv['service_time'].' to '.$_GET['time']

					));
				}
				if($servdata['provider_id']!=$bookedserv['provider_id']){

					$oldstylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$bookedserv['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
					if($oldstylist['code_name']!='') $oldstylist['first_name']=$oldstylist['code_name'];
					$stylist = DB::get_row("select u.*,pi.code_name,pi.althern_sched, pi.fut_enable_sched,pi.fut_althern_sched,pi.fut_date_sched,pi.double_book from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$servdata['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
					if($stylist['code_name']!='') $stylist['first_name']=$stylist['code_name'];
					DB::insert('salon_appointments_log',array(
						'appt_id' => $appt_id,
						'user_id' => $user_id,
						'dtime' => time(),
						'event_type' => 'stylist_change',
						'details' => 'Service '.$serv_q['service_name'].' provider changed from '.$oldstylist['first_name'].' to '.$stylist['first_name']

					));
				}
			}


			DB::update('salon_appointments_services',$servdata," appt_id='".(int)$appt_id."' and appt_serv_id='".(int)$appt_service."' limit 1");
			if(!$appt['is_paid']) {
				DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
				if(isset($_GET['old_stylist_id']) && $_GET['old_stylist_id']!=$stylist) DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_id."' and stylist_id='".(int)$_GET['old_stylist_id']."' ");
			}
			$reset_tips=1;
		}else if($role==1 && $appt['is_paid']){
			log_event('edit_service', $appt_id, $appt['customer_id'], $serv_id, '');
			$old_serv = DB::get_row("select * from salon_appointments_services where appt_id='".(int)$appt_id."' and appt_serv_id='".(int)$appt_service."' limit 1");
			$servdata =  array(
				'provider_id' => (int)$stylist
			);
			if(getTimeSec($_GET['time'])){
				$servdata['service_time']=$_GET['time'];
				$servdata['service_time_sec']=getTimeSec($_GET['time']);
				
				if($servdata['service_time_sec']!=$old_serv['service_time_sec']){
					
					$details= array(
						'old_time'=>$old_serv['service_time'],
						'new_time'=>$servdata['service_time']
					);
					log_event('proc_service_time', $appt_id, $appt['customer_id'], $serv_id, '',$appt['appt_date'],'',(int)$stylist,json_encode($details));
					
					
				}
			}
			DB::update('salon_appointments_services',$servdata," appt_id='".(int)$appt_id."' and appt_serv_id='".(int)$appt_service."' limit 1");
			if(isset($_GET['old_stylist_id']) && $_GET['old_stylist_id']!=$stylist){


				$do_refnd = true;
				if(date('Y-m-d') == date('Y-m-d',strtotime($appt['date_paid']?$appt['date_paid']:$appt['appt_date']))) $do_refnd = false;
				/*else{
					$wstart = $salon_info['week_start'];
					if(!$wstart)$wstart=1;
					$wend = $wstart-1;
					if($wend<1) $wend=7;
					$day_num = date('N');
					$end_date = time();
					if($day_num!=$wend){
						if($day_num>$wend){
							$end_date-=($day_num-$wend)*24*3600;
						}else{
							$end_date-=((7-$wend)+$day_num)*24*3600;
						}
					}
					$start_date = strtotime(date('Y-m-d',$end_date-6*24*3600));
					if(strtotime($appt['date_paid']?$appt['date_paid']:$appt['appt_date'])>$start_date) $do_refnd = false;
				}*/

				if($do_refnd){

					$servdata =  array(
						'is_stl_change' =>  (int)$_GET['old_stylist_id']
					);
					DB::update('salon_appointments_services',$servdata," appt_id='".(int)$appt_id."' and appt_serv_id='".(int)$appt_service."' and (is_stl_change is null or is_stl_change=0) limit 1");

					if($old_serv['used_series_id']>0){
						$ser_inf = DB::get_row("select * from salon_appointments_series where appt_series_id='".(int)$old_serv['used_series_id']."' limit 1");
						if($ser_inf  ) {
							$old_serv['book_price'] = $ser_inf['discount_price'];
						}
					}
					$serv_q = DB::get_row("SELECT sas.*,ss.default_timing_min,ps.timing_min,ps.pricing,ss.default_pricing,ss.default_fee_perc,ss.service_name, ps.fee_perc FROM `salon_appointments_services` sas left join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id=ss.id and ps.provider_id=sas.provider_id WHERE sas.appt_id='".(int)$appt_id."' and sas.appt_serv_id='".(int)$appt_service."' limit 1");

					$fee_amt = 0;
					if($serv_q ){
						if($serv_q['fee_perc']>0) $serv_q['default_fee_perc']=$serv_q['fee_perc'];
						if($serv_q['fee_perc']==-1) $serv_q['default_fee_perc']=0;
						if($serv_q['default_fee_perc']>0 && $serv_q['default_fee_perc']<=100){
							$fee_amt = ($old_serv['book_price']*($serv_q['default_fee_perc']/100));
							if($salon_info['service_fee_type']=='amnt') $fee_amt = $serv_q['default_fee_perc'];
							//echo $salon_info['service_fee_type'].', ';

						}
					}

					$ref_date = date('Y-m-d');
					/*$check_exists = DB::get_row("select * from provider_refunds where salon_id='".SALON_ID."' and provider_id='".(int)$_GET['old_stylist_id']."' and refund_date='".$ref_date."' limit 1 ");*/
					if(0 /*$check_exists*/ ){
						$upd_data = array(
							'refunded_serv_com' => ($check_exists['refunded_serv_com']+$old_serv['book_price']),
							'refunded_fees' => ($check_exists['refunded_fees']+$fee_amt )
						);
						DB::update('provider_refunds',$upd_data," salon_id='".SALON_ID."' and provider_id='".(int)$_GET['old_stylist_id']."' and refund_date='".$ref_date."' limit 1  ");
					}else{

						$ins_data = array(
							'salon_id' => SALON_ID,
							'provider_id' => (int)$_GET['old_stylist_id'],
							'appt_id' => $appt_id,
							'refund_date' => $ref_date ,
							'refunded_tips' =>  0,
							'refunded_serv_com' =>  $old_serv['book_price'],
							'refunded_ret_com' =>  0,
							'refunded_fees' =>  $fee_amt
						);

						DB::insert('provider_refunds',$ins_data);
					}
					/*$check_exists = DB::get_row("select * from provider_addedsales where salon_id='".SALON_ID."' and provider_id='".(int)$stylist."' and add_date='".$ref_date."' limit 1 ");*/
					if(0 /*$check_exists*/ ){
						$upd_data = array(
							'added_serv_com' => ($check_exists['added_serv_com']+$old_serv['book_price']) ,
							'added_fees' => ($check_exists['added_fees']+$fee_amt )
						);
						DB::update('provider_addedsales',$upd_data," salon_id='".SALON_ID."' and provider_id='".(int)$stylist."' and add_date='".$ref_date."' limit 1  ");
					}else{

						$ins_data = array(
							'salon_id' => SALON_ID,
							'provider_id' => (int)$stylist,
							'appt_id' => $appt_id,
							'add_date' => $ref_date ,
							'added_tips' =>  0,
							'added_serv_com' =>  $old_serv['book_price'],
							'added_ret_com' =>  0,
							'added_fees' => $fee_amt
						);

						DB::insert('provider_addedsales',$ins_data);
					}

				}
				log_event("proct_provider", 0, 0, 0, '', '', '', $user_id, json_encode( array( 'appt_id' => (int)$appt_id , 'new_val' => $stylist, 'old_val' => $_GET['old_stylist_id'] ) ) );

			}
			$reset_tips=1;
		}
	}



	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_remove_promoitem'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$promo_id = isset($_GET['promo_id'])?(int)$_GET['promo_id']:0;
	$obj_id = isset($_GET['obj_id'])?(int)$_GET['obj_id']:0;
	$promo_type = isset($_GET['type'])? $_GET['type']:'';

	if($promo_id && !$appt['is_paid']){
		$toremove = true;
		if($promo_type=='service'){
			DB::update('salon_appointments_services',array('promo_id'=>null,'promo_amnt'=>null)," appt_serv_id='".(int)$obj_id."' limit 1 ");
			$check = DB::get_row("select count(*) as c from salon_appointments_services where appt_id='".(int)$appt_id."' and promo_id='".(int)$promo_id."' " );
			if($check['c']) $toremove = false;

		}else{
			DB::update('salon_appointments_retail',array('promo_id'=>null,'promo_amnt'=>null)," appt_retail_id='".(int)$obj_id."' limit 1 ");
			$check = DB::get_row("select count(*) as c from salon_appointments_retail where appt_id='".(int)$appt_id."' and promo_id='".(int)$promo_id."'   " );
			if($check['c']) $toremove = false;
		}
		if($toremove) DB::delete('salon_appointments_promos'," promo_id='".(int)$promo_id."' and appt_id='".(int)$appt_id."' ");

	}
	if(!$appt['is_paid']) DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");

	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_promoitem'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$promo_id = isset($_GET['promo_id'])?(int)$_GET['promo_id']:0;
	$obj_id = isset($_GET['obj_id'])?(int)$_GET['obj_id']:0;
	$promo_type = isset($_GET['type'])? $_GET['type']:'';

	if($promo_id && !$appt['is_paid']){
		$promo_info = DB::get_row("select * from  salon_promotions where salon_id='".SALON_ID."' and id='".(int)$promo_id."' and status=1 and (start_date is null or start_date<='".date('Y-m-d')."') and (end_date is null or end_date>='".date('Y-m-d')."') limit 1");
		if($promo_info){

			$obj_data = array();

			if($promo_type=='service'){
				$obj_data = DB::get_row("select distinct sapp.appt_date, sapp.customer_id, sas.*, ss.service_name, ss.service_category_id, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ps.pricing, ss.employee_price, sc.sales_tax, ss.service_category_id from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join service_categories sc on sc.id=ss.service_category_id left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where sas.appt_serv_id='".(int)$obj_id."' limit 1");

			}else{
				$obj_data = DB::get_row("select sar.*, si.code, si.description, si.price as inv_price, si.employee_amt, si.brand,si.category from salon_appointments_retail sar left join salon_inventory si on si.id=sar.inv_id where sar.appt_retail_id='".(int)$obj_id."'");
			}

			if($obj_data && $obj_data['promo_id']!=$promo_id){
				if($obj_data['promo_id']>0){
					$toremove = true;
					if($promo_type=='service'){
						$check = DB::get_row("select count(*) as c from salon_appointments_services where appt_id='".(int)$appt_id."' and promo_id='".(int)$obj_data['promo_id']."' and appt_serv_id!='".(int)$obj_id."' " );
						if($check['c']) $toremove = false;

					}else{
						$check = DB::get_row("select count(*) as c from salon_appointments_retail where appt_id='".(int)$appt_id."' and promo_id='".(int)$obj_data['promo_id']."' and appt_retail_id!='".(int)$obj_id."'  " );
						if($check['c']) $toremove = false;
					}
					if($toremove) DB::delete('salon_appointments_promos'," promo_id='".(int)$obj_data['promo_id']."' and appt_id='".(int)$appt_id."' ");
				}

				$check2 = DB::get_row("select * from  salon_appointments_promos where promo_id='".(int)$promo_id."' and appt_id='".(int)$appt_id."' limit 1");
				if(!$check2){
					DB::insert('salon_appointments_promos',array(
						'appt_id' => (int)$appt_id,
						'promo_id' => (int)$promo_id,
						'promo_code' => $promo_info['promo_code'],
						'promo_type' => $promo_info['promo_type'],
						'discount_perc' => $promo_info['discount_perc'],
						'discount_total' => 0
					));
				}


				if($promo_type=='service'){
					DB::update('salon_appointments_services',array('promo_id'=>$promo_id)," appt_serv_id='".(int)$obj_id."' limit 1 ");

				}else{
					DB::update('salon_appointments_retail',array('promo_id'=>$promo_id)," appt_retail_id='".(int)$obj_id."' limit 1 ");
				}
			}
		}
	}
	if(!$appt['is_paid']) DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");

	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_promo'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$promo_id = isset($_GET['promo_id'])?(int)$_GET['promo_id']:0;

	if($promo_id && !$appt['is_paid']){
		$promo_info = DB::get_row("select * from  salon_promotions where salon_id='".SALON_ID."' and id='".(int)$promo_id."' and status=1 and (start_date is null or start_date<='".date('Y-m-d')."') and (end_date is null or end_date>='".date('Y-m-d')."') limit 1");
		if($promo_info){
			$check2 = DB::get_row("select * from  salon_appointments_promos where promo_id='".(int)$promo_id."' and appt_id='".(int)$appt_id."' limit 1");
			if(!$check2){
				DB::insert('salon_appointments_promos',array(
					'appt_id' => (int)$appt_id,
					'promo_id' => (int)$promo_id,
					'promo_code' => $promo_info['promo_code'],
					'promo_type' => $promo_info['promo_type'],
					'discount_perc' => $promo_info['discount_perc'],
					'discount_total' => 0
				));
			}
		}
	}
	if(!$appt['is_paid']) DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");

	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_series'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$series_id = isset($_GET['series_id'])?(int)$_GET['series_id']:0;


	if($series_id && !$appt['is_paid']){
		$series = DB::get_row("select ss.*, ser.service_name, ser.default_pricing from salon_series ss left join salon_services ser on ser.id=ss.service_id where ss.id='".(int)$series_id."' and ss.salon_id='".SALON_ID."'   and ss.status=1 order by ss.id desc");
		if($series){

			$disc_price_srv = (float)$series['discounted_price'];
			if(!$disc_price_srv && $series['discount']>0) $disc_price_srv = ($series['default_pricing'])*(1-$series['discount']/100);
			else{
				if($series['discount']==0) $series['discount'] = $series['default_pricing']>0?(($series['default_pricing']-$disc_price_srv) / ($series['default_pricing']/100)):0;
			}
			$data = array(
				'appt_id' =>(int)$appt_id,
				'series_id' =>(int)$series_id,
				'service_id' => $series['service_id'],
				'base_price' => $series['default_pricing'],
				'discount_perc' => $series['discount'],
				'discount_price' => $disc_price_srv,
				'qty' => $series['qty'],
				'price_total' => ($disc_price_srv*$series['qty']),
				'qty_used' => 0,
				'expdate'=>date('Y-m-d',time()+365*24*3600).' 23:59:59'
			);
			DB::insert('salon_appointments_series',$data);
		}
	}
	if(!$appt['is_paid']) DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");



	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_service'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$serv_id = isset($_GET['serv_id'])?(int)$_GET['serv_id']:0;
	$stylist = isset($_GET['stylist'])?(int)$_GET['stylist']:0;

	if($serv_id && $stylist && !$appt['is_paid']){
		$serv_q = DB::get_row("select ss.service_name, ifnull(ssp.level_price,ss.default_pricing) as default_pricing, ss.employee_price, ss.ep_price, ps.pricing from salon_services ss left join provider_services ps on ps.service_id= ss.id and ps.provider_id = '".(int)$stylist."' left join provider_info pi on pi.user_id=ps.provider_id and pi.salon_id=ss.salon_id left join salon_services_prices ssp on ssp.service_id=ss.id and ssp.level_id=pi.pricing_level where ss.id='".(int)$serv_id."' limit 1");
		$price = $serv_q['pricing']>0?$serv_q['pricing']:$serv_q['default_pricing'];
		$is_employee = 0;
		$is_ep = 0;
		$bef_empl_pr = 0;

		$uinf = DB::fetch_assoc(DB::query("select u.* from users u left join salon_customers sc on sc.customer_id=u.id   where u.id='".$appt['customer_id']."' and  sc.salon_id='".SALON_ID."'  and u.status=1 "));

		if( $uinf['is_fnf'] && $serv_q['employee_price'] > 0 ){  $bef_empl_pr = $price;$price = $serv_q['employee_price']; $is_employee=1; }
		if( isset($uinf['role_id']) && $uinf['role_id']!=3 && $serv_q['ep_price'] > 0 ){ $price = $serv_q['ep_price']; $is_ep=1; }

		//if(!getTimeSec($_GET['time'])) exit('An error occurred!');
		DB::insert('salon_appointments_services',array(
			'appt_id' => $appt_id,
			'provider_id' => (int)$stylist,
			'service_id'=> (int)$serv_id,
			'service_time' => $_GET['time'],
			'book_price' => $price,
			'service_time_sec' => getTimeSec($_GET['time']),
			'is_hidden' =>0,
			'is_employee' =>$is_employee,
			'is_ep' =>$is_ep,
			'bef_empl_pr' =>$bef_empl_pr,
			'is_upsell'=>1
		));
		log_event('add_service', $appt_id, $appt['customer_id'], $serv_id, '');
		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_id."' and stylist_id='".(int)$stylist."' ");
		$reset_tips=1;
	}



	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'appt_add_product'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){ exit('Bad request!'); }
	$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	if(!$appt){ exit('Bad request!'); }

	$prod_id = isset($_GET['prod_id'])?(int)$_GET['prod_id']:0;

	$pinfo = DB::get_row("select * from salon_inventory where id='".(int)$prod_id."' and salon_id='".SALON_ID."' limit 1");
	if($pinfo && !$appt['is_paid']){
		$check = DB::get_row("select * from salon_appointments_retail where appt_id='".(int)$appt_id."' and  inv_id='".(int)$prod_id."'  limit 1");
		if($check){
			DB::update('salon_appointments_retail',array('qty'=>$check['qty']+1)," appt_retail_id='".$check['appt_retail_id']."' limit 1");
		}else{
			$retinfo =array(
				'appt_id' => $appt_id,
				'inv_id' => $prod_id,
				'qty'=>1,
				'price' => $pinfo['price']
			);
			$stylistinf = DB::get_row("SELECT `provider_id` FROM `salon_appointments_services` WHERE `appt_id`='".(int)$appt_id."' and `provider_id`>0 order by `appt_serv_id` limit 1");
			if($stylistinf) $retinfo['stylist_id']=$stylistinf['provider_id'];
			//$retinfo['stylist_id']=0;
			DB::insert('salon_appointments_retail',$retinfo);
		}

		DB::update('salon_appointments',array('tax'=>-1,'tip'=>-1)," id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	}



	$contents_only=1;
	include('includes/checkout_modal.php');

	exit;
}

if($action == 'search_inventory'){
	if(isset($_GET['kw']) && strlen(trim($_GET['kw']))>1){
		$kws = explode(' ',$_GET['kw']);
		$conds = array();
		foreach($kws as $kw){
			$kw=addslashes(trim($kw));
			if($kw!=''){
				$conds[]="(`code` like '%".$kw."%' or sku like '%".$kw."%'  or altern_codes like '%".$kw."%' or altern_skus like '%".$kw."%'   or `description` like '%".$kw."%' or `category` like '%".$kw."%' or `brand` like '%".$kw."%')";
			}
		}
		$inv = DB::query("select * from salon_inventory where salon_id='".SALON_ID."' ".(!empty($conds)?" and ".implode(' and ',$conds):"")." and active=1 and (`type`='retail' or `type`='' or `sell_staff`='1') order by `code` limit 20");
		if(DB::num_rows($inv)){
			?>
			<div class="table-responsive">
				<table class="table table-lg">
					<tbody class="follow">
						<?php while($r=DB::fetch_assoc($inv)){ ?>
						<tr class="modlrw">
							<td><?php if( strtolower(htmlspecialchars($r['type'])) == 'retail' || strtolower(htmlspecialchars($r['type'])) == '' ) { ?><span class="cust_pill_ret">R</span><?php }else{ ?><span class="cust_pill_pro">P</span><?php } ?></td>
							<td class="desk_only" <?php if($r['on_hand']<=0) echo ' style="color:red;" '; ?>><?php echo $r['code']; ?></td>
							<td <?php if($r['on_hand']<=0) echo ' style="color:red;" '; ?>><?php echo $r['description']; ?></td>
							<td style="text-align:right; <?php if($r['on_hand']<=0) echo 'color:red;'; ?>">$<?php echo $r['price']; ?></td>
							<td style="text-align:center;"><a href="javascript:;" onclick="<?php if($r['sell_staff']=='1'){ ?>if(confirm('You are adding a product that is designated as professional use only. Would you like to proceed?')){ add_to_appointment(<?php echo $r['id']; ?>) }<?php }else{ ?>add_to_appointment(<?php echo $r['id']; ?>)<?php } ?>" class="addserv"><i class="las la-plus-circle"></i></a></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php
		}
	}
	exit;
}
if($action == 'walkin_set_cust'){
	$appt_id = isset($_GET['appt_id'])?(int)$_GET['appt_id']:0;
	$all_salons= array(SALON_ID);
	if(defined('ALL_SALON_IDS')) $all_salons=ALL_SALON_IDS;
	if(!$appt_id){ exit('Bad request!'); }

	if(isset($_GET['set_customer']) &&$_GET['set_customer']){
		DB::update('salon_appointments',array('customer_id'=>(int)$_GET['set_customer']),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
	}else if(isset($_GET['create_customer']) && $_GET['create_customer']){

		$use_uid = 0;

		if($_POST['email']) {
			$checkuser = DB::get_row("select distinct u.* from users u inner join salon_customers sc on sc.customer_id=u.id and sc.salon_id in (".implode(',',$all_salons).") where email='".addslashes($_POST['email'])."' limit 1");
			if($checkuser) $use_uid=$checkuser['id'];
		}
		if(!empty($_POST['phone'])){
			$checkuser = DB::get_row("select distinct u.* from users u inner join salon_customers sc on sc.customer_id=u.id and sc.salon_id in (".implode(',',$all_salons).") where phone_clean='".addslashes(preg_replace("/[^0-9]/", "", $_POST['phone'] ))."' limit 1");
			if($checkuser)  $use_uid=$checkuser['id'];
		}

		if($use_uid){
			$checkuser = DB::get_row("select * from salon_customers where customer_id='".(int)$use_uid."' and salon_id='".SALON_ID."' limit 1");
			if(!$checkuser) DB::insert('salon_customers',array('customer_id'=>$use_uid,'salon_id'=>SALON_ID));
		}else{
			$upddata = array(
				'first_name' => $_POST['fname'],
				'last_name' => $_POST['lname'],
				'phone' => $_POST['phone'],
				'phone_clean' => preg_replace("/[^0-9]/", "", $_POST['phone'] ),
				'email' => $_POST['email'],
				'role_id' => 3,
			);
			DB::insert('users',$upddata );

			$use_uid = DB::insert_id();
			if($use_uid){
				DB::insert('salon_customers',array('customer_id'=>$use_uid,'salon_id'=>SALON_ID));
			}
		}

		if($use_uid){
			DB::update('salon_appointments',array('customer_id'=>(int)$use_uid),"  id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
		}
	}
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'create_walkin'){
	DB::insert('salon_appointments',array(
		'salon_id'=>SALON_ID,
		'appt_date' =>date('Y-m-d H:i:s',tz_time(time())),
		'is_walk_in'=>1
	));
	$appt_id = DB::insert_id();

	if(!$appt_id){ exit('Bad request!'); }
	log_event('create_walkin', $appt_id);
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'get_checkout'){
	$appt_id = isset($_GET['appt_id'])?$_GET['appt_id']:0;
	if(!$appt_id){
		$appt_serv = DB::get_row("select sas.* from salon_appointments_services sas  where  sas.appt_serv_id='".(int)$_GET['serv_id']."' limit 1");
		if($appt_serv) $appt_id = $appt_serv['appt_id'];
	}
	if(!$appt_id){ exit('Bad request!'); }
	if(isset($_GET['_f']) && $_GET['_f'] == 'cappt') {
		$client_appt = 1;//from client appointment
	}
	include('includes/checkout_modal.php');

	exit;
}
if($action == 'delete_appt'){
	if(isset($_GET['appt_id']) && $_GET['appt_id']){
		$appt_inf = DB::get_row("select * from salon_appointments sapp  where  sapp.id='".(int)$_GET['appt_id']."' and sapp.salon_id='".SALON_ID."' and sapp.is_paid=0 limit 1");
		if($appt_inf){
			DB::delete('salon_appointments_services'," appt_id='".(int)$appt_inf['id']."'");
			DB::delete('salon_appointments_retail'," appt_id='".(int)$appt_inf['id']."'");
			DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_inf['id']."'");
			DB::delete('salon_appointments_series'," appt_id='".(int)$appt_inf['id']."'");
			DB::delete('gift_cards'," appt_id='".(int)$appt_inf['id']."'");
			DB::delete('salon_appointments_promos'," appt_id='".(int)$appt_inf['id']."'");

			DB::delete('salon_appointments'," id='".(int)$appt_inf['id']."' limit 1 ");
			log_event('remove_appt', (int)$appt_inf['id'], $appt_inf['customer_id'], 0, 'Resolve clicked on open tickets',date('Y-m-d',strtotime($appt_inf['appt_date'])));
			exit('ok');
		}
	}
	exit('Error, deletion not successful');
}
if($action == 'cancel_apptservice'){

	$wlistdt = '';
	$min_time = 0;
	$max_time = 0;

	if(1){

	if(isset($_GET['serv_id']) && $_GET['serv_id']){
		$appt_serv = DB::get_row("select sapp.appt_date, sapp.customer_id, sas.*, u.first_name, u.image_filename,ss.service_name from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id where  sas.appt_serv_id='".(int)$_GET['serv_id']."' and sapp.salon_id='".SALON_ID."' and sapp.is_paid=0 limit 1");

		if($appt_serv){

			$wlistdt = strtotime($appt_serv['appt_date']);
			$min_time = getTimeSec($appt_serv['service_time']);
			$max_time = $min_time;

			$appt_id = $appt_serv['appt_id'];
			$bookedapt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' limit 1");
			$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
			$u_info = DB::get_row("select * from users where id='".$appt_serv['customer_id']."' limit 1");

			$all_servs = DB::query("select sapp.appt_date, sapp.customer_id, sas.*, u.first_name, u.image_filename,ss.service_name from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id where  sas.appt_id='".(int)$appt_id."' and sapp.salon_id='".SALON_ID."' ");
			$subj = 'Appointment Cancelled';
			$body = "Your appointment on ".date('M d, Y',strtotime($bookedapt['appt_date']))." at ".$salon_info['name']." has been cancelled.";
			while($serv = DB::fetch_assoc($all_servs)){
				$bookedserv = $serv;
				$service = DB::get_row("select * from salon_services where id='".(int)$bookedserv['service_id']."' limit 1");

				log_event('delete_service', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'], ( isset($_POST['reason_dd'])?$_POST['reason_dd'].'. ':'' ) . (  isset($_POST['reason'])?$_POST['reason']:'Cancel service'),date('Y-m-d',strtotime($bookedapt['appt_date'])),$bookedserv['service_time'],$bookedserv['provider_id']);


				//$body = "Your appointment on ".date('M d, Y',strtotime($bookedapt['appt_date']))." at ".$bookedserv['service_time']." at ".$salon_info['name']." for ".$service['service_name']." has been cancelled.";

				$body.="\r\n\r\n";
				$body.="Service Type: ".$service['service_name']."\r\n";
				$body.="Stylist: ".$bookedserv['first_name']."\r\n";
				$body.="Time: ".$bookedserv['service_time'];

				$start_sec = getTimeSec($bookedserv['service_time']);
				if($start_sec>0 && $start_sec<$min_time) $min_time=$start_sec;

				if($start_sec>0){
					$appt_serv_info = DB::get_row("select  sas.*, ss.default_timing_min,   ps.timing_min, if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration,
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time, if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0)))) as process_time,
					if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0)))) as service_time_2 from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join salon_services ss on ss.id=sas.service_id left join provider_services ps on ps.service_id= sas.service_id and ps.provider_id = sas.provider_id left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id where  sas.appt_serv_id='".(int)$serv['appt_serv_id']."' limit 1 ");
					if($appt_serv_info && $appt_serv_info['service_duration']){
						$end_sec = $start_sec + $appt_serv_info['service_duration']*60;
						if($end_sec>0 && $end_sec>$max_time) $max_time=$end_sec;
					}
				}

				//mail($u_info['email'],$subj,$body,(MAIL_FROM?'From: '.MAIL_FROM:''));

				$stylist = DB::get_row("select pi.appointment_alerts from users u inner join provider_info pi on pi.user_id=u.id  where u.id='".(int)$bookedserv['provider_id']."' and pi.salon_id='".SALON_ID."' limit 1");
				
				include_once('includes/appointment_alerts.php');
				cancelled_appt_alert( $bookedserv['provider_id'], $u_info['first_name'] . ' ' . $u_info['last_name'], strtotime($bookedapt['appt_date']), $bookedserv['service_time'] );
				

				DB::delete('salon_appointments_services'," appt_serv_id='".(int)$serv['appt_serv_id']."' limit 1 ");
				//DB::delete('salon_appointments_services'," appt_id='".(int)$appt_id."' ");
			}
			if(isset($_POST['mail_client']) && $_POST['mail_client']) sendEmail($u_info['email'],$subj,$body,false);
			$check_more_serv = DB::get_row("select * from salon_appointments_services where appt_id='".(int)$appt_id."' limit 1");
			if(!$check_more_serv){
				DB::delete('salon_appointments_services'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_retail'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_series'," appt_id='".(int)$appt_id."'");
				DB::delete('gift_cards'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_promos'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments'," id='".(int)$appt_id."' limit 1 ");
				log_event('delete_appt', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'],  ( isset($_POST['reason_dd'])?$_POST['reason_dd'].'. ':'' ) . ( isset($_POST['reason'])?$_POST['reason']:'All services cancelled'),date('Y-m-d',strtotime($bookedapt['appt_date'])), '', 0, $u_info['first_name'].' '. $u_info['last_name']);
			}
			/*$bookedserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['serv_id']."'   limit 1");
			$bookedapt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' limit 1");
			$service = DB::get_row("select * from salon_services where id='".(int)$bookedserv['service_id']."' limit 1");
			$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
			$u_info = DB::get_row("select * from users where id='".$appt_serv['customer_id']."' limit 1");

			log_event('delete_service', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'], 'Cancel service',date('Y-m-d',strtotime($bookedapt['appt_date'])),$bookedserv['service_time']);

			$subj = 'Appointment Cancelled';
			$body = "Your appointment on ".date('M d, Y',strtotime($bookedapt['appt_date']))." at ".$bookedserv['service_time']." at ".$salon_info['name']." for ".$service['service_name']." has been cancelled.";

			//mail($u_info['email'],$subj,$body,(MAIL_FROM?'From: '.MAIL_FROM:''));
			sendEmail($u_info['email'],$subj,$body,false);



			//DB::delete('salon_appointments_services'," appt_serv_id='".(int)$_GET['serv_id']."' limit 1 ");
			DB::delete('salon_appointments_services'," appt_id='".(int)$appt_id."' ");
			$check_more_serv = DB::get_row("select * from salon_appointments_services where appt_id='".(int)$appt_id."' limit 1");
			if(!$check_more_serv){
				DB::delete('salon_appointments'," id='".(int)$appt_id."' limit 1 ");
				log_event('delete_appt', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'], 'All services cancelled',date('Y-m-d',strtotime($bookedapt['appt_date'])));
			}*/
		}


	}


	if(isset($_POST['cancel_standing']) && $_POST['cancel_standing']>0){
		$appt_list = DB::query("select sapp.*  from salon_appointments sapp   where  sapp.is_standing='".(int)$_POST['cancel_standing']."' and sapp.salon_id='".SALON_ID."' and sapp.is_paid=0 limit 100");
		$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");

		while($bookedapt = DB::fetch_assoc($appt_list)){
			$appt_id = $bookedapt['id'];

			$u_info = DB::get_row("select * from users where id='".$bookedapt['customer_id']."' limit 1");

			$all_servs = DB::query("select sapp.appt_date, sapp.customer_id, sas.*, u.first_name, u.image_filename,ss.service_name from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sas.provider_id inner join salon_services ss on ss.id=sas.service_id where  sas.appt_id='".(int)$appt_id."' and sapp.salon_id='".SALON_ID."' ");

			while($serv = DB::fetch_assoc($all_servs)){
				$bookedserv = $serv;

				log_event('delete_service', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'], ( isset($_POST['reason_dd'])?$_POST['reason_dd'].'. ':'' ) . (  isset($_POST['reason'])?$_POST['reason']:'Cancel service').' [standing]',date('Y-m-d',strtotime($bookedapt['appt_date'])),$bookedserv['service_time'],$bookedserv['provider_id']);

				DB::delete('salon_appointments_services'," appt_serv_id='".(int)$serv['appt_serv_id']."' limit 1 ");

			}
			$check_more_serv = DB::get_row("select * from salon_appointments_services where appt_id='".(int)$appt_id."' limit 1");
			if(!$check_more_serv){
				DB::delete('salon_appointments_services'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_retail'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_tips'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_series'," appt_id='".(int)$appt_id."'");
				DB::delete('gift_cards'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments_promos'," appt_id='".(int)$appt_id."'");
				DB::delete('salon_appointments'," id='".(int)$appt_id."' limit 1 ");
				log_event('delete_appt', $appt_id, $appt_serv['customer_id'], $bookedserv['service_id'],  ( isset($_POST['reason_dd'])?$_POST['reason_dd'].'. ':'' ) . ( isset($_POST['reason'])?$_POST['reason']:'All services cancelled').' [standing]',date('Y-m-d',strtotime($bookedapt['appt_date'])), '', 0, $u_info['first_name'].' '. $u_info['last_name']);
			}
		}
	}
	}
	if(isset($_GET['test123'])){

		$wlistdt = strtotime('08/11/2021');
		$min_time = getTimeSec('11:15 AM');
		$max_time = getTimeSec('12:45 PM');
	}

	if($wlistdt){
		include('includes/functions_waitlist.php');
		/*
	$wlistdt = '';
	$min_time = 0;
	$max_time = 0;*/
		$min_time-=7200;
		$max_time+=7200;

		$dt_form = date('Y-m-d',$wlistdt);
		$avail_wlist = array();
		$services = array();
		$providers = array();

		$q = DB::query("SELECT distinct w.*,u.first_name,u.last_name, u.phone, u.phone_clean ,u.image_filename from  `waitlist` w left join users u on u.id=w.customer_id   WHERE w.salon_id='".SALON_ID."' and w.`end_date`>='".$dt_form."' and w.`start_date`<='".$dt_form."' and (w.start_time_sec>=".$min_time." and w.start_time_sec<=".$max_time.") ");



		while($r=DB::fetch_assoc($q)){
			$r['start_date'] = $dt_form;
			$r['end_date'] = $dt_form;

			$result = checkWaitlist($r,0);
			if($result){
				$r['result'] = $result;
				$avail_wlist[]=$r;
			}
		}

		if(count($avail_wlist)){
			exit('WLIST_AVAILABLE');
/*			?>

			<div class="modal-header">
				<h5 class="modal-title" style="font-weight:bold;">Waitlist for <?php echo date('D m/d/Y',$wlistdt).' at '.getSecTime($min_time).'-'.getSecTime($max_time); ?></h5><button type="button" class="close" data-dismiss="modal" ><i class="las la-times"></i></button>
			</div>
			<div class="modal-body"  >
				<div class="row" style="margin:0 -20px;">
					<table class="table" style="font-size: 14px;">
					<?php
					foreach($avail_wlist as $r){
						$service_data = json_decode($r['service_data'],true);

						$serv_list = array();
						$prov_list = array();
						if(is_array($service_data)) foreach($service_data as $s){

							if(!isset($services[$s['service']])){
								$services[$s['service']] = DB::get_row("SELECT `id`,`service_name`,`service_code` FROM `salon_services` WHERE `salon_id`='".SALON_ID."' and id='".(int)$s['service']."' limit 1  ");
								if($services[$s['service']]['service_code']=='') $services[$s['service']]['service_code']=$services[$s['service']]['service_name'];

							}
							if(!isset($providers[$s['provider']])){
								$providers[$s['provider']] = DB::get_row("select distinct u.id, u.first_name, pi.code_name from users u inner join provider_info pi on pi.user_id=u.id where pi.salon_id='".SALON_ID."' and u.id='".(int)$s['provider']."' limit 1  ");
								if($providers[$s['provider']]['code_name']!='') $providers[$s['provider']]['first_name']=$providers[$s['provider']]['code_name'];
							}

							if($s['service'] && !isset($serv_list[$s['service']]) && isset($services[$s['service']])) $serv_list[$s['service']]='<span class="ttip" title="'.htmlspecialchars($services[$s['service']]['service_name']).'">'.$services[$s['service']]['service_code'].'</span>';

							if($s['provider'] && !isset($prov_list[$s['provider']]) && isset($providers[$s['provider']])) $prov_list[$s['provider']]=$providers[$s['provider']]['first_name'] ;
						}

						$b_date = $r['start_date'];
						$b_time = $r['start_time'];

						if($r['avail_slot_data']!=''){
							$avail_slot_data = json_decode($r['avail_slot_data'],true);
							if(isset($avail_slot_data[0]) && $avail_slot_data[0]['date']){
								$b_date = $avail_slot_data[0]['date'];
								$b_time = $avail_slot_data[0]['time'];
							}
						}

				?>
				<tr class="apptrow mwaitrow_<?php echo $r['id']; ?>">
					<td>
					<?php if($r['image_filename']){ ?><img src="<?php echo IMG_URL; ?>/avatars/thumbs/<?php echo $r['image_filename']; ?>" class="smallround" /><?php } else echo '<span class="smallround_initials">' . strtoupper($r['first_name'][0]) . strtoupper($r['last_name'][0]) . '</span>'; ?>
					</td>
					<td>
						<a href="client_profile.php?id=<?php echo $r['customer_id']; ?>" target="_blank"><?php echo $r['first_name'].' '.$r['last_name']; ?></a>
					</td>
					<td>
						<?php echo $r['phone']; ?>
					</td>
					<td >
						<?php echo implode(', ',$serv_list); ?>
					</td>
					<td >
						<?php echo implode(', ',$prov_list); ?>
					</td>
					<td style="text-align:center;">
						<?php if($r['status']){
							if($r['alert_sent']){
							?>
							<a href="javascript:;" class="ttip" title="<?php echo date('m/d/y h:i a',tz_time($r['alert_sent'])); ?>"><i class="fa-regular fa-message-dots" style="color:#4caf50;"></i></a>
							<?php
							}else{
							?>
							<a href="javascript:;" class="ttip" onclick="cal_sendWlAlert(<?php echo $r['id']; ?>,this);" title="Send alert!"><i class="fa-regular fa-message"></i></a>
							<?php
							}
						} ?>
					</td>
				</tr>
				<?php }
					?>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<?php
*/
		}

		//echo 'end';

	}


	exit;
}
if($action == 'get_apptservice_notes'){

	if(isset($_GET['serv_id']) && $_GET['serv_id']){
		$res = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['serv_id']."' limit 1");
		if($res ) echo $res['notes'];
	}
	exit;
}
if($action == 'save_apptservice_notes'){

	if(isset($_GET['serv_id']) && $_GET['serv_id']){
		DB::update('salon_appointments_services',array('notes'=> $_POST['notes'])," appt_serv_id='".(int)$_GET['serv_id']."' limit 1");
	}
	exit;
}
if($action == 'change_apptservice_status'){

	if(isset($_GET['serv_id']) && $_GET['serv_id']){
		$bookedserv = DB::get_row("select * from salon_appointments_services where appt_serv_id='".(int)$_GET['serv_id']."'   limit 1");
		if($bookedserv){
			DB::update('salon_appointments_services',array('status'=>(int)$_GET['status'])," appt_id='".(int)$bookedserv['appt_id']."' ");

			if($_GET['status']==2){
				$appt_info = DB::get_row("select * from salon_appointments where id='".(int)$bookedserv['appt_id']."' limit 1");
				$cust_info = DB::get_row("select * from users where id='".(int)$appt_info['customer_id']."' limit 1");
				$stylist_info = DB::get_row("select * from users where id='".(int)$bookedserv['provider_id']."' limit 1");
				$service_info = DB::get_row("select * from salon_services where id='".(int)$bookedserv['service_id']."' limit 1");

				if($stylist_info['phone_clean']!='' && $salon_info['stylist_sms_notif']){
					$txt = 'Hi '.$stylist_info['first_name'].'! '.$cust_info['first_name'].' '.$cust_info['last_name'].' has checked in for '.$service_info['service_name'].($bookedserv['service_time']!=''?' at '.$bookedserv['service_time']:'').'.';
					DB::insert('sms_stylists',array(
						'salon_id'=>$appt_info['salon_id'],
						'appt_id' => (int)$bookedserv['appt_id'],
						'appt_serv_id' => $bookedserv['appt_serv_id'],
						'stylist_id' => $bookedserv['provider_id'],
						'to_number' => $stylist_info['phone_clean'],
						'body' => $txt,
						'send_after_time' => time(),
						'status' => 1
					));
				}


				DB::insert('salon_appointments_log',array(
					'appt_id' => (int)$bookedserv['appt_id'],
					'user_id' => $user_id,
					'dtime' => time(),
					'event_type' => 'arrived',
					'details' => ''

				));
			}
			if($_GET['status']==1){

				DB::insert('salon_appointments_log',array(
					'appt_id' => (int)$bookedserv['appt_id'],
					'user_id' => $user_id,
					'dtime' => time(),
					'event_type' => 'confirmed',
					'details' => ''

				));
			}
		}
	}
	exit;
}

if($action == 'change_user_status'){
	if(isset($_GET['uid']) && $_GET['uid']){
		DB::update('users',array('status'=>(int)$_GET['status'])," id='".(int)$_GET['uid']."' limit 1");
	}
	exit;
}
if($action == 'change_supp_status'){
	if(isset($_GET['uid']) && $_GET['uid']){
		DB::update('suppliers',array('status'=>(int)$_GET['status'])," id='".(int)$_GET['uid']."' and salon_id='".SALON_ID."'  limit 1");
	}
	exit;
}
if($action == 'update_customer_services'){
	$editable_fields = array(
		'timing_min','service_time_1','process_time','service_time_2'
	);
	$customer_id = isset($_POST['customer'])?$_POST['customer']:'';
	$servdata=isset($_POST['servdata'])?$_POST['servdata']:'';

	if($servdata!=''){
		$servdata=json_decode($servdata,true);
		if(is_array($servdata) && count($servdata)){
			foreach($servdata as $sid=>$serv){

				$updata = array();
				foreach($serv as $k=>$v){
					if(in_array($k,$editable_fields)) $updata[$k]=$v;
				}
				if(count($updata)){
					$check = DB::get_row("select * from customer_services where customer_id='".(int)$customer_id."' and service_id='".(int)$sid."' limit 1");
					if($check){
						DB::update('customer_services',$updata, " customer_id='".(int)$customer_id."' and service_id='".(int)$sid."' limit 1");
					} else {
						$updata['customer_id']=(int)$customer_id;
						$updata['service_id']=(int)$sid;
						DB::insert('customer_services',$updata);
					}
					log_event('updated_smart_booking', 0, (int)$customer_id, (int)$sid);
				}

			}
		}
	}

	exit;
}
if($action == 'update_stylist_services'){
	$editable_fields = array(
		'pricing','timing_min','service_time_1','process_time','service_time_2','fee_perc'
	);
	$stylist = isset($_POST['stylist'])?$_POST['stylist']:'';
	$servdata=isset($_POST['servdata'])?$_POST['servdata']:'';

	if($servdata!=''){
		$servdata=json_decode($servdata,true);
		if(is_array($servdata) && count($servdata)){
			foreach($servdata as $sid=>$serv){
				if(isset($serv['remove']) && $serv['remove']){
					DB::delete('provider_services'," provider_id='".(int)$stylist."' and service_id='".(int)$sid."' limit 1");
				}else{
					$updata = array();
					foreach($serv as $k=>$v){
						if(in_array($k,$editable_fields)) $updata[$k]=$v;
					}
					if(count($updata)){
						$check = DB::get_row("select * from provider_services where provider_id='".(int)$stylist."' and service_id='".(int)$sid."' limit 1");
						if($check){
							DB::update('provider_services',$updata, " provider_id='".(int)$stylist."' and service_id='".(int)$sid."' limit 1");
						} else {
							$updata['provider_id']=(int)$stylist;
							$updata['service_id']=(int)$sid;
							DB::insert('provider_services',$updata);
						}
					}
				}
			}
		}
	}

	exit;
}
if($action == 'service_change_duration_multi'){
	if($_GET['appt_serv_id'] && $_GET['new_d1']){

		$appt_id = 0;
		$appt_serv = DB::get_row("select sas.* from salon_appointments_services sas  where  sas.appt_serv_id='".(int)$_GET['appt_serv_id']."' limit 1");
		if($appt_serv) $appt_id = $appt_serv['appt_id'];
		if($appt_id){

			$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			if(!$appt){ exit('Bad request!'); }
			if($_GET['new_d1']>=15 && $_GET['new_d1']%15==0){
				$new_dur  = $_GET['new_d1']+$_GET['new_d2']+$_GET['new_d3'];
				DB::update('salon_appointments_services',array('duration_changed'=>(int)$new_dur,'dur_appl_chng'=>(int)$_GET['new_d1'],'dur_procs_chng'=>(int)$_GET['new_d2'],'dur_finsh_chng'=>(int)$_GET['new_d3'])," appt_serv_id='".(int)$_GET['appt_serv_id']."' limit 1 ");

				if(isset($_GET['save_to_profile']) && $_GET['save_to_profile']){
					$updata = array(
						'timing_min' => (int)$new_dur,
						'service_time_1' => (int)$_GET['new_d1'],
						'process_time' => (int)$_GET['new_d2'],
						'service_time_2' => (int)$_GET['new_d3']
					);
					$check = DB::get_row("select * from customer_services where customer_id='".(int)$appt['customer_id']."' and service_id='".(int)$appt_serv['service_id']."' limit 1");
					if($check){
						DB::update('customer_services',$updata, " customer_id='".(int)$appt['customer_id']."' and service_id='".(int)$appt_serv['service_id']."' limit 1");
					} else {
						$updata['customer_id']=(int)$appt['customer_id'];
						$updata['service_id']=(int)$appt_serv['service_id'];
						DB::insert('customer_services',$updata);
					}
					log_event('updated_smart_booking', $appt_id, (int)$appt['customer_id'], (int)$appt_serv['service_id']);
				}
			}
		}
	}

	exit;
}
if($action == 'service_change_duration'){
	if($_GET['appt_serv_id'] && $_GET['new_duration']){

		$appt_id = 0;
		$appt_serv = DB::get_row("select sas.* from salon_appointments_services sas  where  sas.appt_serv_id='".(int)$_GET['appt_serv_id']."' limit 1");
		if($appt_serv) $appt_id = $appt_serv['appt_id'];
		if($appt_id){

			$appt = DB::get_row("select * from salon_appointments where id='".(int)$appt_id."' and salon_id='".SALON_ID."' limit 1");
			if(!$appt){ exit('Bad request!'); }
			if($_GET['new_duration']>=15 && $_GET['new_duration']%15==0){
				DB::update('salon_appointments_services',array('duration_changed'=>(int)$_GET['new_duration'])," appt_serv_id='".(int)$_GET['appt_serv_id']."' limit 1 ");


				if(isset($_GET['save_to_profile']) && $_GET['save_to_profile']){
					$updata = array(
						'timing_min' => (int)$_GET['new_duration'],
						'service_time_1' => 0,
						'process_time' => 0,
						'service_time_2' => 0
					);
					$check = DB::get_row("select * from customer_services where customer_id='".(int)$appt['customer_id']."' and service_id='".(int)$appt_serv['service_id']."' limit 1");
					if($check){
						DB::update('customer_services',$updata, " customer_id='".(int)$appt['customer_id']."' and service_id='".(int)$appt_serv['service_id']."' limit 1");
					} else {
						$updata['customer_id']=(int)$appt['customer_id'];
						$updata['service_id']=(int)$appt_serv['service_id'];
						DB::insert('customer_services',$updata);
					}
					log_event('updated_smart_booking', (int)$appt_id, (int)$appt['customer_id'], (int)$appt_serv['service_id']);
				}
			}
		}
	}

	exit;
}
if($action == 'update_services'){
	$editable_fields = array(
		'service_name','service_code','default_pricing','default_timing_min','default_fee_perc','default_tip_tax','service_time_1','process_time','service_time_2','hide_on_front', 'equipment_id','equipments_list','deposit','employee_price','disabled', 'tax', 'service_usell_id', 'retail_usell_id', 'ep_price','description','first_book_enbl','dashboard_report','pinned_service'
	);
	if( $salon_info['enable_labor_fee'] ) $editable_fields[] = 'labor_fee';

	$servdata=isset($_POST['servdata'])?$_POST['servdata']:'';
	if($servdata!=''){
		$servdata=json_decode($servdata,true);
		//pre_print($servdata); exit;
		if(is_array($servdata) && count($servdata)){
			foreach($servdata as $serv){
				$serv_salon_id = SALON_ID;
				$s_info = DB::get_row("select * from salon_services where id='".(int)$serv['id']."' limit 1");
				if($s_info && $s_info['salon_id'] && $s_info['salon_id']!=SALON_ID) $serv_salon_id = $s_info['salon_id'];
				$updata = array();
				foreach($serv as $k=>$v){
					if(in_array($k,$editable_fields)) $updata[$k]=$v;
					elseif(strpos($k,'level_')!==false){
						$lev = explode('level_',$k);
						$lev=(int)$lev[1];
						if($lev){
							$check = DB::get_row("select * from salon_services_prices where service_id='".(int)$serv['id']."' and level_id='".$lev."' and salon_id='".$serv_salon_id ."' limit 1");
							if($check){
								DB::update('salon_services_prices',array('level_price'=>$v)," service_id='".(int)$serv['id']."' and level_id='".$lev."' and salon_id='".$serv_salon_id ."' limit 1");
							}else{
								DB::insert('salon_services_prices',array(
									'service_id'=>(int)$serv['id'],
									'level_id'=>(int)$lev,
									'salon_id'=>$serv_salon_id ,
									'level_price'=>$v
								) );
							}
						}
					}
				}
				if(count($updata)) DB::update('salon_services',$updata, " id='".(int)$serv['id']."' and salon_id='".$serv_salon_id ."' limit 1");
			}
		}
	}

	exit;
}
if($action == 'set_not_new_client'){
	if(isset($_GET['apptid'])&&$_GET['apptid']){
		$appt = DB::get_row("select * from salon_appointments where id='".(int)$_GET['apptid']."' and salon_id='".SALON_ID."' limit 1");
		if($appt){
			DB::update('users',array('is_new_customer'=>0)," id='".$appt['customer_id']."' limit 1");
		}
	}
	exit;
}

if($action == 'edit_gift_card_comment'){
	if(isset($_GET['comment'])&&isset($_GET['gcid'])&&$_GET['gcid']){

		$salon_ids = array(SALON_ID);
		if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;
		$gfinfo = DB::get_row("select * from gift_cards where id='".(int)$_GET['gcid']."'  and salon_id in (".implode(',',$salon_ids).") limit 1 ");
		if($gfinfo){
			DB::update('gift_cards',array('comment'=>addslashes($_GET['comment']))," id='".(int)$_GET['gcid']."' limit 1");
			DB::insert('giftcard_log',array(
				'giftcard_id' => (int)$_GET['gcid'],
				'salon_id' => SALON_ID,
				'user_id' => $user_id,
				'event_action' => 'change_comment',
				'event_time' => date('Y-m-d H:i:s'),
				'details' => $_GET['comment'],
				'gf_balance' => $gfinfo['card_balance']
			));
			log_event("gc_comment", 0, 0, 0, '', '', '', $user_id, json_encode( array( 'gift_card' => (int)$_GET['gcid'] , 'new_val' => addslashes($_GET['comment']), 'old_val' => $gfinfo['comment'] ) ) );
		}
	}
	exit;
}

if($action == 'edit_gift_card_balance'){
	if(isset($_GET['balance'])&&isset($_GET['gcid'])&&$_GET['gcid']){
		$salon_ids = array(SALON_ID);
		if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1) $salon_ids = ALL_SALON_IDS;
		$gfinfo = DB::get_row("select * from gift_cards where id='".(int)$_GET['gcid']."'  and salon_id in (".implode(',',$salon_ids).") limit 1 ");
		if($gfinfo){
			DB::update('gift_cards',array('card_balance'=>addslashes($_GET['balance']))," id='".(int)$_GET['gcid']."' limit 1");
			DB::insert('giftcard_log',array(
				'giftcard_id' => (int)$_GET['gcid'],
				'salon_id' => SALON_ID,
				'user_id' => $user_id,
				'event_action' => 'change_balance',
				'event_time' => date('Y-m-d H:i:s'),
				'details' => 'Prev: $'.number_format($gfinfo['card_balance'],2),
				'gf_balance' => $_GET['balance']
			));
			log_event("gc_balance", 0, 0, 0, '', '', '', $user_id, json_encode( array( 'gift_card' => (int)$_GET['gcid'] , 'new_val' => number_format($_GET['balance'],2), 'old_val' => number_format($gfinfo['card_balance'],2) ) ) );
		}
	}
	exit;
}

if($action == 'get_all_services_per_date'){
	$result = array();
	$date = isset($_GET['date'])?$_GET['date']:date('Y-m-d');
	$datetm = strtotime($date);
	$single_stylist = isset($_GET['single_stylist'])?(int)$_GET['single_stylist']:0;



	$checkdate = date('Y-m-d',$datetm);
	if(strtotime($checkdate)>time()-1*365*24*3600) $_SESSION['last_cal_date'] = $checkdate;
	$end_checkdate = $checkdate;
	if(isset($_GET['cal_type']) && $_GET['cal_type']=='week'){
		$tmp = $datetm+(7*24-3)*3600;
		$end_checkdate = date('Y-m-d',$tmp);
	}

	$appts = DB::query("select sapp.id,cf.pdf,cf.sent_time,sapp.appt_date,sapp.is_paid,sapp.customer_id, sapp.allow_checkout,sapp.is_prebook,sapp.merged_with,sapp.is_standing, sas.*, u.first_name, u.last_name,u.is_vip ,u.is_new_customer,u.dcc_pay, u.guest_notes,u.has_secondary_rewards,u.secondary_rewards,ss.service_name,ss.service_code,
		if(sas.duration_changed is not null and sas.duration_changed>0,sas.duration_changed,if(cs.timing_min>0, cs.timing_min, if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15)))) as service_duration,
		if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_appl_chng,if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0)))) as service_time_1,
		if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_procs_chng,if(cs.service_time_1>0, cs.process_time, if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0)))) as process_time,
		if(sas.dur_appl_chng is not null and sas.dur_appl_chng>0,sas.dur_finsh_chng,if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0)))) as service_time_2, cs.timing_min as is_smart,
		sal.user_id as booker from salon_appointments sapp inner join salon_appointments_services sas on sas.appt_id=sapp.id inner join users u on u.id=sapp.customer_id inner join salon_services ss on ss.id=sas.service_id  left join provider_services ps on ps.provider_id=sas.provider_id and ps.service_id=sas.service_id left join customer_services cs on cs.customer_id=sapp.customer_id and cs.service_id=sas.service_id  left join salon_appointments_log sal on sal.appt_id=sapp.id and sal.event_type='book' left join covid_forms cf on cf.appt_id=sapp.id where sapp.salon_id='".SALON_ID."' and (sapp.appt_date between '".$checkdate." 00:00:00' and '".$end_checkdate." 23:59:59') ".(isset($single_stylist)&&$single_stylist?" and sas.provider_id='".(int)$single_stylist."' ":"")."  and sas.merged_appt is null  order by sapp.appt_date, sapp.id, service_time_sec asc, sapp.id");
	$ec=0;
	$latetime = tz_time(time())-15*60;
	$is_multi_cache = array();
	$stlcache = array();
	while($r = DB::fetch_assoc($appts)){
		$ec++;

		$is_new = 0;
		$is_app = 0;
		$is_stl = 0;
		$is_vip = 0;
		$has_secondary_rewards = 0;
		$secondary_rewards = '';
		$cf_pdf = 0;
		$is_smart = 0;
		$is_request = 0;
		$is_prebook = 0;
		$has_notes = 0;
		$has_img = 0;
		$is_multiprov = 0;
		$is_standing = (int)$r['is_standing'];
		$is_dcc = $salon_info['enable_dcc_pay'] && $r['dcc_pay']?1:0;


		if($r['merged_with']){
			$merge_appt_main = DB::get_row("select * from salon_appointments where id='".$r['merged_with']."' limit 1");
			if($merge_appt_main){
				$r['is_paid']=$merge_appt_main['is_paid'];
				$r['allow_checkout']=$merge_appt_main['allow_checkout'];
			}
		}

		if(isset($is_multi_cache[$r['appt_id']])){
			$is_multiprov =$is_multi_cache[$r['appt_id']];
		}else{
			$check = DB::get_row("select count(distinct provider_id) as c from salon_appointments_services where appt_id='".$r['appt_id']."'");
			if($check['c']>1) $is_multiprov = 1;
			$is_multi_cache[$r['appt_id']] = $is_multiprov;
		}

		if($r['is_vip']) $is_vip=1;
		if($r['has_secondary_rewards']){ $has_secondary_rewards=1; $secondary_rewards=addslashes(htmlspecialchars(trim($r['secondary_rewards']))); }
		if($r['is_request']) $is_request=1;
		if($r['is_prebook']) $is_prebook=1;
		if($r['is_smart']>0) $is_smart=1;
		if($r['guest_notes']!='') $has_notes=1;
		if($r['inspirational_pic']!='') $has_img=1;
		if( !isset($r['sent_time']) ) { $cf_pdf = -1; } else if( $r['pdf']===NULL ) { $cf_pdf = 0; }else if( $r['pdf']!='' ) { $cf_pdf = $r['pdf']; }
		/*$checknew = DB::get_row("select count(id) as c from salon_appointments where customer_id='".$r['customer_id']."' and salon_id='".SALON_ID."' and id<".$r['id']." ");
		if(!$checknew['c']) $is_new = 1;*/

		if($r['is_new_customer']){
			$checknew = DB::get_row("select count(id) as c from salon_appointments where customer_id='".$r['customer_id']."' and salon_id='".SALON_ID."' and is_paid=1 ");
			if(!$checknew['c']) $is_new = 1;
			else{
				DB::update('users',array('is_new_customer'=>0)," id='".$r['customer_id']."' limit 1");
			}
		}

		if($r['booker']==$r['customer_id']) $is_app = 1;
		else{
			if(!isset($stlcache[$r['booker']])) $stlcache[$r['booker']] = DB::get_row("select role_id from users where id='".(int)$r['booker']."' limit 1");
			if($stlcache[$r['booker']] && ($stlcache[$r['booker']]['role_id']==2 )) $is_stl = 1;
		}

		$addClasses = '';
		if($r['is_paid'])$addClasses .= ' paid';
		else if($r['allow_checkout'])$addClasses .= ' allowcheckout';
		else if($r['status']==1) $addClasses .= ' confirmed'; else if($r['status']==2) $addClasses .= ' arrived';

		$servtime = $times_list_24h[$r['service_time']];
		if(strlen($servtime)==4) $servtime='0'.$servtime;
		$sertimestamp = strtotime(date('Y-m-d',strtotime($r['appt_date'])).' '.$r['service_time']);
		$is_late = 0;
		if($r['status']<=1 && $sertimestamp<$latetime && !$r['is_paid'] && !$r['allow_checkout']) $is_late = 1;

		if($r['service_time_1']>0  && ($r['process_time']>0) ){
			$durstr = ((int)$r['service_time_1']).','.((int)$r['process_time']).','.((int)$r['service_time_2']);
			$serv_endtime = getEndTime($r['service_time'],$r['service_time_1']);
			if(strlen($serv_endtime)==4) $serv_endtime='0'.$serv_endtime;

			$event=array(
				'id' => $r['appt_serv_id'],
				'appt_id' => $r['appt_id'],
				'customer_id' => $r['customer_id'],
				'title' => '<b onclick="showCustomerInfo('.$r['customer_id'].','.$r['appt_id'].',\'\','.$r['appt_serv_id'].')">'.$r['first_name'] .' '.$r['last_name'] .'</b>'.($r['service_time_1']>15?'<br/><!--dts--><span class="sname' . ($r['service_code']?' scttip" title="'.ucwords($r['service_name']):'') . '">'.ucwords($r['service_code']?$r['service_code']:$r['service_name']).'</span>':''),
				'start' => date('Y-m-d',strtotime($r['appt_date']))."T".$servtime.":00",
				'className' => 'yellowevent startev'.$addClasses,
				'stylist' => $r['provider_id'],
				'notes' => htmlspecialchars($r['notes']),
				'duration'=>$durstr,
				'new' => $is_new,
				'app' => $is_app,
				'late' => $is_late,
				'stl' => $is_stl,
				'vip' => $is_vip,
				'has_secondary_rewards' => $has_secondary_rewards,
				'secondary_rewards' => $secondary_rewards,
				'cf_pdf' => $cf_pdf,
				'smart' => $is_smart,
				'is_req'=>$is_request,
				'is_prebook'=>$is_prebook,
				'is_stand'=>$is_standing,
				'is_dcc'=>$is_dcc,
				'has_notes' => $has_notes,
				'has_img' => $has_img,
				'is_multiprov' => $is_multiprov
			);
			if($serv_endtime) $event['end'] = date('Y-m-d',strtotime($r['appt_date']))."T".$serv_endtime.":00";
			$result[]=$event;
			$last_ended = $serv_endtime;
			if($r['process_time']>0){
				$proc_starttime = $last_ended;
				$proc_endtime = getEndTime($last_ended,$r['process_time']);
				if(strlen($proc_endtime)==4) $proc_endtime='0'.$proc_endtime;
				$last_ended = $proc_endtime;

				/*$event=array(
					'id' => $r['appt_serv_id'],
					'appt_id' => $r['appt_id'],
					'title' => '&nbsp;<br/>&nbsp;',
					'start' => date('Y-m-d',strtotime($r['appt_date']))."T".$proc_starttime.":00",
					'className' => 'processtime',
					'stylist' => $r['provider_id'],
					'notes' => '',
					'duration'=>$durstr,
					'new' => 0,
					'app' => 0
				);
				if($proc_endtime) $event['end'] = date('Y-m-d',strtotime($r['appt_date']))."T".$proc_endtime.":00";
				$result[]=$event;*/
			}
			if($r['service_time_2']>0){
				$proc_starttime = $last_ended;
				$proc_endtime = getEndTime($last_ended,$r['service_time_2']);
				if(strlen($proc_endtime)==4) $proc_endtime='0'.$proc_endtime;
				$last_ended = $proc_endtime;
				$event=array(
					'id' => $r['appt_serv_id'],
					'appt_id' => $r['appt_id'],
					'customer_id' => $r['customer_id'],
					'title' => '<b onclick="showCustomerInfo('.$r['customer_id'].','.$r['appt_id'].',\'\','.$r['appt_serv_id'].')">'.$r['first_name'] .' '.$r['last_name'] .'</b>'.($r['service_time_2']>15?'<br/>'.ucwords($r['service_code']?$r['service_code']:$r['service_name']):''),
					'start' => date('Y-m-d',strtotime($r['appt_date']))."T".$proc_starttime.":00",
					'className' => 'yellowevent startev'.$addClasses,
					'stylist' => $r['provider_id'],
					'notes' => '',
					'duration'=>$durstr,
					'new' => $is_new,
					'app' => $is_app,
					'late' => $is_late,
					'stl' => $is_stl,
					'vip' => $is_vip,
					'has_secondary_rewards' => $has_secondary_rewards,
					'secondary_rewards' => $secondary_rewards,
					'cf_pdf' => $cf_pdf,
					'smart' => $is_smart,
					'is_req'=>$is_request,
					'is_prebook'=>$is_prebook,
					'is_stand'=>$is_standing,
					'is_dcc'=>$is_dcc,
					'has_notes' => $has_notes,
					'has_img' => $has_img,
					'is_multiprov' => $is_multiprov
				);
				if($proc_endtime) $event['end'] = date('Y-m-d',strtotime($r['appt_date']))."T".$proc_endtime.":00";
				$result[]=$event;
			}
		}else{
			$serv_endtime = getEndTime($r['service_time'],$r['service_duration']);
			if(strlen($serv_endtime)==4) $serv_endtime='0'.$serv_endtime;
			$event=array(
				'id' => $r['appt_serv_id'],
				'appt_id' => $r['appt_id'],
				'customer_id' => $r['customer_id'],
				'title' => '<b onclick="showCustomerInfo('.$r['customer_id'].','.$r['appt_id'].',\'\','.$r['appt_serv_id'].')">'.$r['first_name'] .' '.$r['last_name'] .'</b><br/><!--dts--><span class="sname' . ($r['service_code']?' scttip" title="'.ucwords($r['service_name']):'') . '">'.($r['service_duration']>15?ucwords($r['service_code']?$r['service_code']:$r['service_name']):'').'</span>',
				'start' => date('Y-m-d',strtotime($r['appt_date']))."T".$servtime.":00",
				'className' => 'yellowevent'.$addClasses,
				'stylist' => $r['provider_id'],
				'notes' => htmlspecialchars($r['notes']),
				'duration'=>$r['service_duration'],
				'new' => $is_new,
				'app' => $is_app,
				'late' => $is_late,
				'stl' => $is_stl,
				'vip' => $is_vip,
				'has_secondary_rewards' => $has_secondary_rewards,
				'secondary_rewards' => $secondary_rewards,
				'cf_pdf' => $cf_pdf,
				'smart' => $is_smart,
				'is_req'=>$is_request,
				'is_prebook'=>$is_prebook,
				'is_stand'=>$is_standing,
				'is_dcc'=>$is_dcc,
				'has_notes' => $has_notes,
				'has_img' => $has_img,
				'is_multiprov' => $is_multiprov
			);
			if($serv_endtime) $event['end'] = date('Y-m-d',strtotime($r['appt_date']))."T".$serv_endtime.":00";
			$result[]=$event;
		}

	}

	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}


if($action == 'get_all_booked_paid'){
	header('Content-type: application/json');
	$result=array();
	if(isset($_GET['date'])&&$_GET['date']){

		$result['booked'] = 0;
		$result['providers'] = array();

		$booked = DB::query("select sum(sas.book_price) as booked_total, sas.provider_id from salon_appointments sa inner join salon_appointments_services sas on sa.id=sas.appt_id where (sa.appt_date between '".date('Y-m-d',strtotime($_GET['date']))." 00:00:00' and '".date('Y-m-d',strtotime($_GET['date']))." 23:59:59') and sa.merged_with is null and sa.salon_id='".SALON_ID."' ".(isset($_GET['stylist_id'])&&$_GET['stylist_id']>0?" and sas.provider_id='".(int)$_GET['stylist_id']."' ":"")." group by sas.provider_id");
		while($r = DB::fetch_assoc($booked)){
			$result['booked']+=$r['booked_total'];
			$result['providers'][$r['provider_id']] = number_format($r['booked_total'],2);
		}
		$result['booked'] = number_format($result['booked'],2);

		$qpaid = DB::get_row("select sum(sas.book_price) as paid_total from salon_appointments sa inner join salon_appointments_services sas on sa.id=sas.appt_id where (sa.appt_date between '".date('Y-m-d',strtotime($_GET['date']))." 00:00:00' and '".date('Y-m-d',strtotime($_GET['date']))." 23:59:59') and sa.salon_id='".SALON_ID."' and sa.merged_with is null and is_paid=1  ".(isset($_GET['stylist_id'])&&$_GET['stylist_id']>0?" and sas.provider_id='".(int)$_GET['stylist_id']."' ":"")." ");
		$result['paid'] = number_format($qpaid['paid_total'],2);
	}

	echo json_encode($result);
	exit;
}


if($action == 'set_salon_seats'){
	if( isset( $_GET['salon_size'] ) && $_GET['salon_size'] != '' && $_GET['salon_size'] > 0 ){
		DB::update('salons',array( 'salon_size'=>(int)$_GET['salon_size'] )," id='".SALON_ID."' limit 1 ");
	}
	exit;
}

if($action == 'change_online_booking_stat'){
	if( isset( $_POST['uid'] ) && $_POST['uid'] ){
		$disable_online_booking = 0;
		if($_POST['disable_online_booking']==1)$disable_online_booking = 1;
		DB::update('users',array('disable_online_booking'=>$_POST['disable_online_booking'])," id='".(int)$_POST['uid']."' limit 1 ");
	}
	exit;
}

if($action == 'change_disable_sms'){
	if( isset( $_POST['uid'] ) && $_POST['uid'] ){
		$disable_sms = 1;
		if($_POST['disable_sms']==1)$disable_sms = 0;
		DB::update('users',array('disable_sms'=>$disable_sms)," id='".(int)$_POST['uid']."' limit 1 ");
	}
	exit;
}

if($action == 'change_disable_sms_cust'){
	if( isset( $_POST['uid'] ) && $_POST['uid'] ){
		$disable_sms_cust = 1;
		if($_POST['disable_sms_cust']==1)$disable_sms_cust = 0;
		DB::update('users',array('disable_sms_cust'=>$disable_sms_cust)," id='".(int)$_POST['uid']."' limit 1 ");
	}
	exit;
}


if($action == 'search_retail_upsell'){
	$results = array("results"=>array(), "pagination"=>array("more"=>false) );
	$pnum = (int)$_POST['pnum'];
	if($pnum<1) $pnum = 1;
	$prods_per_page=20;
	$where = '';

	if(isset($_POST['search']) && $_POST['search']!=''){


		if(isset($_POST['search']) && $_POST['search']!=''){
			$addtourl ='&kw='.urlencode($_POST['search']);
			$kws= explode(' ',trim($_POST['search']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$conds[]="(`code` like '%".$k."%' or `description` like '%".$k."%' or `category` like '%".$k."%' or `brand` like '%".$k."%' or `sku` like '%".$k."%' or `altern_skus` like '%".$k."%' or `altern_codes` like '%".$k."%')";
				}
			}
		}
	}

	$inv = DB::query("select * from salon_inventory where salon_id='".SALON_ID."' ".(!empty($conds)?" and ".implode(' and ',$conds):"")." and active=1 and (`type`='retail' or `type`='') order by `code` limit ".($prods_per_page*($pnum-1)).",".$prods_per_page);

	$ic=0;
	while($r=DB::fetch_assoc($inv)){
		$results['results'][]=array("id"=>$r['id'],"text"=>ucwords( strtolower( $r['description'] ) ) );
		$ic++;
	}

	if($ic==$prods_per_page) $results['pagination']['more']=true;

	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}


if($action == 'search_retail_upsell_oldd'){
	$results = array("results"=>array(), "pagination"=>array("more"=>false) );
	$pnum = (int)$_POST['pnum'];
	if($pnum<1) $pnum = 1;
	$prods_per_page=20;
	$where = '';

	if(isset($_POST['search']) && $_POST['search']!=''){


		if(isset($_POST['search']) && $_POST['search']!=''){
			$addtourl ='&kw='.urlencode($_POST['search']);
			$kws= explode(' ',trim($_POST['search']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$conds[]="(`code` like '%".$k."%' or `description` like '%".$k."%' or `category` like '%".$k."%' or `brand` like '%".$k."%' or `sku` like '%".$k."%' or `altern_skus` like '%".$k."%' or `altern_codes` like '%".$k."%')";
				}
			}
		}
	}

	$inv = DB::query("select * from salon_inventory where salon_id='".SALON_ID."' ".(!empty($conds)?" and ".implode(' and ',$conds):"")." and active=1 and (`type`='retail' or `type`='') order by `description` limit ".($prods_per_page*($pnum-1)).",".$prods_per_page);

	$ic=0;
	while($r=DB::fetch_assoc($inv)){
		$results['results'][]=array("id"=>$r['id'],"text"=>ucwords( strtolower( $r['description'] ) ) );
		$ic++;
	}

	if($ic==$prods_per_page) $results['pagination']['more']=true;

	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}

if($action == 'search_service_upsell'){
	$results = array("results"=>array(), "pagination"=>array("more"=>false) );
	$pnum = (int)$_POST['pnum'];
	if($pnum<1) $pnum = 1;
	$servs_per_page=20;
	$where = '';

	if(isset($_POST['search']) && $_POST['search']!=''){


		if(isset($_POST['search']) && $_POST['search']!=''){
			$addtourl ='&kw='.urlencode($_POST['search']);
			$kws= explode(' ',trim($_POST['search']));
			foreach($kws as $k){
				$k=addslashes(trim($k));
				if($k!=''){
					$conds[]="(`service_name` like '%".$k."%')";
				}
			}
		}
	}

	$services = DB::query("select id,service_name from salon_services ss where ss.salon_id='".SALON_ID."' ".(!empty($conds)?" and ".implode(' and ',$conds):"")." order by `service_name` limit ".($servs_per_page*($pnum-1)).",".$servs_per_page );

	$ic=0;
	while($r=DB::fetch_assoc($services)){
		$results['results'][]=array("id"=>$r['id'],"text"=>ucwords( strtolower( $r['service_name'] ) ) );
		$ic++;
	}

	if($ic==$servs_per_page) $results['pagination']['more']=true;

	header('Content-type: application/json');
	echo json_encode($results);
	exit;
}


if($action == 'populate_retail_upsell'){

	if( isset( $_GET['id'] ) && $_GET['id'] > 0 ){
		$ret_row = DB::get_row( "select id, description from salon_inventory where id='" . (int)$_GET['id'] . "' limit 1" );
		if(isset($ret_row['id'])) echo '<option value="' . $ret_row['id'] . '">' . $ret_row['description'] . '</option>';
	}
	exit;
}

if($action == 'populate_service_upsell'){

	if( isset( $_GET['id'] ) && $_GET['id'] > 0 ){
		$ret_row = DB::get_row("select id,service_name from salon_services where id='" . (int)$_GET['id'] . "' limit 1" );
		if(isset($ret_row['id'])) echo '<option value="' . $ret_row['id'] . '">' . $ret_row['service_name'] . '</option>';
	}
	exit;
}

if($action == 'get_user_modal_cmob'){

	$id=$_GET['uid'];
	echo '<div class="cl_um_stats" >';

	$next_appt = DB::get_row("select sapp.appt_date, sapp.id from salon_appointments sapp where sapp.appt_date>'".date('Y-m-d')."' and sapp.customer_id='".$id."' and sapp.salon_id='".SALON_ID."' order by sapp.appt_date asc, sapp.id asc limit 1");
	if($next_appt){
		echo '<div class="cl_um_next"><i class="las la-chevron-circle-left"></i>'.date('n.j.y',strtotime($next_appt['appt_date'])).'<div class="cl_um_title">Next Visit</div></div>';
	}else echo '<div class="cl_um_next"><i class="las la-chevron-circle-left"></i>None<div class="cl_um_title">Next Visit</div></div>';

	$last_appt = DB::get_row("select sapp.appt_date, sapp.id from salon_appointments sapp where sapp.appt_date<='".date('Y-m-d')."' and sapp.customer_id='".$id."' and sapp.salon_id='".SALON_ID."' order by sapp.appt_date desc, sapp.id desc limit 1");
	if($last_appt){
		echo '<div class="cl_um_last"><i class="las la-chevron-circle-right"></i>'.date('n.j.y',strtotime($last_appt['appt_date'])).'<div class="cl_um_title">Last Visit</div></div>';
	}else echo '<div class="cl_um_last"><i class="las la-chevron-circle-right"></i>None<div class="cl_um_title">Last Visit</div></div>';

	$total_avg = DB::get_row("SELECT sum(total) / count(*) as average, count(*) as visits FROM `salon_appointments` WHERE `customer_id` = {$id} and `appt_date` <= NOW() and  salon_id='".SALON_ID."'");
	if(count($total_avg) > 0) {
		echo '<div class="cl_um_average"><i class="las la-chart-area"></i>$'.($total_avg['average'] > 0 ? number_format($total_avg['average'],'2','.',',') : '0.00').'<div class="cl_um_title">Average Visit</div></div>';
	} else {
		echo '<div class="cl_um_average"><i class="las la-chart-area"></i>$0.00<div class="cl_um_title">Average Visit</div></div>';
	}

	$lifetime = DB::get_row("SELECT sum(total) as total, count(*) as total_appt FROM `salon_appointments` WHERE `customer_id` = {$id} and `appt_date` <= NOW() and  salon_id='".SALON_ID."'");
	if(count($lifetime) > 0) {
		echo '<div class="cl_um_lt"><i class="las la-heartbeat"></i>'.$lifetime['total_appt'].' ($'.($lifetime['total'] > 0 ? number_format($lifetime['total'],2,'.',',') : '0.00') .')<div class="cl_um_title">Lifetime Value</div></div>';
	} else {
		echo '<div class="cl_um_lt"><i class="las la-heartbeat"></i>0 ($0.00)<div class="cl_um_title">Lifetime Value</div></div>';
	}
	echo '</div>';

	exit;
}

if($action == 'get_user_modal_cinfo'){
	$id = (int)$_GET['uid'];
	$client = DB::get_row("select u.* from users u where u.id='".(int)$_GET['uid']."' limit 1");
	$pref_salon = 0;
	if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1){
		$sal_cust = DB::get_row("select * from salon_customers where salon_id in (".implode(',',ALL_SALON_IDS).") and customer_id='".(int)$_GET['uid']."' order by pref_salon desc limit 1");
		if($sal_cust['pref_salon']) $pref_salon = $sal_cust['pref_salon'];
	}else{
		$sal_cust = DB::get_row("select * from salon_customers where salon_id='".SALON_ID."' and customer_id='".(int)$_GET['uid']."' limit 1");
	}
		
		
	$gate = $salon_info['payment_gate'];
	if($gate=='evo'){
		include('../includes/functions_evo.php');
	}

	$cc_filter = "";

	if($gate=='evo'){
		$evoset = getEvoSettings();
		if(!isset($evoset['merch_id'])||$evoset['merch_id']=='') $evoset['merch_id']='none';
		$cc_filter = " and evo_merch_id='".addslashes($evoset['merch_id'])."'";
	}else{
		$cc_filter = "  and authnet_acc='".addslashes(AUTHNET_NAME)."' ";
	}


	?>
		<div class="profile_fields">
			<div class="row">
				<div class="col-md-12">
					<h6>Client Profile</h6>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="first_name_s" value="<?php echo htmlspecialchars($client['first_name']); ?>">
						<span class="help-block">First Name</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="last_name_s" value="<?php echo htmlspecialchars($client['last_name']); ?>">
						<span class="help-block">Last Name</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="phone_s" value="<?php echo htmlspecialchars($client['phone']); ?>">
						<span class="help-block">Mobile Phone</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="phone_home_s" value="<?php echo htmlspecialchars($client['phone_home']); ?>">
						<span class="help-block">Home Phone</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="email_s" value="<?php echo htmlspecialchars($client['email']); ?>">
						<span class="help-block">Email</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="address_s" value="<?php echo htmlspecialchars($client['address']); ?>">
						<span class="help-block">Address</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="city_s" value="<?php echo htmlspecialchars($client['city']); ?>">
						<span class="help-block">City</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border" id="state_us">
						<select class="profile-txt styleselect_cp" id="state_us_s">
							<option value="">-</option>
							<?php
									$states = get_states_list();
									foreach($states as $k=>$v){
										echo '<option value="'.$k.'"'.($k==$client['state']?' selected':'').'>'.$v.'</option>';
									}
									?>
						</select>
					</div>
					<div class="form-group form-control-select-border" id="state_ca" style="display:none;">
						<select class="profile-txt styleselect_cp" id="state_ca_s">
							<option value="">-</option>
							<?php
									$states = get_ca_states_list();
									foreach($states as $k=>$v){
										echo '<option value="'.$k.'"'.($k==$client['state']?' selected':'').'>'.$v.'</option>';
									}
									?>
						</select>
					</div>
					<div class="form-group" id="state_all" style="display:none;">
						<input type="text" placeholder="" class="form-control-box-border profile-txt" id="state_txt_s" value="<?php echo htmlspecialchars($client['state']); ?>">
					</div>
					<span class="help-block">State</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input class="form-control-box-border profile-txt" type="text" id="zip_s" value="<?php echo htmlspecialchars($client['zip']); ?>">
						<span class="help-block">Zip</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_srch_cp" id="country_s" onchange="setStateField()">
							<option value="">-</option>
							<?php
									$countries = getCountriesList();
									foreach($countries as $k=>$v){
										echo '<option value="'.$k.'"'.($k==$client['country'] ?' selected':'').'>'.$v.'</option>';
									}
									?>
						</select>
					</div>
					<span class="help-block">Country</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="date" class="form-control-box-border profile-txt" id="dob_s" value="<?php if($client['dob'] && $client['dob']!='0000-00-00') echo date('Y-m-d',strtotime($client['dob'])); ?>">
						<span class="help-block">Birthday</span>
					</div>
				</div>
			</div>
			<div class="row last_before_footer">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="gender_s">
							<option value="">-</option>
							<option value="female" <?php if($client['gender']=='female') echo 'selected' ?>>Female</option>
							<option value="male" <?php if($client['gender']=='male') echo 'selected' ?>>Male</option>
							<option value="other" <?php if($client['gender']=='other') echo 'selected' ?>>Identify as Other</option>
						</select>
					</div>
					<span class="help-block">Gender</span>
				</div>
			</div>
			<div class="row fixed_footer">
				<div class="col-md-12">
					<div class="form-group" style="padding-top:0px ;margin-bottom: 0px">
						<button style="margin-bottom: 0px; margin-top:10px;" onclick="savePSF(<?php echo $_GET['uid']; ?>)">save</button>
					</div>
				</div>
			</div>
		</div>

		<div class="settings_fields">
			<div class="row">
				<?php if( $is_manager || $is_owner || ( $frontdesk  && $salon_info['desc_vip_assign'] ) ) { ?>
				<div class="col-xs-4" style="text-align: left;">
					<?php /*
					<!-- 
					<label
						class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
						<input type="checkbox" id="is_vip_s" value="1" class="switch"
							<?php if($client['is_vip']) echo 'checked'; ?>>
						<strong style="position: relative; top: 2px; font-size:10px">VIP Client</strong>
					</label>
					--> */ ?>
					<div class="form-check is_vip">
						<label class="form-check-label">
							<span>VIP Client</span>
							<div class="checker">
								<input type="checkbox" id="is_vip_s" value="1" class="styled" <?php if($client['is_vip']) echo 'checked'; ?>>
							</div>
						</label>
					</div>
				</div>
				<?php } ?>
				<?php if( $role == 1 || $role == 5 ){ ?>
				<div class="col-xs-4" style="text-align: center; padding:0px;">
					<?php /*
					<!-- 
					<label
						class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
						<input type="checkbox" id="is_fnf_s" value="1" class="switch"
							<?php if($client['is_fnf']) echo 'checked'; ?>>
						<strong style="position: relative; top: 2px; font-size:10px">Friends & Family</strong>
					</label> 
					--> */ ?>
					<div class="form-check is_fnf">
						<label class="form-check-label">
							<span>Friends & Family</span>
							<div class="checker">
								<input type="checkbox" id="is_fnf_s" value="1" class="styled" <?php if($client['is_fnf']) echo 'checked'; ?>>
							</div>
						</label>
					</div>
				</div>
				<?php } ?>
				<?php if( $role == 1 || $role == 5 ){ ?>
				<div class="col-xs-4" style="text-align: right;">
					<div class="form-check disable_online_booking">
						<label class="form-check-label">
							<span title="Disable Online Booking">Disable OLB</span>
							<div class="checker">
								<input type="checkbox" class="styled <?php if($client['disable_online_booking']) echo 'ttip " title="Disabled'; ?>" style="margin-top: 2px;" id="disable_online_booking_s" <?php if($client['disable_online_booking']) echo 'checked'; ?>>
							</div>
						</label>
					</div>
				</div>
				<?php } ?>
			</div>
			<hr style="border:1px solid #ccc; margin-left:10px; margin-right:10px; margin-top: 0; margin-bottom: 20px;" />
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="loc_id_s">
							<option value="0">-</option>
							<?php
							if(defined('ALL_SALON_IDS') && count(ALL_SALON_IDS)>1){
								$salons_q = DB::query("select id,name,main_location from salons where id in (".implode(',',ALL_SALON_IDS).")");
								if(DB::num_rows($salons_q)>1){
									while($sr = DB::fetch_assoc($salons_q)){
										echo '<option value="'.$sr['id'].'"'.($sr['id']==$pref_salon?' selected':'').'>'.$sr['main_location'].'</option>';
									}
								}
							}
							?>
						</select>
					</div>
					<span class="help-block">Preferred Location</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="pref_stylist_id1_s">
							<option value="0">-</option>
							<?php
								$stylists = DB::query("select u.*,pi.code_name from users u inner join provider_info pi on pi.user_id=u.id where pi.salon_id='".SALON_ID."' and  u.role_id in (2,5,6)");
								while($r=DB::fetch_assoc($stylists)){
									if($r['code_name']!='') $r['first_name']=$r['code_name'];
									echo '<option value="'.$r['id'].'"'.($r['id']==$client['pref_stylist_id']?' selected':'').'>'.$r['first_name'].'</option>';
								}

								?>
						</select>
					</div>
					<span class="help-block"><strong>Preferred Provider</strong></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_srch_cp" id="pref_serv_id_s">
							<option value="0">-</option>
							<?php
								$sevices = array();
								$cats = DB::query("select * from service_categories where salon_id='".SALON_ID."' and parent_id>0 AND  disabled=0 order by position_num");

								while($result1=DB::fetch_assoc($cats)){
									$sevices[] = $result1;
								}
								$ic=0;
								foreach( $sevices as $rc ){
									$ic++;
									echo '<optgroup label="'.$rc['category_name'].'">';
									$services = DB::query("select id,service_name,service_code from salon_services ss where ss.salon_id='".SALON_ID."' and ss.service_category_id='".$rc['id']."' and ss.disabled=0 order by order_number,service_name");
									while($r=DB::fetch_assoc($services)){
									?>
									<option value="<?php echo $r['id']; ?>" <?php if($sal_cust['pref_service']==$r['id']) echo 'selected'; ?> ><?php echo ucwords(strtolower($r['service_name'])); if($r['service_code']!='') echo  ' / '.$r['service_code']; ?></option>
									<?php
									}
									echo '</optgroup>';
								}
								?>
						</select>
					</div>
					<span class="help-block">Preferred Service</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="number" placeholder="" class="form-control-box-border profile-txt" id="appoint_interval_s" value="<?php echo htmlspecialchars($sal_cust['appoint_interval']); ?>">
						<span class="help-block">Weeks Between Visits</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="hair_length_s">
							<option value="">-</option>
							<option value="short" <?php if($client['hair_length']=='short') echo 'selected'; ?>>Short</option>
							<option value="medium" <?php if($client['hair_length']=='medium') echo 'selected'; ?>>Medium</option>
							<option value="long" <?php if($client['hair_length']=='long') echo 'selected'; ?>>Long</option>
						</select>
					</div>
					<span class="help-block">Hair Length</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="color_treated_s">
							<option value="">-</option>
							<option value="yes" <?php if($client['color_treated']=='yes') echo 'selected'; ?>>Yes</option>
							<option value="no" <?php if($client['color_treated']=='no') echo 'selected'; ?>>No</option>
						</select>
					</div>
					<span class="help-block">Color Treated</span>
				</div>
			</div>
			<?php if(SALON_ID!=26){  ?>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="pref_brand_id_s">
							<option value="0">-</option>
							<?php
									$brands_q= DB::query("select b.* from brands b  where 1 /*sb.salon_id='".SALON_ID."'*/ order by b.name");
									while($r=DB::fetch_assoc($brands_q)){
										echo '<option value="'.$r['id'].'"'.($r['id']==$client['pref_brand_id']?' selected':'').'>'.$r['name'].'</option>';
									} ?>
						</select>
					</div>
					<span class="help-block">Preferred Brand</span>
				</div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt cl_bookstyleselect_cust" id="referrer_id_s">
							<option value="0">-</option>
							<?php
								if($client['referrer_id']>0){
									$refinfo = DB::get_row("select id,first_name,last_name from users where id='".(int)$client['referrer_id']."' limit 1");
									if($refinfo){
										echo '<option value="'.$refinfo['id'].'" selected>'.$refinfo['first_name'].' '.$refinfo['last_name'].'</option>';
									}
								}
									?>
						</select>
					</div>
					<span class="help-block">Referred by</span>
				</div>
			</div>
			<div class="row last_before_footer">
				<div class="col-md-12">
					<div class="form-group form-control-select-border">
						<select class="profile-txt styleselect_cp" id="beverage_s">
							<option value="">-</option>
							<option value="wine" <?php if($client['beverage']=='wine') echo 'selected'; ?>>Wine</option>
							<option value="tea" <?php if($client['beverage']=='tea') echo 'selected'; ?>>Tea</option>
							<option value="coffee" <?php if($client['beverage']=='coffee') echo 'selected'; ?>>Coffee</option>
							<option value="water" <?php if($client['beverage']=='water') echo 'selected'; ?>>Water</option>
						</select>
					</div>
					<span class="help-block">Beverage of Choice</span>
				</div>
			</div>
			<div class="row fixed_footer">
				<div class="col-md-12" >
					<div class="form-group" style="padding-top:0px ;margin-bottom: 0px">
						<button style="margin-bottom: 0px; margin-top:10px;" onclick="savePSF(<?php echo $_GET['uid']; ?>)">save</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="cc_fields" style="display:none;">
			<?php $cc = DB::query("select * from customer_cc where (user_id='".$id."'  or id in (select cc_id from customer_cc_linked where linked_user_id='".$id."'))  ".$cc_filter.""); ?>
			<div class="row">
				<div class="col-md-12">
					<h6>
						Credit Cards on File
						<i class="las la-plus-circle" data-toggle="modal" data-target="#modal_form_horizontal" style="font-size:30px; float:right; margin-top:-2px;"></i>
						<span class="open_ticks_ico" style="color:#000; font-size:16px; text-align:center; float:right;"><?php echo $cc->num_rows; ?></span>
					</h6>
					<?php // <!-- <button type="button" class="btn btn-default legitRipple" data-toggle="modal" data-target="#modal_form_horizontal" style="text-transform:capitalize;">Add New</button> --> ?>
				</div>
			</div>
			<?php
			while($r=DB::fetch_assoc($cc))
			{
			?>
			<div class="row">
				<div class="col-md-12">
					<div style="border:2px solid #E5E5E5; border-radius: 10px; background-color: white; padding:10px; margin: 6px 0;">
						<a href="javascript:;" onclick="deleteMethod(<?php echo $r['id']; ?> );" class="user_modal_remove_cc">Remove</a>
						<a href="javascript:;" onclick="setDefaultMethodMobile(<?php echo $r['id']; ?>,this)" class="btn btn-default <?php echo ($r['is_default'] ? 'btn-info' : '') ?> btn_cc_default">Default</a>
						<!--			
						<div class="checker">
							<input type="checkbox" class="styled is_default" name="is_default" <?php if($r['is_default']) echo ' checked ';  ?> onclick="setDefaultMethod(<?php echo $r['id']; ?>,this)" >
						</div>
						-->
						<div class="cc_fields_row cc_type_num">
							<?php echo strtoupper($r['cc_type']); ?>
							<?php echo str_replace('X','*',$r['cc_num']); ?>
						</div>
						<div class="cc_fields_row cc_date_exp">Exp <?php echo date('m/y',strtotime($r['cc_exp'])); ?></div>
						<div class="cc_fields_row cc_share_with">
						<?php 
						if($id == $r['user_id'])
						{
							$linked_q = DB::query("select cl.*, u.first_name,u.last_name,u.phone from customer_cc_linked cl inner join users u on u.id=cl.linked_user_id where cl.cc_id='".$r['id']."' order by cl.id");
							while($ur = DB::fetch_assoc($linked_q)){
								//pre_print($ur);
								echo 'Shared with <a href="'.ADM_URL.'/clients.php?client_id='.$ur['linked_user_id'].'">'.$ur['first_name'].' '.$ur['last_name'].'</a>'; 
								echo '<a href="javascript:;" onclick="removelink('.$ur['id'].')"><i class="las la-times-circle"  style="float:none;"></i></a><br/>';
							}
							
							echo '<a href="javascript:;"onclick="startLinkCard('.$r['id'].',\''.str_replace('X','*',$r['cc_num']).'\');">Create New Share</a>';
						}
						
						
						if($id!=$r['user_id'])
						{
							echo ' <a href="payment-method.php?id='.$r['user_id'].'" style="margin-right: 4px;"><i class="las la-link" style="color:#5f6368; font-size: 16px;  "></i>123</a>';
						}
						?>
						</div>
					</div>
				</div>
			</div>
			<?php 
			} 
			?>
		</div>
	<?php
	exit;
}

if($action == 'save_psf'){

	$id = (int)$_GET['id'];

	$state = $_POST['state_us'];
	if($_POST['country']!='US' && $_POST['country']!=''){
		if($_POST['country']=='CA') $state = $_POST['state_ca'];
		else $state = $_POST['state_txt'];
	}

	$u_data = array(
		'first_name' => $_POST['first_name'],
		'last_name' => $_POST['last_name'],
		'address' => $_POST['address'],
		'city' => $_POST['city'],
		'state' => $state,
		'zip' => $_POST['zip'],
		'country' => $_POST['country'],
		'gender' => $_POST['gender'],
		'appoint_interval' => (int) $_POST['appoint_interval'],
		'is_vip'=>isset($_POST['is_vip'])&&$_POST['is_vip']?1:0,
		'is_fnf'=>isset($_POST['is_fnf'])&&$_POST['is_fnf']?1:0,
		'disable_online_booking'=>isset($_POST['disable_online_booking'])&&$_POST['disable_online_booking']?1:0,
		'hair_length' => $_POST['hair_length'],
		'color_treated' => $_POST['color_treated'],
		'beverage' => $_POST['beverage'],
		'referrer_id'=>$_POST['referrer_id']
	);

	if( $_POST['referral_source']!='' && $_POST['referral_source']!= '0' ){
		$u_data['referral_source'] = $_POST['referral_source'];
	}

	if($_POST['pref_stylist_id1']) $u_data['pref_stylist_id']=$_POST['pref_stylist_id1'];
	if($_POST['loc_id']){
		DB::update('salon_customers',array('pref_salon'=>(int)$_POST['loc_id'])," customer_id='".(int)$id."' and salon_id in (".implode(',',ALL_SALON_IDS).") limit 1 ");
	}
	if($_POST['hair_type_id']) $u_data['hair_type_id']=$_POST['hair_type_id'];
	if($_POST['pref_brand_id']) $u_data['pref_brand_id']=$_POST['pref_brand_id'];
	if($_POST['dob']) $u_data['dob']=$_POST['dob'];

	if(!$hide_contact_inf){
		$u_data['email']=$_POST['email'];
		$u_data['phone']=$_POST['phone'];
		$u_data['phone_clean']=preg_replace("/[^0-9]/", "", $_POST['phone'] );
		$u_data['phone_home']=$_POST['phone_home'];
	}
	DB::update('users',$u_data," id='".$id."' limit 1 ");


	DB::update('salon_customers',array(
		'appoint_interval'=>(int)$_POST['appoint_interval'],
		'pref_service'=>(int)$_POST['pref_serv_id']
	), " customer_id='".(int)$id."' and salon_id='".SALON_ID."' limit 1 ");

	//print_r( $_POST );
	exit;
}

if($action == 'set_transition_min'){
	DB::update('provider_info',array( 'transition_min' => (int)$_GET['transition_min'] )," user_id='".(int)$_GET['id']."'  and salon_id='".SALON_ID."'  limit 1");
	exit;
}
if($action == 'set_booking_interval'){
	DB::update('provider_info',array( 'booking_interval' => (int)$_GET['booking_interval'] )," user_id='".(int)$_GET['id']."'  and salon_id='".SALON_ID."'  limit 1");
	exit;
}

if($action == 'set_double_book'){
	$double_book = 0;
	if($_GET['double_book']==1) $double_book = 1;
	DB::update('provider_info',array( 'double_book' => $double_book )," user_id='".(int)$_GET['id']."' limit 1");
	exit;
}

if($action == 'set_services_provider_settings'){

	$data = array('double_book'=>0,
				'display_in_app'=>0,
				'show_on_cal'=>0,
				'appointment_alerts'=>0,
				'transition_min'=>(int)$_POST['transition_min'],
				'booking_interval'=>(int)$_POST['booking_interval'],
				'pricing_level'=>(int)$_POST['pricing_level'],
				'department'=>$_POST['department']
				);

	if($_POST['double_book']==1) $data['double_book'] = 1;
	if($_POST['display_in_app']==1) $data['display_in_app'] = 1;
	if($_POST['show_on_cal']==1) $data['show_on_cal'] = 1;
	if($_POST['appointment_alerts']==1) $data['appointment_alerts'] = 1;

	DB::update('provider_info',$data," user_id='".(int)$_POST['save_service_settings']."' limit 1");
	exit;
}

/* START standing_app_modal */
if($action == 'get_standing_appointments'){

	$client = DB::get_row("select * from users where id ='".(int)$_POST['client_id']."' limit 1");
	$new_book_date = $_POST['new_book_date'];
	$new_book_time = $_POST['new_book_time'];
	$new_book_servtimes = json_decode( $_POST['new_book_servtimes'], true );
	$services = json_decode( $_POST['services'], true );
	$stylists = json_decode( $_POST['stylists'], true );
	$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");

	$scc = count( $services );

	$sp_info = array();
	for( $i=0; $i < $scc; $i++ ){
		$st = DB::get_row("select id, first_name from users where id ='".(int)$stylists[$i]."' limit 1");
		/*$srv = DB::get_row("select ps.timing_min, ss.service_name, ss.id from provider_services as ps left join salon_services ss on ps.service_id = ss.id where ps.provider_id ='".(int)$stylists[$i]."' and ps.service_id = '".(int)$services[$i]."' and ss.id = '".(int)$services[$i]."' limit 1");*/

		$srv = DB::get_row("select ss.service_name,if(cs.timing_min>0, cs.timing_min,if(ps.timing_min>0, ps.timing_min, if(ss.default_timing_min>0,ss.default_timing_min,15))) as service_duration, if(cs.service_time_1>0, cs.service_time_1,if(ps.service_time_1>0, ps.service_time_1, if(ss.service_time_1>0,ss.service_time_1,0))) as service_time_1,if(cs.service_time_1>0, cs.process_time,  if(ps.service_time_1>0, ps.process_time, if(ss.process_time>0,ss.process_time,0))) as process_time, if(cs.service_time_1>0, cs.service_time_2,if(ps.service_time_1>0, ps.service_time_2, if(ss.service_time_2>0,ss.service_time_2,0))) as service_time_2, ss.equipment_id from salon_services ss inner join provider_services ps on ps.service_id=ss.id left join customer_services cs on cs.customer_id='".$client['id']."' and cs.service_id=ss.id where ss.id='".(int)$services[$i]."' and ps.provider_id='".(int)$stylists[$i]."' limit 1");

		$sp_info[] = array( 'provider_id' => $st['id'], 'provider_name' => $st['first_name'], 'service_id' =>(int)$services[$i], 'service_name' => $srv['service_name'], 'timing_min' => $srv['service_duration'] );
	}

	include('includes/standing_appointments_modal.php');
	exit;
}

if($action == 'get_standing_appointments_d'){



	include('includes/standing_appointments_d_modal.php');
	exit;
}
/* END standing_app_modal */


if($action == 'save_gift_card_settings'){

	$salon_info = DB::get_row("select * from salons where id='".SALON_ID."' limit 1");
	$giftcard_settings = array(
		'min' => isset($_POST['min'])?$_POST['min']:0,
		'max' => isset($_POST['max'])?$_POST['max']:0,
		'term' => isset($_POST['term'])?$_POST['term']:0,
		'acc_num' => SALON_ID,
		'location' => $salon_info['main_location'],
		'alert' => $_POST['alert']
	);

	DB::update('salons',array( 'giftcard_settings'=>json_encode($giftcard_settings) )," id='".SALON_ID."' limit 1 ");
	exit;
}


if($action == 'save_sms_notifications_settings'){

		$settings = array();
		if($salon_info['settings']!='') $settings = json_decode($salon_info['settings'],true);

		$settings['sb_active']=addslashes($_POST['soc_buddy']);
		$settings['sb_facebook']=addslashes($_POST['soc_buddy_f']);
		$settings['sb_google']=addslashes($_POST['soc_buddy_g']);
		$settings['sb_text']=addslashes($_POST['soc_buddy_text']);

		$upd = array(
			'enable_sms_notif' => isset($_POST['enable_sms'])&&$_POST['enable_sms']?1:0,
			'sms_remind_h' => isset($_POST['sms_remind'])&&$_POST['sms_remind']?(int)$_POST['sms_remind']:48,
			'sms_remind_h2' => isset($_POST['sms_remind2'])&&$_POST['sms_remind2']?(int)$_POST['sms_remind2']:24,
			'covid_form_notif_h' => isset($_POST['covid_form_notif'])&&$_POST['covid_form_notif']?$_POST['covid_form_notif']:'12 hrs before Appointment',
			'settings' =>  json_encode($settings)
		);
		//print_r($upd);

		DB::update('salons',$upd," id='".SALON_ID."' limit 1 ");
	exit;
}


if($action == 'qsent_cvw'){
	if( isset($_POST['client']) &&  $_POST['client'] != '' ){
		$cf = DB::query("select cf.appt_id from covid_forms cf where cf.user_id='" . (int)$_POST['client'] . "'");
		$cfs=array();
		while($r=DB::fetch_assoc($cf)){
			$cfs[] = $r['appt_id'];
		}
		$srv = DB::get_row("select sa.id from salon_appointments sa where sa.is_paid = '0' and sa.customer_id='" . (int)$_POST['client'] . "' and sa.appt_date>='".date('Y-m-d')."' " . (!empty($cfs)?" and sa.id not in (".implode(',',$cfs).")":"") . " order by sa.appt_date asc");

		if( isset( $srv['id'] ) ){
			DB::insert('covid_forms', array( 'user_id' => (int)$_POST['client'],
											'appt_id' => $srv['id'],
											'sent_time' => time() ) );
			$waver_id = DB::insert_id();
			include('includes/functions_sms.php');
			sendCoverWaver($waver_id);

			echo 'Covid Waiver was sent.';
		}else{
			echo 'This client has no future appointments.';
		}
	}


	exit;
}


if($action == 'save_payroll_settings'){
	if( isset($_POST['upd_payroll_settings']) &&  $_POST['upd_payroll_settings'] == '1' ){
		$settings = array();
		if($salon_info['settings']!='') $settings = json_decode($salon_info['settings'],true);

		$settings['overtime_rate']=(float)$_POST['overtime_rate'];
		$settings['reg_hours_shift']=(float)$_POST['reg_hours_shift'];
		$settings['reg_hours_unit']= addslashes($_POST['reg_hours_unit']);
		$settings['overtime_calc']=isset($_POST['overtime_calc'])&&$_POST['overtime_calc']?1:0;

		$upd = array(
			'overtime_rate' =>  (float)$_POST['overtime_rate'],
			'week_start' => (int) $_POST['week_start'],
			'settings' =>  json_encode($settings)
		);

		DB::update('salons',$upd," id='".SALON_ID."' limit 1 ");
	}


	exit;
}


if($action == 'save_client_image'){

	if($_FILES['prof_image'] && $_FILES['prof_image']['tmp_name']){
		include('../includes/resize.class.php');
		$ext = explode('.',$_FILES['prof_image']['name']);
		$ext=strtolower($ext[count($ext)-1]);
		if(in_array($ext,array('jpg','jpeg','png','gif'))){
			$newname = $user_id.'-'.time().'.'.$ext;
			if(move_uploaded_file($_FILES['prof_image']['tmp_name'],'../images/avatars/'.$newname)){
				$resize = new resize('../images/avatars/'.$newname);
				$resize->resizeImage(500, 500, "crop");
				$resize->saveImage('../images/avatars/'.$newname);
				$resize = new resize('../images/avatars/'.$newname);
				$resize->resizeImage(100, 100, "crop");
				$resize->saveImage('../images/avatars/thumbs/'.$newname);

				if($stylist['image_filename']!=''){
					unlink('../images/avatars/'.$stylist['image_filename']);
					unlink('../images/avatars/thumbs/'.$stylist['image_filename']);
				}

				DB::update('users',array('image_filename'=>$newname)," id='".(int)$_GET['uid']."' limit 1 ");

			}
		}
	}

	exit;
}

if($action == 'save_stylist_image'){

	if($_FILES['prof_image'] && $_FILES['prof_image']['tmp_name']){
		include('../includes/resize.class.php');
		$ext = explode('.',$_FILES['prof_image']['name']);
		$ext=strtolower($ext[count($ext)-1]);
		if(in_array($ext,array('jpg','jpeg','png','gif'))){
			$newname = $user_id.'-'.time().'.'.$ext;
			if(move_uploaded_file($_FILES['prof_image']['tmp_name'],'../images/avatars/'.$newname)){
				$resize = new resize('../images/avatars/'.$newname);
				$resize->resizeImage(500, 500, "crop");
				$resize->saveImage('../images/avatars/'.$newname);
				$resize = new resize('../images/avatars/'.$newname);
				$resize->resizeImage(100, 100, "crop");
				$resize->saveImage('../images/avatars/thumbs/'.$newname);

				if($stylist['image_filename']!=''){
					unlink('../images/avatars/'.$stylist['image_filename']);
					unlink('../images/avatars/thumbs/'.$stylist['image_filename']);
				}

				DB::update('users',array('image_filename'=>$newname)," id='".(int)$_GET['uid']."' limit 1 ");

			}
		}
	}

	exit;
}

if($action == 'waitlist_add'){

	if($_POST['customer_id'] && $_POST['wl_sdate'] && $_POST['services']){

		DB::insert('waitlist',array(
			'salon_id' => SALON_ID,
			'customer_id' => (int) $_POST['customer_id'],
			'start_date' => date('Y-m-d',strtotime($_POST['wl_sdate'])),
			'end_date' =>  date('Y-m-d',strtotime($_POST['wl_edate'])),
			'start_time' => $_POST['wl_stime'],
			'end_time' => $_POST['wl_etime'],
			'start_time_sec' => getTimeSec($_POST['wl_stime']),
			'end_time_sec' => getTimeSec( $_POST['wl_etime']),
			'service_data' => json_encode($_POST['services']),
			'notes' => $_POST['notes'],
			'visit_type' => $_POST['visit_type'],
			'status' =>0,
			'date_added' => date('Y-m-d H:i:s')

		));

	}


	/*wl_stime: 10:15 AM
wl_etime: 11:00 AM
wl_sdate: 08/11/21
wl_edate: 08/11/21
customer_id: 221259
services[0][provider]: 203
services[0][service]: 1514
services[0][num]:
services[0][dur]:
services[0][dur_save]:
services[0][time]: 10:15 AM
date: 08/11/21
notes:
visit_type: */
	exit;
}
if($action == 'waitlist_modal'){

	include('includes/waitlist_add_modal.php');
	exit;
}


if($action == 'appt_booked_modal'){
	if($_POST['appt_id']){
		$appt = DB::get_row("select * from salon_appointments where id='".(int)$_POST['appt_id']."' and salon_id='".SALON_ID."' limit 1");
		if(!$_POST['uid'] && $appt) $_POST['uid']=$appt['customer_id'];
	}
	$uinfo = DB::get_row("select * from users where id='".(int)$_POST['uid']."' limit 1");
	include('includes/appt_booked_modal.php');
	exit;
}


if($action == 'save_additional_email'){

	$additional_email = trim($_POST['additional_email']);
	$client_id = (int)$_POST['client_id'];
	$errmsg = '';
	$result = array( 'err' => 0 );

	if( $additional_email == "" ){

		$result['err_msg'] .= 'Field is empty.';
		$result['err'] = 1;

	}else if(!filter_var($additional_email, FILTER_VALIDATE_EMAIL)){

		$result['err_msg'] .= 'Provide valid email address.';
		$result['err'] = 1;

	}else{

		$all_salons = array(SALON_ID);
		if(defined('ALL_SALON_IDS')) $all_salons=ALL_SALON_IDS;

		$checkmail = DB::get_row("select distinct u.* from users u inner join salon_customers sc on sc.customer_id=u.id and sc.salon_id in (".implode(',',$all_salons).") where u.email='".addslashes($additional_email)."' limit 1");

		if($checkmail){
			$result['err_msg'] .= 'This email address is already registered.';
			$result['err'] = 1;
		}

		if($result['err']==0){
			print_r(DB::update('users',array('email'=>addslashes($additional_email)),"  id='".$client_id."' limit 1 "));
		}

	}


	header('Content-type: application/json');
	echo json_encode($result);
	exit;
}



if($action == 'load_formulas'){

	$guest_old_formulas = DB::get_row("select id, guest_formulas from users where id='".(int)$_GET['user_id']."'");
	
	$guest_formulas = DB::query("select cf.*, u.first_name, u.last_name from customer_formulas as cf inner join users u on u.id=cf.provider_id where cf.customer_id='".(int)$_GET['user_id']."'");
	
	while($r = DB::fetch_assoc($guest_formulas)){
?>

<h3><div><?php echo date( 'm/d/y, h:i a', $r['date_time'] ); ?></div></h3>
<div class="guest_formulas_single">
	<div class="gfs_row">
		<label>Formula</label>
		<input type="text" class="guest_formulas_title" value="<?php echo $r['formula']; ?>">
	</div>
	<div class="gfs_row">
		<label>Formula Notes</label>
		<textarea class="guest_formulas_note" ><?php echo htmlspecialchars($r['notes']); ?></textarea>
	</div>
	<div class="guest_formulas_bottom">
		<div class="guest_formulas_user">Entered by <?php echo $r['first_name']; ?> <?php echo $r['last_name']; ?></div>
		<div class="guest_formulas_bttns">
			<button type="button" class="btn btn-primary" style="background:#fff !important; color:#000 !important; border:1px solid #ddd !important;" onclick="removeFormula(<?php echo $r['id']; ?>)" >Remove</button>
			<button type="button" class="btn btn-primary" style="background:#000; color:#fff; margin-left:10px;"  onclick="saveFormula( <?php echo $r['id']; ?>, this )" >Save</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php } ?>

<?php if($guest_old_formulas['guest_formulas']!=''){ ?>
<h3><div>Old Formulas</div></h3>
<div class="guest_formulas_single">
	<div class="gfs_row">
		<label>Formula Notes</label>
		<textarea class="guest_formulas guest_formulas_note" style="height: 153px;" ><?php echo htmlspecialchars($guest_old_formulas['guest_formulas']); ?></textarea>
	</div>
	<div class="guest_formulas_bottom">
		<div class="guest_formulas_bttns">
			<button type="button" class="btn btn-primary" style="background:#000; color:#fff; margin-left:10px;"  onclick="saveUserModal(<?php echo $guest_old_formulas['id']; ?>)" >Save</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	}
	exit;
}

if( $action == 'delete_formula' ){
	
	if( (int)$_GET['id'] > 0 ){
		DB::delete('customer_formulas'," id='".(int)$_GET['id']."' limit 1");
	}
	
	exit;
}
if( $action == 'save_formula' ){
	if( (int)$_GET['id'] > 0 ){
		DB::update('customer_formulas',array('notes'=>addslashes($_POST['notes']), 'formula'=>addslashes($_POST['formula'])),"  id='".(int)$_GET['id']."' limit 1 ");
	}else{
		DB::insert('customer_formulas',array(
			'notes' => addslashes($_POST['notes']),
			'formula'=> addslashes($_POST['formula']),
			'provider_id' => $user_id,
			'customer_id' => (int)$_POST['customer_id'],
			'date_time' => time()
		));
	}
	
	exit;
} 