-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 16 апр 2021 в 10:57
-- Версия на сървъра: 10.3.25-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enevsoft_academy`
--

-- --------------------------------------------------------

--
-- Структура на таблица `phones`
--

CREATE TABLE `phones` (
  `id` int(11) NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reg_status` enum('registered','verified') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'registered',
  `login_status` set('allowed','banned') COLLATE utf8_unicode_ci NOT NULL,
  `banned_to` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `phones_log`
--

CREATE TABLE `phones_log` (
  `id` int(11) NOT NULL,
  `phone_id` int(11) NOT NULL,
  `try_login_with` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `log_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `phones_otp`
--

CREATE TABLE `phones_otp` (
  `id` int(11) NOT NULL,
  `phone_id` int(11) NOT NULL,
  `otp_value` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `active_from` timestamp NOT NULL DEFAULT current_timestamp(),
  `active_to` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `phones_sms`
--

CREATE TABLE `phones_sms` (
  `id` int(11) NOT NULL,
  `phone_id` int(11) NOT NULL,
  `sms_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sent_timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones_log`
--
ALTER TABLE `phones_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones_otp`
--
ALTER TABLE `phones_otp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone_id` (`phone_id`);

--
-- Indexes for table `phones_sms`
--
ALTER TABLE `phones_sms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phones_log`
--
ALTER TABLE `phones_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phones_otp`
--
ALTER TABLE `phones_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phones_sms`
--
ALTER TABLE `phones_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
